import React, { Component } from "react";
import I18n from "i18n-js";
import {
    Text,
    View,
    StyleSheet,
} from "react-native";

import Filter_select from "./filters/filter_select";
import Filter_checkbox from "./filters/filter_checkbox";
import Filter__checkBoxCat from "./filters/filter_checkboxCat";

export default class FilterBox extends Component {
    constructor(props) {
        super(props);

        this.state = {
            filters: this.props.filters
        }
    }
    componentDidMount() {
        this.props.prepare(this.state.filters)
        // Keyboard
    }
    getFilter = (item, key) => {
        let inner = null
        var set = item => {
            this.setState({
                ...this.state.filters,
                [key]: item
            }, () => {
                this.props.prepare(this.state.filters)
            })
        }
        switch (item.type) {
            case "checkbox_category":
                inner = <Filter__checkBoxCat
                    item={item}
                    key={'filter_' + key}
                    set={set}
                />;
                break;
            case "checkbox":
                inner = <Filter_checkbox
                    item={item}
                    key={'filter_' + key}
                    set={set}
                />
                break;
            case "select":
                inner = <Filter_select
                    item={item}
                    key={'filter_' + key}
                    set={set}
                />

                break;
        }
        return inner
    }
    getItemOptions = (item) => {
        let options = {}

        item.options.map((v, k) => {
            options[v.name] = v.value;
        })
        
        return options
    }
    render() {
        return (
            <View style={styles.filterBox}>
                {this.state.filters.map((item, key) => {
                    let options = this.getItemOptions(item)
                    return <View style={styles.filter}>
                        {options.is_title_visible ?
                            <View style={styles.filter_titleBox}>
                                <Text style={styles.filter_title}>{item.title}</Text>
                            </View>
                            : null}
                        {this.getFilter(item, key)}
                    </View>
                })}
            </View>
        );
    }
}
const styles = StyleSheet.create({
    filterBox: {},
    filter: {
        marginBottom: 32
    },
    filter_titleBox: {
        marginBottom: 16,
    },
    filter_title: {
        fontSize: 18,
        fontWeight: 'bold'
    }
});
