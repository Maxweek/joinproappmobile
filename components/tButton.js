import React, { Component } from "react";
import { View, Text, StyleSheet, Animated, Button, ActivityIndicator } from "react-native";
import { TextInput } from "react-native";
import { TouchableHighlight, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import { COLORS, BUTTON_STYLES, STYLES } from "../assets/styles/styles";
// import { TextInputMask } from 'react-native-masked-text'

export default class TButton extends Component {
    constructor(props) {
        super(props);
        // this.state = {
        //     text: this.props.value,
        //     onPress: this.props.onPress,
        //     icon: this.props.icon,
        //     status: this.props.status
        // };
        // console.log(this.props.isLoading)
    }

    onPress = () => {
        // console.log('123')
        if (!this.props.isLoading && !this.props.isLocked) {
            this.props.onPress()
        }
    }

    render() {
        let color = '',
            textColor = '',
            iconColor = '',
            padding = this.props.mini ? 8 : 16,
            shadow = null;
        let textBox = {
            width: this.props.notStretch ? 'auto' : '100%',
            position: 'relative',
            minWidth: this.props.mini ? 10 : 20,
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: this.props.text ? 'flex-start' : 'center',
        }
        let activityIndicatorBox = {
            display: this.props.isLoading ? 'flex' : 'none',
            // position: 'absolute',
            width: '100%',
            alignItems: 'center',
            justifyContent: 'center',
        }
        let loadingHide = {
            display: !this.props.isLoading ? 'flex' : 'none',
        }
        let touchBox = {
            position: 'relative',
            alignItems: 'center',
            borderRadius: this.props.isCircled ? 50 : 12,
        }
        switch (this.props.type) {
            default:
            case 'primary':
                color = COLORS.primary;
                textColor = COLORS.white;
                iconColor = COLORS.white;
                shadow = {
                    elevation: STYLES.base.elevation.default,
                }
                break;
            case 'secondary':
                color = COLORS.lightgray;
                iconColor = COLORS.primary;
                textColor = COLORS.primary;
                shadow = {
                    elevation: STYLES.base.elevation.default,
                }
                break;
            case 'simple':
                color = COLORS.transparent;
                iconColor = COLORS.primary;
                textColor = COLORS.primary;
                break;
            case 'danger':
                color = COLORS.danger;
                iconColor = COLORS.white;
                textColor = COLORS.white;
                shadow = {
                    elevation: STYLES.base.elevation.default,
                }
                break;
            case 'warning':
                color = COLORS.alert;
                iconColor = COLORS.white;
                textColor = COLORS.white;
                shadow = {
                    elevation: STYLES.base.elevation.default,
                }
                break;
            case 'locked':
                color = COLORS.gray;
                textColor = COLORS.white;
                iconColor = COLORS.white;
                break;
        }
        if (this.props.isLocked) {
            color = COLORS.gray;
            textColor = COLORS.white;
            iconColor = COLORS.white;
            shadow = {
                elevation: 0,
            }
        }
        return (
            <View style={this.props.style}>
                <TouchableOpacity
                    style={[touchBox, { backgroundColor: color, padding }, shadow]}
                    // hitSlop={{ top: 30, bottom: 30, left: 30, right: 30 }}
                    onPress={this.onPress}>
                    <View style={textBox}>
                        <ActivityIndicator
                            size={24}
                            color={textColor}
                            style={activityIndicatorBox}
                        />
                        <View style={[STYLES.BUTTON.textIcon, loadingHide]}>
                            <Icon name={this.props.icon} size={24} color={iconColor} />
                        </View>
                        <Text style={[STYLES.BUTTON.text, loadingHide, { color: textColor, width: textBox.width }]}>{this.props.text}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}
