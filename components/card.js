import React, { Component } from "react";
import I18n from "i18n-js";
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    Dimensions,
} from "react-native";
import { getDateFormat, translate } from "../App";
import { COLORS } from "../assets/styles/styles";

export default class Card extends Component {
    constructor(props) {
        super(props);

        this.state = {

        }
    }
    onPress = (item) => {
        this.props.onPress(item)
    }
    getBigCard = (item, style) => {
        let dateObj = getDateFormat(item.date)
        let card = <TouchableOpacity style={[styles.cardBox, style]} onPress={this.onPress}>
            <View style={styles.cardBox__imageBox}>
                {item.streaming === false ? null : null}
                {item.streaming === true ? <View style={styles.cardBox__label_live}>
                    <Text style={styles.cardBox__label_live_text}>{translate('impression.label.online')}</Text>
                </View> : null}
                {item.streaming === 'soon' ? <View style={[styles.cardBox__label_live, styles.cardBox__label_soon]}>
                    <Text style={styles.cardBox__label_live_text}>{translate('impression.label.soon')}</Text>
                </View> : null}
                {item.streaming === 'recorded' ? <View style={[styles.cardBox__label_live, styles.cardBox__label_live_recorded]}>
                    <Text style={styles.cardBox__label_live_text}>{translate('impression.label.inRecord')}</Text>
                </View> : null}
                {item.streaming === 'recorded' || item.streaming === false ? null :

                    <View style={styles.cardBox__dateBox}>
                        <Text style={styles.cardBox__dateBox_day}>{dateObj.formatted.day}</Text>
                        <View style={styles.cardBox__dateBox_info}>
                            <Text style={styles.cardBox__dateBox_month}>{dateObj.monthNameShort}</Text>
                            <Text style={styles.cardBox__dateBox_time}>{dateObj.formatted.hours}:{dateObj.formatted.minutes}</Text>
                        </View>
                    </View>}
                <Image
                    style={styles.cardBox__image}
                    source={{ uri: item.preview }}
                />
            </View>
            <View style={styles.cardBox__titleBox}>
                <Text style={styles.cardBox__title}>{item.title}</Text>
            </View>
            <View style={styles.cardBox__boxes}>
                <View style={[styles.cardBox__priceBox, styles.cardBox__box]}>
                    <Text style={styles.cardBox__price}>{item.price === 0 ? translate("default.free") : item.price + ' ₽'}</Text>
                </View>
            </View>
        </TouchableOpacity>;


        return card;
    }
    render() {
        return (
            <>
                {this.getBigCard(this.props.item, this.props.style)}
            </>
        );
    }
}

const styles = StyleSheet.create({
    cardBox: {
        // margin: 8,
        padding: 8,
        borderRadius: 16,
        shadowColor: COLORS.gray,
        backgroundColor: COLORS.white,
        elevation: 6,
        shadowOffset: {
            width: 0,
            height: 4
        },
        shadowRadius: 10,
        shadowOpacity: .2,
        // width: Dimensions.get('screen').width * 0.7
    },
    cardBox__imageBox: {
        width: '100%',
        borderRadius: 12,
        overflow: 'hidden',
        height: 160,
        position: 'relative'
    },
    cardBox__image: {
        width: '100%',
        height: '100%',
        position: 'absolute'
    },
    cardBox__label_live: {
        position: 'absolute',
        top: 0,
        zIndex: 20,
        backgroundColor: '#e45050',
        paddingVertical: 4,
        paddingHorizontal: 12,
        borderBottomRightRadius: 10,
        elevation: 10
    },
    cardBox__label_live_recorded: {
        backgroundColor: COLORS.gray,
    },
    cardBox__label_soon: {
        backgroundColor: COLORS.green,
    },
    cardBox__label_live_text: {
        fontSize: 12,
        color: COLORS.white,
        fontWeight: 'bold',
        textTransform: 'uppercase'
    },
    cardBox__dateBox: {
        position: 'absolute',
        zIndex: 10,
        bottom: 8,
        left: 8,
        borderRadius: 8,
        paddingHorizontal: 8,
        paddingVertical: 4,
        backgroundColor: COLORS.white,
        flexDirection: 'row',
        alignItems: 'center'
    },
    cardBox__dateBox_day: {
        fontSize: 24,
        lineHeight: 28,
    },
    cardBox__dateBox_info: {
        marginLeft: 4
    },
    cardBox__dateBox_month: {
        fontSize: 10,
        lineHeight: 12,
        color: COLORS.gray
    },
    cardBox__dateBox_time: {
        // marginTop: 2,
        fontSize: 12,
        lineHeight: 12,
    },
    cardBox__titleBox: {
        marginTop: 8,
        flexGrow: 1
    },
    cardBox__title: {
        fontSize: 16,
        fontWeight: 'bold',
    },
    cardBox__boxes: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        margin: -4,
        marginTop: 4,
    },
    cardBox__box: {
        margin: 4,
        borderRadius: 12,
        paddingHorizontal: 8,
        paddingVertical: 4,
    },
    cardBox__priceBox: {
        backgroundColor: COLORS.green,
    },
    cardBox__price: {
        color: COLORS.white
    },
});