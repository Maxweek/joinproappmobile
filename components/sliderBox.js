import React, { Component } from "react";
import I18n from "i18n-js";
import {
    Text,
    View,
    StyleSheet,
    Dimensions,
    FlatList,
} from "react-native";
import Card from "./card";

export default class SliderBox extends Component {
    constructor(props) {
        super(props);

        this.state = {

        }
        this.CARD_WIDTH = Dimensions.get('window').width * 0.7
    }
    render() {
        return (
            <FlatList
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                data={this.props.items}
                snapToOffsets={[...Array(this.props.items.length)].map((el, idx) => { return idx * (this.CARD_WIDTH + 8) })}
                // snapToInterval={100}
                decelerationRate="fast"
                renderItem={({ item }) => <Card style={{ margin: 4, width: this.CARD_WIDTH }} item={item} onPress={e => { this.props.onItemPress(item) }} />}
                keyExtractor={item => item.id}
                contentContainerStyle={styles.flatList__inner}
                style={[styles.flatList, this.props.style]}
            />
        );
    }
}

const styles = StyleSheet.create({
    flatList: {
        width: Dimensions.get('screen').width,
        margin: -16,
        padding: 16,
    },
    flatList__inner: {
        margin: -4,
        paddingRight: 24,
        paddingBottom: 12
    }
});