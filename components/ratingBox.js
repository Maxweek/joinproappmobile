import React, { Component } from "react";
import { View, Text, StyleSheet, Image } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import { COLORS } from "../assets/styles/styles";

export default class RatingBox extends Component {
    constructor(props) {
        super(props);
    }

    getRate = (rate) => {
        let stars = 5;
        let starsObj = [];
        let outline = false
        let value = rate.value - 1

        for (let i = 0; i < stars; i++) {
            let type = 'ios-star-outline';

            if (i < value) {
                type = 'ios-star';
            } else if (i == value) {
                type = 'ios-star';
                outline = true;
            } else if (i > value && i < value + 1) {
                type = 'ios-star-half'
                outline = true;
            }

            starsObj.push(
                <View key={"rate__" + i} style={styles.stars__item}>
                    <Icon name={type} size={14} color={COLORS.yellow} />
                </View>
            )
        }

        return starsObj.map(el => { return el })
    }

    render() {
        return (
            <View style={[this.props.style, styles.stars]}>
                {this.props.rate ?
                    <>
                        { this.getRate(this.props.rate)}
                        < View style={styles.stars__count}>
                            <Text style={styles.stars__count_text}>{this.props.rate.count}</Text>
                        </View>
                    </> : null
                }
            </View>
        );
    }
}
const styles = StyleSheet.create({
    stars: {
        flexDirection: 'row',
        marginHorizontal: -2,
        alignItems: 'center'
    },
    stars__item: {
        width: 16,
        height: 16,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 2
    },
    stars__count: {
        marginLeft: 4,
    },
    stars__count_text: {
        color: COLORS.gray,
        fontSize: 12
    },
})