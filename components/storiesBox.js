import React, { Component } from "react";
import I18n from "i18n-js";
import {
    Text,
    View,
    Animated,
    Image,
    StyleSheet,
    Dimensions,
    TouchableWithoutFeedback,
} from "react-native";
import { color, Easing } from "react-native-reanimated";
import LinearGradient from "react-native-linear-gradient";

class Timer {
    constructor(delay) {
        this.defaultDelay = delay
        this.isStopped = false;
        this.timer = null;
        this.setCallback()
    }

    pause() {
        clearTimeout(this.timer);
        this.remaining = (parseInt(Date.now() - this.startTime));
        this.delay = this.delay - this.remaining
        this.isStopped = true;
    };
    getRemaining() {
        this.remaining = (parseInt(Date.now() - this.startTime));
        return this.remaining
    }
    setCallback(callback = () => { }) {
        this.callback = callback
    }
    start() {
        this.delay = this.defaultDelay
        this.startTime = Date.now();
        this.timer = setTimeout(() => { this.callback() }, this.delay);
        this.isStopped = false;
    }
    resume() {
        this.startTime = Date.now();
        clearTimeout(this.timer);
        this.timer = setTimeout(() => { this.callback() }, this.delay);
        this.isStopped = false;
    };
    clear() {
        this.startTime = Date.now();
        clearTimeout(this.timer);
        this.isStopped = true;
    }
};

export default class StoriesBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            stories: [],
            isInited: false,
        }
        this.delay = this.props.delay ? this.props.delay : 5000
        this.timer = new Timer(this.delay);
        this.playerRef = {}

    }
    componentDidMount() {
        this.setStories()
        if (this.props.onRef != null) {
            this.props.onRef(this)
        }
    }
    componentDidUpdate() {

    }
    componentWillUnmount() {
        let stories = [...this.state.stories];
        stories.map(el => {
            el.isActive = false;
            Animated.timing(el.value).stop();
        })
        this.timer.clear()
    }
    slidePrev = () => {
        if (!this.state.isInited) {
            return
        }
        let stories = [...this.state.stories];
        let prevLine = stories.filter(el => el.isActive === true)[0];
        let nextLineId = prevLine.id - 1;
        
        if (nextLineId < 0) {
            nextLineId = 0
        }

        if (this.timer.getRemaining() > 1000) {
            nextLineId = prevLine.id
        }
        let nextLine = stories.filter(el => el.id === nextLineId)[0];

        this.setState({ _info: prevLine.id })
        this.setState({ _info_2: stories.length })
        this.timer.clear()

        stories.map(el => {
            el.isActive = false;
            Animated.timing(el.value).stop();
            if (el.id < nextLine.id) {
                el.value.setValue(el.width)
            }
            if (el.id > nextLine.id) {
                el.value.setValue(0)
            }
            if (nextLine.id === el.id) {
                el.isActive = true;
                el.value.setValue(0)

                Animated.timing(el.value, {
                    toValue: el.width,
                    duration: this.delay,
                    easing: Easing.linear,
                    useNativeDriver: true
                }).start();

                this.timer.setCallback(this.slideNext)
                this.timer.start()
            }
        })

        this.setState({ stories: stories })
    }
    slideNext = () => {
        if (!this.state.isInited) {
            return
        }
        let stories = [...this.state.stories];

        let prevLine = stories.filter(el => el.isActive === true)[0];
        if (prevLine === undefined) {
            return;
        }

        let nextLineId = prevLine.id + 1;

        if (nextLineId === stories.length) {
            nextLineId = 0
        }
        this.setState({ _info: nextLineId })
        this.setState({ _info_2: stories.length })

        this.timer.clear()

        stories.map(el => {
            el.isActive = false;
            if (el.id < nextLineId) {
                el.value.setValue(el.width)
            }
            if (el.id > nextLineId) {
                el.value.setValue(0)
            }
            if (nextLineId === el.id) {
                el.isActive = true;

                el.value.setValue(0)
                Animated.timing(el.value, {
                    toValue: el.width,
                    duration: this.delay,
                    easing: Easing.linear,
                    useNativeDriver: true
                }).start();
                this.timer.setCallback(this.slideNext)
                this.timer.start()
            }
        })
        this.setState({ stories: stories })
    }
    slideStop = () => {
        if (!this.state.isInited) {
            return
        }
        this.timer.pause();

        let stories = [...this.state.stories];

        stories.map(el => {
            if (el.isActive) {
                Animated.timing(el.value).stop();
            }
        })
    }
    slideResume = () => {
        if (!this.state.isInited) {
            return
        }
        let stories = [...this.state.stories];
        if (this.timer.isStopped) {
            this.timer.resume()
            
            stories.map(el => {
                if (el.isActive) {
                    Animated.timing(el.value, {
                        toValue: el.width,
                        duration: this.timer.delay,
                        easing: Easing.linear,
                        useNativeDriver: true
                    }).start();
                }
            })
        }
    }
    resetStories = () => {
        this.timer.clear();
        this.setState({ stories: [], isInited: false }, () => {
            this.setStories()
        })

    }
    setStories = () => {
        var prepare = () => {
            let stories = [...this.state.stories];
            let prevLine = stories.filter(el => el.isActive === true)[0];
            
            stories.map(el => {
                if (prevLine.id === el.id) {
                    el.value.setValue(0)
                    Animated.timing(el.value, {
                        toValue: el.width,
                        duration: this.delay,
                        easing: Easing.linear,
                        useNativeDriver: true
                    }).start()
                    this.timer.setCallback(this.slideNext)
                    this.timer.start()
                }
            })
        }
        let i = 0;
        let stories = [];
        if (this.props.stories < 2) {
            return;
        }
        this.props.stories.map(el => {
            let isActive = i === 0 ? true : false;
            stories.push({ id: i, isActive, value: new Animated.Value(0), url: el })
            i++;
        })
        this.setState({ stories: stories, isInited: true }, () => { setTimeout(() => { prepare() }, 1000) })
    }
    render() {
        return (
            <View style={styles.storiesBox}>
                {/* {this.props.children} */}
                {this.state.stories[0] !== undefined ?
                    this.state.stories.length ?
                        this.state.stories.map(el => {
                            return (
                                <Image key={el.id} style={[styles.image, {opacity: el.isActive ? 1 : 0 }]}
                                    source={{ uri: el.url }} />
                            )
                        })
                        : <Image
                            style={styles.image}
                            source={{ uri: this.props.app.impression.current.gallery[0] }}
                        />
                    : <Image
                        style={styles.image}
                        source={require('../assets/images/ref__impression_image_default.jpg')}
                    />}
                <TouchableWithoutFeedback
                    onPressOut={this.slideResume}
                    onLongPress={() => { this.slideStop() }}
                    onPress={() => { this.slidePrev() }}>
                    <View style={styles.touchAreaLeft}></View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback
                    onPressOut={this.slideResume}
                    onLongPress={() => { this.slideStop() }}
                    onPress={() => { this.slideNext() }}>
                    <View style={styles.touchAreaRight}></View>
                </TouchableWithoutFeedback>
                <View style={styles.preview}>
                    <LinearGradient colors={['#00000000', '#00000088', '#000000aa', '#000000af',]} style={styles.preview_shadow}>
                    </LinearGradient>
                    <Text style={styles.title}>{this.props.title}</Text>
                    <Text style={styles.description}>{this.props.description}</Text>
                    <View style={styles.bar}>
                        {this.state.stories.map(el => {
                            return (<View key={el.id} style={styles.bar_item} onLayout={(event) => {
                                el.width = event.nativeEvent.layout.width
                            }}>
                                <Animated.View style={[styles.bar_item_progress, { transform: [{ translateX: el.value }] }]}></Animated.View>
                            </View>
                            )
                        })}
                    </View>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    storiesBox:
    {
        display: 'flex',
        width: Dimensions.get('window').width,
        backgroundColor: '#dedede',
        height: Dimensions.get('window').height / 1.5,
        position: 'relative',
        justifyContent: 'flex-end',
    },
    image: {
        width: '100%',
        height: '100%',
        position: 'absolute'
    },
    title: {
        color: '#ffffff',
        fontWeight: 'bold',
        fontSize: 24,
        lineHeight: 32
    },
    touchAreaLeft: {
        position: 'absolute',
        zIndex: 200,
        left: 0,
        top: 0,
        width: '30%',
        height: '100%'
    },
    touchAreaRight: {
        position: 'absolute',
        zIndex: 200,
        right: 0,
        top: 0, width: '70%',
        height: '100%'
    },
    description: {
        color: '#ffffff',
        fontSize: 16,
        lineHeight: 24
    },
    bar: {
        marginHorizontal: -4,
        marginVertical: 8,
        flexDirection: 'row'
    },
    bar_item: {
        flexGrow: 1,
        marginHorizontal: 4,
        height: 3,
        borderRadius: 3,
        backgroundColor: "#ffffff88",
        overflow: "hidden",
        position: 'relative',
    },
    bar_item_progress: {
        position: 'absolute',
        right: '100%',
        width: '100%',
        height: '100%',
        backgroundColor: '#ffffff'
    },
    preview: {
        position: 'relative',
        padding: 16,
        paddingTop: 48
    },
    preview_shadow: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0
    },
});
