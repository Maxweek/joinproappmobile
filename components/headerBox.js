import React, { Component } from "react";
import { View, Text, StyleSheet, Animated, Dimensions, TouchableOpacity, StatusBar } from "react-native";
import { Platform, NativeModules } from 'react-native';
import { COLORS } from "../assets/styles/styles";
import Icon from "react-native-vector-icons/Ionicons";
import LinearGradient from "react-native-linear-gradient";
const { StatusBarManager } = NativeModules;

const settings = {
    height: 60,
    iconBoxPadding: 16,
    iconSize: 24
}

{/* <HeaderBox
    back={true}
    shadow={true}
    scheme={'light'}
    navigation={this.props.navigation }
    title={"yowyowyowyowyow"}
    right={
        [
            { icon: 'ios-heart-outline', action: () => { } },
            { icon: 'ios-share-social-outline', action: () => { } },
        ]
    }
/> */}

export default class HeaderBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
        this.colPreKey = "_header__"
        this.colKey = 0
    }

    componentDidMount() {
    }

    getGradient = () => {
        let grad = null;
        if (this.props.gradient) {
            if (this.props.scheme === "light") {
                grad = <LinearGradient colors={['#000000af', '#00000000']} style={styles.headerBox__shadow}>
                </LinearGradient>
            } else {
                grad = <LinearGradient colors={['#ffffffaf', '#ffffff00']} style={styles.headerBox__shadow}>
                </LinearGradient>
            }
        }
        return grad;
    }
    getIcon = item => {
        let color = COLORS.black;
        let key = this.colPreKey + this.colKey
        if (this.props.scheme === "light") {
            color = COLORS.white
        }
        if (item.color) {
            color = item.color
        }
        let icon = <TouchableOpacity key={key} onPress={item.action} style={styles.headerBox__icon}>
            {item.icon ?
                <Icon name={item.icon} size={settings.iconSize} color={color} />
                : null}
            {item.text ?
                <Text style={[styles.headerBox__icon_text, { color: color }]}>{item.text}</Text>
                : null}
        </TouchableOpacity>

        this.colKey++

        return icon;
    }
    getCol = (arr) => {
        if (!arr) {
            return null
        }
        let col = [];
        arr.map(el => {
            col.push(this.getIcon(el))
        })
        return <>{col}</>
    }
    getStatusBar = () => {
        let scheme = this.props.scheme === 'light' ? "light-content" : 'dark-content';
        if (Platform.OS === 'android') {
            scheme = 'light-content'
        }
        return <StatusBar
            barStyle={scheme}
        />
    }
    render() {
        let titleColor = {
            color: this.props.scheme === "light" ? COLORS.white : COLORS.black
        }
        let headerBox = {
            height: StatusBarManager.HEIGHT + settings.height
        };
        // if (!this.props.shadow) {
        // }
        if (this.props.shadow) {
            headerBox.elevation = 4;
            headerBox.backgroundColor = COLORS.white;

            headerBox.shadowColor = 'black';
            headerBox.shadowOffset = {
                width: 0,
                height: 4
            }
            headerBox.shadowOpacity = 0.1;
            headerBox.shadowRadius = 4;

        }
        return (
            <View style={[[styles.headerBox, this.props.style, headerBox]]}>
                {this.getStatusBar()}
                {this.getGradient()}
                <View style={styles.headerBox_col_left}>
                    <View style={styles.headerBox__inner}>
                        <View style={styles.headerBox_col_left}>
                            {this.props.back ?
                                this.getIcon({
                                    icon: 'ios-arrow-back-outline',
                                    action: () => {
                                        this.props.navigation.goBack();
                                    }
                                })
                                : null}
                            {this.props.left ? this.getCol(this.props.left) : null}
                        </View>
                        {this.props.title ?
                            <View style={styles.headerBox_col_center}>
                                <Text style={[styles.headerBox__title, titleColor]}>{this.props.title}</Text>
                            </View>
                            : null}
                        <View style={styles.headerBox_col_right}>
                            {this.props.right ?
                                this.getCol(this.props.right)
                                : null}
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    headerBox: {
        position: 'absolute',
        zIndex: 20,
        top: 0,
        left: 0,
        width: Dimensions.get('window').width,
    },
    headerBox__shadow: {
        position: 'absolute',
        width: '100%',
        height: '100%',
    },
    headerBox__inner: {
        width: '100%',
        position: 'relative',
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: StatusBarManager.HEIGHT,
        justifyContent: 'space-between',
        // backgroundColor: 'red'
    },
    headerBox_col_left: {
        flex: 1,
        // flexBasis: '33%',
        minHeight: settings.height,
        flexDirection: 'row',
    },
    headerBox_col_center: {
        flex: 1,
        minWidth: '30%',
        // flexBasis: '33%',
        minHeight: settings.height,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerBox_col_right: {
        flex: 1,
        // flexBasis: '33%',
        minHeight: settings.height,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        // backgroundColor: 'red',
    },
    headerBox__title: {
        fontSize: 18,
        textAlign: 'center',
        lineHeight: 20,
        fontWeight: 'bold'
    },
    headerBox__icon: {
        padding: 16,
        alignItems: 'center',
        justifyContent: 'center'
    },
    headerBox__icon_text: {
        fontSize: 16,
        fontWeight: 'bold',
        // flex:1
    }
});