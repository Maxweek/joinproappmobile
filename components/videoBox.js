import React, { Component } from "react";
import I18n from "i18n-js";
import {
    ScrollView,
    Text,
    TextInput,
    View,
    Animated,
    Button,
    Image,
    StyleSheet,
    KeyboardAvoidingView,
    SafeAreaView,
    Keyboard,
    Dimensions,
    RefreshControl,
    ActivityIndicator,
    StatusBar
} from "react-native";
import { name as appName, version as appVersion } from '../app.json';

import { Platform, NativeModules } from 'react-native';
import { COLORS } from "../assets/styles/styles";
import { TouchableOpacity } from "react-native-gesture-handler";
import { color } from "react-native-reanimated";
import { translate } from "../App";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import LoaderBox from "./loaderBox";
import VideoPlayer from 'react-native-video-controls';
import Video from 'react-native-video';
import TButton from "./tButton";
import Orientation, { OrientationLocker, PORTRAIT, LANDSCAPE } from "react-native-orientation-locker";
import FullScreenAndroid from "react-native-fullscreen-chz";

const { StatusBarManager } = NativeModules;

var dimensions = {
    screen: {
        width: Dimensions.get('screen').width,
        height: Dimensions.get('screen').height
    }, window: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height
    }
}

export default class VideoPlayerBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isVideoFullscreen: false,
            isVideoLoaded: false,
            toggleResizeModeOnFullscreen: false,
            dimensions: {
                width: Dimensions.get('screen').width,
                height: Dimensions.get('screen').height
            }
        }
        this.playerRef = {}
    }
    componentDidMount() {
        // Keyboard
    }
    componentWillUnmount() {
        StatusBar.setHidden(false)
        Orientation.lockToPortrait();
    }
    // _onOrientationDidChange = (orientation) => {
    //     console.log('WQWER')
    //     // console.log(Dimensions.get('screen').width)
    //     dimensions = {
    //         screen: {
    //             width: Dimensions.get('screen').width,
    //             height: Dimensions.get('screen').height
    //         }, window: {
    //             width: Dimensions.get('window').width,
    //             height: Dimensions.get('window').height
    //         }
    //     }
    //     if (!orientation) {
    //         if (dimensions.window.width < dimensions.window.height) {
    //             orientation = 'PORTRAIT'
    //         }
    //     }
    //     this.setState({
    //         dimensions: {
    //             width: orientation === 'PORTRAIT' ? dimensions.screen.width : dimensions.screen.height,
    //             height: orientation === 'PORTRAIT' ? dimensions.screen.height : dimensions.screen.width,
    //         }
    //     })

    // }
    _onOrientationDidChange = (orientation) => {
        dimensions = {
            screen: {
                width: Dimensions.get('screen').width,
                height: Dimensions.get('screen').height
            }, window: {
                width: Dimensions.get('window').width,
                height: Dimensions.get('window').height
            }
        }
        // console.log(orientation)
        if (orientation) {
            if (orientation === 'PORTRAIT') {
                orientation = true
            } else if (orientation === 'LANDSCAPE-LEFT' || orientation === 'LANDSCAPE-RIGHT') {
                orientation = false
            }
        } else {
            if (dimensions.window.width < dimensions.window.height) {
                orientation = true
            } else {
                orientation = false
            }
        }

        console.log('orientation: ' + orientation)

        this.setState({
            dimensions: {
                width: dimensions.screen.width,
                height: dimensions.screen.height,

                // width: dimensions.screen.width,
                // height: dimensions.screen.height,
            },
            isPortrait: orientation
        }, this._checkOrientation)

    }
    setVideoFullscreen = (type) => {
        if (type) {
            // this.player.presentFullscreenPlayer()
        } else {
            // this.player.dismissFullscreenPlayer()
        }
    }
    onFullscreenPlayerWillPresent = () => {
        this.props.setVisibleElements(false)
        Orientation.addOrientationListener(this._onOrientationDidChange);
        Orientation.unlockAllOrientations();
        // Orientation.lockToLandscape();
        this.playerRef.player.ref.presentFullscreenPlayer()
        // StatusBar.setHidden(true)
        FullScreenAndroid.enable()
        this.setState({ isVideoFullscreen: true }, () => {
            this.updateScroll()
            this.playerRef.player.ref.presentFullscreenPlayer()
            FullScreenAndroid.enable()
        })

    }
    updateScroll = () => {
        if (this.state.isVideoFullscreen) {
            this.props.scrollRef.scrollTo({ x: -50, y: this.props.position.y, animated: false })
            setTimeout(() => {
                this.props.scrollRef.scrollTo({ x: -50, y: this.props.position.y, animated: false })
            }, 100)
        }
    }
    onFullscreenPlayerWillDismiss = () => {
        Orientation.removeOrientationListener(this._onOrientationDidChange);
        Orientation.lockToPortrait();
        StatusBar.setHidden(false)
        this.playerRef.player.ref.dismissFullscreenPlayer()
        FullScreenAndroid.disable()
        this.setState({ isVideoFullscreen: false }, () => {

        })
        this.props.setVisibleElements(true)
        // this.props.scrollRef.scrollEnabled(true)
    }
    render() {
        let height = {
            height: this.state.isVideoFullscreen ? this.state.dimensions.height : (Dimensions.get('window').width - 32) * 9 / 16
        }
        let padding = {
            paddingHorizontal: this.state.isVideoFullscreen ? 0 : 16,
        }
        if (Platform.OS == 'android') {

            return (


                <View style={[styles.box, padding]}>
                    <View style={[styles.videoBox, height]} onLayout={e => { this._onOrientationDidChange(); this.updateScroll() }}>
                        {!this.state.isVideoLoaded ?
                            <LoaderBox text={translate("default.loadingVideo")}></LoaderBox>
                            : null}
                        <VideoPlayer
                            source={{ uri: this.props.video.source, type: this.props.video.type }}
                            navigator={this.props.navigator}
                            ref={ref => this.playerRef = ref}
                            style={{
                                // position: 'absolute',
                                // width: '100%',
                                borderRadius: this.state.isVideoFullscreen ? 0 : 16,
                                // height: '100%',
                                ...StyleSheet.absoluteFill
                            }}
                            onSeek={e => console.log(e)}
                            seekColor={COLORS.primary}
                            // tapAnywhereToPause={true}
                            onEnterFullscreen={this.onFullscreenPlayerWillPresent}
                            onExitFullscreen={this.onFullscreenPlayerWillDismiss}
                            disableVolume={true}
                            disableBack={true}
                            toggleResizeModeOnFullscreen={false}
                            onLoad={e => { this.setState({ isVideoLoaded: true }) }}
                            paused={true}
                        >

                        </VideoPlayer>
                    </View>
                </View>
            );
        } else {
            return (
                <View style={[styles.box, styles.padding]}>
                    <View style={[styles.videoBox, height]} onLayout={e => { this._onOrientationDidChange() }}>
                        {!this.state.isVideoLoaded ?
                            <LoaderBox text={translate("default.loadingVideo")}></LoaderBox>
                            : null}
                        <Video source={{ uri: this.props.video.source, type: this.props.video.type }}   // Can be a URL or a local file.
                            ref={(ref) => {
                                this.player = ref
                            }}
                            onBuffer={e => { console.log(e) }}
                            onError={e => { console.log(e) }}
                            onLoad={e => { this.setState({ isVideoLoaded: true }) }}
                            controls={true}
                            paused={this.state.isVideoLoaded}
                            resizeMode={"contain"}
                            onFullscreenPlayerWillPresent={this.onFullscreenPlayerWillPresent}
                            onFullscreenPlayerWillDismiss={this.onFullscreenPlayerWillDismiss}
                            style={{
                                position: 'absolute',
                                width: '100%',
                                borderRadius: 16,
                                height: (Dimensions.get('window').width - 32) * 9 / 16,
                            }} />
                    </View>
                </View>
            )
        }
    }
}
const styles = StyleSheet.create({
    videoBox: {
        backgroundColor: '#efefef',
        borderRadius: 16,
        position: 'relative',
        alignItems: 'center',
        justifyContent: 'center'
    },
    padding: {
        paddingHorizontal: 16
    }
});
