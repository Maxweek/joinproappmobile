import React, { Component } from "react";
import { View, Text, StyleSheet, Animated } from "react-native";
import { TextInput } from "react-native";
// import { TextInputMask } from 'react-native-masked-text'

import RNPickerSelect from 'react-native-picker-select';
import Icon from "react-native-vector-icons/Ionicons";
import { COLORS } from "../assets/styles/styles";

export default class SelectPicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFocused: false,
            isEmpty: true,
            value: this.props.value,
            selection: null
        };
    }

    render() {
        return (
            <RNPickerSelect
                onValueChange={this.props.onValueChange}
                useNativeAndroidPickerStyle={false}
                placeholder={this.props.placeholder ? {
                    label: this.props.placeholder,
                    value: null,
                    color: COLORS.gray,
                } : {}}
                style={{
                    ...pickerSelectStyles,
                    iconContainer: {
                        top: 16,
                        right: 12,
                    },
                }}
                value={this.props.value}
                items={this.props.items}
                Icon={() => {
                    return <Icon name="chevron-down-outline" size={16} color={COLORS.gray} />
                }}
            />
        );
    }
}

const pickerSelectStyles = StyleSheet.create({
    placeholder: {
        color: COLORS.gray,
    },

    inputIOS: {
        fontSize: 16,
        paddingVertical: 12,
        paddingLeft: 14,
        borderWidth: 1,
        borderColor: COLORS.middlegray,
        borderRadius: 8,
        color: COLORS.black,
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    inputAndroid: {
        fontSize: 16,
        padding: 16,
        paddingVertical: 12,
        borderWidth: 1,
        borderColor: COLORS.middlegray,
        borderRadius: 8,
        color: COLORS.black,
        paddingLeft: 14,
        paddingRight: 30, // to ensure the text is never behind the icon
    },
})