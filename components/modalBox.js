import React, { Component } from "react";
import {
    Text,
    View,
    StyleSheet,
    SafeAreaView,
    Dimensions,
    Platform,
} from "react-native";

import { Modalize } from "react-native-modalize";
import { Portal } from "react-native-portalize";
import { COLORS } from "../assets/styles/styles";

export default class ModalBox extends Component {
    constructor(props) {
        super(props);

        this.state = {
            filters: this.props.filters,
            isActive: false,
        }
        this.modalRef
    }
    componentDidMount() {
        // Keyboard

        if (this.props.onRef != null) {
            this.props.onRef(this)
        }
    }
    open() {
        this.modalRef.open()
    }
    close() {
        this.modalRef.close()
    }

    render() {
        let modalInner_body_padding = {
            padding: this.props.padding !== undefined ? this.props.padding : 16,
        }
        return (
            <Portal>

                <Modalize ref={this.props.modalRef}
                    // snapPoint={Dimensions.get('window').height - 100}
                    // closeSnapPointStraightEnabled={false}
                    // reactModalProps={{
                    //     presentationStyle: "formSheet",
                    //     transparent: false,
                    //     animationType: 'slide'
                    // }}
                    // transparent={false}
                    withOverlay={true}
                    // withReactModal={true}
                    // panGestureComponentEnabled={true}
                    // tapGestureEnabled={this.props.tapGestureEnabled}
                    onOpened={this.props.onOpened}
                    onClosed={this.props.onClosed}
                    // panGestureAnimatedValue={this.props.animated ? this.props.animated : undefined}
                    modalTopOffset={20}
                    adjustToContentHeight={true}
                    disableScrollIfPossible={Platform.OS === 'ios' ? true : false}
                    HeaderComponent={
                        this.props.headerTitle ?
                        <View style={styles.modalInner_header}>
                            <Text style={styles.modalInner_header_text}>{this.props.headerTitle}</Text>
                        </View>
                        : null
                    }
                    FooterComponent={this.props.footer ?
                        <SafeAreaView>
                            <View style={styles.modalInner_action}>
                                {this.props.footer}
                            </View>
                        </SafeAreaView>
                        : null}
                >
                    <View style={[styles.modalInner_body, modalInner_body_padding]}>
                        {this.props.children}
                    </View>
                </Modalize>
            </Portal>
        );
    }
}
const styles = StyleSheet.create({
    modalInner: {
        backgroundColor: 'white',
        bottom: 0,
        width: '100%',
        // paddingTop: 24,
        overflow: 'hidden',
        borderTopLeftRadius: 16,
        borderTopRightRadius: 16,
        position: 'relative',
        // paddingBottom: 20
    },
    modalInner_header: {
        padding: 12,
        paddingHorizontal: 16,
        borderBottomColor: COLORS.lightgray,
        borderBottomWidth: 1,
        borderStyle: 'solid'
    },
    modalInner_header_text: {
        textAlign: 'center'
    },
    modalInner_body: {
        // padding: 16,
        // minHeight: Dimensions.get('window').height / 3,
        // paddingBottom: 20
    },
    modalInner_title: {
        // padding: 16,
        fontSize: 20,
        marginBottom: 16,
    },
    modalInner_action: {
        padding: 16,
        borderTopColor: COLORS.lightgray,
        borderTopWidth: 1,
        borderStyle: 'solid'
    },
});
