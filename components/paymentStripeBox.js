import React, { PureComponent } from 'react'
import { View, Text, StyleSheet, StatusBar, ScrollView, Dimensions } from 'react-native'
// import TButton from "../../components/tButton";
import { CardField, useConfirmPayment, useStripe } from '@stripe/stripe-react-native';
import { useState } from 'react';
import { StripeProvider } from '@stripe/stripe-react-native';
import TButton from './tButton';
import API from '../API';
import { COLORS } from '../assets/styles/styles';
import FloatingLabelInput from './floatingLabelInput';
import { translate } from '../App';

function PaymentStripeBox(props) {
    const { confirmPayment, loading } = useConfirmPayment();

    const [cardDetails, setCardDetails] = useState({ complete: false });
    const [paymentError, setPaymentError] = useState({ localizedMessage: '' });
    const [paymentSuccess, setPaymentSuccess] = useState({ status: '' });
    const [paymentInfo, setPaymentInfo] = useState(false);

    const handlePayPress = async () => {
        setPaymentSuccess({ status: '' })
        setPaymentError({ localizedMessage: '' })
        console.log(props)

        if (cardDetails.complete) {
            console.log(cardDetails)

            const formData = new FormData();

            formData.append('amount', props.amount);
            formData.append('currency', props.currency);
            formData.append('impression_id', props.impression_id);

            console.log(formData)

            let response = await API.post('/payment/stripe/paymentIntents/create/', formData)

            console.log(response)

            const billingDetails = props.billingDetails;

            // Fetch the intent client secret from the backend
            const clientSecret = response.data.client_secret

            // Confirm the payment with the card details
            console.log(props)
            const { paymentIntent, error } = await confirmPayment(clientSecret, {
                type: 'Card',
                billingDetails,
            });

            if (error) {
                console.log('Payment confirmation error', error);
                setPaymentError(error)
                setPaymentSuccess({ status: '' })
            } else if (paymentIntent) {
                console.log('Success from promise', paymentIntent);
                setPaymentError({ localizedMessage: '' })
                setPaymentSuccess(paymentIntent)
                setTimeout(() => {
                    setPaymentInfo(true);
                }, 1000)
            }
        }
    }
    const handlePaymentAgain = () => {
        setPaymentInfo(false)
        setPaymentError('')
        setPaymentSuccess('')
    }
    const boxStyles = {
        padding: props.padding ? 16 : 0,
        borderWidth: props.border ? 1 : 0,
        elevation: props.shadow ? 8 : 0,
    }

    const getAddFields = () => {
        if (props.addFields) {
            return props.addFields.map(el => {
                return (
                    <FloatingLabelInput
                        key={el.fieldName}
                        label={el.fieldName}
                        value={el.fieldValue}
                        onChangeText={el.fieldAction}
                    />
                )
            })
        } else {
            return null;
        }
    }
    console.log(props)
    return (

        <StripeProvider
            // publishableKey={"pk_test_cWoBmAdxLVg9Bz04kVaolNcD"}
            publishableKey={"pk_live_51J8QxCKp1eGwT7m8MSJ8zmPEoqAHk7PZ6l6ktw21Xe9GbqW7YTgadHpGJtrN4YIJI4f875AVPWdJAHkLYIVzSB7s00N6N7DElj"}
            merchantIdentifier="merchant.identifier"
        >
            <View style={props.style}>
                <View style={[boxStyles, styles.box]}>
                    {paymentInfo ?
                        <View style={styles.thanksBox}>
                            <Text style={styles.thanksBoxText}>{translate("pay.title")}</Text>
                            <Text style={styles.thanksBoxDescription}>{translate("pay.subtitle")}</Text>
                            <TButton
                                onPress={() => { handlePaymentAgain() }}
                                text={translate("pay.paymentAgain")}
                                type="primary"
                                style={{ marginTop: 4 }}
                            />
                            <TButton
                                onPress={props.actionAfterPayment}
                                text={translate("pay.actionAfterPayment")}
                                type="primary"
                                style={{ marginTop: 16 }}
                            />
                        </View>
                        :
                        <>
                            {getAddFields()}
                            <View style={styles.cardFieldBox}>
                                <CardField
                                    postalCodeEnabled={false}
                                    placeholder={{
                                        number: '0000 0000 0000 0000',
                                    }}
                                    cardStyle={{
                                        backgroundColor: '#FFFFFF',
                                        textColor: '#000000',
                                    }}
                                    style={{
                                        width: '100%',
                                        height: 50,
                                    }}
                                    onCardChange={(cardD) => {
                                        setCardDetails(cardD)
                                        console.log('cardDetails', cardD);
                                    }}
                                    onFocus={(focusedField) => {
                                        console.log('focusField', focusedField);
                                    }}
                                />
                            </View>
                            {paymentError.localizedMessage !== '' ?
                                <View style={styles.errorBox}>
                                    <Text style={styles.errorTitle}>{translate("default.error")}:</Text>
                                    <Text style={styles.errorText}>{paymentError.localizedMessage}</Text>
                                </View>
                                : null}
                            {paymentSuccess.status !== '' ?
                                <View style={styles.successBox}>
                                    <Text style={styles.successTitle}>{translate("default.status")}:</Text>
                                    <Text style={styles.successText}>{paymentSuccess.status}</Text>
                                </View>
                                : null}
                            <View style={{ position: 'relative', zIndex: 20 }}>
                                <TButton
                                    onPress={() => { handlePayPress() }}
                                    text={translate("pay.completeButtonText")}
                                    type="primary"
                                    isLoading={loading}
                                    isLocked={!cardDetails.complete}
                                    style={{ marginTop: 4 }}
                                />
                            </View>
                        </>
                    }
                </View>
            </View>
        </StripeProvider>
    );
}

const styles = StyleSheet.create({
    box: {
        borderStyle: 'solid',
        borderColor: COLORS.lightgray,
        borderRadius: 8,
        backgroundColor: COLORS.white,
        overflow: 'hidden',
        position: 'relative',
        maxWidth: Dimensions.get('window').width - 32

        // backgroundColor: COLORS.alert
    },
    cardFieldBox: {
        marginVertical: 8,
        borderRadius: 8,
        borderStyle: 'solid',
        borderColor: COLORS.lightgray,
        borderWidth: 1,
        overflow: 'hidden',
        marginBottom: 8
    },
    errorBox: {
        flexDirection: 'row',
        marginVertical: 8,
        paddingHorizontal: 8
    },
    errorTitle: {
        fontSize: 14,
        color: COLORS.danger,
        marginRight: 4
    },
    errorText: {
        flex: 1,
        fontSize: 14,
        color: COLORS.danger
    },
    successBox: {
        flexDirection: 'row',
        marginVertical: 8,
        paddingHorizontal: 8
    },
    successTitle: {
        fontSize: 14,
        color: COLORS.primary,
        marginRight: 4
    },
    successText: {
        flex: 1,
        fontSize: 14,
        color: COLORS.primary
    },
    thanksBox: {
        backgroundColor: COLORS.white,
        zIndex: 60,
        alignItems: 'center',
        justifyContent: 'center'
    },
    thanksBoxText: {
        fontSize: 26,
        marginBottom: 4
    },
    thanksBoxDescription: {
        fontSize: 14,
        marginBottom: 16
    }
})

export default PaymentStripeBox;
