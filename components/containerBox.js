import React, { Component } from "react";
import I18n from "i18n-js";
import {
    Text,
    View,
    StyleSheet,
    RefreshControl,
    Animated
} from "react-native";

import { Platform, NativeModules } from 'react-native';
import { COLORS } from "../assets/styles/styles";
import { translate } from "../App";
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view'
import LoaderBox from "./loaderBox";

const { StatusBarManager } = NativeModules;


export default class ContainerBox extends Component {
    _isMounted = false;
    _headerHeight = 60;
    _offsetScrollView = false;
    constructor(props) {
        super(props);

        if (this.props.header) {
            if (this.props.header.props.shadow) {
                this._offsetScrollView = true
            }
        } else {
            this._headerHeight = 0
        }
    }
    componentDidMount() {
        // Keyboard
    }
    getTitle = () => {
        let content = null;
        if (this.props.title) {
            content = <Text style={styles.mainTitle}>{this.props.title}</Text>
        }
        return content
    }
    getSubTitle = () => {
        let content = null;
        if (this.props.subtitle) {
            content = <Text style={styles.mainSubTitle}>{this.props.subtitle}</Text>
        }
        return content
    }
    render() {
        let loaded = true;

        let padding = {
            paddingTop: this.props.paddingTop ? StatusBarManager.HEIGHT + this._headerHeight + 36 : StatusBarManager.HEIGHT + this._headerHeight,
            paddingHorizontal: this.props.paddingSide ? this.props.paddingSide : 16
        }
        let offset = {
            marginTop: 0
        }
        if (this.props.isContentLoaded !== undefined) {
            if (!this.props.isContentLoaded) {
                loaded = false;
            } else {
                loaded = true;
            }
        }
        if (this._offsetScrollView) {
            padding.paddingTop = 0
            offset.marginTop = StatusBarManager.HEIGHT + this._headerHeight
        }
        return (
            <View style={styles.mainBack}>
                <Animated.View style={[styles.container, this.props.style]}>
                    {this.props.header ? this.props.header : null}
                    {loaded ?
                        <KeyboardAwareScrollView
                            refreshControl={this.props.onRefresh ? <RefreshControl
                                colors={["#14ccb4", "#f22424"]}
                                // progressViewOffset={400}
                                refreshing={this.props.refreshing}
                                onRefresh={this.props.onRefresh}
                            /> : null}
                            viewIsInsideTabBar={false}
                            style={offset}
                            contentContainerStyle={[styles.scrollView]}
                            keyboardShouldPersistTaps='handled'
                            indicatorStyle={'black'}
                            bounces={this.props.bounces ? this.props.bounces : true}
                        >
                            <View style={[styles.scrollView__inner, padding]}>
                                {this.getTitle()}
                                {this.getSubTitle()}
                                {this.props.children}
                            </View>
                        </KeyboardAwareScrollView>
                        : <View style={styles.loaderBox}>
                            <LoaderBox text={translate("impression.loading")}></LoaderBox>
                        </View>}
                </Animated.View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    mainBack: {
        flex: 1,
        position: 'relative',
        backgroundColor: '#fefefe'
    },
    mainTitle: {
        fontSize: 27,
        textAlign:'center',
        color: COLORS.black,
    },
    mainSubtitle: {
        marginTop: 4,
        fontSize: 16,
        color: COLORS.black,
    },
    container: {
        top: 0,
        flex: 1,
        // backgroundColor: '#ffff00',
        position: "relative",
        overflow: 'hidden',
    },
    scrollView: {
        top: 0,
        width: "100%",
    },
    scrollView__inner: {
        paddingBottom: StatusBarManager.HEIGHT,
        alignItems: 'center',
        backgroundColor: '#fefefe'
    },
    loaderBox: {
        paddingTop: StatusBarManager.HEIGHT,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    loaderBox__indicator: {
    },
    loaderBox__text: {
        marginTop: 12,
        letterSpacing: 2,
        fontSize: 12,
        fontWeight: 'bold',
        color: COLORS.gray
    }
});
