import React, { Component } from "react";
import {
    Text,
    ActivityIndicator,
    StyleSheet
} from "react-native";
import { COLORS } from "../assets/styles/styles";

export default class LoaderBox extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <>
                <ActivityIndicator
                    size={'large'}
                    color={COLORS.primary}
                    style={styles.loaderBox__indicator}
                />
                <Text style={styles.loaderBox__text}>{this.props.text}</Text>
            </>
        );
    }
}
const styles = StyleSheet.create({
    loaderBox__indicator: {
    },
    loaderBox__text: {
        marginTop: 12,
        letterSpacing: 2,
        fontSize: 12,
        fontWeight: 'bold',
        color: COLORS.gray
    }
});
