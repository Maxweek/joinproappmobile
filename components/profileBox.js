import React, { Component } from "react";
import { View, Text, StyleSheet, Image } from "react-native";
import { TouchableHighlight, TouchableOpacity } from "react-native";
import { COLORS } from "../assets/styles/styles";
import RatingBox from "./ratingBox";

export default class ProfileBox extends Component {
    constructor(props) {
        super(props);
    }

    onPress = () => {
        this.props.onPress()
    }

    render() {
        let boxStyles = {
            padding: this.props.padding ? 16 : 0,
            borderWidth: this.props.border ? 1 : 0,
        }
        return (
            <View style={this.props.style}>
                <TouchableOpacity style={[styles.box, boxStyles]} onPress={this.onPress}>
                    {this.props.image ?
                        <View style={styles.imageBox}>
                            <Image
                                style={{ width: '100%', height: '100%' }}
                                source={{ uri: this.props.image }}
                            />
                        </View>
                        : null}
                    <View style={styles.infoBox}>
                        {this.props.topText ?
                            <Text style={styles.topText}>{this.props.topText}</Text>
                            : null}
                        <Text style={styles.name}>{this.props.name}</Text>
                        {this.props.botText ?
                            <Text style={styles.botText}>{this.props.botText}</Text>
                            : null}
                        {this.props.rate ?
                            <RatingBox rate={this.props.rate} style={{marginTop: 2}}/>
                            : null}
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    box: {
        flexDirection: 'row',
        alignItems: 'center',
        borderStyle: 'solid',
        borderColor: COLORS.lightgray,
        borderRadius: 8,
    },
    imageBox: {
        width: 56,
        height: 56,
        position: 'relative',
        borderRadius: 56,
        overflow: 'hidden',
        marginRight: 16
    },
    infoBox: {
    },
    topText: {
        fontSize: 14,
        color: COLORS.black,
        lineHeight: 20
    },
    name: {
        fontSize: 18,
        lineHeight: 24,
        color: COLORS.black,
        fontWeight: 'bold'
    },
    botText: {
        fontSize: 14,
        color: COLORS.black,
        lineHeight: 20
    },
})