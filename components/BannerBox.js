import React, { Component } from "react";
import I18n from "i18n-js";
import {
    Text,
    View,
    StyleSheet,
    Dimensions,
    FlatList,
    Image,
    MaskedViewBase,
} from "react-native";
import Card from "./card";
import { COLORS } from "../assets/styles/styles";
import Icon from "react-native-vector-icons/Ionicons";
import { TouchableOpacity } from "react-native-gesture-handler";
import LinearGradient from "react-native-linear-gradient";
import MaskedView from "@react-native-community/masked-view";

export default class BannerBox extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: this.props.id,
            data: {
                image: {
                    mask: false,
                    url: 'https://joinpro.ru/upload/uf/eb8/eb8dd1868ddabedb66fe28923e0052f7.png'
                },
                title: {
                    value: 'Invite your friends',
                    style: {
                        fontSize: 22,
                        lineHeight: 24,
                        fontWeight: 'bold',
                        color: COLORS.black
                    }
                },
                subtitle: {
                    value: 'Get $20 for ticket',
                    style: {
                        fontSize: 16,
                        lineHeight: 18,
                        marginTop: 8,
                        color: COLORS.gray
                    }
                },
                actions: [
                    {
                        value: 'action 1',
                        func: () => { },
                        boxStyle: {
                            backgroundColor: COLORS.primary,
                        },
                        textStyle: {
                            color: COLORS.white,
                        }
                    },
                    {
                        value: 'action 2',
                        func: () => { },
                        style: {
                            backgroundColor: COLORS.primary,
                            color: COLORS.white,
                        }
                    },
                ]
            },
            options: {
                closable: true,
                style: {
                    // backgroundColor: COLORS.lightgreen,
                },
            }
        }
    }
    componentDidMount() {
        // this.getData();
    }
    getData = () => {
        API.post(API_IMPRESSION_LIST)
            .then(res => {
                console.log(res)
                if (typeof res.data.status !== 'undefined') {
                    if (res.data.status === 'success') {
                        console.log(res.data.impressions)
                        this.setList(res.data.impressions);
                        this.setState({ refreshing: false, items: res.data.impressions, isNotEmpty: true, isContentLoaded: true });
                    } else {
                        console.log('list_load_error_1')
                        this.setState({ refreshing: false, isNotEmpty: false });
                    }
                } else {
                    console.log('list_load_error_2')
                    this.setState({ refreshing: false, isNotEmpty: false });
                }
            })
            .catch(err => {
                console.log(err)
                this.setState({ refreshing: false, isNotEmpty: false });
            })
    }
    getImageBox = () => {
        let imageBox = null;
        let mask = <View style={styles.bannerBox__image_mask_gradient}></View>;
        let image = null

        if (this.state.data.image.url) {
            image = <Image style={styles.bannerBox__image}
                source={{ uri: this.state.data.imageUri }}
            />
        }
        if (image === null) {
            return imageBox;
        }
        if (this.state.data.image.mask) {
            mask = <LinearGradient
                start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                colors={['#FFFFFF00',
                    '#FFFFFF',
                    '#FFFFFF',]}
                locations={[0, 0.4, 1]}
                style={styles.bannerBox__image_mask_gradient}>
            </LinearGradient>
        }

        imageBox = <View style={styles.bannerBox__imageBox}>
            <MaskedView style={styles.bannerBox__image_mask}
                maskElement={mask}>
                {image}
            </MaskedView>
        </View>

        return imageBox;
    }
    getActionsBox = () => {
        let actionBox = null;

        if (this.state.data.actions.length) {
            actionBox = <View style={styles.bannerBox__actionBox}>
                {this.state.data.actions.map(el => {
                    return <TouchableOpacity style={[styles.bannerBox__action, el.boxStyle]} onPress={el.func}>
                        <Text style={[styles.bannerBox__action_text, el.textStyle]}>{el.value}</Text>
                    </TouchableOpacity>
                })}
            </View>
        }

        return actionBox;
    }
    getCloseBox = () => {
        let closeBox = null;

        if (this.state.options.closable) {
            closeBox = <View style={styles.bannerBox__closeBox}>
                <TouchableOpacity style={styles.bannerBox__close} onPress={this.onClose}>
                    <Icon name={'close'} size={14} color={COLORS.gray} />
                </TouchableOpacity>
            </View>
        }

        return closeBox
    }
    onClose = () => {

    }
    render() {
        return (
            <View style={[styles.bannerBox, this.props.style, this.state.options.style]}>
                {this.getImageBox()}
                {this.getCloseBox()}
                <View style={styles.bannerBox__box}>
                    <Text style={[styles.bannerBox__title, this.state.data.title.style]}>{this.state.data.title.value}</Text>
                    <Text style={[styles.bannerBox__subtitle, this.state.data.subtitle.style]}>{this.state.data.subtitle.value}</Text>
                    {this.getActionsBox()}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    bannerBox: {
        width: '100%',
        position: 'relative',
        padding: 16,
        borderRadius: 16,
        overflow: 'hidden',
        backgroundColor: COLORS.lightgreen
    },
    bannerBox__box: {
        position: 'relative',
        zIndex: 10
    },
    bannerBox__title: {
        fontSize: 22,
        lineHeight: 24,
        fontWeight: 'bold',
        color: COLORS.black
    },
    bannerBox__subtitle: {
        fontSize: 16,
        lineHeight: 18,
        marginTop: 8,
        color: COLORS.gray
    },
    bannerBox__actionBox: {
        flexDirection: 'row',
        margin: -4,
        marginTop: 8
    },
    bannerBox__action: {
        margin: 4,
        padding: 8,
        paddingHorizontal: 16,
        borderRadius: 4,
        backgroundColor: COLORS.primary,
    },
    bannerBox__action_text: {
        color: COLORS.white
    },
    bannerBox__imageBox: {
        position: 'absolute',
        right: 0,
        top: 0,
        bottom: 0,
        width: '70%',
        zIndex: 6
    },
    bannerBox__image_mask: {
        width: '100%',
        height: '100%'
    },
    bannerBox__image_mask_gradient: {
        width: '100%',
        height: '100%'
    },
    bannerBox__image: {
        width: '100%',
        height: '100%'
    },
    bannerBox__closeBox: {
        position: 'absolute',
        zIndex: 14,
        right: 16,
        top: 16,
    },
    bannerBox__close: {

        backgroundColor: COLORS.white,
        borderRadius: 40,
        width: 24,
        height: 24,
        alignItems: 'center',
        justifyContent: 'center'
    }
});