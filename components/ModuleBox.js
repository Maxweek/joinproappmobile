import React, { Component } from "react";
import I18n from "i18n-js";
import {
    Text,
    View,
    StyleSheet,
    Dimensions,
    FlatList,
    Animated,
    TouchableOpacity,
} from "react-native";
import Card from "./card";
import Icon from "react-native-vector-icons/Ionicons";
import { COLORS } from "../assets/styles/styles";

export default class ModuleBox extends Component {
    constructor(props) {
        super(props);

        this.state = {

        }
        this.animateVars = {
            loaded: new Animated.Value(0),
        }
    }
    componentDidUpdate(){
        Animated.spring(this.animateVars.loaded, {
            toValue: this.props.isLoaded ? 1 : 0,
            duration: 200,
            useNativeDriver: false
        }).start();
    }
    render() {
        let loadedStyle = {
            opacity: 1
        }
        return (
            <Animated.View style={[styles.moduleBox, loadedStyle, this.props.style]}>
                <View style={styles.moduleBox__header}>
                    <View style={styles.moduleBox__title}>
                        <Text style={styles.moduleBox__header_text}>{this.props.title}</Text>
                    </View>
                    {!this.props.actionHidden ? 
                    <View style={styles.moduleBox__actionBox}>
                        <TouchableOpacity style={styles.moduleBox__action} onPress={this.props.action}>
                            <Text style={styles.moduleBox__action_text}>{this.props.actionText}</Text>
                            {this.props.actionIcon ?
                                <Icon name={this.props.actionIcon} size={14} color={COLORS.primary} />
                                : null}

                        </TouchableOpacity>
                    </View>
                    : null}
                </View>
                <View style={styles.moduleBox__body}>
                    {this.props.children}
                </View>
            </Animated.View>
        );
    }
}

const styles = StyleSheet.create({
    moduleBox: {
        width: '100%',
        marginVertical: 24,
        // padding: 16,
    },
    moduleBox__header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        marginBottom: 16,
        // backgroundColor: 'yellow',
    },
    moduleBox__header_text: {
        fontSize: 22,
        lineHeight: 22,
        fontWeight: 'bold'
    },
    moduleBox__actionBox: {
    },
    moduleBox__action: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    moduleBox__action_text: {
        fontSize: 14,
        lineHeight: 20,
        color: COLORS.primary
    },
    moduleBox__body: {
        flexDirection: 'row',
    },
});