import React, { Component } from "react";
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    ScrollView
} from "react-native";
// import { ScrollView } from "react-native-gesture-handler";
import Icon from "react-native-vector-icons/Ionicons";
import { Portal } from "react-native-portalize";
import CalendarBox from "../calendarBox";
import { COLORS } from "../../assets/styles/styles";
import ModalBox from "../modalBox";
import TButton from "../tButton";
import { SvgUri } from "react-native-svg";

export default class Filter__checkBoxCat extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        // Keyboard
    }
    set = (item, key) => {
        this.props.set(
            {
                ...this.props.item,
                data: [...this.props.item.data, { [key]: item }]
            }
        )
    }
    setItem = (item, key) => {
        if (item.type === 'calendar') {
            if (!item.isActive) {
                item.isActive = true
                item.modalRef.open()
            } else {
                item.value = item.default_value
                item.isActive = false
            }
        } else {
            if (item.isActive) {
                item.isActive = false
            } else {
                item.isActive = true
            }
        }
        this.set(item, key)
    }
    getCalendar = (item, key) => {

        return <Portal>
            <ModalBox
                modalRef={ref => item.modalRef = ref}
                headerTitle={item.value}
                padding={0}
                footer={
                    <TButton
                        onPress={e => {
                            item.modalRef.close()
                        }}
                        type={'primary'}
                        text={translate('default.selectDate')}
                    />
                }
            >
                <CalendarBox
                    onDaySelect={date => {
                        item.value = date.dateString
                        this.setItem(item, key)
                    }}
                />
            </ModalBox>
        </Portal>
    }
    getCheckBoxCategory = () => {
        let i = 0
        var getCheckBoxCategory__item = (item, key) => {
            let icon = undefined
            if (item.icon) {
                if (item.icon.type) {
                    if (item.icon.type === 'font') {
                        icon =
                            <Icon name={item.icon.src} size={24} color={item.isActive ? COLORS.white : COLORS.gray} />
                    }
                    if (item.icon.type === 'svg') {
                        icon = <SvgUri
                            width={40}
                            height={40}
                            uri={item.icon.src}
                        />
                    }
                }
            }
            i++;
            return <>
                {item.type === 'calendar' ? this.getCalendar(item, key) : null}
                <TouchableOpacity key={'checbox_category_item_' + i} style={styles.item} onPress={e => { this.setItem(item, key) }}>
                    <View style={[styles.item_circ, {
                        borderColor: item.isActive ? COLORS.transparent : COLORS.middlegray,
                        backgroundColor: item.isActive ? COLORS.primary : '#00000000',
                        shadowOpacity: item.isActive ? 1 : 0,
                        shadowOffset: [0, 4],
                    }]}>
                        {icon}
                    </View>
                    <Text style={styles.item_text}>{item.value}</Text>
                </TouchableOpacity>
            </>
        }

        return <ScrollView horizontal={true}
            showsHorizontalScrollIndicator={false}
            style={styles.scroll}
        >
            {this.props.item.data.map((item, key) => getCheckBoxCategory__item(item, key))}

            {/* {this.props.item.data.map((item, key) => getCheckBoxCategory__item(item, key))} */}
        </ScrollView>

    }
    render() {
        return (
            <View>
                {this.getCheckBoxCategory()}
            </View>
        );
    }
}
const styles = StyleSheet.create({
    scroll: {
        // backgroundColor: 'red',
        marginHorizontal: -16,
        marginVertical: -8,
        paddingHorizontal: 8,
        paddingVertical: 8
    },
    item: {
        alignItems: 'center',
        marginRight: 8,
        // backgroundColor: 'blue',
        width: 80
    },
    item_circ: {
        shadowColor: COLORS.primary,
        shadowRadius: 8,
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 8,
        width: 60,
        height: 60,
        borderRadius: 100,
    },
    item_text: {
        textAlign: 'center',
        fontSize: 14
    }
});
