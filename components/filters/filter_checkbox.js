import React, { Component } from "react";
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity
} from "react-native";
// import { ScrollView } from "react-native-gesture-handler";
import Icon from "react-native-vector-icons/Ionicons";
import { Portal } from "react-native-portalize";
import CalendarBox from "../calendarBox";
import { COLORS } from "../../assets/styles/styles";
import ModalBox from "../modalBox";
import TButton from "../tButton";
import { translate } from "../../App";

export default class Filter_checkbox extends Component {
    constructor(props) {
        super(props);

        this.state = {
            filters: this.props.filters
        }
    }
    componentDidMount() {
        // Keyboard
    }
    set = (item, key) => {
        this.props.set(
            {
                ...this.props.item,
                data: [...this.props.item.data, { [key]: item }]
            }
        )
    }
    setItem = (item, key) => {
        if (item.type === 'calendar') {
            if (!item.isActive) {
                item.isActive = true
                item.modalRef.open()
            } else {
                item.value = "Выбрать" //TODO defaultValue
                item.isActive = false
            }
        } else {
            if (item.isActive) {
                item.isActive = false
            } else {
                item.isActive = true
            }
        }
        this.set(item, key)
    }
    getCalendar = (item, key) => {
        return <Portal key={'calendar_portal__' + key}>
            <ModalBox
                modalRef={ref => item.modalRef = ref}
                headerTitle={item.value}
                padding={0}
                onOpened={e => {
                    item.value = item.default_value
                    item.isActive = false
                    this.set(item, key)
                }}
                footer={
                    <TButton
                        onPress={e => {
                            item.modalRef.close()
                        }}
                        type={'primary'}
                        text={translate('default.selectDate')}
                    />
                }
            >
                <CalendarBox
                    onDaySelect={date => {
                        item.isActive = true
                        item.value = date.dateString
                        this.set(item, key)
                    }}
                />
            </ModalBox>
        </Portal>
    }
    getCheckBox = () => {
        let i = 0
        var getCheckBox__item = (item, key) => {
            i++;
            return <>
                {item.type === 'calendar' ? this.getCalendar(item, key) : null}
                <TouchableOpacity key={'checbox_category_item_' + i} style={[{
                    borderColor: item.isActive ? COLORS.transparent : COLORS.middlegray,
                    backgroundColor: item.isActive ? COLORS.primary : '#00000000',
                    shadowOpacity: item.isActive ? 1 : 0,
                    shadowOffset: [0, 4],
                    paddingHorizontal: item.type === 'calendar' ? 16 : 8
                }, styles.item]} onPress={e => { this.setItem(item, key) }} key={'checkbox__touch_' + key}>
                    {item.type === 'calendar' ?
                            <Icon name={'calendar'} size={16} color={item.isActive ? COLORS.white : COLORS.primary} />
                        
                        : null}
                    <Text style={[{
                        color: item.isActive ? COLORS.white : COLORS.gray,
                    }, styles.item_text]}>{item.value}</Text>
                    {item.type === 'calendar' ?
                        <Icon name={'chevron-forward-outline'} size={16} color={item.isActive ? COLORS.white : COLORS.primary} />
                        : null}

                </TouchableOpacity>
            </>
        }

        return <>
            <View style={styles.list}>
                {this.props.item.data.map((item, key) => getCheckBox__item(item, key))}
                {/* {this.props.item.data.map((item, key) => getCheckBox__item(item, key))} */}
            </View>

        </>
    }
    render() {
        return (
            <View>
                {this.getCheckBox()}
            </View>
        );
    }
}
const styles = StyleSheet.create({
    list: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginHorizontal: -8,
    },
    item: {
        alignItems: 'center',
        margin: 8,
        flexGrow: 1,
        flexDirection: 'row',
        // padding: 16,
        paddingVertical: 12,
        borderRadius: 8,
        borderWidth: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        shadowColor: COLORS.primary,
        shadowRadius: 8,
        // borderColor: COLORS.middlegray,
    },
    item_text: {
        flexGrow: 1,
        textAlign: 'center',
        fontSize: 14,
        marginHorizontal: 8
    }
});
