import React, { Component } from "react";
import I18n from "i18n-js";
import {
    Text,
    View,
    StyleSheet,
    RefreshControl,
    Animated,
    ScrollView,
    TouchableOpacity
} from "react-native";
import SelectPicker from "../selectPicker";
import { translate } from "../../App";
// import { ScrollView } from "react-native-gesture-handler";
// import SelectPicker from "./selectPicker";

export default class Filter_select extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        // Keyboard
    }
    setItem = (item, key) => {
        console.log('setItem')
        this.props.set(
            {
                ...this.props.item,
                data: [...this.props.item.data, { [key]: item }]
            }
        )
    }
    getSelectBox = () => {
        let labels = [];
        let activeItem = {};
        this.props.item.data.map((item, key) => {
            labels.push({
                label: item.value,
                value: item.key
            })
            if (item.isActive) {
                activeItem = item.key
            }
        })
        return <SelectPicker
            onValueChange={(value) => {
                this.props.item.data.map((item, key) => {
                    if (value === item.key) {
                        item.isActive = true
                    } else {
                        item.isActive = false
                    }
                    this.setItem(item, key)
                })
            }}
            value={activeItem}
            placeholder={translate('default.selectCountry')}
            items={labels}
        />
    }
    render() {
        return (
            <View>
                {this.getSelectBox()}
            </View>
        );
    }
}
const styles = StyleSheet.create({

});
