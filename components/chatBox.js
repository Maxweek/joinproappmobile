import React, { Component } from "react";
import I18n from "i18n-js";
import {
    ScrollView,
    Text,
    TextInput,
    View,
    Animated,
    Button,
    Image,
    StyleSheet,
    KeyboardAvoidingView,
    SafeAreaView,
    Keyboard,
    Dimensions,
    RefreshControl,
    ActivityIndicator,
    FlatList
} from "react-native";
import { name as appName, version as appVersion } from '../app.json';

import { Platform, NativeModules } from 'react-native';
import { COLORS } from "../assets/styles/styles";
import { TouchableOpacity } from "react-native-gesture-handler";
import { translate } from "../App";
import Icon from "react-native-vector-icons/Ionicons";
import MaskedView from "@react-native-community/masked-view";
import LinearGradient from "react-native-linear-gradient";

const { StatusBarManager } = NativeModules;

var dimensions = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
};

export default class ChatBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            animateVars: {
                sendButtonActivity: new Animated.Value(0),
            },
            isFocused: false,
        }
        this.FlatListRef;
    }
    componentDidMount() {
        // Keyboard
        this.checkSendButtonnActivity()
        this.scroll()
    }
    componentDidUpdate() {
        this.scroll()
    }

    setSendButtonActivity = (type) => {
        Animated.timing(this.state.animateVars.sendButtonActivity, {
            toValue: type ? 1 : 0,
            duration: 200,
            useNativeDriver: true
        }).start();
    }

    onChangeText = (text) => {
        this.props.onChangeText(text)
        this.checkSendButtonnActivity()
    }

    checkSendButtonnActivity = () => {
        if (this.props.inputValue !== '') {
            this.setSendButtonActivity(true)
        } else {
            this.setSendButtonActivity(false)
        }
    }

    onInputFocus = () => {
        this.props.onInputFocus()
        this.props.onChangeText(this.props.inputValue)
        this.scroll()
        setTimeout(() => {
            this.scroll()
            this.setState({ isFocused: true })
            this.props.onChangeText(this.props.inputValue)
        }, 300)
    }
    onInputBlur = () => {
        setTimeout(() => {
            this.scroll()
            this.setState({ isFocused: false })
        }, 300)
    }
    onSend = () => {
        this.props.onSend()
        this.scroll()
        setTimeout(() => {
            this.checkSendButtonnActivity()
        }, 100)
    }
    _onOrientationDidChange = () => {
        this.scroll()
    }
    reverse = a => { return a.reverse(); }
    scroll = () => {
        // this.FlatListRef.scrollToOffset({ offset: 0, animated: true })
        setTimeout(() => {
            if (this.FlatListRef !== null) {
                this.FlatListRef.scrollToEnd()
            }
        }, 10)
    }

    renderMessage = ({ item }) => {
        // console.log(item.user.type)
        let date = item.timestamp.getHours().toString().padStart(2, '0') + ':' + item.timestamp.getMinutes().toString().padStart(2, '0');
        let typeMessage = {}
        let typeMessageItemImageBox = {}
        if (item.user.type === 'host') {
            typeMessage = {
                // backgroundColor: 'red'
            }
            typeMessageItemImageBox = {
                borderColor: COLORS.primary
            }
        }
        return (
            <View style={[styles.messageItem, typeMessage]} key={'key__' + item.id}>
                <View style={[styles.messageItemImageBox, typeMessageItemImageBox]}>
                    <View style={styles.messageItemImageBox_inner}>
                        <Image style={styles.messageItemImage}
                            source={{ uri: item.user.avatar }} />
                    </View>
                </View>
                <View style={styles.messageItemTextBox}>
                    <View style={styles.messageItemInfo}>
                        <Text style={styles.messageItemName}>{item.user.username}</Text>
                        <Text style={styles.messageItemDate}>{date}</Text>
                    </View>
                    <Text style={styles.messageItemText}>{item.message}</Text>
                </View>
            </View>
        )
    }
    render() {
        let sendButtonActivity = {
            opacity: this.state.animateVars.sendButtonActivity
            // opacity: 1
        }
        dimensions = this.props.dimensions ? this.props.dimensions : dimensions;
        return (
            <>
                <MaskedView style={[styles.messageListMask]}
                    maskElement={this.props.masked ?
                        <LinearGradient colors={['#FFFFFF00',
                            '#FFFFFF',
                            '#FFFFFF',
                            '#FFFFFF00']}
                            locations={[0, 0.2, 0.97, 1]}
                            style={styles.linearGradient}>
                        </LinearGradient> : null}>

                    <FlatList
                        // inverted
                        data={this.props.messages}
                        // inverted={true}
                        ref={ref => (this.FlatListRef = ref)}
                        // onContentSizeChange={this.scrollToEnd}
                        style={styles.messageList}
                        keyboardDismissMode="on-drag" Î
                        renderItem={this.renderMessage}
                        keyExtractor={item => item.id}
                        indicatorStyle={'white'}
                        contentContainerStyle={{ paddingTop: dimensions.height / 3, paddingBottom: 0, justifyContent: 'flex-end' }}
                    // contentContainerStyle={{ justifyContent: 'flex-end' }}
                    />
                </MaskedView>
                <View style={styles.chatBox_inputBox} onLayout={this._onOrientationDidChange}>
                    <TouchableOpacity onPress={this.props.leftButtonAction} style={[styles.btn, {
                        // width: 156,
                        // position: '',
                        // bottom: 16,
                        // left: 0,
                        // left: -108,
                        // justifyContent: 'flex-start',
                        // paddingHorizontal: 16
                    }]}>
                        <Icon name={'chevron-back-outline'} size={24} color={COLORS.black} />
                    </TouchableOpacity>
                    <TextInput
                        value={this.props.inputValue}
                        onFocus={this.onInputFocus}
                        onBlur={this.onInputBlur}
                        onChangeText={text => { this.onChangeText(text) }}
                        refInput={ref => { this.input = ref }}
                        style={[styles.chatBox_input]}
                        autoCorrect={true}
                        placeholderTextColor={COLORS.gray}
                        multiline={true}
                        placeholder={translate("screen.viewStream.typeMessage")}
                    />
                    <TouchableOpacity onPress={this.onSend} style={[styles.btn, { backgroundColor: COLORS.gray }]}>
                        <Animated.View style={[styles.innerSendBtnView, sendButtonActivity]}></Animated.View>
                        <Icon name={'send'} size={24} color={COLORS.white} />
                    </TouchableOpacity>
                </View>
            </>
        );
    }
}
const styles = StyleSheet.create({
    messageListMask: {
        flex: 1,
        zIndex: 20,
    },
    messageList: {
        marginBottom: 10,
        flex: 1
    },
    chatBox_inputBox: {
        paddingLeft: 16,
        paddingRight: 16,
        paddingBottom: 16,
        flexDirection: 'row',
        alignItems: 'flex-end',
        position: 'relative',
        zIndex: 200,
    },
    chatBox_input: {
        minHeight: 48,
        flexGrow: 1,
        marginLeft: 8,
        marginRight: 8,
        backgroundColor: COLORS.white,
        borderRadius: 12,
        elevation: 8,
        paddingHorizontal: 12,
        paddingTop: Platform.OS === 'android' ? 10 : 12,
        paddingBottom: Platform.OS === 'android' ? 10 : 12,
        lineHeight: 22,
        fontSize: 16,
        maxWidth: '100%',
        alignItems: 'center',
        flex: 1,
        textAlignVertical: 'center',
        color: COLORS.black
    },

    linearGradient: {
        flexGrow: 1,
        width: '100%',
        height: '100%',
        borderRadius: 5,
        top: 0,
        position: 'absolute'
    },
    btn: {
        width: 48,
        height: 48,
        backgroundColor: 'white',
        borderRadius: 12,
        elevation: 8,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 2,
        shadowColor: 'black',
        shadowOffset: {
            width: 0,
            height: 0
        },
        shadowOpacity: 0.1,
        shadowRadius: 4,
    },

    innerSendBtnView: {
        backgroundColor: COLORS.primary,
        width: '100%',
        height: '100%',
        position: 'absolute',
        borderRadius: 12,
    },
    messageItem: {
        marginHorizontal: 16,
        flexDirection: 'row',
        marginBottom: 10,
    },
    messageItemImageBox: {
        width: 42,
        height: 42,
        borderRadius: 56,
        margin: -4,
        overflow: 'hidden',
        borderWidth: 2,
        borderColor: '#ffffff00',
    },
    messageItemImageBox_inner: {
        borderRadius: 56,
        borderWidth: 2,
        borderColor: '#FFFFFF00',
    },
    messageItemImage: {
        backgroundColor: 'lightgray',
        borderRadius: 56,
        borderWidth: 0.5,
        borderColor: 'white',
        width: '100%',
        height: '100%'
    },
    messageItemTextBox: {
        marginLeft: 16,
        width: 0,
        flexGrow: 1,
        flex: 1,
    },
    messageItemInfo: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end'
    },
    messageItemName: {
        // color: '#ffffffdd',
        color: COLORS.primary,
        textShadowRadius: 40,
        fontWeight: 'bold',
        fontSize: 12
    },
    messageItemDate: {
        color: '#ffffff88',
        textShadowRadius: 4,
        fontWeight: 'bold',
        fontSize: 10
    },
    messageItemText: {
        color: 'white',
        textShadowRadius: 4,
        paddingRight: 32
    },
    hostMessage: {
        backgroundColor: 'red'
    }
});
