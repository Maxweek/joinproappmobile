import React, { Component, createRef } from "react";
import I18n from "i18n-js";
import {
    Text,
    View,
    StyleSheet,
    RefreshControl,
    Animated,
    Dimensions,
    StatusBar,
    SafeAreaView,
    KeyboardAvoidingView,
    TouchableOpacity,
    Keyboard,
    Alert,
    PermissionsAndroid
} from "react-native";

import RtcEngine, {
    ChannelProfile,
    ClientRole,
    RtcRemoteView,
    RtcLocalView,
    VideoDimensions,
    VideoEncoderConfiguration,
    VideoFrameRate,
    VideoRemoteState,
    VideoRenderMode
} from "react-native-agora";
import { Platform, NativeModules } from 'react-native';
import { translate } from "../App";
import Orientation from "react-native-orientation-locker";
import KeepAwake from "react-native-keep-awake";
import LoaderBox from "./loaderBox";
import LinearGradient from "react-native-linear-gradient";
import Icon from "react-native-vector-icons/Ionicons";
import ChatBox from "./chatBox";
import { COLORS } from "../assets/styles/styles";
import { color, or, set } from "react-native-reanimated";
import Fire from "../firebase";
import API, {
    API_IMPRESSION_STREAM_RECORDING_CHECK,
    API_IMPRESSION_STREAM_RECORDING_START,
    API_IMPRESSION_STREAM_RECORDING_STOP
} from "../API";

const { StatusBarManager } = NativeModules;

var dimensions = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
};

export default class StreamBox extends Component {
    constructor(props) {
        super(props);

        this.settings = {
            agoraId: '987fd1c2247346a082c28e84f03d5de6',
            videoEncoderConfiguration: {
                width: 1280,
                height: 720,
                bitrate: 15420,
                frameRate: VideoFrameRate.Fps60,
                minFrameRate: VideoFrameRate.Fps60,
                degradationPrefer: 2,
            },
            channelProfile: ChannelProfile.LiveBroadcasting,
            host: {
                clientRole: ClientRole.Broadcaster
            },
            view: {
                clientRole: ClientRole.Audience
            },

        }

        this.state = {
            user: 1,
            channel: this.props.channelId,
            broadcasterVideoState: VideoRemoteState.Decoding,

            userCount: 0,
            isPortrait: true,
            isJoined: false,
            isBroadcaster: false,
            isAudioActive: true,
            isChatActive: false,
            isStreamStarted: false,
            isStreamFinished: false,
            isSendButtonActive: false,
            isVideoStretched: true,

            isCameraSwitched: true,
            isMicVolumeUp: false,
            isVideoActive: true,

            dimensions: {
                width: Dimensions.get('window').width,
                height: Dimensions.get('window').height,
            },
            recording: {
                active: false,
                text: 'Waiting...',
                indicatorPulse: true,
                indicatorActive: true,
            },
            messages: [
            ]
        }
        this.animateVars = {
            chatVar: new Animated.Value(0),
            chatButtonVar: new Animated.Value(0),
            inputWidth: new Animated.Value(0),
            shadowOpacity: new Animated.Value(0),
            windowOpacity: new Animated.Value(0),
            iwindowOpacity: new Animated.Value(0),
            sendButtonActivity: new Animated.Value(0),
            windowWidthVar: new Animated.Value(0),
            windowHeightVar: new Animated.Value(0),
        };
        this.AgoraEngine = null;
        this.pollingRecordingCheck = null;
        this.isPolling = true;
        this.isPlatformIOS = Platform.OS === 'ios'
    }
    //component Default methods
    componentDidMount() {
        Orientation.unlockAllOrientations()
        Orientation.addOrientationListener(this._onOrientationDidChange);
        // Orientation.lockToPortrait()
        this.props.setAppTabBarVisibility(false)
        this.setOSRequest();
        this.setFirebase(true)
        this._checkOrientation()
        this.addListener();
    }
    addListener = () => {
        if (!this.state.isListenerAdded) {
            this.unsubscribe = this.props.navigation.addListener('focus', () => {
                // this.getData();
                this.isScreenFocused = true
                console.log('focus')
            });
            this.setState({ isListenerAdded: true })
        }
    }

    componentDidUpdate() {

    }

    componentWillUnmount() {
        this.getOut()
    }

    getOut = () => {
        this.isScreenFocused = false;
        Orientation.removeOrientationListener(this._onOrientationDidChange);
        Orientation.lockToPortrait()
        StatusBar.setHidden(false)

        console.log(this.AgoraEngine)

        this.AgoraEngine.current.leaveChannel()
        this.AgoraEngine.current.destroy();

        this.setFirebase(false)
        clearTimeout(this.pollingRecordingCheck);
        this.isPolling = false
    }
    // HELPERS
    _onOrientationDidChange = (orientation) => {
        console.log('this.isScreenFocused')
        console.log(this.isScreenFocused)
        if (!this.isScreenFocused) {
            return
        }

        dimensions = {
            screen: {
                width: Dimensions.get('screen').width,
                height: Dimensions.get('screen').height
            }, window: {
                width: Dimensions.get('window').width,
                height: Dimensions.get('window').height
            }
        }
        // console.log(orientation)
        if (orientation) {
            if (orientation === 'PORTRAIT') {
                orientation = true
            } else if (orientation === 'LANDSCAPE-LEFT' || orientation === 'LANDSCAPE-RIGHT') {
                orientation = false
            }
        } else {
            if (dimensions.window.width < dimensions.window.height) {
                orientation = true
            } else {
                orientation = false
            }
        }

        console.log('orientation: ' + orientation)

        this.setState({
            dimensions: {
                width: dimensions.screen.width,
                height: dimensions.screen.height,

                // width: dimensions.screen.width,
                // height: dimensions.screen.height,
            },
            isPortrait: orientation
        }, this._checkOrientation)

    }

    _checkOrientation = () => {

        if (this.state.isPortrait) {
            if (!this.isPlatformIOS) {
                StatusBar.setHidden(false)
            }
        } else {
            if (!this.isPlatformIOS) {
                StatusBar.setHidden(true)
            }
        }
        if (!this.props.isHost) {
            if (!this.isPlatformIOS) {
                StatusBar.setHidden(true)
            }
        }
        if (this.state.isChatActive) {
            this.openChat()
        } else {
            this.closeChat()
        }

        this.checkVideoStretch()
    }

    checkVideoStretch = () => {
        let width = this.state.dimensions.width;
        let height = this.state.dimensions.height;
        if (this.state.isPortrait) {
            if (this.state.isVideoStretched) {
                width = this.state.dimensions.width;
                height = this.state.dimensions.height;
            } else {
                width = this.state.dimensions.width;
                height = this.state.dimensions.width * 16 / 9;
            }
        } else {
            if (this.state.isVideoStretched) {
                width = this.state.dimensions.width;
                height = this.state.dimensions.height;
            } else {
                width = this.state.dimensions.height * 16 / 9;
                height = this.state.dimensions.height;
            }
        }

        Animated.spring(this.animateVars.windowWidthVar, {
            toValue: width,
            duration: 200,
            useNativeDriver: false
        }).start();

        Animated.spring(this.animateVars.windowHeightVar, {
            toValue: height,
            duration: 200,
            useNativeDriver: false
        }).start();
    }

    async onShare() {
        try {
            const result = await Share.share({ message: this.props.app.stream_view.impression.link });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            console.log(error.message);
        }
    }

    setUserCount(count) {
        if (this.state.userCount !== count) {
            this.setState({
                userCount: count
            })
        }
    }

    setJoined(type) {
        console.log('joined: ' + type)
        this.setState({
            isJoined: type
        })
    }

    setBroadcasterVideoState(data) {
        this.setState({
            broadcasterVideoState: data
        })
    }

    onInputFocus = () => {

    }

    goDonate = () => {
        this.getOut()
        this.props.navigation.navigate('donateScreen', {
            impression_id: this.props.app.stream_view.impression.id,
            impression: this.props.app.stream_view.impression,
        })
    }
    goExit = () => {
        if (this.props.isHost) {

            Alert.alert(
                translate('screen.hostStream.exit'),
                translate('screen.hostStream.exitNotify'),
                [
                    { text: translate('screen.createImpression.confirm.cancel'), style: "cancel" },
                    { text: translate('screen.createImpression.confirm.gotIt'), onPress: () => this.stopStream() }
                ]
            );
        } else {
            console.log("DONATE: " + this.props.app.stream_view.impression.donate)

            var go = () => {
                if (!this.props.isHost && this.props.app.stream_view.impression.donate && Platform.OS !== 'ios') {
                    this.goDonate()
                } else {
                    // this.props.navigation.popToTop()
                    this.props.navigation.navigate('mainScreen', {
                    });
                }
                this.getOut()
                // this.props.setAppTabBarVisibility(true)
            }
            Alert.alert(
                translate("screen.viewStream.exit"),
                "",
                [
                    { text: translate('screen.createImpression.confirm.cancel'), style: "cancel" },
                    { text: translate('screen.createImpression.confirm.gotIt'), onPress: () => go() }
                ]
            );

        }
    }
    async requestCameraAndAudioPermission() {
        try {
            const granted = await PermissionsAndroid.requestMultiple([
                PermissionsAndroid.PERMISSIONS.CAMERA,
                PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
            ]);
            if (
                granted['android.permission.RECORD_AUDIO'] ===
                PermissionsAndroid.RESULTS.GRANTED &&
                granted['android.permission.CAMERA'] ===
                PermissionsAndroid.RESULTS.GRANTED
            ) {
                console.log('You can use the cameras & mic');
            } else {
                console.log('Permission denied');
            }
        } catch (err) {
            console.warn(err);
        }
    }

    // STREAM
    setOSRequest() {
        if (!this.isPlatformIOS) {
            this.requestCameraAndAudioPermission()
                .then(() => {
                    this.setStream()
                });
        } else {
            this.setStream()
        }
    }

    async initStream() {
        console.log('setStream')
        console.log(this.state.isJoined)

        this.AgoraEngine = createRef()
        this.AgoraEngine.current = await RtcEngine.create(this.settings.agoraId);
        this.AgoraEngine.current.setVideoEncoderConfiguration(this.settings.videoEncoderConfiguration)
        this.AgoraEngine.current.enableVideo(true);
        this.AgoraEngine.current.setChannelProfile(this.settings.channelProfile);

        if (this.props.isHost) {
            this.AgoraEngine.current.setClientRole(this.settings.host.clientRole);
        } else {
            this.AgoraEngine.current.setClientRole(this.settings.view.clientRole);
        }

        this.AgoraEngine.current.enableAudioVolumeIndication(100, 3, true)
        this.AgoraEngine.current.addListener('RemoteVideoStateChanged', (uid, state) => {
            if (uid === 1) this.setBroadcasterVideoState(state);
        });

        console.log(this.AgoraEngine.current)

        this.AgoraEngine.current.addListener(
            'JoinChannelSuccess',
            (channel, uid, elapsed) => {
                console.log('JoinChannelSuccess', channel, uid, elapsed);
                this.setJoined(true);
            },
        );
        this.AgoraEngine.current.addListener(
            'VideoSizeChanged',
            (stats, width, height, rot) => {
                console.log({ stats, width, height, rot });
            },
        );
    }

    setStream = () => {
        const uid = this.props.isHost ? this.state.user : 3;
        this.initStream().then(() => {
            this.AgoraEngine.current.joinChannel(
                null,
                this.state.channel,
                null,
                uid,
            );
            if (this.props.isHost) {
                this.setRecording();
                this.firebase.start()
            }
        });
    }

    startStream = () => {
        this.setState({
            isStreamFinished: true,
        })

    }

    finishStream = () => {
        this.setState({
            isStreamStarted: true,
            isStreamFinished: true,
        })
    }

    stopStream() {
        // this.props.setAppTabBarVisibility(true)
        // this.props.navigation.navigate('profileScreen');
        clearTimeout(this.pollingRecordingCheck);

        this.getOut();

        const formData = new FormData();

        console.log(this.state.channel)

        formData.append('channel_id', this.state.channel);

        API.post(API_IMPRESSION_STREAM_RECORDING_STOP, formData)
            .then(res => {
                console.log(res)
                if (typeof res.data.status !== "undefined") {
                    if (res.data.status === 'success') {
                        this.setState({ recording: { ...this.state.recording, active: false, text: 'Over' } })
                    }
                    if (res.data.status === 'error') {
                        this.setState({ recording: { ...this.state.recording, active: false, text: 'Over' } })
                    }
                } else {
                    this.setState({ recording: { ...this.state.recording, active: false, text: 'Over' } })
                }

                this.props.navigation.navigate('profileScreenStack');
            }).catch(res => {
                console.log(res.data)
                this.setState({ recording: { ...this.state.recording, active: false, text: 'New err' } })
            })


        this.firebase.stop()
        // this.setIndicator(false)
    }

    // RECORDING & POLLING
    // setIndicator = (type = null) => {
    //     if (type) {
    //         this.setState({ recording: { ...this.state.recording, indicatorPulse: true } })
    //     } else {
    //         this.setState({ recording: { ...this.state.recording, indicatorPulse: false } })
    //     }
    // }

    setRecording = () => {

        const formData = new FormData();

        formData.append('channel_id', this.state.channel);

        API.post(API_IMPRESSION_STREAM_RECORDING_START, formData)
            .then(res => {
                console.log(res.data)
                if (typeof res.data.status !== "undefined") {
                    if (res.data.status === 'success') {
                        console.log('success')
                        this.setState({ recording: { ...this.state.recording, active: true, text: 'REC' } })
                        // this.setIndicator(true)
                    }
                    if (res.data.status === 'error') {
                        this.setState({ recording: { ...this.state.recording, active: false, text: 'Err' } })
                        // this.setIndicator(false)
                    }
                } else {
                    this.setState({ recording: { ...this.state.recording, active: false, text: 'Err' } })
                    // this.setIndicator(false)
                }
                this.setPollingRecordingStreamCheck();
            }).catch(res => {
                console.log("TEST RECORDING START")
                console.log(res)
                this.setState({ recording: { ...this.state.recording, active: false, text: 'Net err' } })
                // this.setIndicator(false)
            })

    }

    setPollingRecordingStreamCheck = (type = true) => {
        this.checkRecordingStream();
        console.log('CHECKED')
        this.pollingRecordingCheck = setTimeout(() => {
            if (this.isPolling) {
                this.setPollingRecordingStreamCheck();
            }
        }, 5000)
    }
    checkRecordingStream = () => {

        const formData = new FormData();

        formData.append('channel_id', this.state.channel);

        //console.log('STREAM CHECK')
        API.post(API_IMPRESSION_STREAM_RECORDING_CHECK, formData)
            .then(res => {
                //console.log(res.data)
                if (typeof res.data.status !== "undefined") {
                    if (res.data.status === 'success') {
                        // console.log('STREAM CHECK SUCCESS')
                        // console.log(res.data)
                        if (!this.state.recording.active) {

                            this.setState({ recording: { ...this.state.recording, active: true, text: 'REC' } })
                        }
                    }
                    if (res.data.status === 'error') {
                        this.setState({ recording: { ...this.state.recording, active: false, text: 'Err' } })
                        // this.setIndicator(false)
                    }
                } else {
                    this.setState({ recording: { ...this.state.recording, active: false, text: 'Err' } })
                    // this.setIndicator(false)
                }
            }).catch(res => {
                console.log(res.data)
                this.setState({ recording: { ...this.state.recording, active: false, text: 'Net err' } })
                // this.setIndicator(false)
            })
    }

    // FIREBASE
    setFirebase = (type) => {
        if (type) {

            this.firebase = new Fire(this.state.channel);
            this.firebase.onAdd(snapshotBox => {
                console.log(snapshotBox)
                let snapshot = snapshotBox.val();
                if (snapshotBox.val().count !== undefined) {
                    this.setUserCount(snapshotBox.val().count)
                    return
                }
                if (snapshotBox.val().status !== undefined) {
                    if (snapshotBox.val().status === 'on') {
                        this.startStream()
                    }
                    if (snapshotBox.val().status === 'off') {
                        this.finishStream()
                    }
                    return
                }
                let mess = [...this.state.messages];
                let nextId = this.state.messages.length ? this.state.messages[this.state.messages.length - 1].id + 1 : 0;
                mess.push(
                    {
                        id: nextId,
                        message: snapshot.message,
                        timestamp: snapshot.timestamp ? new Date(snapshot.timestamp) : new Date(),
                        user: {
                            id: snapshot.user.id,
                            type: snapshot.user ? snapshot.user.type : '',
                            avatar: snapshot.user.avatar,
                            username: snapshot.user.username,
                        }
                    }
                )
                this.setState({
                    messages: mess
                }, () => {
                    // console.log(this.state.messages)
                })
                // console.log(message)
                // this.setState(previousState => ({
                //     messages: GiftedChat.append(previousState.messages, message),
                // })
            });
            this.firebase.onChange(snapshotBox => {
                console.log(snapshotBox)
                console.log(snapshotBox.val())
                if (snapshotBox.val().count !== undefined) {
                    this.setUserCount(snapshotBox.val().count)
                }
            })
        } else {
            this.firebase.off()

        }
    }

    // CHAT
    openChat = () => {
        Animated.spring(this.animateVars.chatVar, {
            toValue: 0,
            duration: 200,
            useNativeDriver: true
        }).start();
        Animated.timing(this.animateVars.shadowOpacity, {
            toValue: 1,
            duration: 400,
            useNativeDriver: true
        }).start();
        Animated.timing(this.animateVars.windowOpacity, {
            toValue: 0,
            duration: 100,
            useNativeDriver: true
        }).start();
        Animated.timing(this.animateVars.iwindowOpacity, {
            toValue: 1,
            duration: 100,
            useNativeDriver: true
        }).start();
        Animated.spring(this.animateVars.chatButtonVar, {
            toValue: -this.state.dimensions.width,
            duration: 200,
            useNativeDriver: true
        }).start();
    }
    closeChat = () => {
        console.log('width: ' + this.state.dimensions.width)
        Animated.spring(this.animateVars.chatVar, {
            toValue: this.state.dimensions.width,
            duration: 200,
            useNativeDriver: true
        }).start();
        Animated.timing(this.animateVars.shadowOpacity, {
            toValue: 0,
            duration: 400,
            useNativeDriver: true
        }).start();
        Animated.timing(this.animateVars.windowOpacity, {
            toValue: 1,
            duration: 100,
            useNativeDriver: true
        }).start();
        Animated.timing(this.animateVars.iwindowOpacity, {
            toValue: 0,
            duration: 100,
            useNativeDriver: true
        }).start();
        Animated.spring(this.animateVars.chatButtonVar, {
            toValue: 0,
            duration: 200,
            useNativeDriver: true
        }).start();

        Keyboard.dismiss()
    }
    toggleChat = (type = true) => {
        if (!this.state.isChatActive) {
            if (type) {
                this.setState({ isChatActive: true })
            }
            this.openChat()
        } else {
            if (type) {
                this.setState({ isChatActive: false })
            }
            this.closeChat()
        }
    }

    setChatInputText = (text) => {
        this.props.setChatInputText(text)
    }

    onMessageSend = () => {
        if (this.props.app.local.chatInputText.length) {
            this.firebase.append({
                message: this.props.app.local.chatInputText,
                timestamp: this.firebase.timestamp,
                user: {
                    avatar: this.props.app.user.info.photo !== '' ? this.props.app.user.info.photo : 'https://joinpro.ru/upload/uf/eb8/eb8dd1868ddabedb66fe28923e0052f7.png',
                    username: this.props.app.user.info.name
                }
            })
            this.props.setChatInputText('');
        }
    }

    // CONTROLS
    onSwitchAudio = () => {
        if (!this.state.isAudioActive) {
            this.setState({
                isAudioActive: true
            })
            this.AgoraEngine.current.muteAllRemoteAudioStreams(false);
            this.AgoraEngine.current.muteLocalAudioStream(false);
        } else {
            this.setState({
                isAudioActive: false
            })
            this.AgoraEngine.current.muteAllRemoteAudioStreams(true);
            this.AgoraEngine.current.muteLocalAudioStream(true);
        }
    }
    onVideoStretch = () => {
        if (!this.state.isVideoStretched) {
            this.setState({
                isVideoStretched: true
            }, this.checkVideoStretch)
        } else {
            this.setState({
                isVideoStretched: false
            }, this.checkVideoStretch)
        }
    }
    onSwitchVideo = () => {
        if (!this.state.isVideoActive) {
            this.setState({
                isVideoActive: true
            })
            this.AgoraEngine.current.muteLocalVideoStream(false);
        } else {
            this.setState({
                isVideoActive: false
            })
            this.AgoraEngine.current.muteLocalVideoStream(true);
        }
    }
    onSwitchCamera = () => {
        if (!this.state.isCameraSwitched) {
            this.setState({
                isCameraSwitched: true
            })
        } else {
            this.setState({
                isCameraSwitched: false
            })
        }
        this.AgoraEngine.current.switchCamera();
    }
    onMicVolumeUp = () => {
        if (!this.state.isMicVolumeUp) {
            this.setState({
                isMicVolumeUp: true
            })
            this.AgoraEngine.current.adjustRecordingSignalVolume(400)
        } else {
            this.setState({
                isMicVolumeUp: false
            })
            this.AgoraEngine.current.adjustRecordingSignalVolume(100)
        }
    }

    getButton = (sets) => {
        let icon = null
        let text = null
        let textStyles = { marginRight: 12 }

        if (sets.icon) {
            icon = <Icon name={sets.icon.name} size={24} color={sets.icon.color} />
        }

        if (sets.text) {
            text = <Text style={[textStyles, sets.text.style]}>{sets.text.value}</Text>
            if (sets.text.align !== 'left') {
                textStyles = {
                    marginLeft: 12
                }
            }
        }

        return <TouchableOpacity
            style={sets.style}
            onPress={sets.onPress}
        >
            {sets.text ?
                sets.text.align = 'left' ?
                    <>
                        {text}
                        {icon}
                    </>
                    :
                    <>
                        {icon}
                        {text}
                    </>
                : icon}
        </TouchableOpacity>
    }

    render() {
        let liveIndicatorStyles = { backgroundColor: this.state.recording.active ? COLORS.primary : COLORS.red }
        let chatOffset = { transform: [{ translateX: this.animateVars.chatVar }] }
        let chatAvoid = { transform: [{ translateX: this.animateVars.chatButtonVar }] }

        let shadowOpacity = { opacity: this.animateVars.shadowOpacity }
        let windowOpacity = { opacity: this.animateVars.windowOpacity }
        // let iwindowOpacity = { opacity: this.animateVars.iwindowOpacity }
        let iwindowOpacity = { opacity: 1 }

        let fullscreenBoxWide = {
            width: this.animateVars.windowWidthVar,
            height: this.animateVars.windowHeightVar
        }
        let androidPaddingTopByStatusBar = {
            paddingTop: this.state.isPortrait ? !this.isPlatformIOS ? StatusBarManager.HEIGHT : 0 : 0
        }
        // if (!this.state.isPortrait) {
        //     fullscreenBoxWide = {
        //         height: Dimensions.get('window').height,
        //         width: Dimensions.get('window').height * 16 / 9
        //     }
        // }
        // if (this.state.isVideoStretched) {
        //     fullscreenBoxWide = {
        //         width: '100%',
        //         height: '100%'
        //     }
        // }
        console.log('isPortait: ' + StatusBarManager.HEIGHT)
        return (
            <>
                {!this.state.isJoined ? (
                    <View style={styles.loadingContainer}>
                        <LoaderBox text={translate("screen.viewStream.joinWait")}></LoaderBox>
                    </View>
                ) : (
                    <>
                        <KeepAwake />
                        <StatusBar barStyle={'light-content'} />
                        <View style={styles.streamBox} onLayout={e => { !this.isPlatformIOS ? this._onOrientationDidChange() : false }}>
                            <View style={styles.streamBox_fullscreen}>
                                <Animated.View style={fullscreenBoxWide}>

                                    {/* <View style={fullscreenBoxWide}></View> */}
                                    {this.props.isHost ?
                                        <RtcLocalView.SurfaceView
                                            style={{ width: '100%', height: '100%' }}
                                            channelId={this.state.channel}
                                            renderMode={VideoRenderMode.Hidden}
                                        />
                                        :
                                        <RtcRemoteView.SurfaceView
                                            style={{ width: '100%', height: '100%' }}
                                            uid={1}
                                            channelId={this.state.channel}
                                        />
                                    }
                                </Animated.View>
                            </View>
                            <Animated.View style={[{
                                position: 'absolute',
                                width: '100%',
                                height: '100%',
                            }, shadowOpacity]}>
                                <LinearGradient colors={['#00000000', '#00000044', '#00000066', '#00000088', '#000000aa', '#000000cc', '#000000fa',]} style={[styles.linearGradientBack]}>
                                </LinearGradient>
                            </Animated.View>
                            <LinearGradient colors={['#000000aa', '#00000000',]} style={styles.topGrad}>
                            </LinearGradient>
                            <LinearGradient colors={['#00000000', '#000000aa']} style={styles.botGrad}>
                            </LinearGradient>
                        </View>
                        <SafeAreaView style={[styles.streamBox_safetyView, androidPaddingTopByStatusBar]}
                        // onLayout={() => { console.log('YAW'); this._onOrientationDidChange() }}
                        >
                            <KeyboardAvoidingView style={styles.streamBox_keyboardAvoidingView} behavior={"padding"}>
                                <Animated.View style={[styles.animatedWrapper, windowOpacity, chatAvoid]}>
                                    <Animated.View style={styles.topLeft}>
                                        <View style={styles.userCountContainer}>
                                            <Icon name={'eye'} size={16} color={'white'} />
                                            <Text style={styles.userCountText}>{this.state.userCount}</Text>
                                        </View>
                                        {this.props.isHost ? <View style={[styles.recordingContainer, { marginLeft: 4 }]}>
                                            <Animated.View style={[liveIndicatorStyles, styles.recordingContainerIndicator]}></Animated.View>
                                            <Text style={styles.recordingContainerText}>{this.state.recording.text}</Text>
                                        </View> : null}
                                    </Animated.View>
                                    <Animated.View style={styles.topRight}>

                                        {this.props.isHost ? this.getButton({
                                            style: [styles.btn, { marginRight: 4 }],
                                            onPress: this.onVideoStretch,
                                            icon: {
                                                name: this.state.isVideoStretched ? "crop-outline" : "expand-outline",
                                                color: COLORS.black,
                                            }
                                        }) : null}
                                        {!this.props.isHost && this.props.app.stream_view.impression.donate && Platform.OS !== 'ios' ?
                                            this.getButton({
                                                style: [styles.btn, { marginRight: 4, backgroundColor: COLORS.primary }],
                                                onPress: this.goDonate,
                                                icon: {
                                                    name: 'heart',
                                                    color: COLORS.white,
                                                }
                                            })
                                            : null}
                                        {this.getButton({
                                            style: [styles.btn],
                                            onPress: this.goExit,
                                            icon: {
                                                name: 'exit-outline',
                                                color: COLORS.black,
                                            }
                                        })}
                                    </Animated.View>
                                    <Animated.View style={styles.bottomLeft}>
                                        {this.getButton({
                                            style: [styles.btn, { backgroundColor: !this.state.isAudioActive ? COLORS.red : 'white' }],
                                            onPress: this.onSwitchAudio,
                                            icon: {
                                                name: this.props.isHost ? "mic-outline" : this.state.isAudioActive ? "volume-high-outline" : "volume-mute-outline",
                                                color: COLORS.black,
                                            }
                                        })}
                                        {!this.props.isHost ?
                                            this.getButton({
                                                style: [styles.btn, { marginLeft: 4 }],
                                                onPress: this.onVideoStretch,

                                                icon: {
                                                    name: this.state.isVideoStretched ? "crop-outline" : "expand-outline",
                                                    color: COLORS.black,
                                                }
                                            }) : null}
                                        {this.props.isHost ? <>
                                            {this.getButton({
                                                style: [styles.btn, { marginLeft: 4, backgroundColor: this.state.isMicVolumeUp ? COLORS.primary : 'white' }],
                                                onPress: this.onMicVolumeUp,
                                                icon: {
                                                    name: "mic-circle-outline",
                                                    color: COLORS.black,
                                                }
                                            })}
                                            {this.getButton({
                                                style: [styles.btn, { marginLeft: 4, backgroundColor: !this.state.isVideoActive ? COLORS.red : 'white' }],
                                                onPress: this.onSwitchVideo,
                                                icon: {
                                                    // name: this.state.isVideoActive ? "videocam-outline" : "videocam-off-outline",
                                                    name: "videocam-outline",
                                                    color: COLORS.black,
                                                }
                                            })}
                                            {this.getButton({
                                                style: [styles.btn, { marginLeft: 4 }],
                                                onPress: this.onSwitchCamera,
                                                icon: {
                                                    name: this.state.isCameraSwitched ? "camera-reverse-outline" : "camera-reverse",
                                                    color: COLORS.black,
                                                }
                                            })}
                                        </> : null}
                                    </Animated.View>
                                    <Animated.View style={styles.bottomRight}>
                                        {this.getButton({
                                            style: [styles.btn, { width: 96 }],
                                            onPress: this.toggleChat,
                                            text: {
                                                style: { marginRight: 12, opacity: this.state.isChatActive ? 0 : 1 },
                                                value: translate("screen.viewStream.openChat"),
                                                align: 'left'
                                            },
                                            icon: {
                                                name: this.state.isChatActive ? 'chevron-back-outline' : 'chatbubbles-outline',
                                                color: COLORS.black,
                                            }
                                        })}
                                    </Animated.View>
                                </Animated.View>

                                <Animated.View style={[styles.animatedWrapper, chatOffset, iwindowOpacity]}>
                                    <ChatBox
                                        masked={true}
                                        messages={this.state.messages}
                                        inputValue={this.props.app.local.chatInputText}
                                        onInputFocus={this.onInputFocus}
                                        onChangeText={this.setChatInputText}
                                        onSend={this.onMessageSend}
                                        dimensions={this.state.dimensions}
                                        leftButtonAction={this.toggleChat}
                                    ></ChatBox>
                                </Animated.View>
                            </KeyboardAvoidingView>
                        </SafeAreaView>
                    </>
                )
                }
            </>
        );
    }
}
const styles = StyleSheet.create({
    loadingContainer: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
    },
    streamBox: {
        height: '100%',
        width: '100%',
        backgroundColor: '#f5f5f5',
        position: 'absolute',
    },
    streamBox_fullscreen: {
        width: '100%',
        height: '100%',
        position: 'relative',
        alignItems: 'center',
        justifyContent: 'center',
    },
    streamBox_safetyView: {
        flex: 1,
        borderColor: "gray",
        position: 'relative',
    },
    streamBox_keyboardAvoidingView: {
        // overflow: 'hidden',
        flex: 1,
        position: 'relative'
    },
    animatedWrapper: {
        flex: 1,
        height: '100%',
        width: '100%',
        position: 'absolute'
    },
    topGrad: {
        position: 'absolute',
        top: 0,
        height: StatusBarManager.HEIGHT,
        width: '100%'
    },
    botGrad: {
        position: 'absolute',
        bottom: 0,
        height: StatusBarManager.HEIGHT - 10,
        width: '100%'
    },
    btn: {
        width: 48,
        height: 48,
        backgroundColor: 'white',
        borderRadius: 12,
        elevation: 8,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        elevation: 2,
        shadowColor: 'black',
        shadowOffset: {
            width: 0,
            height: 0
        },
        shadowOpacity: 0.1,
        shadowRadius: 4,
    },
    donate: {
        backgroundColor: COLORS.primary,
        marginRight: 4,
    },
    topLeft: {
        position: 'absolute',
        top: 16,
        left: 16,
        flexDirection: 'row',
        // backgroundColor: 'green',
        zIndex: 120
    },
    topRight: {
        position: 'absolute',
        top: 16,
        right: 16,
        flexDirection: 'row',
        // backgroundColor: 'green',
        zIndex: 120
    },
    bottomLeft: {
        position: 'absolute',
        bottom: 16,
        left: 16,
        flexDirection: 'row',
        zIndex: 20
    },
    bottomRight: {
        position: 'absolute',
        bottom: 16,
        right: 16,
        flexDirection: 'row',
        height: 48,
        zIndex: 20
    },
    linearGradientBack: {
        position: 'absolute',
        width: '100%',
        height: '100%',
    },
    userCountContainer: {
        flexDirection: 'row',
        zIndex: 20,
        backgroundColor: '#00000044',
        paddingVertical: 4,
        paddingHorizontal: 8,
        borderRadius: 8,
        alignItems: 'center',
        // marginHorizontal: 10,
        color: '#fff',
    },
    userCountText: {
        fontSize: 14,
        marginLeft: 6,
        color: '#fff'
    },
    recordingContainer: {
        flexDirection: 'row',
        // position: 'absolute',
        // top: 90,
        // width: 100,
        // right: 16,
        backgroundColor: '#00000044',
        paddingVertical: 4,
        paddingHorizontal: 8,
        borderRadius: 8,
        justifyContent: 'flex-end',
        alignItems: 'center',
        // marginHorizontal: 10,
        color: '#fff',
    },
    recordingContainerIndicator: {
        width: 8,
        height: 8,
        borderRadius: 8,
        marginRight: 8
    },
    recordingContainerText: {
        color: '#fff',
        fontSize: 14,
    },
});
