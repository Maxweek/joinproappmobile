import { toHumanSize } from "i18n-js";
import React, { Component } from "react";
import {
    Text,
    View,
    StyleSheet,
    Dimensions,
} from "react-native";
import { CalendarList, LocaleConfig } from "react-native-calendars";

import Icon from "react-native-vector-icons/Ionicons";
import { translate } from "../App";
import { COLORS } from "../assets/styles/styles";

export default class CalendarBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            calendar: {}
        }
        this.curDate = {
            dateObject: new Date()
        };
        this.curDate.formatted = this.curDate.dateObject.getFullYear() + '-' + ((this.curDate.dateObject.getMonth() + 1).toString().padStart(2, '0')) + '-' + ((this.curDate.dateObject.getDate() + 1).toString().padStart(2, '0'))
        this.curDate.minDate = this.curDate.dateObject.getFullYear() + '-' + ((this.curDate.dateObject.getMonth() + 1).toString().padStart(2, '0')) + '-' + ((this.curDate.dateObject.getDate()).toString().padStart(2, '0'))

        LocaleConfig.locales['ru'] = {
            monthNames: translate("datetime.monthNames"),
            monthNamesShort: translate("datetime.monthNamesShort"),
            dayNames: translate("datetime.dayNames"),
            dayNamesShort: translate("datetime.dayNamesShort"),
            today: translate("datetime.today"),
        };
        LocaleConfig.defaultLocale = 'ru';
    }
    componentDidMount() {
        // Keyboard
    }
    setNewDaySelected = date => {
        const selectDate = {};
        selectDate[date.dateString] = {
            selected: true,
        };
        this.setState({ calendar: { markedDates: selectDate, selectDate: date } }, () => {
            this.props.onDaySelect(date)
        });
    };
    render() {
        let maxDate = new Date();
        maxDate.setDate(maxDate.getDate() + 90);
        maxDate = maxDate.getFullYear() + '-' + ((maxDate.getMonth() + 1).toString().padStart(2, '0')) + '-' + ((maxDate.getDate() + 1).toString().padStart(2, '0'));
        return (
            <CalendarList
                horizontal={true}
                scrollEnabled={false}
                pagingEnabled={true}
                calendarWidth={Dimensions.get('window').width}
                current={this.curDate.formatted}
                markedDates={this.state.calendar.markedDates}
                minDate={this.curDate.minDate}
                maxDate={maxDate}
                onDayPress={this.setNewDaySelected}
                onDayLongPress={(day) => { console.log('selected day', day) }}
                onMonthChange={(month) => { console.log('month changed', month) }}
                hideArrows={false}
                renderArrow={(direction) => (direction === 'left' ? <Icon name={'chevron-back-outline'} size={24} color={'#14ccb4'} /> : <Icon name={'chevron-forward-outline'} size={24} color={'#14ccb4'} />)}
                hideExtraDays={false}
                hideDayNames={false}
                firstDay={1}
                onPressArrowLeft={subtractMonth => subtractMonth()}
                onPressArrowRight={addMonth => addMonth()}
                disableAllTouchEventsForDisabledDays={false}
                enableSwipeMonths={true}
                // style={{ height: 400 }}
                pastScrollRange={0}
                futureScrollRange={0}
                renderHeader={date => {
                    const header = date.toString('MMMM yyyy');
                    const [month, year] = header.split(' ');
                    const textStyle = {
                        fontSize: 18,
                        fontWeight: 'bold',
                        paddingTop: 10,
                        paddingBottom: 10,
                        color: '#2a2d43',
                        paddingRight: 5
                    };

                    return (
                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                marginTop: 10,
                                marginBottom: 10
                            }}
                        >
                            <Text style={{ marginLeft: 5, ...textStyle }}>{`${month}`}</Text>
                            <Text style={{ marginRight: 5, ...textStyle }}>{year}</Text>
                        </View>
                    );
                }}
                theme={{
                    backgroundColor: '#000000',
                    calendarBackground: '#ffffff',
                    textSectionTitleColor: '#b6c1cd',
                    textSectionTitleDisabledColor: '#d9e1e8',
                    selectedDayBackgroundColor: '#14ccb4',
                    selectedDayTextColor: 'white',
                    todayTextColor: '#14ccb4',
                    // dayTextColor: 'yellow',
                    textDisabledColor: '#d9e1e8',
                    dotColor: '#00adf5',
                    selectedDotColor: '#ffffff',
                    arrowColor: 'orange',
                    disabledArrowColor: '#d9e1e8',
                    monthTextColor: 'blue',
                    indicatorColor: 'blue',
                    textDayFontWeight: '300',
                    textMonthFontWeight: 'bold',
                    textDayHeaderFontWeight: '300',
                    textDayFontSize: 16,
                    textMonthFontSize: 16,
                    textDayHeaderFontSize: 16
                }}

            />
        );
    }
}
const styles = StyleSheet.create({
});
