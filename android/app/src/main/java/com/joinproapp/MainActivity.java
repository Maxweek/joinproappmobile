package com.joinproapp;

import com.facebook.react.ReactActivity;

// modalize geture add
import com.facebook.react.ReactActivityDelegate;
import com.facebook.react.ReactRootView;
import com.swmansion.gesturehandler.react.RNGestureHandlerEnabledRootView;

// orientation locker add
import android.content.Intent;
import android.content.res.Configuration;
import org.wonday.orientation.OrientationActivityLifecycle;

public class MainActivity extends ReactActivity {

  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */
  @Override
  protected String getMainComponentName() {
    return "joinProApp";
  }

  /** orientation locker add */
  @Override
  protected ReactActivityDelegate createReactActivityDelegate() {
    return new ReactActivityDelegate(this, getMainComponentName()) {
      @Override
      protected ReactRootView createRootView() {
       return new RNGestureHandlerEnabledRootView(MainActivity.this);
      }
    };
  }

  /** orientation locker add */
  @Override
  public void onConfigurationChanged(Configuration newConfig) {
    super.onConfigurationChanged(newConfig);
    Intent intent = new Intent("onConfigurationChanged");
    intent.putExtra("newConfig", newConfig);
    this.sendBroadcast(intent);
  }
  /** 
  @Override
  public void onCreate() {
    super.onCreate();
    registerActivityLifecycleCallbacks(OrientationActivityLifecycle.getInstance());
  }
  */
}
