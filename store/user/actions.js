export const SET_PUSH_TOKEN = 'SET_PUSH_TOKEN';
export const SET_APP_LOGGED = 'SET_APP_LOGGED';
export const SET_APP_LOADING = 'SET_APP_LOADING';
export const SET_USER_LANG = 'SET_USER_LANG';
export const SET_USER_USERNAME = 'SET_USER_USERNAME';
export const SET_USER_PHONE = 'SET_USER_PHONE';
export const SET_USER_PASSWORD = 'SET_USER_PASSWORD';
export const SET_USER_EMAIL = 'SET_USER_EMAIL';
export const SET_USER_IMAGE = 'SET_USER_IMAGE';
export const SET_USER_NAME = 'SET_USER_NAME';
export const SET_USER_LAST_NAME = 'SET_USER_LAST_NAME';
export const SET_USER_BIRTHDAY = 'SET_USER_BIRTHDAY';
export const SET_USER_CITY = 'SET_USER_CITY';
export const SET_USER_GENDER = 'SET_USER_GENDER';
export const SET_USER_INFO = 'SET_USER_INFO';
export const SET_NEW_IMPRESSION_NAME = 'SET_NEW_IMPRESSION_NAME';
export const SET_NEW_IMPRESSION_DESCRIPTION = 'SET_NEW_IMPRESSION_DESCRIPTION';
export const SET_NEW_IMPRESSION_ID = 'SET_NEW_IMPRESSION_ID';
export const SET_NEW_IMPRESSION_IS_NEW = 'SET_NEW_IMPRESSION_IS_NEW';
export const SET_NEW_IMPRESSION_TYPE_ID = 'SET_NEW_IMPRESSION_TYPE_ID';
export const SET_NEW_IMPRESSION_IMAGE = 'SET_NEW_IMPRESSION_IMAGE';
export const SET_NEW_IMPRESSION_SHEDULE_DURATION_DAYS = 'SET_NEW_IMPRESSION_SHEDULE_DURATION_DAYS';
export const SET_NEW_IMPRESSION_SHEDULE_DURATION_HOURS = 'SET_NEW_IMPRESSION_SHEDULE_DURATION_HOURS';
export const SET_NEW_IMPRESSION_SHEDULE_DURATION_MINUTES = 'SET_NEW_IMPRESSION_SHEDULE_DURATION_MINUTES';
export const SET_NEW_IMPRESSION_SHEDULE_DURATION_SUMMARY = 'SET_NEW_IMPRESSION_SHEDULE_DURATION_SUMMARY';
export const SET_NEW_IMPRESSION_SHEDULE_DATE_ADD = 'SET_NEW_IMPRESSION_SHEDULE_DATE_ADD';
export const SET_NEW_IMPRESSION_SHEDULE_DATE_REMOVE = 'SET_NEW_IMPRESSION_SHEDULE_DATE_REMOVE';
export const SET_NEW_IMPRESSION_CLEAR = 'SET_NEW_IMPRESSION_CLEAR';
export const SET_IMPRESSION_STREAM = 'SET_IMPRESSION_STREAM';
export const SET_IMPRESSION_STREAM_VIEW = 'SET_IMPRESSION_STREAM_VIEW';
export const SET_IMPRESSION_LIST_OWN = 'SET_IMPRESSION_LIST_OWN';
export const SET_IMPRESSION_LIST = 'SET_IMPRESSION_LIST';
export const SET_IMPRESSION_CURRENT = 'SET_IMPRESSION_CURRENT';
export const SET_APP_TABBAR_VISIBILITY = 'SET_APP_TABBAR_VISIBILITY';
export const SET_APP_DEVICE_ID = 'SET_APP_DEVICE_ID';
export const SET_CHAT_INPUT_TEXT = 'SET_CHAT_INPUT_TEXT';

export const setPushToken = data => ({
    type: SET_PUSH_TOKEN,
    payload: data
});
export const setAppLogged = data => ({
    type: SET_APP_LOGGED,
    payload: data
});
export const setAppLoading = data => ({
    type: SET_APP_LOADING,
    payload: data
});
export const setUserLang = data => ({
    type: SET_USER_LANG,
    payload: data
})
export const setUserUsername = data => ({
    type: SET_USER_USERNAME,
    payload: data
})
export const setUserEmail = data => ({
    type: SET_USER_EMAIL,
    payload: data
})
export const setUserPhone = data => ({
    type: SET_USER_PHONE,
    payload: data
})
export const setUserName = data => ({
    type: SET_USER_NAME,
    payload: data
})
export const setUserLastName = data => ({
    type: SET_USER_LAST_NAME,
    payload: data
})
export const setUserBirthday = data => ({
    type: SET_USER_BIRTHDAY,
    payload: data
})
export const setUserImage = data => ({
    type: SET_USER_IMAGE,
    payload: data
})
export const setUserCity = data => ({
    type: SET_USER_CITY,
    payload: data
})
export const setUserGender = data => ({
    type: SET_USER_GENDER,
    payload: data
})
export const setUserInfo = data => ({
    type: SET_USER_INFO,
    payload: data
})
export const setUserPassword = data => ({
    type: SET_USER_PASSWORD,
    payload: data
})
export const setNewImpressionName = data => ({
    type: SET_NEW_IMPRESSION_NAME,
    payload: data
})
export const setNewImpressionDescription = data => ({
    type: SET_NEW_IMPRESSION_DESCRIPTION,
    payload: data
})
export const setNewImpressionId = data => ({
    type: SET_NEW_IMPRESSION_ID,
    payload: data
})
export const setNewImpressionIsNew = data => ({
    type: SET_NEW_IMPRESSION_IS_NEW,
    payload: data
})
export const setNewImpressionTypeId = data => ({
    type: SET_NEW_IMPRESSION_TYPE_ID,
    payload: data
})
export const setNewImpressionImage = data => ({
    type: SET_NEW_IMPRESSION_IMAGE,
    payload: data
})
export const clearNewImpression = data => ({
    type: SET_NEW_IMPRESSION_CLEAR,
    payload: data
})
export const setImpressionStream = data => ({
    type: SET_IMPRESSION_STREAM,
    payload: data
})
export const setImpressionStreamView = data => ({
    type: SET_IMPRESSION_STREAM_VIEW,
    payload: data
})
export const setAppTabBarVisibility = data => ({
    type: SET_APP_TABBAR_VISIBILITY,
    payload: data
})
export const setAppDeviceId = data => ({
    type: SET_APP_DEVICE_ID,
    payload: data
})
export const setImpressionListOwn = data => ({
    type: SET_IMPRESSION_LIST_OWN,
    payload: data
})
export const setImpressionList = data => ({
    type: SET_IMPRESSION_LIST,
    payload: data
})
export const setImpressionCurrent = data => ({
    type: SET_IMPRESSION_CURRENT,
    payload: data
})

export const setNewImpressionSheduleDurationDays = data => ({
    type: SET_NEW_IMPRESSION_SHEDULE_DURATION_DAYS,
    payload: data
})
export const setNewImpressionSheduleDurationHours = data => ({
    type: SET_NEW_IMPRESSION_SHEDULE_DURATION_HOURS,
    payload: data
})
export const setNewImpressionSheduleDurationMinutes = data => ({
    type: SET_NEW_IMPRESSION_SHEDULE_DURATION_MINUTES,
    payload: data
})
export const setNewImpressionSheduleDurationSummary = data => ({
    type: SET_NEW_IMPRESSION_SHEDULE_DURATION_SUMMARY,
    payload: data
})
export const setNewImpressionSheduleDateAdd = data => ({
    type: SET_NEW_IMPRESSION_SHEDULE_DATE_ADD,
    payload: data
})
export const setNewImpressionSheduleDateRemove = data => ({
    type: SET_NEW_IMPRESSION_SHEDULE_DATE_REMOVE,
    payload: data
})
export const setChatInputText = data => ({
    type: SET_CHAT_INPUT_TEXT,
    payload: data
})
