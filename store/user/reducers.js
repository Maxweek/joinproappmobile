import {
  SET_PUSH_TOKEN,
  SET_APP_LOGGED,
  SET_APP_LOADING,
  SET_USER_LANG,
  SET_USER_PHONE,
  SET_USER_PASSWORD,
  SET_USER_EMAIL,
  SET_NEW_IMPRESSION_NAME,
  SET_NEW_IMPRESSION_DESCRIPTION,
  SET_NEW_IMPRESSION_ID,
  SET_NEW_IMPRESSION_TYPE_ID,
  SET_NEW_IMPRESSION_IMAGE,
  SET_APP_TABBAR_VISIBILITY,
  SET_APP_DEVICE_ID,
  SET_USER_INFO,
  SET_IMPRESSION_LIST,
  SET_IMPRESSION_LIST_OWN,
  SET_IMPRESSION_STREAM,
  SET_IMPRESSION_STREAM_VIEW,
  SET_NEW_IMPRESSION_CLEAR,
  SET_NEW_IMPRESSION_SHEDULE_DATE_REMOVE,
  SET_NEW_IMPRESSION_SHEDULE_DATE_ADD,
  SET_NEW_IMPRESSION_SHEDULE_DURATION_DAYS,
  SET_NEW_IMPRESSION_SHEDULE_DURATION_HOURS,
  SET_NEW_IMPRESSION_SHEDULE_DURATION_MINUTES,
  SET_NEW_IMPRESSION_SHEDULE_DURATION_SUMMARY,
  SET_NEW_IMPRESSION_IS_NEW,
  SET_IMPRESSION_CURRENT,
  SET_USER_NAME,
  SET_CHAT_INPUT_TEXT,
  SET_USER_BIRTHDAY,
  SET_USER_IMAGE,
  SET_USER_CITY,
  SET_USER_GENDER,
  SET_USER_LAST_NAME,
} from "./actions";

export const defaultState = {
  pushToken: '',
  user: {
    password: "soos9Rah@",
    // password: "",
    // phone: "+79160109774",
    phone: "",
    email: "yb@plumpbear.ru",
    // email: "",
    name: '',
    info: {
      id: "",
      lang: null,
      photo: "https://joinpro.ru/upload/uf/eb8/eb8dd1868ddabedb66fe28923e0052f7.png",
      last_name: "",
      name: "",
      gender: "",
      birthday: "",
      type: 0,
      rate: {
        value: '',
        count: ''
      },
      city: ""
    }
  },
  stream: {
    impression: null,
  },
  stream_view: {
    channel_id: '93d84f609e0ba5c77a6babbc68e00684',
    date: 'date',
    impression: {
      id: '454846',
      preview: null,
      title: 'Title',
      link: 'link',
    },
  },
  local: {
    isTabBarVisible: true,
    isLoggedIn: false,
    isLoading: false,
    deviceId: "",
    channel: 'test',
    chatInputText: '',
    languages: [
      {
        value: 'ru',
        label: "Русский",
      },
      {
        value: 'en',
        label: "English",
      },
    ],
  },
  impression: {
    new: {
      id: null,
      isNew: true,
      name: '',
      description: '',
      type_id: null,
      image: {},
      shedule: {
        duration: {
          days: '0',
          hours: '1',
          minutes: '0',
          summary: '',
        },
        dates: []
      }
    },
    current: {},
    list: [
      {
        id: "161790",
        type_id: 0,
        title: "Мое впечатление",
        preview: "https://joinpro.su/upload/resize_cache/iblock/513/500_500_1/51378e5a10be7df80978a9dbacf6fadf.jpg",
        price: 0,
        date: 1615809600,
        date_local: "15 марта 2021 г",
        status_id: "0",
        status_description: "Черновик"
      },
      {
        id: 2,
        name: 'ыыыыыыыыыыыыыыыыыыыыы',
        image: '../../../assets/images/ref__profile.jpg',
        type: 0,
        date: '10.10.2021'
      },
      {
        id: 3,
        name: 'title',
        image: '../../../assets/images/ref__profile.jpg',
        type: 0,
        date: '10.10.2021'
      },
      {
        id: 4,
        name: 'title',
        image: '../../../assets/images/ref__profile.jpg',
        type: 1,
        date: '10.10.2021'
      },
      {
        id: 5,
        name: 'title',
        image: '../../../assets/images/ref__profile.jpg',
        type: 0,
        date: '10.10.2021'
      },
      {
        id: 6,
        name: 'title',
        image: '../../../assets/images/ref__profile.jpg',
        type: 0,
        date: '10.10.2021'
      },
      {
        id: 7,
        name: 'title',
        image: '../../../assets/images/ref__profile.jpg',
        type: 0,
        date: '10.10.2021'
      },
    ]
  }
};

export const appReducer = (state = defaultState, action) => {
  switch (action.type) {
    case SET_PUSH_TOKEN:
      return {
        ...state,
        pushToken: action.payload
      };
    case SET_APP_LOGGED:
      console.log('23231321321312321312')
      return {
        ...state,
        local: {
          ...state.local,
          isLoggedIn: action.payload,
        },
      };
    case SET_APP_LOADING:
      return {
        ...state,
        local: {
          ...state.local,
          isLoading: action.payload,
        },
      };
    case SET_USER_LANG:
      return {
        ...state,
        user: {
          ...state.user,
          info: {
            ...state.user.info,
            lang: action.payload
          }
        },
      };
    case SET_USER_INFO:
      return {
        ...state,
        user: {
          ...state.user,
          info: action.payload
        },
      };
    case SET_APP_TABBAR_VISIBILITY:
      return {
        ...state,
        local: {
          ...state.local,
          isTabBarVisible: action.payload,
        },
      };
    case SET_APP_DEVICE_ID:
      return {
        ...state,
        local: {
          ...state.local,
          deviceId: action.payload,
        }
      };
    case SET_USER_EMAIL:
      return {
        ...state,
        user: {
          ...state.user,
          email: action.payload,
        }
      };
    case SET_USER_PHONE:
      return {
        ...state,
        user: {
          ...state.user,
          phone: action.payload,
        }
      };
    case SET_USER_NAME:
      return {
        ...state,
        user: {
          ...state.user,
          info: {
            ...state.user.info,
            name: action.payload,
          }
        }
      };
    case SET_USER_LAST_NAME:
      return {
        ...state,
        user: {
          ...state.user,
          info: {
            ...state.user.info,
            last_name: action.payload,
          }
        }
      };
    case SET_USER_GENDER:
      return {
        ...state,
        user: {
          ...state.user,
          info: {
            ...state.user.info,
            gender: action.payload,
          }
        }
      };
    case SET_USER_CITY:
      return {
        ...state,
        user: {
          ...state.user,
          info: {
            ...state.user.info,
            city: action.payload,
          }
        }
      };
    case SET_USER_IMAGE:
      return {
        ...state,
        user: {
          ...state.user,
          info: {
            ...state.user.info,
            photo: action.payload,
          }
        }
      };
    case SET_USER_BIRTHDAY:
      return {
        ...state,
        user: {
          ...state.user,
          info: {
            ...state.user.info,
            birthday: action.payload,
          }
        }
      };
    case SET_USER_PASSWORD:
      return {
        ...state,
        user: {
          ...state.user,
          password: action.payload,
        }
      };
    case SET_NEW_IMPRESSION_CLEAR:
      return {
        ...state,
        impression: {
          ...state.impression,
          new: defaultState.impression.new
        }
      };
    case SET_NEW_IMPRESSION_NAME:
      return {
        ...state,
        impression: {
          ...state.impression,
          new: {
            ...state.impression.new,
            name: action.payload
          }
        }
      };
    case SET_NEW_IMPRESSION_DESCRIPTION:
      return {
        ...state,
        impression: {
          ...state.impression,
          new: {
            ...state.impression.new,
            description: action.payload
          }
        }
      };
    case SET_NEW_IMPRESSION_TYPE_ID:
      return {
        ...state,
        impression: {
          ...state.impression,
          new: {
            ...state.impression.new,
            type_id: action.payload
          }
        }
      };
    case SET_NEW_IMPRESSION_ID:
      return {
        ...state,
        impression: {
          ...state.impression,
          new: {
            ...state.impression.new,
            id: action.payload
          }
        }
      };
    case SET_NEW_IMPRESSION_IS_NEW:
      return {
        ...state,
        impression: {
          ...state.impression,
          new: {
            ...state.impression.new,
            isNew: action.payload
          }
        }
      };
    case SET_NEW_IMPRESSION_IMAGE:
      return {
        ...state,
        impression: {
          ...state.impression,
          new: {
            ...state.impression.new,
            image: action.payload
          }
        }
      };
    case SET_NEW_IMPRESSION_SHEDULE_DURATION_DAYS:
      return {
        ...state,
        impression: {
          ...state.impression,
          new: {
            ...state.impression.new,
            shedule: {
              ...state.impression.new.shedule,
              duration: {
                ...state.impression.new.shedule.duration,
                days: action.payload
              }
            }
          }
        }
      };
    case SET_NEW_IMPRESSION_SHEDULE_DURATION_HOURS:
      return {
        ...state,
        impression: {
          ...state.impression,
          new: {
            ...state.impression.new,
            shedule: {
              ...state.impression.new.shedule,
              duration: {
                ...state.impression.new.shedule.duration,
                hours: action.payload
              }
            }
          }
        }
      };
    case SET_NEW_IMPRESSION_SHEDULE_DURATION_MINUTES:
      return {
        ...state,
        impression: {
          ...state.impression,
          new: {
            ...state.impression.new,
            shedule: {
              ...state.impression.new.shedule,
              duration: {
                ...state.impression.new.shedule.duration,
                minutes: action.payload
              }
            }
          }
        }
      };
    case SET_NEW_IMPRESSION_SHEDULE_DURATION_SUMMARY:
      return {
        ...state,
        impression: {
          ...state.impression,
          new: {
            ...state.impression.new,
            shedule: {
              ...state.impression.new.shedule,
              duration: {
                ...state.impression.new.shedule.duration,
                summary: action.payload
              }
            }
          }
        }
      };
    case SET_NEW_IMPRESSION_SHEDULE_DATE_ADD:
      return {
        ...state,
        impression: {
          ...state.impression,
          new: {
            ...state.impression.new,
            shedule: {
              ...state.impression.new.shedule,
              dates: state.impression.new.shedule.dates.push(action.payload)
            }
          }
        }
      };
    case SET_NEW_IMPRESSION_SHEDULE_DATE_REMOVE:
      return {
        ...state,
        impression: {
          ...state.impression,
          new: {
            ...state.impression.new,
            shedule: {
              ...state.impression.new.shedule,
              dates: state.impression.new.shedule.dates.map(el => {
                if (el.id !== action.payload) {
                  return el;
                }
              })
            }
          }
        }
      };
    case SET_IMPRESSION_LIST:
      return {
        ...state,
        impression: {
          ...state.impression,
          list: action.payload
        }
      };
    case SET_IMPRESSION_LIST_OWN:
      return {
        ...state,
        impression: {
          ...state.impression,
          list: action.payload
        }
      };
    case SET_IMPRESSION_CURRENT:
      return {
        ...state,
        impression: {
          ...state.impression,
          current: action.payload
        }
      };
    case SET_IMPRESSION_STREAM:
      return {
        ...state,
        stream: action.payload
      };
    case SET_IMPRESSION_STREAM_VIEW:
      return {
        ...state,
        stream_view: action.payload
      };
    case SET_CHAT_INPUT_TEXT:
      return {
        ...state,
        local: {
          ...state.local,
          chatInputText: action.payload
        }
      };

    default:
      return state;
  }
};
