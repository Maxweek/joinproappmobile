import { combineReducers } from 'redux';
import { appReducer } from './user/reducers';

export default rootReducer = combineReducers({
    app: appReducer,
})