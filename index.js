/**
 * @format
 */

import messaging from '@react-native-firebase/messaging';
import { AppRegistry, Platform } from 'react-native';
import PushNotification from 'react-native-push-notification';
import App from './App';


import { name as appName } from './app.json';

console.log('123')
console.log(messaging)

async function requestUserPermission() {
    const authorizationStatus = await messaging().requestPermission();
    const token = await messaging().getToken();
    const atoken = await messaging().getAPNSToken();

    if (authorizationStatus) {
        console.log('Permission status:', authorizationStatus);
    }
    console.log(token);
    console.log(atoken);
    return authorizationStatus
}

requestUserPermission().then((data) => {
    console.log(3231)
    console.log(data)
})

messaging().subscribeToTopic('MAIN').then(() => {console.log('subscribed')})

// messaging().onNotificationOpenedApp(remoteMessage => {
//     console.log(
//       'Notification caused app to open from background state:',
//       remoteMessage.notification,
//     );
//     console.log(remoteMessage)
//     // navigation.navigate(remoteMessage.data.type);
//   });

// PushNotification.configure({
//     // (optional) Called when Token is generated (iOS and Android)
//     onRegister: function (token) {
//         console.log("TOKEN:", token);
//     },

//     // (required) Called when a remote is received or opened, or local notification is opened
//     onNotification: function (notification) {
//         console.log("NOTIFICATION:", notification);

//         // process the notification

//         // (required) Called when a remote is received or opened, or local notification is opened
//         // notification.finish(PushNotificationIOS.FetchResult.NoData);
//     },

//     // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
//     onAction: function (notification) {
//         console.log("ACTION:", notification.action);
//         console.log("NOTIFICATION:", notification);

//         // process the action
//     },

//     // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
//     onRegistrationError: function (err) {
//         console.error(err.message, err);
//     },

//     // IOS ONLY (optional): default: all - Permissions to register.
//     permissions: {
//         alert: true,
//         badge: true,
//         sound: true,
//     },

//     // Should the initial notification be popped automatically
//     // default: true
//     popInitialNotification: true,

//     /**
//      * (optional) default: true
//      * - Specified if permissions (ios) and token (android and ios) will requested or not,
//      * - if not, you must call PushNotificationsHandler.requestPermissions() later
//      * - if you are not using remote notification or do not have Firebase installed, use this:
//      *     requestPermissions: Platform.OS === 'ios'
//      */
//     requestPermissions: true,
// });

AppRegistry.registerComponent(appName, () => App);
