import firebase from 'firebase'; // 4.8.1

class Fire {
    constructor(channel = null) {
        this.channel = channel;
        this.init();
        this.observeAuth();
    }

    init = () => {
        if (!firebase.apps.length) {
            firebase.initializeApp({
                apiKey: "AIzaSyBargcQkffbOqVDZKEDakFDJag5SpaptxY",
                authDomain: "joinpp-c4e49.firebaseapp.com",
                databaseURL: "https://joinpp-c4e49-default-rtdb.firebaseio.com",
                projectId: "joinpp-c4e49",
                storageBucket: "joinpp-c4e49.appspot.com",
                messagingSenderId: "1022135749146",
                appId: "1:1022135749146:web:69c7d0cd9b8e2e51da9fa3"
            });
        }
    }

    observeAuth = () => {
        if (!firebase.apps.length) {
            firebase.auth().onAuthStateChanged(this.onAuthStateChanged);
        }
    }

    onAuthStateChanged = user => {
        if (!user) {
            try {
                firebase.auth().signInAnonymously();
            } catch ({ message }) {
                alert(message);
            }
        }
    };

    get uid() {
        return (firebase.auth().currentUser || {}).uid;
    }

    get ref() {
        return firebase.database().ref(this.channel);
    }

    parse = snapshot => {
        console.log(snapshot)
        const { timestamp: numberStamp, text, user } = snapshot.val();
        const { key: _id } = snapshot;
        const timestamp = new Date(numberStamp);
        const message = {
            _id,
            timestamp,
            text,
            user,
        };
        return message;
    };

    onAdd = callback =>
        this.ref
            .limitToLast(40)
            .on('child_added', snapshot => callback(snapshot));
    onChange = callback =>
        this.ref
            .on('child_changed', snapshot => callback(snapshot));

    get timestamp() {
        return firebase.database.ServerValue.TIMESTAMP;
    }
    // send the message to the Backend
    send = messages => {
        for (let i = 0; i < messages.length; i++) {
            const { text, user } = messages[i];
            const message = {
                text,
                user,
                timestamp: this.timestamp,
            };
            this.append(message);
        }
    };

    append = message => this.ref.push(message);

    stop = () => {
        let message = {
            status: 'off'
        }
        this.append(message)
    }
    start = () => {
        let message = {
            status: 'on'
        }
        this.append(message)
    }

    // close the connection to the Backend
    off() {
        this.ref.off();
    }
}


export default Fire;
