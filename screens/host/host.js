import React, { Component } from "react";
import {
    ScrollView,
    Text,
    TextInput,
    View,
    Animated,
    Button,
    Image,
    StyleSheet, Linking, Alert
} from "react-native";
import { v4 as uuid } from 'uuid';
import { timestampToDate, translate } from "../../App";
import { COLORS } from "../../assets/styles/styles";
import ContainerBox from "../../components/containerBox";
import HeaderBox from "../../components/headerBox";
import TButton from "../../components/tButton";

export default class Host extends Component {
    constructor(props) {
        super(props);
    }
    setStream = () => {
        this.props.navigation.navigate('streamScreen');
    }

    render() {
        return (
            <ContainerBox
                navigation={this.props.navigation}
                paddingTop={true}
                header={<HeaderBox
                    gradient={true}
                    scheme={'dark'}
                    shadow={false}
                    navigation={this.props.navigation}
                    back={true}
                />}
                title={translate("navigator.hostStream.streamSettingsScreen")}
            >
            <View style={{width: '100%'}}>
                {this.props.app.stream !== null ? (
                    <View style={styles.inner}>
                        {this.props.app.stream.impression.preview !== undefined ?
                            <Image
                                style={styles.imageLogo}
                                source={{ uri: this.props.app.stream.impression.preview }}
                            />
                            : null}
                        <Text style={styles.title}>{this.props.app.stream.impression.title}</Text>
                        <View style={styles.streamInfo_list}>
                            <View style={styles.streamInfo_item}>
                                <Text style={styles.streamInfo_item__title}>channel_id: </Text>
                                <Text style={styles.streamInfo_item__value}>{this.props.app.stream.channel_id}</Text>
                            </View>
                            <View style={styles.streamInfo_item}>
                                <Text style={styles.streamInfo_item__title}>impression_id: </Text>
                                <Text style={styles.streamInfo_item__value}>{this.props.app.stream.impression.id}</Text>
                            </View>
                            <View style={styles.streamInfo_item}>
                                <Text style={styles.streamInfo_item__title}>impression_title: </Text>
                                <Text style={styles.streamInfo_item__value}>{this.props.app.stream.impression.title}</Text>
                            </View>
                            <View style={styles.streamInfo_item}>
                                <Text style={styles.streamInfo_item__title}>impression_link: </Text>
                                <Text style={styles.streamInfo_item__value}>{this.props.app.stream.impression.link}</Text>
                            </View>
                            <View style={[styles.streamInfo_item, {borderBottomWidth: 0}]}>
                                <Text style={styles.streamInfo_item__title}>Date: </Text>
                                <Text style={styles.streamInfo_item__value}>{timestampToDate(this.props.app.stream.date * 1000)}</Text>
                            </View>
                        </View>
                        <View style={{ width: '100%', marginVertical: 20, justifyContent: 'center', alignItems: 'center' }}>
                            {/* <Text style={{ width: '100%', textAlign: 'center', marginVertical: 20 }}>Тут будут всякие настройки</Text> */}
                        </View>
                        <TButton
                            onPress={this.setStream}
                            text={translate("screen.host.startButton")}
                            icon="ios-pricetag"
                            type="primary"
                        />
                    </View>
                ) : <Text>{translate("screen.host.errorPullStream")}</Text>}
                </View>
            </ContainerBox>
        );
    }
}
const styles = StyleSheet.create({
    inner: {
        marginTop: 24
    },
    imageLogo: {
        width: 140,
        height: 140,
        borderRadius: 10,
        // marginBottom: 20,
        backgroundColor: '#dedede'
    },
    title: {
        fontSize: 27,
        marginVertical: 16
    },
    streamInfo_list: {
        // width: '100%',
        alignItems: 'stretch',
        backgroundColor: '#f7f7f7',
        paddingHorizontal: 4,
        borderRadius: 8
    },
    streamInfo_item: {
        // width: '100%',
        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderBottomColor: "#e0e0e0",
        paddingVertical: 4,
    },
    streamInfo_item__title: {
        color: '#909090',
        fontSize: 10
    },
    streamInfo_item__value: {
        fontSize: 12
    },
    btnList: {
        flexDirection: 'column'
    }
});
