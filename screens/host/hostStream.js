// import RtmEngine from "agora-react-native-rtm";
import MaskedView from "@react-native-community/masked-view";
import React, { Component } from "react";
import { createRef } from "react";
import {
    ScrollView,
    Text,
    TextInput,
    View,
    Animated,
    ActivityIndicator,
    Button,
    // NativeModules,
    Image,
    NativeModules,
    StyleSheet, Linking, Alert, Share, Dimensions, PermissionsAndroid, FlatList, StatusBar, SafeAreaView, KeyboardAvoidingView, TouchableOpacity, Keyboard, Platform
} from "react-native"
import RtcEngine, {
    ChannelProfile,
    ClientRole,
    RtcLocalView,
    RtcRemoteView,
    VideoRemoteState,
    VideoRenderMode,
} from 'react-native-agora';
import LinearGradient from "react-native-linear-gradient";
import TButton from "../../components/tButton";
import Icon from "react-native-vector-icons/Ionicons";
import Fire from '../../firebase'
import KeepAwake from 'react-native-keep-awake';
import { translate } from "../../App";
import API, { API_IMPRESSION_STREAM_RECORDING_START, API_IMPRESSION_STREAM_RECORDING_STOP, API_IMPRESSION_STREAM_RECORDING_CHECK } from "../../API";
import Orientation from "react-native-orientation-locker";
import LoaderBox from "../../components/loaderBox";
import ChatBox from "../../components/chatBox";
import { COLORS } from "../../assets/styles/styles";
import StreamBox from "../../components/streamBox";
const { StatusBarManager } = NativeModules;

// const {Agora} = NativeModules; //Define Agora object as a native module

// const {FPS30, AudioProfileDefault, AudioScenarioDefault, Adaptative} = Agora;

var dimensions = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
};

export default class HostStream extends Component {
    constructor(props) {
        super(props);
        console.log(props)
        this.state = {
            user: 1,
            channel: this.props.app.stream.channel_id,
            isJoined: false,
            isBroadcaster: true,
            broadcasterVideoState: VideoRemoteState.Decoding,
            isCameraSwitched: true,
            isAudioActive: true,
            isMicVolumeUp: false,
            isVideoActive: true,
            isChatActive: false,
            isChatInited: false,
            userCount: 0,
            animateVars: {
                chatVar: new Animated.Value(0),
                chatButtonVar: new Animated.Value(0),
                inputWidth: new Animated.Value(0),
                shadowOpacity: new Animated.Value(0),
                windowOpacity: new Animated.Value(0),
                iwindowOpacity: new Animated.Value(0),
                sendButtonActivity: new Animated.Value(0),
            },
            dimensions: {
                width: Dimensions.get('window').width,
                height: Dimensions.get('window').height
            },
            recording: {
                active: null,
                text: 'Waiting...',
                indicatorPulse: true,
                indicatorActive: true,
            },
            messages: [
                // {
                //     id: 1,
                //     message: translate("screen.hostStream.bot.message"),
                //     timestamp: new Date(),
                //     user: {
                //         id: 1,
                //         avatar: 'https://placeimg.com/140/140/any',
                //         username: translate("screen.hostStream.bot.name"),
                //     }
                // },
            ],
            isPortrait: true
        }
        console.log(this.state)
        this.onSwitchCamera = this.onSwitchCamera.bind(this)
        this.onShare = this.onShare.bind(this)
        this.onSwitchAudio = this.onSwitchAudio.bind(this)
        this.onSwitchVideo = this.onSwitchVideo.bind(this)
        this.onMicVolumeUp = this.onMicVolumeUp.bind(this)
        this.stopStream = this.stopStream.bind(this)
        // this.
        this.pollingRecordingCheck = null;
        this.AgoraEngine = null;
        this._animatedLiveIndicator = new Animated.Value(0);
        this.FlatListRef;

    }

    async initStream() {
        console.log(this.props.app.stream.channel_id)
        this.AgoraEngine = createRef()
        this.AgoraEngine.current = await RtcEngine.create(
            // '7992c14f45514cd289bf1950758e071e', Основной jp
            // '1e2e1300252c4bb9afd5ef247e30bf00', Запасной мой
            '987fd1c2247346a082c28e84f03d5de6'
            // 'c16a92ce413f41598afd66a0d4555fab',
            // 'c7e742d5df23478285a9dc4f4ff62407',
        );
        this.AgoraEngine.current.setVideoEncoderConfiguration({
            width: 1280,
            height: 720,
            bitrate: 15420,
            frameRate: 60,
            // orientationMode: Adaptative,
        })
        console.log(this.AgoraEngine.current)
        this.AgoraEngine.current.enableVideo();
        this.AgoraEngine.current.setChannelProfile(ChannelProfile.LiveBroadcasting);

        this.AgoraEngine.current.setClientRole(ClientRole.Broadcaster);

        this.AgoraEngine.current.enableAudioVolumeIndication(100, 3, true)

        this.AgoraEngine.current.addListener('RemoteVideoStateChanged', (uid, state) => {
            if (uid === 1) this.setBroadcasterVideoState(state);
        });

        // this.AgoraEngine.current.addListener(
        //     'LocalAudioStatsCallback',
        //     (channel, uid, elapsed) => {
        //         console.log('LocalAudioStatsCallback', channel, uid, elapsed);
        //     },
        // );

        this.AgoraEngine.current.addListener(
            'JoinChannelSuccess',
            (channel, uid, elapsed) => {
                console.log('JoinChannelSuccess', channel, uid, elapsed);
                this.setJoined(true);
            },
        );
        this.AgoraEngine.current.addListener(
            'UserJoined',
            (uid, elapsed) => {
                console.log('UserJoined', uid, elapsed);
            },
        );
        this.AgoraEngine.current.addListener(
            'RtcStats',
            (stats) => {
                // console.log(stats);
                //this.setUserCount(stats.users - 1);
            },
        );
    }

    async onShare() {
        try {
            const result = await Share.share({ message: this.props.app.stream.impression.link });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            console.log(error.message);
        }
    }

    setUserCount(count) {
        if (this.state.userCount !== count) {
            this.setState({
                userCount: count
            })
        }
    }

    onSwitchCamera() {
        if (!this.state.isCameraSwitched) {
            this.setState({
                isCameraSwitched: true
            })
        } else {
            this.setState({
                isCameraSwitched: false
            })
        }
        this.AgoraEngine.current.switchCamera();
    }
    onMicVolumeUp() {
        if (!this.state.isMicVolumeUp) {
            this.setState({
                isMicVolumeUp: true
            })
            this.AgoraEngine.current.adjustRecordingSignalVolume(400)
        } else {
            this.setState({
                isMicVolumeUp: false
            })
            this.AgoraEngine.current.adjustRecordingSignalVolume(100)
        }
    }
    onSwitchAudio() {
        if (!this.state.isAudioActive) {
            this.setState({
                isAudioActive: true
            })
            this.AgoraEngine.current.muteLocalAudioStream(false);
        } else {
            this.setState({
                isAudioActive: false
            })
            this.AgoraEngine.current.muteLocalAudioStream(true);
        }

    }
    onSwitchVideo() {
        if (!this.state.isVideoActive) {
            this.setState({
                isVideoActive: true
            })
            this.AgoraEngine.current.muteLocalVideoStream(false);
        } else {
            this.setState({
                isVideoActive: false
            })
            this.AgoraEngine.current.muteLocalVideoStream(true);
        }
    }
    onSwitchChat = () => {
        this.setState({
            isChatActive: !this.state.isChatActive
        })
    }
    setFirstMessage = () => {
        if (!this.state.isChatInited) {

            let mess = [...this.state.messages];
            mess.push(
                {
                    id: '_0',
                    message: translate("impression.single") + ': ' + this.props.app.stream.impression.title + " #" + this.props.app.stream.impression.id,
                    timestamp: new Date(),
                    user: {
                        id: '_1',
                        type: 'host',
                        avatar: this.props.app.stream.impression.preview ? this.props.app.stream.impression.preview : 'https://placeimg.com/140/140/any',
                        username: 'Joinpro',
                    }
                }
            )
            this.setState({
                messages: mess
            }, () => {
                // console.log(this.state.messages)
                this.updateMessages()
            })
        }
    }
    setFirebase = type => {
        this.setFirstMessage();
        if (type) {

            this.firebase = new Fire(this.state.channel);
            this.firebase.onAdd(snapshotBox => {
                console.log('onAdd')
                console.log(snapshotBox)
                let snapshot = snapshotBox.val();
                console.log(snapshot)
                if (snapshotBox.val().count !== undefined) {
                    this.setUserCount(snapshotBox.val().count)
                    return
                }
                let mess = [...this.state.messages];
                let trgId = this.state.messages.length - 1;
                if (this.state.messages.length > 2) {
                    // trgId = this.state.messages.length - 1
                    trgId = this.state.messages[trgId].id + 1
                }
                if (!snapshot.user) {
                    return
                }
                mess.push(
                    {
                        id: trgId,
                        message: snapshot.user ? snapshot.message : '',
                        timestamp: snapshot.timestamp ? new Date(snapshot.timestamp) : new Date(),
                        user: {
                            id: snapshot.user ? snapshot.user.id : 1,
                            type: snapshot.user ? snapshot.user.type : '',
                            avatar: snapshot.user ? snapshot.user.avatar : 'https://placeimg.com/140/140/any',
                            username: snapshot.user ? snapshot.user.username : 'Joinpro',
                        }
                    }
                )
                this.setState({
                    messages: mess
                }, () => {
                    // console.log(this.state.messages)
                    this.updateMessages()
                })
                // console.log(message)
                // this.setState(previousState => ({
                //     messages: GiftedChat.append(previousState.messages, message),
                // })
            });
            this.firebase.onChange(snapshotBox => {
                console.log(snapshotBox)
                console.log(snapshotBox.val())
                if (snapshotBox.val().count !== undefined) {
                    this.setUserCount(snapshotBox.val().count)
                }
            })
        } else {
            this.firebase.off()
        }
    }

    componentDidMount() {
        // Orientation.addOrientationListener(this._onOrientationDidChange);
        // Orientation.unlockAllOrientations()
        // this.set();
        // this.props.setAppTabBarVisibility(false)
        // this.setFirebase(true)

        // this.closeChat()

    }

    updateMessages = () => {
        // console.log(this.FlatListRef)
        // this.FlatListRef.scrollToEnd()
    }

    componentDidUpdate() {
        // this.set();
        // Animated.timing(this._animatedLiveIndicator, {
        //     toValue: this.state.recording.indicatorActive ? 1 : 0,
        //     duration: 200,
        //     useNativeDriver: false
        // }).start();
    }
    componentWillUnmount() {
        // Orientation.removeOrientationListener(this._onOrientationDidChange);
        // Orientation.lockToPortrait()
        // clearTimeout(this.pollingRecordingCheck);

        // this.props.setAppTabBarVisibility(true)
        // // this.AgoraEngine.current.leaveChannel()
        // // this.AgoraEngine.current.destroy();

        // this.setFirebase(false)
        // if (Platform.OS !== 'ios') {
        //     StatusBar.setHidden(false);
        // }
    }
    _onOrientationDidChange = (orientation) => {
        dimensions = {
            screen: {
                width: Dimensions.get('screen').width,
                height: Dimensions.get('screen').height
            }, window: {
                width: Dimensions.get('window').width,
                height: Dimensions.get('window').height
            }
        }
        // console.log(orientation)
        if (!orientation) {
            if (dimensions.window.width < dimensions.window.height) {
                orientation = 'PORTRAIT'
            }
        }
        // console.log(dimensions)
        // console.log(orientation === 'PORTRAIT' ? dimensions.screen.height:dimensions.screen.height)
        this.setState({
            dimensions: {
                // width: orientation === 'PORTRAIT' ? dimensions.screen.width : dimensions.screen.height,
                // height: orientation === 'PORTRAIT' ? dimensions.screen.height : dimensions.screen.width,
                width: dimensions.screen.width,
                height: dimensions.screen.height,
            }
        }, () => {
            // console.log(this.state.dimensions)
            if (orientation === 'PORTRAIT') {
                if (Platform.OS === 'android') {
                    StatusBar.setHidden(false)
                }
                this.setState({ isPortrait: true })
            } else {
                if (Platform.OS === 'android') {
                    StatusBar.setHidden(true)
                }
                this.setState({ isPortrait: false })
            }
            if (this.state.isChatActive) {
                this.openChat()
            } else {
                this.closeChat()
            }
        })
        // console.log(this.state.dimensions)

    }


    wannaStop = () => {

        Alert.alert(
            translate('screen.hostStream.exit'),
            translate('screen.hostStream.exitNotify'),
            [
                { text: translate('screen.createImpression.confirm.cancel'), style: "cancel" },
                { text: translate('screen.createImpression.confirm.gotIt'), onPress: () => this.stopStream() }
            ]
        );
    }

    stopStream() {
        // this.props.setAppTabBarVisibility(true)
        // this.props.navigation.navigate('profileScreen');
        clearTimeout(this.pollingRecordingCheck);

        const formData = new FormData();

        formData.append('channel_id', this.state.channel);

        API.post(API_IMPRESSION_STREAM_RECORDING_STOP, formData)
            .then(res => {
                console.log(res.data)
                if (typeof res.data.status !== "undefined") {
                    if (res.data.status === 'success') {
                        console.log('success')
                        this.setState({ recording: { ...this.state.recording, active: false, text: 'Over' } })
                        this.setIndicator(false)
                    }
                    if (res.data.status === 'error') {
                        this.setState({ recording: { ...this.state.recording, active: false, text: 'Over' } })
                        this.setIndicator(false)
                    }
                } else {
                    this.setState({ recording: { ...this.state.recording, active: false, text: 'Over' } })
                    this.setIndicator(false)
                }

                this.props.navigation.navigate('profileScreenStack');
            }).catch(res => {
                console.log(res.data)
                this.setState({ recording: { ...this.state.recording, active: false, text: 'New err' } })
                this.setIndicator(false)
            })


        this.firebase.stop()
    }
    setStream() {
        const uid = this.state.user;
        this.initStream().then(() => {
            this.AgoraEngine.current.joinChannel(
                null,
                this.state.channel,
                null,
                uid,
            );
            this.setRecording();
            this.firebase.start()
        }
        );
    }
    set() {
        if (Platform.OS === 'android') {
            this.requestCameraAndAudioPermission()
                .then(() => {
                    this.setStream()
                }
                );
        } else {
            this.setStream()
        }

        // return () => {
        //     this.AgoraEngine.current.destroy();
        // };
    }

    setIndicator = (type = null) => {
        if (type) {
            this.setState({ recording: { ...this.state.recording, indicatorPulse: true } })
        }
        if (type === false) {
            this.setState({ recording: { ...this.state.recording, indicatorPulse: false } })
        }
        if (this.state.recording.indicatorPulse) {
            // this.setState({ recording: { ...this.state.recording, indicatorActive: !this.state.recording.indicatorActive } }, () => {
            //     setTimeout(() => {
            //         this.setIndicator(null);
            //     }, 1000)
            // })
        } else {
            this.setState({ recording: { ...this.state.recording, indicatorActive: false } })
        }

    }

    setRecording = () => {

        const formData = new FormData();

        formData.append('channel_id', this.state.channel);

        API.post(API_IMPRESSION_STREAM_RECORDING_START, formData)
            .then(res => {
                console.log(res.data)
                if (typeof res.data.status !== "undefined") {
                    if (res.data.status === 'success') {
                        console.log('success')
                        this.setState({ recording: { ...this.state.recording, active: true, text: 'REC' } })
                        this.setIndicator(true)
                    }
                    if (res.data.status === 'error') {
                        this.setState({ recording: { ...this.state.recording, active: false, text: 'Err' } })
                        this.setIndicator(false)
                    }
                } else {
                    this.setState({ recording: { ...this.state.recording, active: false, text: 'Err' } })
                    this.setIndicator(false)
                }
                this.setPollingRecordingStreamCheck();
            }).catch(res => {
                console.log("TEST RECORDING START")
                console.log(res)
                this.setState({ recording: { ...this.state.recording, active: false, text: 'Net err' } })
                this.setIndicator(false)
            })

    }
    setPollingRecordingStreamCheck = (type = true) => {
        this.checkRecordingStream();
        this.pollingRecordingCheck = setTimeout(() => {
            this.setPollingRecordingStreamCheck();
        }, 5000)
    }
    checkRecordingStream = () => {

        const formData = new FormData();

        formData.append('channel_id', this.state.channel);

        //console.log('STREAM CHECK')
        API.post(API_IMPRESSION_STREAM_RECORDING_CHECK, formData)
            .then(res => {
                //console.log(res.data)
                if (typeof res.data.status !== "undefined") {
                    if (res.data.status === 'success') {
                        // console.log('STREAM CHECK SUCCESS')
                        // console.log(res.data)
                        if (!this.state.recording.active) {

                            this.setState({ recording: { ...this.state.recording, active: true, text: 'REC' } })
                        }
                        if (!this.state.recording.indicatorPulse) {
                            this.setIndicator(true)
                        }
                    }
                    if (res.data.status === 'error') {
                        this.setState({ recording: { ...this.state.recording, active: false, text: 'Err' } })
                        this.setIndicator(false)
                    }
                } else {
                    this.setState({ recording: { ...this.state.recording, active: false, text: 'Err' } })
                    this.setIndicator(false)
                }
            }).catch(res => {
                console.log(res.data)
                this.setState({ recording: { ...this.state.recording, active: false, text: 'Net err' } })
                this.setIndicator(false)
            })
    }

    setBroadcasterVideoState(data) {
        this.setState({
            broadcasterVideoState: data
        })
    }

    setJoined(type) {
        console.log('joined: ' + type)
        this.setState({ isJoined: type })
    }

    stopStreamCheck = () => {
        this.firebase.stop()
    }
    startStreamCheck = () => {
        this.firebase.start()
    }

    videoStateMessage() {
        switch (this.state.broadcasterVideoState) {
            case VideoRemoteState.Stopped:
                return 'Video turned off by Host';

            case VideoRemoteState.Frozen:
                return 'Connection Issue, Please Wait';

            case VideoRemoteState.Failed:
                return 'Network Error';
        }
    };


    async requestCameraAndAudioPermission() {
        try {
            const granted = await PermissionsAndroid.requestMultiple([
                PermissionsAndroid.PERMISSIONS.CAMERA,
                PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
            ]);
            if (
                granted['android.permission.RECORD_AUDIO'] ===
                PermissionsAndroid.RESULTS.GRANTED &&
                granted['android.permission.CAMERA'] ===
                PermissionsAndroid.RESULTS.GRANTED
            ) {
                console.log('You can use the cameras & mic');
            } else {
                console.log('Permission denied');
            }
        } catch (err) {
            console.warn(err);
        }
    }

    openChat = () => {
        console.log(-dimensions.width)
        Animated.spring(this.state.animateVars.chatVar, {
            toValue: 0,
            duration: 200,
            useNativeDriver: true
        }).start();
        Animated.timing(this.state.animateVars.shadowOpacity, {
            toValue: 1,
            duration: 400,
            useNativeDriver: true
        }).start();
        Animated.timing(this.state.animateVars.windowOpacity, {
            toValue: 0,
            duration: 100,
            useNativeDriver: true
        }).start();
        Animated.timing(this.state.animateVars.iwindowOpacity, {
            toValue: 1,
            duration: 100,
            useNativeDriver: true
        }).start();
        Animated.spring(this.state.animateVars.chatButtonVar, {
            toValue: -this.state.dimensions.width,
            duration: 200,
            useNativeDriver: true
        }).start();
    }
    closeChat = () => {
        Animated.spring(this.state.animateVars.chatVar, {
            toValue: this.state.dimensions.width,
            duration: 200,
            useNativeDriver: true
        }).start();
        Animated.timing(this.state.animateVars.shadowOpacity, {
            toValue: 0,
            duration: 400,
            useNativeDriver: true
        }).start();
        Animated.timing(this.state.animateVars.windowOpacity, {
            toValue: 1,
            duration: 100,
            useNativeDriver: true
        }).start();
        Animated.timing(this.state.animateVars.iwindowOpacity, {
            toValue: 0,
            duration: 100,
            useNativeDriver: true
        }).start();
        Animated.spring(this.state.animateVars.chatButtonVar, {
            toValue: 0,
            duration: 200,
            useNativeDriver: true
        }).start();

        Keyboard.dismiss()
    }
    toggleChat = (type = true) => {
        if (!this.state.isChatActive) {
            if (type) {
                this.setState({ isChatActive: true })
            }
            this.openChat()
        } else {
            if (type) {
                this.setState({ isChatActive: false })
            }
            this.closeChat()
        }
    }

    onInputFocus = () => {

    }
    onMessageSend = () => {
        if (this.props.app.local.chatInputText.length) {
            let snap = {
                message: this.props.app.local.chatInputText,
                timestamp: this.firebase.timestamp,
                user: {
                    avatar: this.props.app.user.info.photo !== '' ? this.props.app.user.info.photo : 'https://joinpro.ru/upload/uf/eb8/eb8dd1868ddabedb66fe28923e0052f7.png',
                    username: this.props.app.user.info.name,
                    type: 'host',
                }
            }
            console.log(snap)
            this.firebase.append(snap)
            this.props.setChatInputText('');
        }
    }
    setChatInputText = (text) => {
        this.props.setChatInputText(text)
    }

    render() {
        let liveIndicatorStyles = {
            opacity: this._animatedLiveIndicator.interpolate({
                inputRange: [0, 1],
                outputRange: [0, 1],
            }),
        };
        let chat = {
            transform: [{
                translateX: this.state.animateVars.chatVar
            }]
        }
        let chatAvoid = {
            transform: [{
                translateX: this.state.animateVars.chatButtonVar
            }]
        }
        let shadowOpacity = {
            opacity: this.state.animateVars.shadowOpacity
        }
        let windowOpacity = {
            opacity: this.state.animateVars.windowOpacity
        }
        let iwindowOpacity = {
            opacity: this.state.animateVars.iwindowOpacity
        }
        let top = {
            top: Platform.OS === 'ios' ? 16 : this.state.isPortrait ? StatusBarManager.HEIGHT + 16 : 16
        }
        let fullscreenBox = {
            // backgroundColor: 'red',
            width: Platform.OS === 'ios' ? '100%' : Dimensions.get('window').width,
            height: Platform.OS === 'ios' ? '100%' : Dimensions.get('window').height
        }
        let fullscreenBoxWide = {
            // backgroundColor: 'red',
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').width * 16 / 9
        }
        if (!this.state.isPortrait) {
            fullscreenBoxWide = {
                // backgroundColor: 'blue',
                height: Dimensions.get('window').height,
                width: Dimensions.get('window').height * 16 / 9
            }
        }
        //TODO write orientation engine

        return (
            <StreamBox
                isHost={true}
                channelId={this.props.app.stream ? this.props.app.stream.channel_id : null}
                app={this.props.app}
                navigation={this.props.navigation}
                setAppTabBarVisibility={this.props.setAppTabBarVisibility}
                setChatInputText={this.props.setChatInputText}
            ></StreamBox>
        )
        // return (
        //     <>
        //         {!this.state.isJoined ? (
        //             <View style={styles.container}>
        //                 <LoaderBox text={translate("screen.hostStream.joinWait")}></LoaderBox>
        //             </View>
        //         ) : (
        //             <>
        //                 {/* <RtmEngine /> */}
        //                 <KeepAwake />
        //                 <StatusBar barStyle={'light-content'} />
        //                 <View style={styles.streamBox} >
        //                     <View style={styles.fullscreen}>
        //                         <RtcLocalView.SurfaceView
        //                             style={fullscreenBoxWide}
        //                             channelId={this.state.channel}
        //                             renderMode={VideoRenderMode.Hidden}
        //                         />
        //                     </View>
        //                     <Animated.View style={[{
        //                         position: 'absolute',
        //                         width: '100%',
        //                         height: '100%',
        //                     }, shadowOpacity]}>
        //                         <LinearGradient colors={['#00000000', '#00000044', '#00000066', '#00000088', '#000000aa', '#000000cc', '#000000fa',]} style={[styles.linearGradientBack]}>
        //                         </LinearGradient>
        //                     </Animated.View>
        //                     <LinearGradient colors={['#000000aa', '#00000000',]} style={styles.topGrad}>
        //                     </LinearGradient>
        //                     <LinearGradient colors={['#00000000', '#000000aa']} style={styles.botGrad}>
        //                     </LinearGradient>
        //                 </View>

        //                 <SafeAreaView style={styles.streamBox_safetyView}>
        //                     <KeyboardAvoidingView style={styles.streamBox_keyboardAvoidingView} behavior={"padding"}>
        //                         <Animated.View style={[styles.animatedWrapper, windowOpacity, chatAvoid, fullscreenBox]}>
        //                             <Animated.View style={[styles.topLeft, top]}>
        //                                 <View style={styles.userCountContainer}>
        //                                     <Icon name={'eye'} size={16} color={'white'} />
        //                                     <Text style={styles.userCountText}>{this.state.userCount}</Text>
        //                                 </View>
        //                                 <View style={[styles.recordingContainer, { marginLeft: 4 }]}>
        //                                     <Animated.View style={[liveIndicatorStyles, styles.recordingContainerIndicator]}></Animated.View>
        //                                     <Text style={styles.recordingContainerText}>{this.state.recording.text}</Text>
        //                                 </View>
        //                             </Animated.View>
        //                             <Animated.View style={[styles.topRight, top]}>
        //                                 <TouchableOpacity onPress={this.wannaStop} style={styles.btn}>
        //                                     <Icon name={'exit-outline'} size={24} color={COLORS.black} />
        //                                 </TouchableOpacity>
        //                             </Animated.View>
        //                             <Animated.View style={styles.bottomLeft}>
        //                                 {/* <TouchableOpacity onPress={this.onShare} style={[styles.btn, { marginLeft: 0 }]}>
        //                                     <Icon name={"share-outline"} size={24} color={COLORS.black} />
        //                                 </TouchableOpacity> */}
        //                                 <TouchableOpacity onPress={this.onSwitchCamera} style={[styles.btn, { marginLeft: 0 }]}>
        //                                     <Icon name={this.state.isCameraSwitched ? "camera-reverse-outline" : "camera-reverse"} size={24} color={COLORS.black} />
        //                                 </TouchableOpacity>
        //                                 <TouchableOpacity onPress={this.onMicVolumeUp} style={[styles.btn, { marginLeft: 4, backgroundColor: this.state.isMicVolumeUp ? COLORS.primary : 'white' }]}>
        //                                     <Icon name={"mic-circle-outline"} size={24} color={COLORS.black} />
        //                                 </TouchableOpacity>
        //                                 <TouchableOpacity onPress={this.onSwitchAudio} style={[styles.btn, { marginLeft: 4, backgroundColor: !this.state.isAudioActive ? COLORS.red : 'white' }]}>
        //                                     <Icon name={"mic-outline"} size={24} color={COLORS.black} />
        //                                 </TouchableOpacity>
        //                                 <TouchableOpacity onPress={this.onSwitchVideo} style={[styles.btn, { marginLeft: 4, backgroundColor: !this.state.isVideoActive ? COLORS.red : 'white' }]}>
        //                                     <Icon name={"videocam-outline"} size={24} color={COLORS.black} />
        //                                 </TouchableOpacity>
        //                             </Animated.View>
        //                             <Animated.View style={styles.bottomRight}>
        //                                 <TouchableOpacity onPress={this.toggleChat} style={[styles.btn, {
        //                                 }]}>
        //                                     <View style={{ marginLeft: this.state.isChatActive ? 60 : 0 }}>
        //                                         <Icon name={!this.state.isChatActive ? 'chatbubbles-outline' : 'chevron-back-outline'} size={24} color={COLORS.black} />
        //                                     </View>
        //                                 </TouchableOpacity>
        //                             </Animated.View>
        //                         </Animated.View>
        //                         <Animated.View style={[styles.animatedWrapper, chat, iwindowOpacity]}>
        //                             <ChatBox
        //                                 masked={true}
        //                                 messages={this.state.messages}
        //                                 inputValue={this.props.app.local.chatInputText}
        //                                 onInputFocus={this.onInputFocus}
        //                                 onChangeText={this.setChatInputText}
        //                                 onSend={this.onMessageSend}
        //                                 dimensions={this.state.dimensions}
        //                                 leftButtonAction={this.toggleChat}
        //                             ></ChatBox>
        //                         </Animated.View>
        //                     </KeyboardAvoidingView>
        //                 </SafeAreaView>
        //                 {/* <View style={styles.exitButtonContainer}>
        //                     <TButton
        //                         text={translate("screen.hostStream.stopStream")}
        //                         notStretch={true}
        //                         onPress={this.wannaStop}
        //                         mini={true}
        //                         type={'danger'}
        //                     />
        //                 </View>
        //                 <View style={styles.recordingContainer}>
        //                     <Animated.View style={[liveIndicatorStyles, styles.recordingContainerIndicator]}></Animated.View>
        //                     <Text style={styles.recordingContainerText}>{this.state.recording.text}</Text>
        //                 </View>
        //                 <View style={styles.userCountContainer}>
        //                     <Icon name={'eye'} size={24} color={'white'} />
        //                     <Text style={styles.userCountText}>{this.state.userCount}</Text>
        //                 </View>

        //                 <View style={styles.messageListBox}>
        //                     <LinearGradient colors={['#00000000', '#00000000', '#00000000', '#00000055', '#00000088', '#000000af',]} style={[styles.linearGradientBack, checkChat]}>
        //                     </LinearGradient>
        //                     <MaskedView style={[styles.messageListMask, checkChat]}
        //                         maskElement={
        //                             <LinearGradient colors={['#FFFFFF00', '#FFFFFF00', '#FFFFFF00', '#FFFFFF00', '#FFFFFF00', '#FFFFFF00', '#FFFFFF00', '#FFFFFF', '#FFFFFF', '#FFFFFF']} style={styles.linearGradient}>
        //                             </LinearGradient>}
        //                     >
        //                         <FlatList
        //                             // inverted
        //                             ref={ref => (this.FlatListRef = ref)}
        //                             onContentSizeChange={() => this.FlatListRef.scrollToEnd()}
        //                             data={this.state.messages}
        //                             style={styles.messageList}
        //                             renderItem={this.renderMessage}
        //                             keyExtractor={item => item.id}
        //                             contentContainerStyle={{ paddingTop: Dimensions.get('window').height / 2 + 222, paddingBottom: 0 }}
        //                         />
        //                     </MaskedView>
        //                     <View style={styles.buttonContainer}>
        //                         <TButton
        //                             icon="share-outline"
        //                             notStretch={true}
        //                             type='secondary'
        //                             isCircled={true}
        //                             onPress={this.onShare}
        //                         />
        //                         <TButton
        //                             icon={this.state.isCameraSwitched ? "camera-reverse-outline" : "camera-reverse"}
        //                             notStretch={true}
        //                             isCircled={true}
        //                             onPress={this.onSwitchCamera}
        //                         />
        //                         <TButton
        //                             icon={"mic-circle-outline"}
        //                             notStretch={true}
        //                             isCircled={true}
        //                             onPress={this.onMicVolumeUp}
        //                             type={this.state.isMicVolumeUp ? 'primary' : 'danger'}
        //                         />
        //                         <TButton
        //                             icon={"mic-outline"}
        //                             notStretch={true}
        //                             isCircled={true}
        //                             onPress={this.onSwitchAudio}
        //                             type={this.state.isAudioActive ? 'primary' : 'danger'}
        //                         />
        //                         <TButton
        //                             icon={"videocam-outline"}
        //                             notStretch={true}
        //                             isCircled={true}
        //                             onPress={this.onSwitchVideo}
        //                             type={this.state.isVideoActive ? 'primary' : 'danger'}
        //                         />
        //                         <TButton
        //                             icon={this.state.isChatActive ? "chevron-down-outline" : "chevron-up-outline"}
        //                             notStretch={true}
        //                             isCircled={true}
        //                             onPress={this.onSwitchChat}
        //                         // type={this.state.isVideoActive ? 'primary' : 'danger'}
        //                         />
        //                     </View>
        //                 </View> */}

        //             </>
        //         )
        //         }
        //     </>
        // );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
    },
    streamBox: {
        height: '100%',
        width: '100%',
        backgroundColor: '#f5f5f5',
        // paddingTop: StatusBarManager.HEIGHT,
        position: 'absolute',
    },
    streamBox_safetyView: {
        flex: 1,
        borderColor: "gray",
        position: 'relative'
    },
    streamBox_keyboardAvoidingView: {
        flex: 1,
        position: 'relative'
    },
    animatedWrapper: {
        flex: 1,
        height: '100%',
        width: '100%',
        position: 'absolute'
    },
    topGrad: {
        position: 'absolute',
        top: 0,
        height: StatusBarManager.HEIGHT,
        width: '100%'
    },
    botGrad: {
        position: 'absolute',
        bottom: 0,
        height: StatusBarManager.HEIGHT - 10,
        width: '100%'
    },
    fullscreen: {
        position: 'relative',
        alignItems: 'center',
        justifyContent: 'center',

        // width: dimensions.width,
        width: '100%',
        // height: dimensions.height,
        height: '100%',
    },
    topLeft: {
        position: 'absolute',
        top: 16,
        left: 16,
        flexDirection: 'row',
        // backgroundColor: 'green',
        zIndex: 120
    },
    topRight: {
        position: 'absolute',
        top: 16,
        right: 16,
        flexDirection: 'row',
        // backgroundColor: 'green',
        zIndex: 120
    },
    bottomLeft: {
        position: 'absolute',
        bottom: 16,
        left: 16,
        flexDirection: 'row',
        zIndex: 20
    },
    bottomRight: {
        position: 'absolute',
        bottom: 16,
        right: 16,
        flexDirection: 'row',
        height: 48,
        zIndex: 20
    },
    btn: {
        width: 48,
        height: 48,
        backgroundColor: 'white',
        borderRadius: 12,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        elevation: 2,
        shadowColor: 'black',
        shadowOffset: {
            width: 0,
            height: 0
        },
        shadowOpacity: 0.1,
        shadowRadius: 4,
    },
    exitButtonContainer: {
        flexDirection: 'row',
        position: 'absolute',
        zIndex: 100,
        top: 40,
        right: 16
    },
    userCountContainer: {
        flexDirection: 'row',
        zIndex: 20,
        backgroundColor: '#00000044',
        paddingVertical: 4,
        paddingHorizontal: 8,
        borderRadius: 8,
        alignItems: 'center',
        // marginHorizontal: 10,
        color: '#fff',
    },
    userCountText: {
        fontSize: 14,
        marginLeft: 6,
        color: '#fff'
    },
    exitButton: {
        width: 160,
        marginBottom: 50,
        paddingVertical: 8,
        borderRadius: 8,
        alignItems: 'center',
        marginHorizontal: 10,
        color: '#fff',
        backgroundColor: 'red'
    },
    exitButtonText: {
        fontSize: 17,
        color: '#fff',
    },
    buttonContainer: {
        flexDirection: 'row',
        position: 'absolute',
        width: Dimensions.get('window').width,
        justifyContent: 'space-between',
        backgroundColor: '#00000055',
        bottom: 0,
        width: '100%',
        padding: 16,
        // marginVertical: 20
    },
    recordingContainer: {
        flexDirection: 'row',
        // position: 'absolute',
        // top: 90,
        // width: 100,
        // right: 16,
        backgroundColor: '#00000044',
        paddingVertical: 4,
        paddingHorizontal: 8,
        borderRadius: 8,
        justifyContent: 'flex-end',
        alignItems: 'center',
        // marginHorizontal: 10,
        color: '#fff',
    },
    recordingContainerIndicator: {
        width: 8,
        height: 8,
        borderRadius: 8,
        backgroundColor: 'red',
        marginRight: 8
    },
    recordingContainerText: {
        color: '#fff',
        fontSize: 14,
    },
    linearGradient: {
        flex: 1,
        width: '100%',
        borderRadius: 5
    },
    linearGradientBack: {
        position: 'absolute',
        width: '100%',
        height: '100%',
    },
});
