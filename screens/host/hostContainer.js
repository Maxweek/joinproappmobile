import React, { Component } from "react";
import { connect } from "react-redux";
import Host from "./host";

class HostContainer extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <Host
                route={this.props.route}
                navigation={this.props.navigation}
                app={this.props.app}
            />
        );
    }
}

const mapStateToProps = (state) => {
    return {
        app: state.app,
    };
};

const mapDispatchToProps = {
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HostContainer);
