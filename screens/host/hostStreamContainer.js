import React, { Component } from "react";
import {
    setChannel,
    setAppTabBarVisibility,
    setChatInputText
} from "../../store/user/actions";
import { connect } from "react-redux";
import HostStream from "./hostStream";

class HostStreamContainer extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <HostStream
                route={this.props.route}
                navigation={this.props.navigation}
                setAppTabBarVisibility={this.props.setAppTabBarVisibility}
                setChatInputText={this.props.setChatInputText}
                app={this.props.app}
            />
        );
    }
}

const mapStateToProps = (state) => {
    return {
        app: state.app,
    };
};

const mapDispatchToProps = {
    setAppTabBarVisibility,
    setChatInputText
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HostStreamContainer);
