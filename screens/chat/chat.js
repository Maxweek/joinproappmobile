import React, { Component } from "react";
import {
    Text,
    View,
    StyleSheet
} from "react-native";
import { GiftedChat } from "react-native-gifted-chat";
import Shared_EmptyList from "../_shared/emptyList";

export default class Chat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            isActiveBtn: true,
        }
    }
    componentDidMount() {
        this.setMessages([
            {
                _id: 1,
                text: 'Hello developer',
                createdAt: new Date(),
                user: {
                    _id: 2,
                    name: 'React Native',
                    avatar: 'https://placeimg.com/140/140/any',
                },
            },
        ])
    }
    setMessages = (messages) => {
        this.setState({ messages })
    }
    onSend = (messages = []) => {
        // console.log(messages)
        // console.log(this.state.messages)
        // let mes = [...this.state.messages]
        // mes.push({
        //     _id: this.state.messages[this.state.messages.length] + 1,
        //     text: 'Hello developer',
        //     createdAt: new Date(),
        //     user: {
        //         _id: 1,
        //         name: 'React Native',
        //         avatar: 'https://placeimg.com/140/140/any',
        //     },
        // },);
        // this.setMessages(mes)
        // console.log(previousMessages => GiftedChat.append(previousMessages, messages))
        this.setState((previousState) => ({
            messages: GiftedChat.append(previousState.messages, messages)
        }))
    }
    quickReply = (messages = []) => {
        this.setState((previousState) => ({
            messages: GiftedChat.append(previousState.messages, messages)
        }))
    }
    render() {
        return (
            // <View style={styles.container}>
            //     <View style={styles.inner}>
            //         <View style={styles.main}>
            //             {/* <Shared_EmptyList
            //                 title='Сообщения'
            //                 subtitle="Этот блок в разработке..."
            //                 actionText={this.state.isActiveBtn ? "Понял" : "Принял"}
            //                 onPress={() => { this.setState({ isActiveBtn: !this.state.isActiveBtn }) }}
            //             /> */}
            //         </View>
            //     </View>
            // </View>
            <GiftedChat
            messages={this.state.messages}
            onSend={messages => this.onSend(messages)}
            onQuickReply={quickReply => this.onQuickReply(quickReply)}
            user={{
              _id: 1,
            }}
          />
        );
    }
}
const styles = StyleSheet.create({
    container: {
        top: 0,
        flex: 1,
        backgroundColor: "#fff",
        position: "relative",
    },
    inner: {
        alignItems: "center",
        justifyContent: 'space-around',
        height: "100%",
    },
    main: {
        alignItems: 'center'
    },
    title: {
        fontSize: 27,
    },
});
