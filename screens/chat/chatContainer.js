import React, { Component } from "react";
// import {} from "../../store/user/actions";
import { connect } from "react-redux";
import Chat from "./chat";

class ChatContainer extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Chat
        route={this.props.route}
        navigation={this.props.navigation}
        user={this.props.state}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    state: state.user
  };
};

const mapDispatchToProps = {
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChatContainer);
