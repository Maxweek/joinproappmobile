import React, { Component } from "react";
import {
    Text,
    View,
    StyleSheet,
    ActivityIndicator,
    ScrollView,
    RefreshControl
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import { COLORS } from "../../assets/styles/styles";
import Shared_EmptyList from "../_shared/emptyList";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import FloatingLabelInput from "../../components/floatingLabelInput";
import HeaderBox from "../../components/headerBox";

export default class Dev extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isActiveBtn: true,
            refreshing: false
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <HeaderBox
                    gradient={true}
                    scheme={'dark'}
                    shadow={false}
                    right={[{
                        icon: 'settings-outline', action: () => { this.props.navigation.navigate('profileSettingsScreen') }
                    }
                    ]}
                />
                {/* <View style={styles.inner}> */}
                    {/* <View style={styles.main}> */}
                        <KeyboardAwareScrollView 
                        refreshControl={<RefreshControl
                            colors={["blue", "#f22424"]}
                            refreshing={this.state.refreshing}
                            onRefresh={e => {}}
                        />}
                        style={{ backgroundColor: 'pink' }}>

                            <View style={styles.payScreen}>
                                <ActivityIndicator
                                    size={80}
                                    color={COLORS.primary}
                                />
                                <Text style={styles.title}>Операция выполняется</Text>
                                <Text style={styles.description}>Мы ожидаем подтверждения операции от платежной системы. Обычно это занимает несколько минут.</Text>
                                <Text style={styles.info}>Сервис предоставлен системой «Монета».
Если столкнулись с проблемой, обратитесь в службу поддержки.</Text>
                            </View>
                            <View style={styles.payScreen}>
                                <Icon name="refresh-circle" size={80} color={COLORS.alert} />

                                <Text style={styles.title}>Операция выполняется</Text>
                                <Text style={styles.description}>Мы ожидаем подтверждения операции от платежной системы. Обычно это занимает несколько минут.</Text>
                                <Text style={styles.info}>Сервис предоставлен системой «Монета».
Если столкнулись с проблемой, обратитесь в службу поддержки.</Text>
                            </View>
                            {/* <View style={styles.payScreen}>
                                <Icon name="alert-circle" size={80} color={COLORS.danger} />

                                <Text style={styles.title}>Операция выполняется</Text>
                                <Text style={styles.description}>Мы ожидаем подтверждения операции от платежной системы. Обычно это занимает несколько минут.</Text>
                                <Text style={styles.info}>Сервис предоставлен системой «Монета».
Если столкнулись с проблемой, обратитесь в службу поддержки.</Text>
                            </View>
                            <View style={styles.payScreen}>
                                <Icon name="checkmark-circle" size={80} color={COLORS.primary} />

                                <Text style={styles.title}>Операция выполняется</Text>
                                <Text style={styles.description}>Мы ожидаем подтверждения операции от платежной системы. Обычно это занимает несколько минут.</Text>
                                <Text style={styles.info}>Сервис предоставлен системой «Монета».
Если столкнулись с проблемой, обратитесь в службу поддержки.</Text>
                            </View> */}
                            <FloatingLabelInput></FloatingLabelInput>
                        </KeyboardAwareScrollView>

                    {/* </View> */}
                {/* </View> */}
            </View>
        );

    }
}
const styles = StyleSheet.create({
    container: {
        top: 0,
        flex: 1,
        backgroundColor: "#fff",
        position: "relative",
    },
    inner: {
        alignItems: "center",
        justifyContent: 'space-around',
        height: "100%",
        width: '100%'
    },
    main: {
        
        alignItems: 'center'
    },
    title: {
        fontSize: 24,
        fontWeight: "600",
        marginTop: 8
    },
    payScreen: {
        backgroundColor: 'yellow',
        alignItems: 'center',
        textAlign: 'center',
        paddingHorizontal: 16,
        paddingVertical: 24
    },
    description: {
        marginTop: 4,
        textAlign: 'center'
    },
    info: {
        marginTop: 16,
        textAlign: 'center',
        color: COLORS.gray
    }

});
