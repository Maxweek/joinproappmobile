import React, { Component } from "react";
import {
    Text,
    View,
    StyleSheet
} from "react-native";
import Shared_EmptyList from "../_shared/emptyList";

export default class Likes extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            isActiveBtn: true,
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.inner}>
                    <View style={styles.main}>
                        <Shared_EmptyList
                            title='Закладки'
                            subtitle="Этот блок в разработке..."
                            actionText={this.state.isActiveBtn ? "Понял" : "Принял"}
                            onPress={() => { this.setState({ isActiveBtn: !this.state.isActiveBtn }) }}
                        />
                    </View>
                </View>
            </View>
        );

    }
}
const styles = StyleSheet.create({
    container: {
        top: 0,
        flex: 1,
        backgroundColor: "#fff",
        position: "relative",
    },
    inner: {
        alignItems: "center",
        justifyContent: 'space-around',
        height: "100%",
    },
    main: {
        alignItems: 'center'
    },
    title: {
        fontSize: 27,
    },
});
