import React, { Component } from "react";
import {
    Text,
    View,
    StyleSheet,
    NativeModules,
    SafeAreaView,
    ScrollView,
    FlatList,
    Dimensions,
    Image
} from "react-native";
import { translate } from "../../App";
import API, { API_IMPRESSION_LIST } from "../../API";
import ContainerBox from "../../components/containerBox";
import HeaderBox from "../../components/headerBox";
import List_ComponentImpressions from "../impressions/components/list";
import Card from "../../components/card";
import TButton from "../../components/tButton";
import SliderBox from "../../components/sliderBox";
import ModuleBox from "../../components/ModuleBox";
import { COLORS } from "../../assets/styles/styles";
import { TouchableOpacity } from "react-native-gesture-handler";
import BannerBox from "../../components/BannerBox";

const { StatusBarManager } = NativeModules;

export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isListenerAdded: false,
            isActiveBtn: true,
            refreshing: false,
            items: [],
            modal: {
                isModalActive_sort: false,
                isModalActive_filter: false,
            },
            isContentLoaded: false,
            instances: [
                {
                    items: [],
                    filters: [{
                        name: 'filter_date_list',
                        title: 'Списки событий',
                        type: 'checkbox',
                        options: [],
                        data: [
                            {
                                type: 'simple',
                                key: 'past',
                                value: 'Недавние трансляции',
                                isActive: false
                            },
                            {
                                type: 'simple',
                                key: 'coming',
                                value: 'Предстоящие трансляции',
                                isActive: true
                            }
                        ]

                    }]
                },
                {
                    items: [],
                    filters: [{
                        name: 'filter_date_list',
                        title: 'Списки событий',
                        type: 'checkbox',
                        options: [],
                        data: [
                            {
                                type: 'simple',
                                key: 'past',
                                value: 'Недавние трансляции',
                                isActive: true
                            },
                            {
                                type: 'simple',
                                key: 'coming',
                                value: 'Предстоящие трансляции',
                                isActive: false
                            }
                        ]

                    }]
                },
            ]
        }

    }

    getData = () => {
        let instances = this.state.instances;

        let i = 0;
        instances.map(el => {
            this.getList(el, i)
            i++
        })
    }

    getList = (instance, i) => {
        let instances = this.state.instances;

        const formData = new FormData();
        console.log(i)
        console.log(instance)
        formData.append('filters', JSON.stringify(instance.filters))
        formData.append('options', JSON.stringify([{ language_code: 'ru' }]))

        API.post(API_IMPRESSION_LIST, formData)
            .then(res => {
                console.log(res)
                if (typeof res.data.status !== 'undefined') {
                    if (res.data.status === 'success') {
                        console.log(res.data.impressions)
                        // this.setList(res.data.impressions);
                        instances[i].items = res.data.impressions
                        this.setState({
                            refreshing: false,
                            isNotEmpty: true,
                            isContentLoaded: true,
                            instances: instances,
                        });
                    } else {
                        console.log('list_load_error_1')
                        this.setState({ refreshing: false, isNotEmpty: false });
                    }
                } else {
                    console.log('list_load_error_2')
                    this.setState({ refreshing: false, isNotEmpty: false });
                }
            })
            .catch(err => {
                console.log(err)
                this.setState({ refreshing: false, isNotEmpty: false });
            })
    }
    setList = list => {
        console.log('list setted')
        // this.props.setImpressionList(list)
        console.log(this.state.items)

    }
    onRefresh = () => {
        this.setState({ refreshing: true })
        this.getData();

        // this.getFilter()
    }
    componentDidMount() {
        this.addListener();
        this.onRefresh()
        // this.getList();
    }
    addListener = () => {
        if (!this.state.isListenerAdded) {
            this.unsubscribe = this.props.navigation.addListener('focus', () => {
                this.props.setAppTabBarVisibility(true)
                this.getData();
            });
            this.setState({ isListenerAdded: true })
        }
    }
    viewImpression = (item) => {
        this.props.navigation.navigate('impressionViewNavigator', {
            screen: "viewImpressionScreen",
            params: {
                id: item.id
            }
        })
    }

    render() {
        return (
            <>
                <ContainerBox
                    navigation={this.props.navigation}
                    isContentLoaded={this.state.isContentLoaded}
                    refreshing={this.state.refreshing}
                    onRefresh={this.onRefresh}
                    
                >
                    <ModuleBox
                        title={translate('impression.coming')}
                        action={() => {
                            this.props.navigation.navigate('listImpressionScreen', {
                                filters: this.state.instances[0].filters,
                                title: translate('impression.coming'),
                            });
                        }}
                        actionText={translate('default.seeAll')}
                        actionIcon={'chevron-forward'}
                        isLoaded={true}
                    >
                        <SliderBox
                            items={this.state.instances[0].items}
                            onItemPress={this.viewImpression}
                        />
                    </ModuleBox>
                    <ModuleBox
                        title={translate('impression.past')}
                        action={() => {
                            this.props.navigation.navigate('listImpressionScreen', {
                                filters: this.state.instances[1].filters,
                                title: translate('impression.past')
                            });
                        }}
                        actionText={translate('default.seeAll')}
                        actionIcon={'chevron-forward'}
                        isLoaded={true}
                    >
                        <SliderBox
                            items={this.state.instances[1].items}
                            onItemPress={this.viewImpression}
                        />
                    </ModuleBox>

                    {/* <BannerBox id={3213} /> */}
                    {/* <ModuleBox
                        title={'Впечатленьки'}
                        action={() => { this.props.navigation.navigate('impressionsScreen') }}
                        actionText={'Смотреть все'}
                        actionIcon={'chevron-forward'}
                        isLoaded={!this.state.refreshing}
                    >
                        <SliderBox
                            items={this.state.items}
                            onItemPress={this.viewImpression}
                        />
                    </ModuleBox> */}
                </ContainerBox>
            </>
        );

    }
}
const styles = StyleSheet.create({
    container: {
        top: 0,
        flex: 1,
        backgroundColor: "#fff",
        position: "relative",
    },
    inner: {
        alignItems: "center",
        justifyContent: 'space-around',
        height: "100%",

        paddingTop: StatusBarManager.HEIGHT + 60
    },
    main: {
        alignItems: 'center',
        // paddingTop: 60
    },
    title: {
        fontSize: 27,
    },
    box: {
        padding: 16
    },

});
