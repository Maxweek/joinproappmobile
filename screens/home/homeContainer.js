import React, { Component } from "react";
// import {} from "../../store/user/actions";
import { connect } from "react-redux";
import { setAppTabBarVisibility, setImpressionList } from "../../store/user/actions";
import Home from "./home";

class HomeContainer extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Home
        route={this.props.route}
        navigation={this.props.navigation}
        app={this.props.app}
        setImpressionList={this.props.setImpressionList}
        setAppTabBarVisibility={this.props.setAppTabBarVisibility}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    app: state.app,
  };
};

const mapDispatchToProps = {
  setImpressionList,
  setAppTabBarVisibility
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeContainer);
