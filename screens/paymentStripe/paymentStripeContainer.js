import React, { Component } from "react";
// import {} from "../../store/user/actions";
import { connect } from "react-redux";
import PaymentStripe from "./paymentStripe";

class PaymentStripeContainer extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <PaymentStripe
        route={this.props.route}
        navigation={this.props.navigation}
        user={this.props.state}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    state: state.user
  };
};

const mapDispatchToProps = {
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PaymentStripeContainer);
