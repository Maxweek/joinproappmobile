import React, { PureComponent } from 'react'
import { View, Text, StyleSheet, StatusBar, ScrollView } from 'react-native'
import TButton from "../../components/tButton";

// import { demoCardFormParameters } from '../../TIPSY_COMPONENTS/scenes/demodata/demodata'

import { StripeProvider } from '@stripe/stripe-react-native';

function PaymentStripe() {
  return (
    <StripeProvider
      publishableKey={"pk_test_cWoBmAdxLVg9Bz04kVaolNcD"}
      merchantIdentifier="merchant.identifier"
    >
      <PaymentScreen />

    </StripeProvider>
  );
}


// PaymentScreen.ts
import { CardField, useStripe } from '@stripe/stripe-react-native';
import { useState } from 'react';
import API from "../../API";

function PaymentScreen() {
  const { confirmPayment } = useStripe();

  const [cardDetails, setCardDetails] = useState({ complete: false });
  const [paymentError, setPaymentError] = useState({ localizedMessage: '' });

  const handlePayPress = async () => {
    if (cardDetails.complete) {
      console.log(cardDetails)

      const formData = new FormData();

      formData.append('amount', 55);
      formData.append('currency', 'usd');

      let response = await API.post('/payment/stripe/paymentIntents/create/', formData)

      console.log(response)

      const billingDetails = {
        email: 'roadToTheMaldives@example.com',
      };

      // Fetch the intent client secret from the backend
      const clientSecret = response.data.client_secret

      // Confirm the payment with the card details
      const { paymentIntent, error } = await confirmPayment(clientSecret, {
        type: 'Card',
        billingDetails,
      });

      if (error) {
        console.log('Payment confirmation error', error);
        setPaymentError(error)
      } else if (paymentIntent) {
        console.log('Success from promise', paymentIntent);
        setPaymentError({ localizedMessage: '' })
      }
    }
  }

  return (
    <View style={styles.container}>
      <View style={styles.inner}>
        <ScrollView style={styles.main}>
          <View style={styles.box}>

            <CardField
              postalCodeEnabled={false}
              placeholder={{
                number: '4242 4242 4242 4242',
              }}
              cardStyle={{
                backgroundColor: '#FFFFFF',
                textColor: '#000000',
              }}
              style={{
                width: '100%',
                height: 50,
                marginVertical: 30,
              }}
              onCardChange={(cardD) => {
                setCardDetails(cardD)
                console.log('cardDetails', cardD);
              }}
              onFocus={(focusedField) => {
                console.log('focusField', focusedField);
              }}
            />
            <TButton
              onPress={() => { handlePayPress() }}
              text={'Забашлять'}
              type="primary"
            />
            <Text>Ошибка оплаты: {paymentError.localizedMessage}</Text>
          </View>
        </ScrollView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
    minHeight: "100%",
    minHeight: 600,
    flex: 1,
    backgroundColor: "#fff",
    position: "relative",
  },
  inner: {
    height: "100%",
    position: 'relative',
    paddingTop: StatusBar.currentHeight
  },
  main: {
    height: '100%'
  },
  box: {
    padding: 16
  },
  
})

export default PaymentStripe;
