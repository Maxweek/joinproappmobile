import React, { Component } from "react";
import { connect } from "react-redux";
import {
    setUserBirthday,
    setUserName,
    setUserLastName,
    setUserImage,
    setUserCity,
    setUserGender,
    setUserLang,
} from "../../../store/user/actions";
import ProfileSettings from "./settings_profile";
// import {
//     setUserBirthdate
// } from "../../../store/user/actions";


class ProfileSettingsContainer extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <ProfileSettings
                route={this.props.route}
                navigation={this.props.navigation}
                app={this.props.app}
                setUserBirthday={this.props.setUserBirthday}
                setUserName={this.props.setUserName}
                setUserLastName={this.props.setUserLastName}
                setUserImage={this.props.setUserImage}
                setUserCity={this.props.setUserCity}
                setUserGender={this.props.setUserGender}
                setUserLang={this.props.setUserLang}
            />
        );
    }
}

const mapStateToProps = (state) => {
    return {
        app: state.app
    };
};

const mapDispatchToProps = {
    setUserBirthday,
    setUserName,
    setUserLastName,
    setUserImage,
    setUserCity,
    setUserGender,
    setUserLang,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProfileSettingsContainer);
