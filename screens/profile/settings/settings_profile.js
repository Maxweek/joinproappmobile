import I18n from "i18n-js";
import React, { Component } from "react";
import {
    Text,
    View,
    StyleSheet,
    Dimensions,
    ScrollView,
    Image,
    TouchableOpacity,
    KeyboardAvoidingView,
} from "react-native";

import { Platform, NativeModules } from 'react-native';
import { COLORS } from "../../../assets/styles/styles";
import FloatingLabelInput from "../../../components/floatingLabelInput";
import HeaderBox from "../../../components/headerBox";
import Icon from "react-native-vector-icons/Ionicons";
import { launchCamera, launchImageLibrary } from "react-native-image-picker";
import SelectPicker from "../../../components/selectPicker";
import { setLanguageCode, translate } from "../../../App";
import ContainerBox from "../../../components/containerBox";
const { StatusBarManager } = NativeModules;

export default class ProfileSettings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            imageUri: null,
            selectedLang: this.props.app.user.info.lang === null ? this.getLang(I18n.locale) : this.getLang(this.props.app.user.info.lang)
        }
        console.log(this.state.selectedLang)
    }
    componentDidMount() {
        // this.onRefresh()
    }
    componentDidUpdate() {
        // this.streamCheck()
    }
    switchLang = (targetLang) => {
        console.log("targetLang")
        console.log(targetLang)

        this.props.setUserLang(targetLang)
        setLanguageCode()
    }
    setImage = () => {
        // console.log(styles);
        launchImageLibrary(
            {
                mediaType: 'photo',
                includeBase64: true,
                maxHeight: 720,
                maxWidth: 720,
            },
            response => {
                console.log(response);
                // this.setState({
                //     imageUri: response.uri
                // })
                this.props.setUserImage(response.uri)
                // this.props.setNewImpressionImage(response)
            }
        )
    }
    getLang = (lang) => {
        console.log("LANG - " + lang)
        let list = this.props.app.local.languages;
        let current = {};
        list.map(el => {
            if (el.value === lang) {
                current = el
            }
        })

        console.log(current)
        return current;
    }
    save = () => {
        console.log(this.state.selectedLang.value)
        this.switchLang(this.state.selectedLang.value)

        this.props.navigation.goBack();
    }
    setLang = (targetLang) => {
        this.setState({
            selectedLang: this.getLang(targetLang)
        })
    }
    render() {
        console.log(this.state)
        let showFields = false;

        return (
            <ContainerBox
                navigation={this.props.navigation}
                header={
                    <HeaderBox
                        navigation={this.props.navigation}
                        back={true}
                        title={translate("screen.auth.settings")}
                        shadow={true}
                        right={[{ text: translate('default.save'), action: this.save, color: COLORS.primary }]}
                    />
                }>

                {/* <View style={styles.container}> */}
                    {/* {this.props.app.local.isLoggedIn ? */}
                    {showFields ?
                        <>
                            <View style={styles.box}>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>

                                    <View style={{ overflow: 'hidden', width: 60, height: 60, borderRadius: 50, borderColor: '#dedede', borderWidth: 2, borderStyle: 'solid', marginRight: 20 }}>
                                        {this.props.app.user.info.photo ?
                                            <Image
                                                style={{ width: 56, height: 56, marginLeft: 0, marginTop: 0 }}
                                                source={{ uri: this.props.app.user.info.photo }}
                                            />
                                            : null}
                                    </View>
                                    <View style={{ flexGrow: 1 }}>
                                        <TouchableOpacity onPress={() => { this.setImage() }}>
                                            <Text>{translate("screen.profileEdit.changeImage")}</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View>
                                        <TouchableOpacity onPress={() => { }}>
                                            <Icon name={'trash-outline'} size={24} color={COLORS.gray} />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.box}>
                                <FloatingLabelInput
                                    label={translate("user.name")}
                                    value={this.props.app.user.info.name}
                                    onChangeText={this.props.setUserName}
                                />
                            </View>
                            <View style={styles.box}>
                                <FloatingLabelInput
                                    label={translate("user.lastName")}
                                    value={this.props.app.user.info.last_name}
                                    onChangeText={this.props.setUserLastName}
                                />
                            </View>
                            <View style={styles.box}>
                                <FloatingLabelInput
                                    mask="99.99.9999"
                                    maskType="custom"
                                    label={translate("user.birthday")}
                                    value={this.props.app.user.info.birthday}
                                    onChangeText={this.props.setUserBirthday}
                                    onComplete={() => { }}
                                    onInComplete={() => { }}
                                    keyboardType="number-pad"
                                />
                            </View>
                            <View style={styles.box}>
                                <Text style={{ marginLeft: 14, color: COLORS.gray, fontSize: 12, marginBottom: 4 }}>{translate("user.gender")}</Text>
                                <SelectPicker
                                    onValueChange={(value) => { this.props.setUserGender(value) }}
                                    // placeholder={'Выберите валюту'}
                                    value={this.props.app.user.info.gender}
                                    items={[{
                                        label: translate("default.genderUndefined"),
                                        value: 0
                                    }, {
                                        label: translate("default.genderMale"),
                                        value: 1,
                                    }, {
                                        label: translate("default.genderFemale"),
                                        value: 2,
                                    }]}
                                />
                            </View>
                            <View style={styles.box}>
                                <FloatingLabelInput
                                    label={translate("user.city")}
                                    value={this.props.app.user.info.city}
                                    onChangeText={this.props.setUserCity}
                                    onComplete={() => { }}
                                    onInComplete={() => { }}
                                />
                            </View>
                        </>
                        :
                        null}
                    <View style={styles.box}>
                        <Text style={{ marginLeft: 14, color: COLORS.gray, fontSize: 12, marginBottom: 4 }}>{translate("user.language")}</Text>
                        <SelectPicker
                            onValueChange={this.setLang}
                            // placeholder={'Выберите валюту'}
                            value={this.state.selectedLang.value}
                            items={this.props.app.local.languages}
                        />
                    </View>
                {/* </View> */}

            </ContainerBox>
            // <View style={styles.container}>
            //     <HeaderBox
            //         navigation={this.props.navigation}
            //         back={true}
            //         title={translate("screen.auth.settings")}
            //         shadow={true}
            //         right={[{ text: translate('default.save'), action: this.save, color: COLORS.primary }]}
            //     />
            //     <KeyboardAvoidingView style={styles.inner} behavior={Platform.OS === 'android' ? "height" : "padding"} keyboardVerticalOffset={0}>
            //         <ScrollView style={styles.main} keyboardDismissMode={Platform.OS === 'android' ? "on-drag" : "on-drag"}>
            //             {/* {this.props.app.local.isLoggedIn ? */}
            //             {showFields ?
            //                 <>
            //                     <View style={styles.box}>
            //                         <View style={{ flexDirection: 'row', alignItems: 'center' }}>

            //                             <View style={{ overflow: 'hidden', width: 60, height: 60, borderRadius: 50, borderColor: '#dedede', borderWidth: 2, borderStyle: 'solid', marginRight: 20 }}>
            //                                 {this.props.app.user.info.photo ?
            //                                     <Image
            //                                         style={{ width: 56, height: 56, marginLeft: 0, marginTop: 0 }}
            //                                         source={{ uri: this.props.app.user.info.photo }}
            //                                     />
            //                                     : null}
            //                             </View>
            //                             <View style={{ flexGrow: 1 }}>
            //                                 <TouchableOpacity onPress={() => { this.setImage() }}>
            //                                     <Text>{translate("screen.profileEdit.changeImage")}</Text>
            //                                 </TouchableOpacity>
            //                             </View>
            //                             <View>
            //                                 <TouchableOpacity onPress={() => { }}>
            //                                     <Icon name={'trash-outline'} size={24} color={COLORS.gray} />
            //                                 </TouchableOpacity>
            //                             </View>
            //                         </View>
            //                     </View>
            //                     <View style={styles.box}>
            //                         <FloatingLabelInput
            //                             label={translate("user.name")}
            //                             value={this.props.app.user.info.name}
            //                             onChangeText={this.props.setUserName}
            //                         />
            //                     </View>
            //                     <View style={styles.box}>
            //                         <FloatingLabelInput
            //                             label={translate("user.lastName")}
            //                             value={this.props.app.user.info.last_name}
            //                             onChangeText={this.props.setUserLastName}
            //                         />
            //                     </View>
            //                     <View style={styles.box}>
            //                         <FloatingLabelInput
            //                             mask="99.99.9999"
            //                             maskType="custom"
            //                             label={translate("user.birthday")}
            //                             value={this.props.app.user.info.birthday}
            //                             onChangeText={this.props.setUserBirthday}
            //                             onComplete={() => { }}
            //                             onInComplete={() => { }}
            //                             keyboardType="number-pad"
            //                         />
            //                     </View>
            //                     <View style={styles.box}>
            //                         <Text style={{ marginLeft: 14, color: COLORS.gray, fontSize: 12, marginBottom: 4 }}>{translate("user.gender")}</Text>
            //                         <SelectPicker
            //                             onValueChange={(value) => { this.props.setUserGender(value) }}
            //                             // placeholder={'Выберите валюту'}
            //                             value={this.props.app.user.info.gender}
            //                             items={[{
            //                                 label: translate("default.genderUndefined"),
            //                                 value: 0
            //                             }, {
            //                                 label: translate("default.genderMale"),
            //                                 value: 1,
            //                             }, {
            //                                 label: translate("default.genderFemale"),
            //                                 value: 2,
            //                             }]}
            //                         />
            //                     </View>
            //                     <View style={styles.box}>
            //                         <FloatingLabelInput
            //                             label={translate("user.city")}
            //                             value={this.props.app.user.info.city}
            //                             onChangeText={this.props.setUserCity}
            //                             onComplete={() => { }}
            //                             onInComplete={() => { }}
            //                         />
            //                     </View>
            //                 </>
            //                 :
            //                 null}
            //             <View style={styles.box}>
            //                 <Text style={{ marginLeft: 14, color: COLORS.gray, fontSize: 12, marginBottom: 4 }}>{translate("user.language")}</Text>
            //                 <SelectPicker
            //                     onValueChange={this.setLang}
            //                     // placeholder={'Выберите валюту'}
            //                     value={this.state.selectedLang.value}
            //                     items={this.props.app.local.languages}
            //                 />
            //             </View>
            //         </ScrollView>
            //     </KeyboardAvoidingView>
            // </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        top: 0,
        flex: 1,
        backgroundColor: "#fff",
        position: "relative",
    },
    inner: {
        // alignItems: "center",
        justifyContent: 'space-around',
        height: "100%",
        paddingTop: StatusBarManager.HEIGHT + 60 + 16
    },
    main: {
        // alignItems: 'center'
    },
    box: {
        width: '100%',
        // paddingHorizontal: 16,
        paddingVertical: 16,
    },
})
