import I18n from "i18n-js";
import React, { Component } from "react";
import {
    Text,
    View,
    StyleSheet,
    Button,
    TouchableOpacity,
    Image,
    Dimensions,
    ScrollView,
    RefreshControl,
    StatusBar
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";

import * as RNLocalize from "react-native-localize";
import Shared_EmptyList from "../../_shared/emptyList";
import TButton from "../../../components/tButton";
import { translate } from "../../../App";
import { TabView } from "react-native-tab-view";
import ScrollableTabBar from "../../_shared/scrollableTabBar";
import FitTabBar from "../../_shared/fitTabBar";

import { Platform, NativeModules } from 'react-native';
const { StatusBarManager } = NativeModules;

export default class ProfileInner extends Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            index: 0,
            routes: [
                { key: 'all', title: "Участвую" },
                { key: 'publish', title: "Организую" },
            ]
        }
    }
    componentDidMount() {
        this.onRefresh()
    }
    getList = () => {
        // this.setList(res.data.impressions);

        let { routes } = this.state;

        routes.map(route => {
            let count = 0;
            let items = [];
            // if (route.key === 'all') {
            //     items = res.data.impressions;
            //     count = items.length
            // } else {
            //     items = res.data.impressions.filter(el => el.status === route.key);
            //     count = items.length
            // }
            route.items = items;
            route.count = count;
        })
        this.setState({ refreshing: false, routes, isNotEmpty: true });
    }
    componentDidUpdate() {
        // this.streamCheck()
    }
    _updateTabBar = i => {
        this.setState({ index: i });
    }
    renderScene = ({ route }) => {
        return <Shared_EmptyList
            title={translate("screen.impression.list.empty.title")}
            subtitle={translate("screen.impression.list.empty.subtitle")}
            actionText={translate("screen.impression.list.empty.actionText")}
            onPress={this.createImpression}
        />

    }
    onRefresh = () => {
        this.setState({ refreshing: true })

        console.log('refreshed')
        this.getList();
    }
    render() {
        const {
            index,
            routes
        } = this.state;
        return (
            <View style={styles.container}>
                <View style={styles.inner}>
                    <View style={styles.profileHeader}>
                        <View style={{ overflow: 'hidden', width: 80, height: 80, borderRadius: 50, borderColor: '#dedede', borderWidth: 2, borderStyle: 'solid', marginRight: 20 }}>
                            {this.props.app.user.info.photo ?
                                <Image
                                    style={{ width: '200%', height: '200%', marginLeft: -24, marginTop: -20 }}
                                    source={{ uri: this.props.app.user.info.photo }}
                                />
                                : null}
                        </View>
                        <View>
                            <Text style={{ fontSize: 24, fontWeight: 'bold' }}>
                                {this.props.app.user.info.name} {this.props.app.user.info.last_name}
                            </Text>
                            <Text>Тыща бонусов</Text>
                        </View>
                        <View style={styles.headerActions}>
                            <TouchableOpacity>
                                <Icon name='settings-outline' size={24} color='#000000' />
                            </TouchableOpacity>
                        </View>
                    </View>

                    {this.state.isNotEmpty ?
                        <TabView
                            renderTabBar={(props) => <FitTabBar {...props} index={index} tabBarUpdate={this._updateTabBar} />}
                            navigationState={{ index, routes }}
                            renderScene={this.renderScene}
                            onIndexChange={index => this._updateTabBar(index)}
                            initialLayout={{ width: Dimensions.get('window').width }}
                        />
                        : null}

                    {/* <Shared_EmptyList
                            title={null}
                            image='none'
                            subtitle={translate('default.languageChange')}
                        /> */}
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        top: 0,
        flex: 1,
        backgroundColor: "#fff",
        position: "relative",
    },
    inner: {
        // alignItems: "center",
        justifyContent: 'space-around',
        height: "100%",
        paddingTop: StatusBarManager.HEIGHT
    },
    main: {
        // alignItems: 'center'
    },
    group: {
        marginBottom: 10,
        paddingTop: 10,
        paddingHorizontal: 16,
        borderTopWidth: 1,
        borderTopColor: '#dedede'
    },
    title: {
        // paddingHorizontal: 10,
        marginVertical: 4
    },
    element: {
        flexDirection: 'row',
        alignItems: 'center',
        // paddingHorizontal: 10,
        paddingVertical: 6,
        fontSize: 20
    },
    profileHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        width: Dimensions.get('window').width,
        justifyContent: 'flex-start',
        padding: 16,
        borderBottomWidth: 1,
        // borderBottomStyle: 'solid',
        borderBottomColor: '#dedede',
        position: 'relative'
    },
    headerActions: {
        position: 'absolute',
        right: 16,
        top: 16,
    }
    // title: {
    //     fontSize: 27,
    //     marginBottom: 20
    // },
});
