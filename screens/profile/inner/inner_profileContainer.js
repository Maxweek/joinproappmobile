import React, { Component } from "react";
import { connect } from "react-redux";
import ProfileInner from "./inner_profile";

class ProfileInnerContainer extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <ProfileInner
        route={this.props.route}
        navigation={this.props.navigation}
        app={this.props.app}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    app: state.app
  };
};

const mapDispatchToProps = {
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileInnerContainer);
