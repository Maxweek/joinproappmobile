import I18n from "i18n-js";
import React, { Component } from "react";
import {
    Text,
    View,
    StyleSheet,
    Button,
    TouchableOpacity,
    Image,
    Dimensions,
    ScrollView,
    RefreshControl
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import API, { API_IMPRESSION_STREAM_CHECK, API_USER_INFO, API_USER_LOGOUT, getApiToken, setApiToken } from "../../API";
import TButton from "../../components/tButton";
import Shared_EmptyList from "../_shared/emptyList";
import { Platform, NativeModules } from 'react-native';
const { StatusBarManager } = NativeModules;

import * as RNLocalize from "react-native-localize";
import { setLanguageCode, translate } from "../../App";
import { version } from '../../app.json'
import HeaderBox from "../../components/headerBox";
import ContainerBox from "../../components/containerBox";
import axios from "axios";
export default class Profile extends Component {
    constructor(props) {
        super(props);

        this.handleLogOut = this.handleLogOut.bind(this)
        this.handleLogin = this.handleLogin.bind(this)

        this.state = {
            isActiveBtn: true,
            isListenerAdded: false,
            refreshing: false,
        }

    }
    addListener = () => {

        if (!this.state.isListenerAdded) {
            this.unsubscribe = this.props.navigation.addListener('focus', () => {
                this.refresh()
            });
            this.setState({ isListenerAdded: true })
        }
    }

    getUserInfo = () => {
        API.get(API_USER_INFO)
            .then(res => {
                console.log('GET USER INFO');
                console.log(res);
                console.log(getApiToken());
                if (res.data.status === 'success') {
                    this.props.setUserInfo(res.data.profile)
                } else {
                    this.props.setAppLogged(false);
                    setApiToken(false)
                }

                this.setState({ refreshing: false })
            })
            .catch(err => {
                console.log(err)
            })

    }
    componentDidMount() {
        this.addListener();
        this.refresh()
    }
    refresh = () => {
        this.streamCheck()
        this.getUserInfo()
        this.props.setAppTabBarVisibility(true)
    }
    onRefresh = () => {
        this.setState({ refreshing: true })
        this.refresh()
    }
    handleLogOut() {
        console.log(this.props.app.stream)
        API.get(API_USER_LOGOUT)
            .then(res => {
                console.log(res)
            });
        this.props.setAppLogged(false)
        setApiToken(false)
    }
    handleLogin() {
        this.props.navigation.navigate('authScreen');
        console.log(RNLocalize)
        // RNLocalize.dispatchEvent('change')
    }
    componentDidUpdate() {
        // this.streamCheck()
    }
    streamCheck = () => {
        if (!this.props.app.local.isLoggedIn) {
            return
        }
        API.get(API_IMPRESSION_STREAM_CHECK)
            .then(res => {
                console.log('checkStreams');
                console.log(res.data)
                if (typeof res.data.status !== 'undefined') {
                    if (res.data.status === 'success') {
                        this.props.setImpressionStream(res.data.stream)
                    } else {
                        this.props.setImpressionStream(null)
                    }
                } else {
                    this.props.setImpressionStream(null)
                }
            })
            .catch(err => {
                // console.log(err)
                this.props.setImpressionStream(null)
            })
    }

    render() {
        return (
            <ContainerBox
                navigation={this.props.navigation}
                paddingTop={false}
                header={<HeaderBox
                    title={translate("screen.profile.title")}
                    shadow={true}
                    right={[{ icon: 'settings-outline', action: () => { this.props.navigation.navigate('profileSettingsScreen') } }]}
                />}
                isContentLoaded={this.state.isContentLoaded}
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
            >

                <View style={{ flexDirection: 'row', alignItems: 'center', width: Dimensions.get('window').width, justifyContent: 'flex-start', padding: 16, borderBottomColor: '#dedede', borderBottomWidth: 1, borderBottomStyle: 'solid', marginBottom: 10 }}>
                    <View style={{ overflow: 'hidden', width: 80, height: 80, borderRadius: 50, borderColor: '#dedede', borderWidth: 0, borderStyle: 'solid', marginRight: 20 }}>
                        {this.props.app.user.info.photo ?
                            <Image
                                style={{ width: 80, height: 80, marginLeft: 0, marginTop: 0 }}
                                source={{ uri: this.props.app.user.info.photo }}
                            />
                            : null}
                    </View>
                    <View>
                        <Text style={{ fontSize: 24, fontWeight: 'bold' }}>
                            {this.props.app.user.info.name}
                            {this.props.app.user.info.last_name ? '\n' : null}
                            {this.props.app.user.info.last_name}
                        </Text>
                        {/* <TouchableOpacity onPress={() => { this.props.navigation.navigate('profileInnerScreen'); }}><Text>{translate("screen.profile.goToProfile")}</Text></TouchableOpacity> */}
                    </View>
                </View>
                {this.props.app.stream !== null && this.props.app.user.info.type === 0 ?
                    <View style={{ borderBottomWidth: 1, marginBottom: 8, paddingBottom: 8, borderBottomColor: '#dedede' }}>
                        <Text style={[styles.title, { textAlign: 'center' }]}>{translate("screen.profile.streamStartSoon")}</Text>
                        <TButton
                            style={{ marginVertical: 10 }}
                            onPress={() => this.props.navigation.navigate('hostStreamScreen')}
                            icon="ios-pricetag"
                            text={translate("screen.profile.startStream")}
                            type="primary"
                        />
                    </View>
                    : null}

                {this.props.app.user.info.type === 0 ?
                    <View style={[styles.group, { borderTopWidth: 0 }]}>
                        <Text style={styles.title}>{translate("impression.single")}</Text>
                        <TouchableOpacity style={styles.element} onPress={() => this.props.navigation.navigate('createImpressionScreen')}>
                            <Icon name="ios-add-outline" size={24} color="#14ccb4" style={{ marginRight: 16 }} />
                        <Text style={{ fontSize: 16 }}>{translate("impression.create")}</Text>
                        </TouchableOpacity>
                        {/* <TouchableOpacity style={styles.element}><Icon name="ios-pricetag" size={24} color="#14ccb4" style={{ marginRight: 10 }} /><Text style={{ fontSize: 16 }}>Создать оффлайн</Text></TouchableOpacity> */}
                        <TouchableOpacity style={styles.element} onPress={() => this.props.navigation.navigate('listImpressionsScreen')}>
                            <Icon name="ios-layers-outline" size={24} color="#14ccb4" style={{ marginRight: 16 }} />
                            <Text style={{ fontSize: 16 }}>{translate("impression.my")}</Text>
                            </TouchableOpacity>

                    </View>
                     : null}
                {/* <View style={{ margin: 16 }}> */}
                    <View style={{marginTop: 10}}></View>
                    {this.props.app.local.isLoggedIn ?
                        <TButton
                            onPress={this.handleLogOut}
                            text={translate("default.logout")}
                            type="danger"
                        />
                        :
                        <TButton
                            onPress={this.handleLogin}
                            text={translate("default.login")}
                            type="warning"
                        />
                    }
            </ContainerBox>
        );
    }
}
const styles = StyleSheet.create({
    group: {
        width:'100%',
        marginBottom: 10,
        paddingTop: 10,
        // paddingHorizontal: 16,
        borderTopWidth: 1,
        borderTopColor: '#dedede'
    },
    title: {
        // paddingHorizontal: 10,
        marginVertical: 4
    },
    element: {
        flexDirection: 'row',
        alignItems: 'center',
        // paddingHorizontal: 10,
        paddingVertical: 6,
        fontSize: 20
    },
});
