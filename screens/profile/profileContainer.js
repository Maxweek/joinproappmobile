import React, { Component } from "react";
import {
  setAppLogged,
  setAppTabBarVisibility,
  setImpressionStream,
  setUserInfo,
  setUserLang
} from "../../store/user/actions";
import { connect } from "react-redux";
import Profile from "./profile";

class ProfileContainer extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Profile
        route={this.props.route}
        navigation={this.props.navigation}
        app={this.props.app}
        setAppLogged={this.props.setAppLogged}
        setUserInfo={this.props.setUserInfo}
        setImpressionStream={this.props.setImpressionStream}
        setUserLang={this.props.setUserLang}
        setAppTabBarVisibility={this.props.setAppTabBarVisibility}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    app: state.app
  };
};

const mapDispatchToProps = {
  setAppLogged,
  setImpressionStream,
  setUserInfo,
  setUserLang,
  setAppTabBarVisibility
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileContainer);
