import React, { Component } from "react";
// import {} from "../../store/user/actions";
import { connect } from "react-redux";
import { setImpressionList } from "../../store/user/actions";
import User from "./user";

class UserContainer extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <User
        route={this.props.route}
        navigation={this.props.navigation}
        app={this.props.app}
        setImpressionList={this.props.setImpressionList}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    app: state.app,
  };
};

const mapDispatchToProps = {
  setImpressionList
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserContainer);
