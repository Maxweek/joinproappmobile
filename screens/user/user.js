import React, { Component } from "react";
import {
    Text,
    View,
    StyleSheet,
    NativeModules,
    SafeAreaView,
    ScrollView,
    FlatList,
    Dimensions,
    Image
} from "react-native";
import { translate } from "../../App";
import API, { API_IMPRESSION_LIST } from "../../API";
import ContainerBox from "../../components/containerBox";
import HeaderBox from "../../components/headerBox";
import * as rootNavigation from "../../navigators/rootNavigator";
import List_ComponentImpressions from "../impressions/components/list";
import Card from "../../components/card";
import { API_REQUEST } from "../../API_functions";
import RatingBox from "../../components/ratingBox";
import { Portal } from "react-native-portalize";
import { TouchableOpacity } from "react-native-gesture-handler";
import ImageViewer from "react-native-image-zoom-viewer";
import ScrollableTabBar from "../_shared/scrollableTabBar";
import { TabView } from "react-native-tab-view";
import ModuleBox from "../../components/ModuleBox";
import SliderBox from "../../components/sliderBox";
import { COLORS } from "../../assets/styles/styles";
import Icon from "react-native-vector-icons/Ionicons";

const { StatusBarManager } = NativeModules;

export default class User extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isListenerAdded: false,
            isActiveBtn: true,
            refreshing: false,
            isGalleryActive: false,
            instances: [
                {
                    items: [],
                    filters: [{
                        name: 'filter_date_list',
                        title: 'Списки событий',
                        type: 'checkbox',
                        options: [{
                            guide_id: this.props.route.params.id
                        }],
                        data: [
                            {
                                type: 'simple',
                                key: 'past',
                                value: 'Недавние трансляции',
                                isActive: false
                            },
                            {
                                type: 'simple',
                                key: 'coming',
                                value: 'Предстоящие трансляции',
                                isActive: true
                            }
                        ]

                    }]
                },
                {
                    items: [],
                    filters: [{
                        name: 'filter_date_list',
                        title: 'Списки событий',
                        type: 'checkbox',
                        options: [{
                            guide_id: this.props.route.params.id
                        }],
                        data: [
                            {
                                type: 'simple',
                                key: 'past',
                                value: 'Недавние трансляции',
                                isActive: true
                            },
                            {
                                type: 'simple',
                                key: 'coming',
                                value: 'Предстоящие трансляции',
                                isActive: false
                            }
                        ]

                    }]
                },
            ]
        }

    }


    getList = (instance, i) => {
        let instances = this.state.instances;

        const formData = new FormData();
        console.log(i)
        console.log(instance)
        formData.append('filters', JSON.stringify(instance.filters))
        formData.append('options', JSON.stringify([{ language_code: 'ru' }]))
        formData.append('guide_id', this.props.route.params.id)

        API.post(API_IMPRESSION_LIST, formData)
            .then(res => {
                console.log(res)
                if (typeof res.data.status !== 'undefined') {
                    if (res.data.status === 'success') {
                        console.log(res.data.impressions)
                        // this.setList(res.data.impressions);
                        instances[i].items = res.data.impressions
                        this.setState({
                            refreshing: false,
                            isNotEmpty: true,
                            isContentLoaded: true,
                            instances: instances,
                        });
                    } else {
                        console.log('list_load_error_1')
                        this.setState({ refreshing: false, isNotEmpty: false });
                    }
                } else {
                    console.log('list_load_error_2')
                    this.setState({ refreshing: false, isNotEmpty: false });
                }
            })
            .catch(err => {
                console.log(err)
                this.setState({ refreshing: false, isNotEmpty: false });
            })
    }

    getData = () => {
        API_REQUEST.getUserInfo(this.props.route.params.id).then(user => {
            this.setState({
                user: user,
                refreshing: false
            })
        })
        let instances = this.state.instances;

        let i = 0;
        instances.map(el => {
            this.getList(el, i)
            i++
        })
    }
    onRefresh = () => {
        this.setState({ refreshing: true })
        this.getData();

        // this.getFilter()
    }
    componentDidMount() {
        this.addListener();
        this.onRefresh();
        // this.getList();
    }
    addListener = () => {
        if (!this.state.isListenerAdded) {
            this.unsubscribe = this.props.navigation.addListener('focus', () => {
                this.getData();
            });
            this.setState({ isListenerAdded: true })
        }
    }
    viewImpression = (item) => {
        this.props.navigation.push("viewImpressionScreen",
            {
                id: item.id
            }
        )
    }

    renderScene = ({ route }) => {
        return
    }

    render() {
        console.log(this.state)
        let langI = 0;
        return (
            <>
                <Portal>
                    {this.state.isGalleryActive ?
                        <ImageViewer imageUrls={[
                            // { source: require('yourApp/image.png'), dimensions: { width: 150, height: 150 } },
                            { url: this.state.user?.photo },
                            { url: this.state.user?.photo },
                            { url: this.state.user?.photo },
                            { url: this.state.user?.photo }
                        ]}
                            backgroundColor={'#ffffffbb'}
                            renderIndicator={() => { }}
                            enableSwipeDown={true}
                            onSwipeDown={() => {
                                this.setState({ isGalleryActive: false })
                            }}
                        />
                        : null}
                </Portal>
                <ContainerBox
                    navigation={this.props.navigation}
                    isContentLoaded={this.state.isContentLoaded}
                    refreshing={this.state.refreshing}
                    onRefresh={this.onRefresh}
                    header={
                        <HeaderBox
                            // scheme={'dark'}
                            title={translate('guide.title')}
                            shadow={true}
                            back={true}
                            navigation={this.props.navigation}
                        />
                    }
                >
                    <View style={{ flexDirection: 'row', alignItems: 'center', width: Dimensions.get('window').width, justifyContent: 'flex-start', padding: 16, borderBottomColor: '#dedede', borderBottomWidth: 1, borderBottomStyle: 'solid' }}>
                        <View style={{ overflow: 'hidden', width: 80, height: 80, borderRadius: 50, borderColor: '#dedede', borderWidth: 0, borderStyle: 'solid', marginRight: 20 }}>
                            {this.state.user?.photo ?
                                <TouchableOpacity onPress={() => {
                                    this.setState({
                                        isGalleryActive: true
                                    })
                                }}>
                                    <Image
                                        style={{ width: 80, height: 80, marginLeft: 0, marginTop: 0 }}
                                        source={{ uri: this.state.user.photo }}
                                    />
                                </TouchableOpacity>
                                : null}
                        </View>
                        <View>
                            <Text style={{ fontSize: 24, fontWeight: 'bold' }}>
                                {this.state.user?.name}
                                {this.state.user?.last_name ? '\n' : null}
                                {this.state.user?.last_name}
                            </Text>
                            <RatingBox rate={this.state.user?.rate} style={{ marginTop: 2 }} />
                            {/* <TouchableOpacity onPress={() => { this.props.navigation.navigate('profileInnerScreen'); }}><Text>{translate("screen.profile.goToProfile")}</Text></TouchableOpacity> */}
                        </View>
                    </View>
                    <ModuleBox
                        title={translate('default.aboutSelf')}
                        isLoaded={true}
                    >
                        <View>

                            <View style={styles.options}>
                                {this.state.user?.language.length ?
                                    <View style={styles.options__item}>
                                        <View style={styles.options__item_icon}>
                                            <Icon name={'language-outline'} size={24} color={COLORS.gray} />
                                        </View>
                                        <Text style={[styles.options__item_title, { color: COLORS.gray }]}>
                                            {this.state.user?.language.map(el => {
                                                let part = el + (el !== this.state.user.language[this.state.user.language.length - 1] ? ', ' : '')
                                                return part;
                                            })}
                                        </Text>
                                    </View>
                                    : null}
                                {this.state.user?.city || this.state.user?.country ?
                                    <View style={styles.options__item}>
                                        <View style={styles.options__item_icon}>
                                            <Icon name={'ios-location-outline'} size={24} color={COLORS.gray} />
                                        </View>
                                        <Text style={[styles.options__item_title, { color: COLORS.gray }]}>
                                            {this.state.user?.city}, {this.state.user?.country}
                                        </Text>
                                    </View>
                                    : null}
                            </View>
                            <View style={{ marginTop: 16 }}>
                                <Text style={styles.p}>{this.state.user?.description}</Text>
                            </View>
                        </View>
                    </ModuleBox>

                    {this.state.instances[0].items.length ?
                        <ModuleBox
                            title={translate('impression.coming')}
                            action={() => {
                                this.props.navigation.push('listImpressionScreen', {
                                    filters: this.state.instances[0].filters,
                                    title: translate('impression.guide'),
                                });
                            }}
                            actionText={translate('default.seeAll')}
                            actionIcon={'chevron-forward'}
                            actionHidden={this.state.instances[0].items.length >= 2 ? false : true}
                            isLoaded={true}
                        >
                            <SliderBox
                                items={this.state.instances[0].items}
                                onItemPress={this.viewImpression}
                            />
                        </ModuleBox>
                        : null}
                    {this.state.instances[1].items.length ?
                        <ModuleBox
                            title={translate('impression.past')}
                            action={() => {
                                this.props.navigation.push('listImpressionScreen', {
                                    filters: this.state.instances[1].filters,
                                    title: translate('impression.guide'),
                                });
                            }}
                            actionText={translate('default.seeAll')}
                            actionIcon={'chevron-forward'}
                            actionHidden={this.state.instances[1].items.length >= 2 ? false : true}
                            isLoaded={true}
                        >
                            <SliderBox
                                items={this.state.instances[1].items}
                                onItemPress={this.viewImpression}
                            />
                        </ModuleBox>
                        : null}
                    {/* <TabView
                        renderTabBar={(props) => <ScrollableTabBar {...props} ref={ref => this.scrollableTabBar = ref} index={index} tabBarUpdate={this._updateTabBar} />}
                        navigationState={{ index, routes }}
                        renderScene={this.renderScene}
                        onIndexChange={index => this._updateTabBar(index)}
                        initialLayout={{ width: Dimensions.get('window').width }}
                    /> */}
                    {/* <List_ComponentImpressions
                        navigation={this.props.navigation}
                        items={this.state.items}
                        noAction={true}
                    ></List_ComponentImpressions> */}
                </ContainerBox>
            </>
        );

    }
}
const styles = StyleSheet.create({
    container: {
        top: 0,
        flex: 1,
        backgroundColor: "#fff",
        position: "relative",
    },
    inner: {
        alignItems: "center",
        justifyContent: 'space-around',
        height: "100%",

        paddingTop: StatusBarManager.HEIGHT + 60
    },
    main: {
        alignItems: 'center',
        // paddingTop: 60
    },
    title: {
        fontSize: 27,
    },
    box: {
        padding: 16
    },

    options: {
        marginVertical: -8
    },
    options__item: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        marginVertical: 8
    },
    options__item_icon: {
        width: 24,
        height: 24,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 16
    },
    options__item_title: {
        fontSize: 16,
        lineHeight: 24,
        color: COLORS.black
    },
    p: {
        color: COLORS.black,
        fontSize: 16,
        lineHeight: 24
    },
});
