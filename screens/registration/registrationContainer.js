import React, { Component } from "react";
import {
  setAppLogged,
  setAppLoading,
  setUserEmail,
  setUserPhone,
  setUserName,
  setUserPassword,
  setAppTabBarVisibility
} from "../../store/user/actions";
import { connect } from "react-redux";
import Registration from "./registration";

class RegistrationContainer extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Registration
        route={this.props.route}
        navigation={this.props.navigation}
        app={this.props.app}
        setAppLogged={this.props.setAppLogged}
        setAppLoading={this.props.setAppLoading}
        setUserEmail={this.props.setUserEmail}
        setUserPhone={this.props.setUserPhone}
        setUserName={this.props.setUserName}
        setUserPassword={this.props.setUserPassword}
        setAppTabBarVisibility={this.props.setAppTabBarVisibility}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    app: state.app
  };
};

const mapDispatchToProps = {
  setAppLogged,
  setAppLoading,
  setUserEmail,
  setUserPhone,
  setUserName,
  setUserPassword,
  setAppTabBarVisibility,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegistrationContainer);
