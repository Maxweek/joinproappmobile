import React, { Component } from "react";
import {
    Text,
    TextInput,
    View,
    StyleSheet,
    TouchableOpacity,
} from "react-native";
import FloatingLabelInput from "../../components/floatingLabelInput";
import TButton from "../../components/tButton";
import { translate, _link } from "../../App";
import { COLORS, STYLES } from "../../assets/styles/styles";
import API, { API_USER_REGISTER, API_USER_REGISTER_GETCODE } from "../../API";
import CountryPicker, { getAllCountries, getCallingCode } from 'react-native-country-picker-modal';

import PhoneMasks from '../../assets/phoneMasks.json';

import { Platform, NativeModules } from 'react-native';
import HeaderBox from "../../components/headerBox";
import I18n from "i18n-js";
import ContainerBox from "../../components/containerBox";
const { StatusBarManager } = NativeModules;


export default class Registration extends Component {
    constructor(props) {
        super(props);
        this.setAppLogged = this.props.setAppLogged.bind(this);
        this.state = {
            smsCode: '',
            isMessageSended: false,
            isGetCodeButtonLoading: false,
            isGetCodeButtonLocked: false,
            isPhoneInputComplete: false,
            timerText: '00:00',
            country: {
                cca2: 'RU',
                callingCode: ['7']
            },
            isModalVisible: false,
            notification: {
                isVisible: false,
                color: COLORS.danger,
                text: ''
            }
        }
    }
    componentDidMount() {
        this.props.setAppTabBarVisibility(false)
    }
    componentWillUnmount() {
        this.props.setAppTabBarVisibility(true)
    }
    setAppLogged(type) {
        this.props.setAppLogged(type);
    }
    handlePress(type = null) {
        this.setAppLogged(true)
    }
    onCountrySelect = (country) => {
        this.props.setUserPhone('')

        console.log(country)
        this.setState({ country, isPhoneInputComplete: false })
    }

    setSmsCode = text => {
        console.log(text)
        this.setState({ smsCode: text }, () => {
            if (this.state.smsCode.length === 6) {
                console.log('FIRES SEND CODE')
                this.handleUserLoggedIn()
            }
        })
    }

    handleUserLoggedIn = () => {
        const formData = new FormData();

        formData.append('registration-phone', '+' + this.state.country.callingCode[0] + this.props.app.user.phone);
        formData.append('registration-sms', this.state.smsCode);
        formData.append('push-token', this.props.app.pushToken);
        formData.append('jwt', true);

        console.log(formData)

        console.log('beforeLogin')

        API.post(API_USER_REGISTER, formData)
            .then(res => {
                if (typeof res.data.status !== "undefined") {
                    if (res.data.status === 'success') {
                        console.log('success')
                        this.setState({ notification: { ...this.state.notification, text: '' } })
                        this.setAppLogged(true)
                    }
                    if (res.data.status === 'error') {
                        console.log('login error status')
                        console.log(res)
                        this.setState({ notification: { text: this.getErrorText(res), color: COLORS.danger, isVisible: true } })
                        this.setAppLogged(false)
                    }
                } else {
                    console.log('login error 2')
                    console.log(res)
                    this.setState({ notification: { text: translate("screen.auth.authError_1"), color: COLORS.danger, isVisible: true } })
                    this.setAppLogged(false)
                }
            }).catch(res => {
                console.log('login catch error')
                console.log(res)
                this.setState({ notification: { text: translate("screen.auth.authError_2"), color: COLORS.danger, isVisible: true } })
                this.setAppLogged(false)
            })
    };

    getErrorText = (res) => {
        console.log(res)
        let errText = [];
        if (res.data.error) {
            Object.keys(res.data.error).map(el => {
                errText.push(res.data.error[el]);
            })
        }
        console.log(errText.join("\n "))

        return errText.join("\n ");
    }

    getCode = () => {
        const formData = new FormData();

        formData.append('registration-name', this.props.app.user.info.name);
        formData.append('registration-email', this.props.app.user.email);
        formData.append('registration-password', this.props.app.user.passwrod);
        formData.append('registration-phone', '+' + this.state.country.callingCode[0] + this.props.app.user.phone);
        this.setState({ isGetCodeButtonLoading: true });
        this.setState({ isGetCodeButtonLocked: true });

        console.log(formData)
        this.setSmsCode('')

        API.post(API_USER_REGISTER_GETCODE, formData)
            .then(res => {
                if (typeof res.data.status !== "undefined") {
                    if (res.data.status === 'success') {
                        console.log('success')
                        this.setState({ isGetCodeButtonLoading: false, isMessageSended: true, notification: { ...this.state.notification, text: '' } });
                        this.smsCodeInput.focus();
                        this.setTimer(60, () => { this.setState({ isGetCodeButtonLocked: false }) });
                    }
                    if (res.data.status === 'error') {
                        console.log('login error status')
                        console.log(res)

                        this.setState({ isGetCodeButtonLoading: false, notification: { text: this.getErrorText(res), color: COLORS.danger, isVisible: true } })
                    }
                } else {
                    console.log('login error 2')
                    console.log(res)
                    this.setState({ isGetCodeButtonLoading: false, notification: { text: translate("screen.auth.authError_1"), color: COLORS.danger, isVisible: true } })
                }
            }).catch(res => {
                console.log('login catch error')
                console.log(res)
                this.setState({ isGetCodeButtonLoading: false, notification: { text: translate("screen.auth.authError_2"), color: COLORS.danger, isVisible: true } })
            })
    }

    setTimer(secs = 0, callback = function () { }) {
        let minutes = Math.floor(secs / 60);
        let seconds = secs % 60;

        let secondsStr = seconds.toString().padStart(2, '0');
        let minutesStr = minutes.toString().padStart(2, '0');

        this.setState({ timerText: minutesStr + ':' + secondsStr })

        this.interval = setInterval(() => {
            if (minutes === 0 && seconds === 1) {
                callback();
                clearInterval(this.interval)
            }
            if (seconds > 0) {
                seconds--;
            } else {
                minutes--;
                seconds = 59;
            }

            secondsStr = seconds.toString().padStart(2, '0');
            minutesStr = minutes.toString().padStart(2, '0');

            this.setState({ timerText: minutesStr + ':' + secondsStr })
        }, 1000)

    }

    render() {
        let notif_styles = {
            textAlign: 'center',
            opacity: this.state.notification.isVisible ? 1 : 0,
            color: this.state.notification.color,
            marginVertical: 8
        }
        return (
            <ContainerBox
                navigation={this.props.navigation}
                paddingTop={true}
                header={<HeaderBox
                    gradient={true}
                    scheme={'dark'}
                    shadow={false}
                    navigation={this.props.navigation}
                    back={true}
                />}
                title={translate("screen.registration.title")}
            >
                <Text style={notif_styles}>{this.state.notification.text}</Text>

                {!this.state.isMessageSended ?
                    <>
                        <FloatingLabelInput
                            label={translate("user.name")}
                            value={this.props.app.user.info.name}
                            onChangeText={this.props.setUserName}
                            autoCorrect={false}
                        // autoCompleteType="name"
                        />
                        <View style={{ flexDirection: 'row', alignItems: 'flex-end', marginVertical: 8 }}>
                            <CountryPicker
                                // withEmoji
                                onSelect={this.onCountrySelect}
                                countryCode={this.state.country !== null ? this.state.country.cca2 : null}
                                closeButtonImageStyle={{ opacity: 0.5 }}
                                containerButtonStyle={{
                                    backgroundColor: COLORS.lightgray,
                                    borderRadius: 8,
                                    borderRadiusTopRight: 20,
                                    // padding: 8,
                                    paddingVertical: 8,
                                    paddingHorizontal: 8,
                                    minWidth: 80,
                                    height: 48,
                                    marginRight: 4,
                                    // paddingRight: 8,
                                    // flexDirection: 'row',
                                    // alighItems: 'center'
                                }}
                                translation={I18n.locale === 'ru' ? "rus" : 'common'}
                                filterProps={{
                                    placeholder: translate("default.enterCountryName")
                                }}
                                withFilter={true}
                                withFlag={false}
                                withCallingCodeButton={true}
                                withAlphaFilter={true}
                                withCallingCode={true}
                                withModal={true}
                                withFlagButton={true}
                                flatListProps={{
                                    style: {
                                        paddingLeft: 12
                                    }
                                }}
                            />
                            <View style={{ flexGrow: 1 }}>
                                <FloatingLabelInput
                                    style={{ marginVertical: 0 }}
                                    mask={PhoneMasks[this.state.country.cca2]}
                                    maskType="custom"
                                    label={translate("user.phone")}
                                    value={this.props.app.user.phone}
                                    onChangeText={this.props.setUserPhone}
                                    onComplete={() => { this.setState({ isPhoneInputComplete: true }) }}
                                    onInComplete={() => { this.setState({ isPhoneInputComplete: false }) }}
                                    autoCompleteType="tel"
                                    keyboardType="phone-pad"
                                />
                            </View>
                        </View>
                        <FloatingLabelInput
                            label={translate("user.email")}
                            value={this.props.app.user.email}
                            onChangeText={this.props.setUserEmail}
                            autoCorrect={false}
                            autoCompleteType="email"
                        />
                        <FloatingLabelInput
                            label={translate("user.password")}
                            value={this.props.app.user.password}
                            onChangeText={this.props.setUserPassword}
                            autoCorrect={false}
                            secureTextEntry={true}
                        />
                    </>
                    : null}
                {this.state.isMessageSended ?
                    <View style={styles.inputBox}>
                        <Text style={styles.inputBox__title}>{translate("screen.registration.enterCode")}</Text>
                        <Text style={styles.inputBox__subtitle}>{translate("screen.registration.weSendCode")} {'\n'}{'+' + this.state.country.callingCode[0] + this.props.app.user.phone}</Text>
                        <View style={styles.inputBox__list}>
                            <TouchableOpacity onPress={() => { this.smsCodeInput.focus() }} style={styles.inputBox__item}>
                                <Text style={styles.inputBox__item_text}>{this.state.smsCode[0]}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { this.smsCodeInput.focus() }} style={styles.inputBox__item}>
                                <Text style={styles.inputBox__item_text}>{this.state.smsCode[1]}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { this.smsCodeInput.focus() }} style={styles.inputBox__item}>
                                <Text style={styles.inputBox__item_text}>{this.state.smsCode[2]}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { this.smsCodeInput.focus() }} style={styles.inputBox__item}>
                                <Text style={styles.inputBox__item_text}>{this.state.smsCode[3]}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { this.smsCodeInput.focus() }} style={styles.inputBox__item}>
                                <Text style={styles.inputBox__item_text}>{this.state.smsCode[4]}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { this.smsCodeInput.focus() }} style={styles.inputBox__item}>
                                <Text style={styles.inputBox__item_text}>{this.state.smsCode[5]}</Text>
                            </TouchableOpacity>
                        </View>
                        <Text style={styles.inputBox__timer}>{this.state.timerText}</Text>
                    </View>
                    : null}
                <TextInput
                    ref={ref => { this.smsCodeInput = ref }}
                    style={styles.smsCodeInputStyles}
                    // value={this.props.value}
                    onChangeText={this.setSmsCode}
                    keyboardType="phone-pad"
                    maxLength={6}
                />
                <View style={{ marginTop: 20 }}></View>
                {!this.state.isMessageSended ?
                    <TButton
                        onPress={this.getCode}
                        text={translate("screen.registration.requestCode")}
                        type="primary"
                        isLoading={this.state.isGetCodeButtonLoading}
                        isLocked={!this.state.isPhoneInputComplete}
                    />
                    :
                    <TButton
                        onPress={this.getCode}
                        text={translate("screen.registration.requestCodeAgain")}
                        type="primary"
                        isLoading={this.state.isGetCodeButtonLoading}
                        isLocked={this.state.isGetCodeButtonLocked}
                    />
                }

                <Text style={STYLES.footText}>
                    {translate("screen.registration.agree")} {' '}
                    <Text onPress={() => { _link(translate("default.userAgreementUrl")) }} style={{ textDecorationLine: "underline" }}>
                        {translate("default.userAgreement")}
                    </Text>
                    {' '}{translate("default.and")}{' '}
                    <Text onPress={() => { _link(translate("default.privacyPolicyUrl")) }} style={{ textDecorationLine: "underline" }}>
                        {translate("default.privacyPolicy")}
                    </Text>
                </Text>
            </ContainerBox>

        );
    }
}
const styles = StyleSheet.create({
    inputBox: {
        marginVertical: 16,
    },
    inputBox__title: {
        textAlign: 'center',
        color: COLORS.black,
        fontSize: 20,
        marginBottom: 4
    },
    inputBox__subtitle: {
        textAlign: 'center',
        color: COLORS.black,
        fontSize: 16,
        marginBottom: 4
    },
    inputBox__timer: {
        textAlign: 'center',
        color: COLORS.black,
        fontSize: 16,
        marginTop: 16
    },
    inputBox__item: {
        borderWidth: 1,
        borderColor: COLORS.lightgray,
        borderRadius: 8,
        fontSize: 20,
        height: 40,
        textAlign: 'center',
        width: 30,
        padding: 0,
        marginHorizontal: 8,
        paddingHorizontal: 8,
        justifyContent: 'center'
    },
    inputBox__item_text: {
        fontSize: 20
    },
    inputBox__list: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginVertical: 16
    },
    smsCodeInputStyles: {
        opacity: 0,
        width: 0,
        height: 0,
        position: 'absolute'
    }
});
