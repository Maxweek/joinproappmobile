import React, { Component } from "react";
import {
    Text,
    View,
    StyleSheet,
    Image,
    TouchableOpacity
} from "react-native";
import TButton from "../../components/tButton";
import Icon from "react-native-vector-icons/Ionicons";
import { COLORS } from "../../assets/styles/styles";

export default class Shared_EmptyList extends Component {
    constructor(props) {
        super(props);
        this.onPress = this.onPress.bind(this);
        // this.image = this.props.image ? require(this.props.image) : null;
    }
    onPress = () => {
        this.props.onPress();
    }
    render() {
        return (
            <View style={styles.inner}>
                {this.props.image !== 'none' ?
                    <View style={styles.imageBox}>
                        {/* <Image
                            style={styles.image}
                            source={require('../../assets/images/ref__profile.jpg')}
                        /> */}
                        <Icon name='bonfire-outline' size={60} color={COLORS.black} />
                    </View>
                    :
                    null
                }
                {this.props.title !== null ?
                    <Text style={styles.title}>{this.props.title}</Text>
                    : null}
                <Text style={styles.subtitle}>{this.props.subtitle}</Text>
                {!this.props.noAction ?
                    <TButton
                        onPress={this.onPress}
                        style={styles.actionBox}
                        text={this.props.actionText}
                        mini={true}
                        type="simple"
                    />
                    : null}
                {/* <TouchableOpacity  >
                    <Text style={styles.actionText}>
                        
                    </Text>
                </TouchableOpacity> */}
            </View>
        );
    }
}
const styles = StyleSheet.create({
    inner: {
        alignItems: "center",
        backgroundColor: 'white',
        justifyContent: 'center',
        // height: "100%",
        flexGrow: 1,
        paddingVertical: 8,
        paddingHorizontal: 32
    },
    imageBox: {
        width: 60,
        overflow: 'hidden',
        height: 60,
        marginBottom: 4,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    image: {
        width: '100%',
        height: '100%'
    },
    title: {
        fontSize: 20,
        color: "#2a2d43",
        lineHeight: 28,
        textAlign: 'center',
    },
    subtitle: {
        fontSize: 14,
        color: "#2a2d43",
        lineHeight: 20,
        textAlign: 'center',
        marginTop: 4
    },
    actionBox: {
        marginTop: 16
    },
    actionText: {
        fontSize: 14,
        lineHeight: 20,
        textAlign: 'center',
        color: '#14ccb4'
    }
});
