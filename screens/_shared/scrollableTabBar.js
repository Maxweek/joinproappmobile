import React, { Component } from "react";
import {
    Text,
    View,
    StyleSheet,
    ScrollView,
    Image,
    Dimensions,
    FlatList,
    Button,
    TouchableOpacity,
    Animated
} from "react-native";
// import Shared_EmptyList from "../../_shared/emptyList";
import { TabView, SceneMap } from 'react-native-tab-view';
// import Animated from 'react-native-reanimated';
import { createRef } from "react";
import { confirmSetupIntent } from "@stripe/stripe-react-native";
import { COLORS } from "../../assets/styles/styles";

export default class ScrollableTabBar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            index: 0,
            pos: [],
            isReady: false
        }
        this._scrollView = createRef()
        this._scrollView_holder = createRef()
        this.rf = [];
        this.snaps = [];
        this._animateHolderPosition = new Animated.Value(0);
        this._animateHolderWidth = new Animated.Value(0);
        // this.updateTabBar = this.updateTabBar.bind(this);

    }
    _updateTabBar = i => {
        this.props.tabBarUpdate(i);
        this.updateTabBar(i);
        // console.log(i)
        // console.log(this.props.index)
    }
    updateTabBar = i => {
        // console.log(i)
        // console.log(this.state.pos[i].layout.x - 20)
        if(!this.snaps[i] || !this.snaps[i].measure){
            return
        }
        let targetX = this.snaps[i].measure.fx
        let targetWidth = this.snaps[i].layout.width
        this._scrollView.current.scrollTo({ x: targetX  - 16})

        Animated.timing(this._animateHolderPosition, {
            toValue: targetX,
            duration: 200,
            useNativeDriver: false
        }).start();
        Animated.timing(this._animateHolderWidth, {
            toValue: targetWidth,
            duration: 200,
            useNativeDriver: false
        }).start();

    }
    componentDidUpdate() {
        this.updateTabBar(this.props.index);
        // if (this.state.isReady) {
        //     Animated.timing(this._animateHolderPosition, {
        //         toValue: this.state.pos[this.props.index].layout.x,
        //         duration: 200,
        //     }).start();
        // }
    }
    componentDidMount() {
        setTimeout(() => {
            this.build()
            // setTimeout(() => {
                // this.updateTabBar(this.props.index)
            // }, 1000)
        }, 200)
    }

    build = () => {
        let i = 0
        this.rf.map(el => {
            let q = i
            el.measure((width, height, px, py, fx, fy) => {
                console.log("++++++ fy " + width + " " + height);

                this.snaps[q].measure = {
                    width, height, px, py, fx, fy
                }
                console.log(this.snaps)
            });
            i++;
        })
    }

    render() {
        let holder = {
            width: this._animateHolderWidth,
            left: this._animateHolderPosition
        }
        console.log(holder)
        return (
            <ScrollView
                ref={this._scrollView}
                showsHorizontalScrollIndicator={false}
                horizontal={true}
                style={styles.tabBar}
                contentContainerStyle={{
                    paddingLeft: 16,
                    paddingRight: 16,
                }}>
                {this.props.navigationState.routes.map((route, i) => {
                    let curTabItem = {
                        // opacity: i === this.props.index ? 1 : .5
                    }
                    return (
                        <TouchableOpacity
                            ref={ref => this.rf.push(ref)}
                            onLayout={event => {
                                //     console.log('layOut change')
                                const layout = event.nativeEvent.layout;
                                this.snaps[i] = {
                                    layout
                                }
                                //     console.log(layout)
                                //     let poses = [...this.state.pos];
                                //     let curPos = {
                                //         id: i,
                                //         layout,
                                //     }
                                //     poses.push(curPos)
                                //     this.setState({ pos: poses, isReady: this.props.navigationState.routes.length === i + 1 ? true : false });
                            }}
                            style={[styles.tabItem, curTabItem]}
                            onPress={e => {
                                this._updateTabBar(i)
                                // this.updateTabBar(i)
                            }}>
                            <Text>{route.title} {route.count !== 'undefined' ? route.count : null}</Text>
                        </TouchableOpacity>
                    );
                })}
                <Animated.View style={[styles.holder, holder]}></Animated.View>
            </ScrollView>
        )
    }
}
const styles = StyleSheet.create({
    tabBar: {
        flexDirection: 'row',
        elevation: 10,
        flex: 1,
        maxHeight: 50,
        backgroundColor: 'white',
        position: 'relative',
        borderBottomWidth: 1,
        borderBottomColor: COLORS.lightgray
    },
    tabItem: {
        // flex: 1,
        alignItems: 'center',
        paddingHorizontal: 12,
        paddingVertical: 16,
        borderBottomWidth: 2,
        borderBottomColor: 'transparent'
    },
    tabBar_holder: {
        backgroundColor: 'black',
        width: 20,
        height: 2,
        position: 'absolute',
        bottom: 0
    },
    holder: {
        width: 20,
        height: 4,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        backgroundColor: COLORS.primary,
        position: 'absolute',
        bottom: 0,
        left: 20,
    }
});
