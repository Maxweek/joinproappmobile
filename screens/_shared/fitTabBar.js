import React, { Component } from "react";
import {
    Text,
    View,
    StyleSheet,
    ScrollView,
    Image,
    Dimensions,
    FlatList,
    Button,
    TouchableOpacity
} from "react-native";
// import Shared_EmptyList from "../../_shared/emptyList";
import { TabView, SceneMap } from 'react-native-tab-view';
import Animated from 'react-native-reanimated';
import { createRef } from "react";

export default class FitTabBar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            index: 0,
            pos: [],
            isReady: false
        }
        this._view = createRef()
        this._view_holder = createRef()
        // this._animateHolderPosition = new Animated.Value(0);
        this.updateTabBar = this.updateTabBar.bind(this);

    }
    _updateTabBar = i => {
        this.props.tabBarUpdate(i);
    }
    updateTabBar = i => {
        // this._view.current.scrollTo({ x: this.state.pos[i].layout.x - 20 })
    }
    componentDidUpdate() {
        if (this.state.isReady) {
            this.updateTabBar(this.props.index);
        }
        // if (this.state.isReady) {
        //     Animated.timing(this._animateHolderPosition, {
        //         toValue: this.state.pos[this.props.index].layout.x,
        //         duration: 200,
        //     }).start();
        // }
    }
    componentDidMount() {
        // Animated.timing(this._animateHolderPosition, {
        //     toValue: this.state.pos[this.props.index].layout.x,
        //     duration: 200,
        //   }).start();
    }

    render() {

        return (
            <View style={styles.tabBar} ref={this._view}>
                {this.props.navigationState.routes.map((route, i) => {
                    let curTabItem = {
                        borderBottomColor: i === this.props.index ? '#14ccb4' : 'transparent'
                    }
                    return (
                        <TouchableOpacity
                            onLayout={event => {
                                const layout = event.nativeEvent.layout;
                                let poses = [...this.state.pos];
                                let curPos = {
                                    id: i,
                                    layout,
                                }
                                poses.push(curPos)
                                this.setState({ pos: poses, isReady: this.props.navigationState.routes.length === i + 1 ? true : false });
                            }}
                            style={[styles.tabItem, curTabItem]}
                            onPress={e => {
                                this._updateTabBar(i)
                            }}>
                            <Text>{route.title} {route.count !== 'undefined' ? route.count : null}</Text>
                        </TouchableOpacity>
                    );
                })}
            </View>
        )
    }
}
const styles = StyleSheet.create({
    tabBar: {
        flexDirection: 'row',
        elevation: 10,
        flex: 1,
        maxHeight: 50,
        backgroundColor: 'white',
        position: 'relative'
    },
    tabItem: {
        // flex: 1,
        alignItems: 'center',
        padding: 16,
        borderBottomWidth: 2,
        borderBottomColor: 'transparent',
        flexGrow: 1
    },
    tabBar_holder: {
        backgroundColor: 'black',
        width: 20,
        height: 2,
        position: 'absolute',
        bottom: 0
    }
});
