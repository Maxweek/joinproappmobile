import React, { Component } from "react";
import MaskedView from "@react-native-community/masked-view";
import { createRef } from "react";
import {
    ScrollView,
    Text,
    TextInput,
    View,
    Animated,
    ActivityIndicator,
    Button,
    Image,
    StyleSheet, Linking, Alert, Share, Dimensions, PermissionsAndroid, FlatList, PanResponder, StatusBar, StatusBarIOS, Keyboard, useWindowDimensions, SafeAreaView, KeyboardAvoidingView
} from "react-native";
import RtcEngine, {
    ChannelProfile,
    ClientRole,
    RtcLocalView,
    RtcRemoteView,
    VideoDimensions,
    VideoEncoderConfiguration,
    VideoFrameRate,
    VideoRemoteState,
    VideoRenderMode,
} from 'react-native-agora';
import { TouchableOpacity } from "react-native-gesture-handler";
import KeepAwake from "react-native-keep-awake";
import { translate } from "../../App";
import TButton from "../../components/tButton";
import Fire from "../../firebase";
import Icon from "react-native-vector-icons/Ionicons";
import LinearGradient from "react-native-linear-gradient";
import { COLORS } from "../../assets/styles/styles";
var dimensions = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
};

import { Platform, NativeModules } from 'react-native';
import Orientation, { OrientationLocker, PORTRAIT, LANDSCAPE } from "react-native-orientation-locker";
const { StatusBarManager } = NativeModules;

export default class ViewerStream extends Component {
    constructor(props) {
        super(props);
        console.log(props)
        console.log(this.props.app)
        this.state = {
            user: 1,
            channel: this.props.app.stream_view.channel_id,
            isJoined: false,
            isBroadcaster: false,
            broadcasterVideoState: VideoRemoteState.Decoding,
            isAudioActive: true,
            isChatActive: false,
            isMenuActive: false,
            isStreamStarted: false,
            isStreamFinished: false,
            isSendButtonActive: false,
            keyboardAvoidingOffset: dimensions.height - StatusBarManager.HEIGHT - 32,
            animateVars: {
                chatVar: new Animated.Value(0),
                chatButtonVar: new Animated.Value(0),
                burgerMenuVar: new Animated.Value(0),
                inputWidth: new Animated.Value(0),
                shadowOpacity: new Animated.Value(0),
                sendButtonActivity: new Animated.Value(0),
            },
            userCount: 0,
            messages: [
                // {
                //     id: 1,
                //     message: translate("screen.hostStream.bot.message"),
                //     timestamp: new Date(),
                //     user: {
                //         id: 1,
                //         avatar: 'https://placeimg.com/140/140/any',
                //         username: translate("screen.hostStream.bot.name"),
                //     }
                // },
            ]
        }
        // console.log(Orientation)
        // this.
        this.AgoraEngine = null;
        this._animatedLiveIndicator = new Animated.Value(0);
        this.FlatListRef;
        this.isPortrait = true;
    }

    async initStream() {
        console.log(this.props.app.stream_view.channel_id)
        this.AgoraEngine = createRef()
        this.AgoraEngine.current = await RtcEngine.create(
            // '7992c14f45514cd289bf1950758e071e', Основной jp
            // '1e2e1300252c4bb9afd5ef247e30bf00', Запасной мой
            '987fd1c2247346a082c28e84f03d5de6'
            // 'c16a92ce413f41598afd66a0d4555fab',
            // 'c7e742d5df23478285a9dc4f4ff62407',
        );
        this.AgoraEngine.current.setVideoEncoderConfiguration(new VideoEncoderConfiguration({
            dimensions: new VideoDimensions(1280, 720),
            bitrate: 15420,
            frameRate: VideoFrameRate.Fps60,
            degradationPrefer: 0
        }))
        // this.AgoraEngine.current.setVideoEncoderConfiguration({
        //     width: 1280,
        //     height: 720,
        //     bitrate: 15420,
        //     frameRate: 60,
        //     // orientationMode: Adaptative,
        // })
        console.log(this.AgoraEngine.current)

        this.AgoraEngine.current.enableVideo(true);
        this.AgoraEngine.current.setChannelProfile(ChannelProfile.LiveBroadcasting);

        this.AgoraEngine.current.setClientRole(ClientRole.Audience);

        // this.AgoraEngine.current.addListener('RemoteVideoStateChanged', (uid, state) => {
        //     if (uid === 2) this.setBroadcasterVideoState(state);
        // });

        this.AgoraEngine.current.addListener(
            'JoinChannelSuccess',
            (channel, uid, elapsed) => {
                console.log('JoinChannelSuccess', channel, uid, elapsed);
                this.setJoined(true);
            },
        );
        this.AgoraEngine.current.addListener(
            'UserJoined',
            (uid, elapsed) => {
                console.log('UserJoined', uid, elapsed);
            },
        );
        this.AgoraEngine.current.addListener(
            'RtcStats',
            (stats) => {
                // console.log(stats);
                //this.setUserCount(stats.users - 1);
            },
        );
    }

    async onShare() {
        try {
            const result = await Share.share({ message: this.props.app.stream_view.impression.link });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            console.log(error.message);
        }
    }

    setUserCount(count) {
        if (this.state.userCount !== count) {
            this.setState({
                userCount: count
            })
        }
    }
    _onOrientationDidChange = (orientation) => {
        console.log(orientation)
        // console.log(Dimensions.get('screen').width)
        dimensions = {
            width: Dimensions.get('screen').width,
            height: Dimensions.get('screen').height,
        };

        if (orientation === 'PORTRAIT') {
            this.isPortrait = true
        } else {
            this.isPortrait = false;
        }
        if (this.state.isChatActive) {
            this.openChat()
        } else {
            this.closeChat()
        }
        this.updateDimensions();
        Keyboard.dismiss()
    }

    componentDidMount() {
        Orientation.addOrientationListener(this._onOrientationDidChange);
        Orientation.unlockAllOrientations()
        this.set();
        this.props.setAppTabBarVisibility(false)
        this.firebase = new Fire(this.state.channel);
        this.firebase.onAdd(snapshotBox => {
            console.log(snapshotBox)
            let snapshot = snapshotBox.val();
            console.log(snapshot)
            if (snapshotBox.val().count !== undefined) {
                this.setUserCount(snapshotBox.val().count)
                return
            }
            if (snapshotBox.val().status !== undefined) {
                if (snapshotBox.val().status === 'on') {
                    this.startStream()
                }
                if (snapshotBox.val().status === 'off') {
                    this.finishStream()
                }
                return
            }
            let mess = [...this.state.messages];
            let nextId = this.state.messages.length ? this.state.messages[this.state.messages.length - 1].id + 1 : 0;
            mess.push(
                {
                    id: nextId,
                    message: snapshot.message,
                    timestamp: snapshot.timestamp ? new Date(snapshot.timestamp) : new Date(),
                    user: {
                        id: snapshot.user.id,
                        avatar: snapshot.user.avatar,
                        username: snapshot.user.username,
                    }
                }
            )
            this.setState({
                messages: mess
            }, () => {
                // console.log(this.state.messages)
            })
            // console.log(message)
            // this.setState(previousState => ({
            //     messages: GiftedChat.append(previousState.messages, message),
            // })
        });
        this.firebase.onChange(snapshotBox => {
            console.log(snapshotBox)
            console.log(snapshotBox.val())
            if (snapshotBox.val().count !== undefined) {
                this.setUserCount(snapshotBox.val().count)
            }
        })
    }

    componentDidUpdate() {
        // this.set()
    }

    componentWillUnmount() {
        Orientation.removeOrientationListener(this._onOrientationDidChange);
        Orientation.lockToPortrait()
        
        this.props.setAppTabBarVisibility(true)
        this.AgoraEngine.current.leaveChannel()
        this.AgoraEngine.current.destroy();

        this.firebase.off()
    }

    set() {
        // if (Platform.OS === 'android') this.requestCameraAndAudioPermission();
        const uid = 3;
        this.initStream().then(() => {
            this.AgoraEngine.current.joinChannel(
                null,
                this.state.channel,
                null,
                uid,
            );
            // this.setRecording();
            // this.firebase.start()
        }
        );
        // return () => {
        //     this.AgoraEngine.current.destroy();
        // };
    }

    setBroadcasterVideoState(data) {
        this.setState({
            broadcasterVideoState: data
        })
    }

    setJoined(type) {
        console.log('joined: ' + type)
        this.setState({
            isJoined: type
        })
    }
    openChat = () => {
        console.log(-dimensions.width)
        Animated.timing(this.state.animateVars.chatVar, {
            toValue: 0,
            duration: 200,
            useNativeDriver: true
        }).start();
        Animated.timing(this.state.animateVars.shadowOpacity, {
            toValue: 1,
            duration: 400,
            useNativeDriver: true
        }).start();
        Animated.timing(this.state.animateVars.chatButtonVar, {
            toValue: -dimensions.width + 64 + (!this.isPortrait ? StatusBarManager.HEIGHT + 48 : 0),
            duration: 200,
            useNativeDriver: true
        }).start();
    }
    closeChat = () => {
        Animated.timing(this.state.animateVars.chatVar, {
            toValue: dimensions.width,
            duration: 200,
            useNativeDriver: true
        }).start();
        Animated.timing(this.state.animateVars.shadowOpacity, {
            toValue: 0,
            duration: 400,
            useNativeDriver: true
        }).start();
        Animated.timing(this.state.animateVars.chatButtonVar, {
            toValue: 0,
            duration: 200,
            useNativeDriver: true
        }).start();

        Keyboard.dismiss()
    }
    toggleChat = (type = true) => {
        if (!this.state.isChatActive) {
            if (type) {
                this.setState({ isChatActive: true })
            }
            this.openChat()
        } else {
            if (type) {
                this.setState({ isChatActive: false })
            }
            this.closeChat()
        }
    }
    setSendButtonActivity(type) {
        Animated.timing(this.state.animateVars.sendButtonActivity, {
            toValue: type ? 1 : 0,
            duration: 200,
            useNativeDriver: true
        }).start();
    }
    onSwitchAudio = () => {
        if (!this.state.isAudioActive) {
            this.setState({
                isAudioActive: true
            })
            this.AgoraEngine.current.muteAllRemoteAudioStreams(false);
        } else {
            this.setState({
                isAudioActive: false
            })
            this.AgoraEngine.current.muteAllRemoteAudioStreams(true);
        }
    }

    renderMessage = ({ item }) => {
        let date = item.timestamp.getHours() + ':' + item.timestamp.getMinutes();
        return (
            <View style={styles.messageItem}>
                <View style={styles.messageItemImageBox}>
                    <Image style={styles.messageItemImage}
                        source={{ uri: item.user.avatar }} />
                </View>
                <View style={styles.messageItemTextBox}>
                    <View style={styles.messageItemInfo}>
                        <Text style={styles.messageItemName}>{item.user.username}</Text>
                        <Text style={styles.messageItemDate}>{date}</Text>
                    </View>
                    <Text style={styles.messageItemText}>{item.message}</Text>
                </View>
            </View>
        )
    }
    // pan = new Animated.ValueXY();
    // isSidebar = false;
    // panResponder = PanResponder.create({
    //     onStartShouldSetPanResponder: () => !this.isSidebar,
    //     onPanResponderGrant: () => {
    //         console.log("EVENT NUMBER ONE")
    //         console.log(this.pan)

    //     },
    //     onPanResponderMove: Animated.event([
    //         null,
    //         { dx: this.pan.x }
    //     ], {
    //         listener: (e, a) => {
    //             console.log(a)
    //             if (this.pan.x._offset === dimensions.width && a.moveX > a.x0) {
    //                 this.pan.x.setValue(0)
    //             }
    //         }
    //     }),
    //     onPanResponderRelease: () => {
    //         console.log(this.pan.x)
    //         if (this.pan.x._value < 0 && !this.isSidebar) {
    //             this.isSidebar = false;
    //             Animated.spring(
    //                 this.pan.x,
    //                 { toValue: -dimensions.width, friction: 10, useNativeDriver: true },
    //             ).start(() => {
    //                 this.pan.setOffset({
    //                     x: 0
    //                 });
    //                 this.pan.x.setValue(0);
    //             });
    //         } else {
    //             // this.isSidebar = true;
    //             this.isSidebar = true;
    //             Animated.spring(
    //                 this.pan.x,
    //                 { toValue: this.pan.x._offset === dimensions.width ? 0 : dimensions.width, friction: 10, useNativeDriver: true },
    //             ).start(() => {
    //                 // this.pan.flattenOffset();
    //                 // this.pan.setOffset({
    //                 //     x: 0
    //                 // });
    //                 this.pan.x.setValue(dimensions.width);
    //             });
    //         }
    //     },
    //     onPanResponderTerminate: () => {
    //         if (!this.isSidebar) {

    //             this.isSidebar = false;
    //             Animated.spring(
    //                 this.pan.x,
    //                 { toValue: -dimensions.width, friction: 10, useNativeDriver: true },
    //             ).start(() => {
    //                 this.pan.setOffset({
    //                     x: 0
    //                 });
    //             });
    //         }

    //     }
    // });
    // panResponderBar = PanResponder.create({
    //     onMoveShouldSetPanResponder: () => this.isSidebar,
    //     onPanResponderGrant: () => {
    //         console.log(this.pan)
    //         console.log("EVENT NUMBER TWO")
    //         this.pan.setOffset({
    //             x: dimensions.width
    //         });
    //         // this.pan.flattenOffset();
    //     },
    //     onPanResponderMove: Animated.event([
    //         null,
    //         { dx: this.pan.x }
    //     ], {
    //         listener: (e, a) => {
    //             console.log(a)
    //             if (this.pan.x._offset === dimensions.width && a.moveX > a.x0) {
    //                 this.pan.x.setValue(0)
    //             }
    //         }
    //     }),
    //     onPanResponderRelease: () => {
    //         console.log(this.pan.x)
    //         console.log(this.isSidebar)
    //         if (this.pan.x._value < 0 && this.isSidebar) {
    //             console.log('SHOULD!')
    //             this.isSidebar = false;
    //             Animated.spring(
    //                 this.pan.x,
    //                 { toValue: -dimensions.width, friction: 10, useNativeDriver: true },
    //             ).start(() => {
    //                 this.pan.setOffset({
    //                     x: 0
    //                 });
    //             });
    //         } else {
    //             this.isSidebar = true;
    //             Animated.spring(
    //                 this.pan.x,
    //                 { toValue: this.pan.x._offset === dimensions.width ? 0 : dimensions.width, friction: 10, useNativeDriver: true },
    //             ).start(() => {
    //             });
    //         }
    //     },
    // });
    onInputFocus = () => {
        console.log('12332132')
        setTimeout(() => {
            this.FlatListRef.scrollToEnd()
        }, 200)
    }
    onMessageSend = () => {
        if (this.props.app.local.chatInputText.length) {
            this.firebase.append({
                message: this.props.app.local.chatInputText,
                timestamp: this.firebase.timestamp,
                user: {
                    avatar: this.props.app.user.info.photo !== '' ? this.props.app.user.info.photo : 'https://joinpro.ru/upload/uf/eb8/eb8dd1868ddabedb66fe28923e0052f7.png',
                    username: this.props.app.user.info.name
                }
            })
            this.props.setChatInputText('');
        }
        // Keyboard.dismiss()
        this.setSendButtonActivity(false)
    }
    toggleMenu = () => {
        if (!this.state.isMenuActive) {
            this.setState({ isMenuActive: true })
            Animated.timing(this.state.animateVars.burgerMenuVar, {
                toValue: dimensions.width,
                duration: 200,
                useNativeDriver: true
            }).start();
        } else {
            this.setState({ isMenuActive: false })
            Animated.timing(this.state.animateVars.burgerMenuVar, {
                toValue: 0,
                duration: 200,
                useNativeDriver: true
            }).start();
        }
        if (this.state.isChatActive) {
            this.toggleChat()
        }
        Keyboard.dismiss()
    }
    updateDimensions = () => {
        // if (this.state.isChatActive) {
        //     this.state.animateVars.chatVar.setValue(-dimensions.width)
        //     this.state.animateVars.chatButtonVar.setValue(-dimensions.width + 64)
        // } else {
        //     this.state.animateVars.chatVar.setValue(0)
        //     this.state.animateVars.chatButtonVar.setValue(0)
        //     Keyboard.dismiss()
        // }
        // if (this.state.isMenuActive) {
        //     this.state.animateVars.burgerMenuVar.setValue(dimensions.width)
        // } else {
        //     this.state.animateVars.burgerMenuVar.setValue(0)
        // }
        this.setState({ keyboardAvoidingOffset: this.isPortrait ? (dimensions.height - StatusBarManager.HEIGHT - 32) : dimensions.height })
        this.state.animateVars.inputWidth.setValue(dimensions.width - 20)
    }
    goExit = () => {
        if (this.props.app.stream_view.impression.donate) {
            this.goDonate()
        } else {
            var go = () => {
                this.props.navigation.navigate('impressionsScreen', {
                    screen: 'listImpressionScreen',
                });
            }
            Alert.alert(
                translate("screen.viewStream.exit"),
                "",
                [
                    { text: translate('screen.createImpression.confirm.cancel'), style: "cancel" },
                    { text: translate('screen.createImpression.confirm.gotIt'), onPress: () => go() }
                ]
            );
        }
    }
    goDonate = () => {
        this.props.navigation.navigate('donateScreen', {
            impression_id: this.props.app.stream_view.impression.id,
            impression_title: this.props.app.stream_view.impression.title,
        })
    }

    startStream = () => {
        this.setState({
            isStreamFinished: true,
        })

    }
    finishStream = () => {

        this.setState({
            isStreamStarted: true,
            isStreamFinished: true,
        })
    }
    setChatInputText = (text) => {
        this.props.setChatInputText(text)
        if (text !== '') {
            this.setSendButtonActivity(true)
        } else {
            this.setSendButtonActivity(false)
        }
    }
    render() {
        let chat = {
            transform: [{
                translateX: this.state.animateVars.chatVar
            }]
        }
        let chatButton = {
            transform: [{
                translateX: this.state.animateVars.chatButtonVar
            }]
        }
        let burgerMenu = {
            transform: [{
                translateX: this.state.animateVars.burgerMenuVar
            }]
        }
        let inputWidth = {
            maxWidth: this.state.animateVars.inputWidth,
            // backgroundColor: 'blue'
        }
        let shadowOpacity = {
            opacity: this.state.animateVars.shadowOpacity
        }
        let sendButtonActivity = {
            opacity: this.state.animateVars.sendButtonActivity
            // opacity: 1
        }
        let chatBoxOffset = {
            left: 0
        }
        let keyboardAvoidingOffset = {
            height: '100%'
        }
        console.log(keyboardAvoidingOffset.height)
        // console.log(Keyboard.getConstants)
        return (
            <>
                {/* <OrientationLocker
                orientation={LANDSCAPE}
                onChange={orientation => console.log('onChange', orientation)}
                onDeviceChange={orientation => console.log('onDeviceChange', orientation)}
            /> */}
                {!this.state.isJoined ? (
                    <View style={styles.container}>
                        <ActivityIndicator
                            size={60}
                            color={COLORS.primary}
                        />
                        <Text style={{ marginTop: 8 }}>{translate("screen.viewStream.joinWait")}</Text>
                    </View>
                ) : (
                    <>
                        <KeepAwake />
                        <StatusBar barStyle={'light-content'} />
                        <View style={styles.streamBox} onLayout={this.updateDimensions}>
                            <RtcRemoteView.SurfaceView
                                style={styles.fullscreen}
                                uid={1}
                                channelId={this.state.channel} />
                            <Animated.View style={[{
                                position: 'absolute',
                                width: '100%',
                                height: '100%',
                            }, shadowOpacity]}>
                                <LinearGradient colors={['#00000000', '#00000044', '#00000066', '#00000088', '#000000aa', '#000000cc', '#000000fa',]} style={[styles.linearGradientBack]}>
                                </LinearGradient>
                            </Animated.View>
                            <LinearGradient colors={['#000000aa', '#00000000',]} style={{
                                position: 'absolute',
                                top: 0,
                                height: 60,
                                width: '100%'
                            }}>
                            </LinearGradient>
                            <LinearGradient colors={['#00000000', '#000000aa']} style={{
                                position: 'absolute',
                                bottom: 0,
                                height: 50,
                                width: '100%'
                            }}>
                            </LinearGradient>
                        </View>
                        
                        <SafeAreaView style={styles.streamBox_safetyView}>
                        {/* <Image source={{ uri: 'https://pizzatravel.com.ua/uploads/2013/12737.jpg' }} style={{width: '200%', height: dimensions.height, position: 'absolute', top: 0, left: -250}} /> */}
                            <KeyboardAvoidingView style={[styles.streamBox_inner, keyboardAvoidingOffset]} behavior="height"  keyboardVerticalOffset={Platform.OS === 'ios' ? StatusBarManager.HEIGHT + 32 : 0} enabled={true}>

                                {/* <View style={styles.fullscreen}></View> */}

                                {/* {!this.state.isStreamStarted || (this.state.isStreamStarted && this.state.isStreamFinished) ?
                                    <View style={styles.previewBox}>
                                        <LinearGradient
                                            colors={['#FFFFFF00', '#14ccb444', '#14ccb488']}
                                            style={styles.previewBox__gradient}>
                                        </LinearGradient>
                                        {!this.state.isStreamStarted ?
                                            <Text style={styles.previewBox__title}>Ожидание стрима</Text>
                                            : null}
                                        {this.state.isStreamFinished ?
                                            <Text style={styles.previewBox__title}>Стрим закончен</Text>
                                            : null}
                                    </View>
                                    : null} */}
                                <View style={styles.topRight}>
                                    {this.props.app.stream_view.impression.donate ?
                                        <TouchableOpacity style={[styles.btn, styles.donate]} onPress={this.goDonate}>
                                            <Icon name={'heart'} size={24} color={COLORS.white} />
                                        </TouchableOpacity>
                                        : null}
                                    <TouchableOpacity onPress={this.goExit} style={styles.btn}>
                                        <Icon name={'exit-outline'} size={24} color={COLORS.black} />
                                    </TouchableOpacity>
                                    {/* <TouchableOpacity onPress={this.toggleMenu} style={[styles.btn, styles.burger]}>
                                        <Icon name={this.state.isMenuActive ? 'close-outline' : 'menu-outline'} size={32} color={COLORS.black} />
                                    </TouchableOpacity> */}
                                </View>
                                {/* <Animated.View style={[styles.burgerMenu, burgerMenu]}>
                                    <Text>{translate("screen.viewStream.menu")}</Text>
                                </Animated.View> */}

                                <View style={styles.userCountContainer}>
                                    <Icon name={'eye'} size={16} color={'white'} />
                                    <Text style={styles.userCountText}>{this.state.userCount}</Text>
                                </View>

                                <Animated.View style={[styles.bottomLeft, chatButton]}>
                                    {/* <TouchableOpacity style={styles.btn}>
                                        <Icon name={'share-outline'} size={24} color={COLORS.black} />
                                    </TouchableOpacity> */}
                                    <TouchableOpacity onPress={this.onSwitchAudio} style={[styles.btn, { marginLeft: 0 }]}>
                                        <Icon name={this.state.isAudioActive ? "volume-high-outline" : "volume-mute-outline"} size={24} color={COLORS.black} />
                                    </TouchableOpacity>
                                    {/* <TButton
                                        icon="share-outline"
                                        notStretch={true}
                                        type='secondary'
                                        isCircled={true}
                                        onPress={this.onShare}
                                    />
                                    <TButton
                                        icon={this.state.isChatActive ? "volume-high-outline" : "volume-mute-outline"}
                                        notStretch={true}
                                        isCircled={true}
                                        onPress={this.onSwitchAudio}
                                        type={this.state.isAudioActive ? 'primary' : 'danger'}
                                    />
                                    <TButton
                                        icon={this.state.isChatActive ? "chevron-down-outline" : "chevron-up-outline"}
                                        notStretch={true}
                                        isCircled={true}
                                        onPress={this.onSwitchChat}
                                    /> */}
                                </Animated.View>
                                <Animated.View style={[styles.bottomRight, chatButton]}>
                                    <TouchableOpacity style={styles.chatButton} onPress={this.toggleChat}>
                                        <Text style={{ marginRight: 12 }}>{translate("screen.viewStream.openChat")}</Text>
                                        <Icon name={!this.state.isChatActive ? 'chatbubbles-outline' : 'chevron-forward-outline'} size={24} color={COLORS.black} />
                                    </TouchableOpacity>
                                    {/* <View style={[styles.chatButtonBadge, { opacity: this.state.isChatActive ? 0 : 1 }]}>
                                        <Text style={{ fontSize: 12, color: COLORS.white }}>2000</Text>
                                    </View> */}
                                </Animated.View>
                                <View style={[styles.chatBox, chatBoxOffset]}>
                                    <Animated.View style={[styles.chatBox_inner, chat, shadowOpacity]}>

                                        {/* <FlatList
                                                // inverted
                                                ref={ref => (this.FlatListRef = ref)}
                                                onContentSizeChange={() => this.FlatListRef.scrollToEnd()}
                                                data={this.state.messages}
                                                style={styles.messageList}
                                                renderItem={this.renderMessage}
                                                keyExtractor={item => item.id}
                                                contentContainerStyle={{ paddingTop: Dimensions.get('window').height / 2 + 222, paddingBottom: 0 }}
                                            /> */}
                                        <MaskedView style={[styles.messageListMask]}
                                            maskElement={
                                                <LinearGradient colors={['#FFFFFF00', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF']} style={styles.linearGradient}>
                                                </LinearGradient>}>
                                            <FlatList
                                                // inverted
                                                ref={ref => (this.FlatListRef = ref)}
                                                onContentSizeChange={() => this.FlatListRef.scrollToEnd()}
                                                data={this.state.messages}
                                                style={styles.messageList}
                                                keyboardDismissMode="on-drag"
                                                renderItem={this.renderMessage}
                                                keyExtractor={item => item.id}
                                                contentContainerStyle={{ paddingTop: dimensions.height / 2, paddingBottom: 0 }}
                                            />
                                        </MaskedView>
                                        <View style={styles.chatBox_inputBox}>
                                            <Animated.View style={[inputWidth, { flexGrow: 1 }]}>
                                                <TextInput
                                                    refInput={ref => { this.input = ref }}
                                                    style={[styles.chatBox_input]}
                                                    value={this.props.app.local.chatInputText}
                                                    onFocus={this.onInputFocus}
                                                    onChangeText={text => {this.setChatInputText(text)}}
                                                    autoCorrect={true}
                                                    placeholderTextColor={COLORS.gray}
                                                    multiline={true}
                                                    placeholder={translate("screen.viewStream.typeMessage")}
                                                />
                                            </Animated.View>
                                            <TouchableOpacity onPress={this.onMessageSend} style={[styles.btn, { backgroundColor: COLORS.gray }]}>
                                                <Animated.View style={[styles.innerSendBtnView, sendButtonActivity]}></Animated.View>
                                                <Icon name={'send'} size={24} color={COLORS.white} />
                                            </TouchableOpacity>
                                        </View>
                                    </Animated.View>
                                    {/* <View style={styles.messageListBox}>
                                        
                                        
                                    </View> */}
                                </View>
                            </KeyboardAvoidingView>
                        </SafeAreaView>
                        {/* <View style={styles.exitButtonContainer}>
                                <TButton
                                    text={translate("screen.hostStream.stopStream")}
                                    notStretch={true}
                                    onPress={this.wannaStop}
                                    mini={true}
                                    type={'danger'}
                                />
                            </View> */}

                        {/* 
                            <View style={styles.buttonContainer}>
                                <TButton
                                    icon="share-outline"
                                    notStretch={true}
                                    type='secondary'
                                    isCircled={true}
                                    onPress={this.onShare}
                                />
                                <TButton
                                    icon={this.state.isChatActive ? "volume-high-outline" : "volume-mute-outline"}
                                    notStretch={true}
                                    isCircled={true}
                                    onPress={this.onSwitchAudio}
                                    type={this.state.isAudioActive ? 'primary' : 'danger'}
                                />
                                <TButton
                                    icon={this.state.isChatActive ? "chevron-down-outline" : "chevron-up-outline"}
                                    notStretch={true}
                                    isCircled={true}
                                    onPress={this.onSwitchChat}
                                />
                            </View> */}

                    </>
                )
                }
            </>
        );
    }
}
const styles = StyleSheet.create({
    streamBox: {

        height: '100%',
        width: '100%',
        // paddingTop: StatusBarManager.HEIGHT,
        position: 'absolute',
    },
    streamBox_inner: {
        // flex: 1,
        height: '100%',
        width: '100%',
        position: 'relative',
        backgroundColor: '#00000000',
        // backgroundColor: 'pink'
        // paddingTop: StatusBarManager.HEIGHT,
        // backgroundColor: COLORS.white,
    },
    streamBox_safetyView: {
        // backgroundColor: '#0000ff',
        // backgroundColor: 'blue',
        paddingTop: Platform.OS === 'android' ? StatusBarManager.HEIGHT : 0,
        position: 'relative',
        flex: 1,
    },
    btn: {
        width: 48,
        height: 48,
        backgroundColor: 'white',
        borderRadius: 12,
        elevation: 8,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 2,
        shadowColor: 'black',
        shadowOffset: {
            width: 0,
            height: 0
        },
        shadowOpacity: 0.1,
        shadowRadius: 4,
    },
    innerSendBtnView: {
        backgroundColor: COLORS.primary,
        width: '100%',
        height: '100%',
        position: 'absolute',
        borderRadius: 12,
    },
    donate: {
        backgroundColor: COLORS.primary,
        marginRight: 16,
    },
    burger: {
        zIndex: 101
    },
    burgerMenu: {
        position: 'absolute',
        display: 'none',
        zIndex: 100,
        height: '100%',
        width: '100%',
        left: '-100%',
        backgroundColor: COLORS.white
    },
    bottomLeft: {
        position: 'absolute',
        bottom: 16,
        left: 16,
        flexDirection: 'row',
        zIndex: 20
    },
    topRight: {
        position: 'absolute',
        top: 16,
        right: 16,
        flexDirection: 'row',
        // backgroundColor: 'green',
        zIndex: 120
    },
    bottomRight: {
        position: 'absolute',
        bottom: 16,
        right: 16,
        flexDirection: 'row',
        height: 48,
        zIndex: 20
    },
    chatButton: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height: 48,
        backgroundColor: 'white',
        borderRadius: 12,
        elevation: 8,
        paddingHorizontal: 12,
        zIndex: 20,
        elevation: 2,
        shadowColor: 'black',
        shadowOffset: {
            width: 0,
            height: 0
        },
        shadowOpacity: 0.1,
        shadowRadius: 4,
    },
    chatButtonBadge: {
        position: 'absolute',
        right: -5,
        top: -10,
        backgroundColor: COLORS.primary,
        paddingVertical: 2,
        paddingHorizontal: 4,
        borderRadius: 50
    },
    chatBox: {
        position: 'absolute',
        left: 0,
        bottom: 0,
        width: '100%',
        height: '100%',
        // left: dimensions.width,
        paddingTop: 100,
        // zIndex: 190
    },
    chatBox_inner: {
        flexGrow: 1,
        width: '100%',
        position: 'relative'
        // backgroundColor: 'gray'
    },
    chatBox_inputBox: {
        paddingLeft: 56,
        paddingRight: 16,
        paddingBottom: 16,
        flexDirection: 'row',
        // backgroundColor: 'yellow',
        alignItems: 'flex-end',
    },
    chatBox_input: {
        minHeight: 48,
        flexGrow: 1,
        marginRight: 8,
        backgroundColor: COLORS.white,
        borderRadius: 12,
        elevation: 8,
        // padding: 16,
        paddingHorizontal: 12,
        paddingTop: 12,
        paddingBottom: 10,
        lineHeight: 22,
        fontSize: 18,
        textAlignVertical: 'center',
        color: COLORS.black
    },
    linearGradient: {
        flexGrow: 1,
        width: '100%',
        height: '100%',
        borderRadius: 5,
        top: 0,
        position: 'absolute'
    },
    linearGradientBack: {
        position: 'absolute',
        width: '100%',
        height: '100%',
    },
    messageListBox: {
        position: 'absolute',
        zIndex: 20,
        bottom: 0,
        width: '100%',
        height: '100%',
    },
    messageListMask: {
        flex: 1,
        zIndex: 20,
    },
    messageList: {
        // height: 50,
        // width: Dimensions.get('window').width - 32,
        // paddingHorizontal: 16,
        // marginHorizontal: 16,
        marginBottom: 10,
        flex: 1
    },
    messageItem: {
        // width: '100%',
        // height: 20,
        // backgroundColor: 'yellow',
        // borderWidth: 1,
        marginHorizontal: 16,
        flexDirection: 'row',
        marginBottom: 10
    },
    messageItemImageBox: {
        width: 52,
        height: 52,
        borderRadius: 56,
        backgroundColor: 'lightgray',
        overflow: 'hidden',
        borderWidth: 2,
        borderColor: 'white',

    },
    messageItemImage: {
        width: '100%',
        height: '100%'
    },
    messageItemTextBox: {
        marginLeft: 16,
        // flexDirection: 'row',
        width: 0,
        flexGrow: 1,
        flex: 1,
    },
    messageItemInfo: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end'
    },
    messageItemName: {
        color: '#ffffffdd',
        textShadowRadius: 4,
        fontWeight: 'bold',
        fontSize: 12
    },
    messageItemDate: {
        color: '#ffffff88',
        textShadowRadius: 4,
        fontWeight: 'bold',
        fontSize: 10
    },
    messageItemText: {
        color: 'white',
        textShadowRadius: 4,
        paddingRight: 32
        // flex: 1, 
        // flexWrap: 'wrap',
        // flexShrink: 1
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    loadingText: {
        fontSize: 18,
        color: '#222',
    },
    fullscreen: {
        // width: dimensions.width,
        width: '100%',
        // height: dimensions.height,
        height: '100%',
        position: 'absolute',
        paddingTop: 0,
        paddingTop: StatusBarManager.HEIGHT,
        backgroundColor: 'white',
        marginTop: 0,
    },
    exitButtonContainer: {
        flexDirection: 'row',
        position: 'absolute',
        zIndex: 100,
        top: 40,
        right: 16
    },
    userCountContainer: {
        flexDirection: 'row',
        position: 'absolute',
        zIndex: 20,
        top: 16,
        left: 16,
        backgroundColor: '#00000044',
        paddingVertical: 4,
        paddingHorizontal: 8,
        borderRadius: 8,
        alignItems: 'center',
        // marginHorizontal: 10,
        color: '#fff',
    },
    userCountText: {
        fontSize: 14,
        marginLeft: 6,
        color: '#fff'
    },
    buttonContainer: {
        flexDirection: 'row',
        position: 'absolute',
        width: dimensions.width,
        justifyContent: 'space-between',
        backgroundColor: '#00000055',
        bottom: 0,
        width: '100%',
        padding: 16,
        // marginVertical: 20
    },
    recordingContainer: {
        flexDirection: 'row',
        position: 'absolute',
        top: 90,
        // width: 100,
        right: 16,
        backgroundColor: '#00000044',
        paddingVertical: 4,
        paddingHorizontal: 8,
        borderRadius: 8,
        justifyContent: 'flex-end',
        alignItems: 'center',
        // marginHorizontal: 10,
        color: '#fff',
    },
    recordingContainerIndicator: {
        width: 8,
        height: 8,
        borderRadius: 8,
        backgroundColor: 'red',
        marginRight: 8
    },
    recordingContainerText: {
        color: '#fff',
        fontSize: 12
    },
    button: {
        width: 80,
        backgroundColor: '#fff',
        marginBottom: 10,
        paddingVertical: 13,
        borderRadius: 8,
        alignItems: 'center',
        marginHorizontal: 10,
    },
    buttonText: {
        fontSize: 15,
        textAlign: 'center',
    },
    broadcasterVideoStateMessage: {
        position: 'absolute',
        bottom: 0,
        width: '100%',
        height: '100%',
        backgroundColor: '#222',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    broadcasterVideoStateMessageText: {
        color: '#fff',
        fontSize: 20,
    },
    previewBox: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        backgroundColor: COLORS.white,
        zIndex: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    previewBox__gradient: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
    },
    previewBox__title: {
        fontSize: 32,
        fontWeight: '100'
    },
});


// export default class ViewerStream extends Component {
//     constructor(props) {
//         super(props);
//         console.log(props)
//         this.state = {
//             user: this.props.user,
//             uid: null,
//             channel: this.props.user.info.channel,
//             isJoined: false,
//             isBroadcaster: true,
//             isAudioActive: true,
//             broadcasterVideoState: VideoRemoteState.Decoding,
//             userCount: 0
//         }
//         console.log(this.state)
//         // this.onSwitchCamera = this.onSwitchCamera.bind(this)
//         this.onShare = this.onShare.bind(this)
//         this.stopStream = this.stopStream.bind(this)
//         this.onSwitchAudio = this.onSwitchAudio.bind(this)
//         // this.
//         this.AgoraEngine = null
//     }
//     async initStream() {
//         this.AgoraEngine = createRef()
//         this.AgoraEngine.current = await RtcEngine.create(
//             '7992c14f45514cd289bf1950758e071e',
//             // 'c16a92ce413f41598afd66a0d4555fab',
//             // 'c7e742d5df23478285a9dc4f4ff62407',
//         );
//         this.AgoraEngine.current.enableVideo(true);
//         this.AgoraEngine.current.setChannelProfile(ChannelProfile.LiveBroadcasting);

//         this.AgoraEngine.current.setClientRole(ClientRole.Audience);

//         this.AgoraEngine.current.addListener('RemoteVideoStateChanged', (uid, state) => {
//             if (uid === 2) this.setBroadcasterVideoState(state);
//         });

//         this.AgoraEngine.current.addListener(
//             'UserJoined',
//             (uid, elapsed) => {
//                 console.log('UserJoined', uid, elapsed);
//                 this.setJoined(true, uid);
//             },
//         );

//         this.AgoraEngine.current.addListener(
//             'RtcStats',
//             (stats) => {
//                 // console.log('RtcStats', stats);
//                 if (this.state.userCount !== stats.users) {
//                     this.setUserCount(stats.users - 1);
//                 }
//             },
//         );
//     }

//     async onShare() {
//         try {
//             const result = await Share.share({ message: this.state.channel });
//             if (result.action === Share.sharedAction) {
//                 if (result.activityType) {
//                     // shared with activity type of result.activityType
//                 } else {
//                     // shared
//                 }
//             } else if (result.action === Share.dismissedAction) {
//                 // dismissed
//             }
//         } catch (error) {
//             console.log(error.message);
//         }
//     }

//     setUserCount(count) {
//         this.setState({
//             userCount: count
//         })
//     }

//     componentDidMount() {
//         this.set();
//     }

//     componentDidUpdate() {
//         // this.set();
//     }

//     componentWillUnmount() {
//         this.AgoraEngine.current.leaveChannel()
//         this.AgoraEngine.current.destroy();
//     }

//     stopStream() {
//         this.props.navigation.navigate('Домашняя юзера');
//     }

//     set() {
//         if (Platform.OS === 'android') this.requestCameraAndAudioPermission();
//         // const uid = this.state.user.id;
//         const uid = 2;
//         this.initStream().then(() =>
//             this.AgoraEngine.current.joinChannel(
//                 null,
//                 this.state.channel,
//                 null,
//                 uid,
//             ),
//         );
//         // return () => {
//         //     this.AgoraEngine.current.destroy();
//         // };
//     }

//     setBroadcasterVideoState(data) {
//         this.setState({
//             broadcasterVideoState: data
//         })
//     }

//     onSwitchAudio() {
//         if (!this.state.isAudioActive) {
//             this.setState({
//                 isAudioActive: true
//             })
//             this.AgoraEngine.current.muteAllRemoteAudioStreams(false);
//         } else {
//             this.setState({
//                 isAudioActive: false
//             })
//             this.AgoraEngine.current.muteAllRemoteAudioStreams(true);
//         }

//     }

//     setJoined(type, uid) {
//         console.log('joined: ' + type)
//         this.setState({
//             isJoined: type,
//             uid
//         })
//     }
//     videoStateMessage() {
//         switch (this.state.broadcasterVideoState) {
//             case VideoRemoteState.Stopped:
//                 return 'Video turned off by Host';

//             case VideoRemoteState.Frozen:
//                 return 'Connection Issue, Please Wait';

//             case VideoRemoteState.Failed:
//                 return 'Network Error';
//         }
//     };

//     async requestCameraAndAudioPermission() {
//         try {
//             const granted = await PermissionsAndroid.requestMultiple([
//                 PermissionsAndroid.PERMISSIONS.CAMERA,
//                 PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
//             ]);
//             if (
//                 granted['android.permission.RECORD_AUDIO'] ===
//                 PermissionsAndroid.RESULTS.GRANTED &&
//                 granted['android.permission.CAMERA'] ===
//                 PermissionsAndroid.RESULTS.GRANTED
//             ) {
//                 console.log('You can use the cameras & mic');
//             } else {
//                 console.log('Permission denied');
//             }
//         } catch (err) {
//             console.warn(err);
//         }
//     }

//     render() {
//         return (
//             <View style={styles.container}>
//                 {!this.state.isJoined ? (
//                     <>
//                         <ActivityIndicator
//                             size={60}
//                             color="#222"
//                             style={styles.activityIndicator}
//                         />
//                         <Text style={styles.loadingText}>Joining Stream, Please Wait</Text>
//                     </>
//                 ) : (
//                         <>
//                             {this.state.broadcasterVideoState === VideoRemoteState.Decoding ? (
//                                 <RtcRemoteView.SurfaceView
//                                     uid={this.state.uid}
//                                     style={styles.fullscreen}
//                                     channelId={this.state.channel}
//                                     renderMode={VideoRenderMode.Hidden}
//                                     zOrderMediaOverlay={true}
//                                 />
//                             ) : (
//                                     <View style={styles.broadcasterVideoStateMessage}>
//                                         <Text style={styles.broadcasterVideoStateMessageText}>
//                                             {this.videoStateMessage(this.state.broadcasterVideoState)}
//                                         </Text>
//                                     </View>
//                                 )}
//                             <View style={styles.exitButtonContainer}>
//                                 <TouchableOpacity style={styles.exitButton} onPress={this.stopStream}>
//                                     <Text style={styles.exitButtonText}>Покончить с этим</Text>
//                                 </TouchableOpacity>
//                             </View>
//                             <View style={styles.userCountContainer}>
//                                 <Text style={styles.userCountText}>{this.state.userCount} смотрящих</Text>
//                             </View>
//                             <View style={styles.buttonContainer}>
//                                 <TouchableOpacity style={styles.button} onPress={this.onShare}>
//                                     <Text style={styles.buttonText}>Зашарить</Text>
//                                 </TouchableOpacity>
//                                 <TouchableOpacity style={[styles.button, { backgroundColor: this.state.isAudioActive ? 'green' : 'red' }]} onPress={this.onSwitchAudio}>
//                                     <Text style={styles.buttonText}>Звук</Text>
//                                 </TouchableOpacity>
//                             </View>
//                         </>
//                     )}
//             </View>
//             // <View style={styles.container}>
//             //     <View style={styles.inner}>
//             //         <Image
//             //             style={styles.imageLogo}
//             //             source={require("../../assets/images/logo.svg")}
//             //         />
//             //         <Text style={styles.title}>Экран вещания</Text>
//             //         <Text style={styles.title}>Channel ID: {this.props.channel}</Text>
//             //         <Button
//             //             onPress={() => {this.props.navigation.goBack()}}
//             //             title="Отминет"
//             //         ></Button>
//             //     </View>
//             // </View>
//         );
//     }
// }
// const styles = StyleSheet.create({
    // container: {
    //     flex: 1,
    //     justifyContent: 'center',
    //     alignItems: 'center',
    // },
    // loadingText: {
    //     fontSize: 18,
    //     color: '#222',
    // },
    // fullscreen: {
    //     width: dimensions.width,
    //     height: dimensions.height,
    // },
    // buttonContainer: {
    //     flexDirection: 'row',
    //     position: 'absolute',
    //     bottom: 0,
    // },
    // exitButtonContainer: {
    //     flexDirection: 'row',
    //     position: 'absolute',
    //     top: 40,
    //     right: 10
    // },
    // userCountContainer: {
    //     flexDirection: 'row',
    //     position: 'absolute',
    //     top: 40,
    //     left: 10,
    //     backgroundColor: '#0000007a',
    //     paddingVertical: 8,
    //     paddingHorizontal: 16,
    //     borderRadius: 8,
    //     alignItems: 'center',
    //     marginHorizontal: 10,
    //     color: '#fff',
    // },
    // userCountText: {
    //     fontSize: 16,
    //     color: '#fff'
    // },
    // exitButton: {
    //     width: 160,
    //     marginBottom: 50,
    //     paddingVertical: 8,
    //     borderRadius: 8,
    //     alignItems: 'center',
    //     marginHorizontal: 10,
    //     color: '#fff',
    //     backgroundColor: 'red'
    // },
    // exitButtonText: {
    //     fontSize: 17,
    //     color: '#fff',
    // },
    // button: {
    //     width: 80,
    //     backgroundColor: '#fff',
    //     marginBottom: 50,
    //     paddingVertical: 13,
    //     borderRadius: 8,
    //     alignItems: 'center',
    //     marginHorizontal: 10,
    // },
    // buttonText: {
    //     fontSize: 17,
    // },
    // broadcasterVideoStateMessage: {
    //     position: 'absolute',
    //     bottom: 0,
    //     width: '100%',
    //     height: '100%',
    //     backgroundColor: '#222',
    //     justifyContent: 'center',
    //     alignItems: 'center',
    //     flex: 1,
    // },
    // broadcasterVideoStateMessageText: {
    //     color: '#fff',
    //     fontSize: 20,
    // },
    // container: {
    //     height: "100%",
    //     minHeight: "100%",
    //     minHeight: 600,
    //     flex: 1,
    //     backgroundColor: "#fff",
    //     position: "relative",
    // },
    // inner: {
    //     marginTop: "14%",
    //     alignItems: "center",
    //     height: "100%",
    //     padding: 20,
    // },

    // imageLogo: {
    //     width: 220,
    //     height: 66,
    //     marginBottom: 5,
    // },
    // title: {
    //     fontSize: 27,
    // },
    // btnList: {
    //     flexDirection: 'column'
    // }
// });
