import React, { Component } from "react";
import {
    setAppTabBarVisibility,
    setChannel,
    setChatInputText
} from "../../store/user/actions";
import { connect } from "react-redux";
import ViewerStream from "./viewerStream";

class ViewerStreamContainer extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <ViewerStream
                route={this.props.route}
                navigation={this.props.navigation}
                setAppTabBarVisibility={this.props.setAppTabBarVisibility}
                app={this.props.app}
                setChatInputText={this.props.setChatInputText}
            />
        );
    }
}

const mapStateToProps = (state) => {
    return {
        app: state.app,
    };
};

const mapDispatchToProps = {
    setAppTabBarVisibility,
    setChatInputText
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ViewerStreamContainer);
