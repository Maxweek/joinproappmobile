import React, { Component } from "react";
import {
    setChannel,
    setUserType,
    setUserLogged,
    setUserName,
    setImpressionStreamView
} from "../../store/user/actions";
import { connect } from "react-redux";
import Viewer from "./viewer";

class ViewerContainer extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <Viewer
                route={this.props.route}
                navigation={this.props.navigation}
                setUserName={this.props.setUserName}
                setImpressionStreamView={this.props.setImpressionStreamView}
                app={this.props.app}
            />
        );
    }
}

const mapStateToProps = (state) => {
    return {
        app: state.app,
    };
};

const mapDispatchToProps = {
    setUserName,
    setImpressionStreamView
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ViewerContainer);
