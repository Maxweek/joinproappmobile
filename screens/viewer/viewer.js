import { CommonActions } from "@react-navigation/native";
import { HeaderBackButton } from "@react-navigation/stack";
import React, { Component } from "react";
import {
    ScrollView,
    Text,
    TextInput,
    View,
    Animated,
    Button,
    Image,
    StyleSheet, Linking, Alert, StatusBar, ActivityIndicator, KeyboardAvoidingView
} from "react-native";
import { translate } from "../../App";
import { COLORS } from "../../assets/styles/styles";
import FloatingLabelInput from "../../components/floatingLabelInput";
import ProfileBox from "../../components/profileBox";
import TButton from "../../components/tButton";
import * as rootNavigation from "../../navigators/rootNavigator";

import { Platform, NativeModules } from 'react-native';
import HeaderBox from "../../components/headerBox";
import View_Impression from "../impressions/view/view_impression";
import ContainerBox from "../../components/containerBox";
const { StatusBarManager } = NativeModules;

export default class Viewer extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        console.log(View_Impression)
        // View_Impression.timer.clear()
        this.setStream();
    }

    setStream = () => {
        let stream_view = {
            channel_id: this.props.app.impression.current.schedule[0].channel_id,
            schedule: this.props.app.impression.current.schedule[0],
            impression: this.props.app.impression.current,
        }
        this.props.setImpressionStreamView(stream_view);
        console.log("STREAM_VIEW")
        console.log(stream_view)
    }
    goToStream = () => {
        this.props.navigation.push('streamScreen');
    }
    goBack = () => {
        this.props.navigation.goBack()
    }
    goToAuth = () => {
        // this.props.navigation.dispatch(
        //     CommonActions.navigate("profileScreenStack", {
        //         headerShown: true
        //     })
        // );
        // rootNavigation.push('profileScreen')
        // console.log('.............................')
        // console.log(this.props.navigation)
        // console.log(this.props.navigation.getParent())
        rootNavigation.navigate('profileScreen');
    }

    render() {
        let loaded = true;
        if(this.props.app.stream_view.schedule === undefined){
            loaded = false;
        }
        return (
            <ContainerBox
                navigation={this.props.navigation}
                isContentLoaded={loaded}
                paddingTop={true}
                header={
                    <HeaderBox
                        navigation={this.props.navigation}
                        back={true}
                        gradient={true}
                        scheme={'dark'}
                        shadow={false}
                    />
                }>
                <View style={styles.box}>

                    {this.props.app.stream_view.impression.preview !== undefined ?
                        <Image
                            style={styles.imageLogo}
                            source={{ uri: this.props.app.stream_view.impression.preview }}
                        />
                        : null}
                    <Text style={styles.title}>{this.props.app.stream_view.impression.title}</Text>
                     {/* <Text style={{ textAlign: 'center' }}>channel_id: {this.props.app.stream_view.channel_id}</Text>
                            <Text style={{ textAlign: 'center' }}>impression_id: {this.props.app.stream_view.impression.id}</Text>
                            <Text style={{ textAlign: 'center' }}>impression_title: {this.props.app.stream_view.impression.title}</Text>
                            <Text style={{ textAlign: 'center' }}>impression_link: {this.props.app.stream_view.schedule.link}</Text>
                            <Text style={{ textAlign: 'center' }}>date: {this.props.app.stream_view.schedule.date}</Text>
                            <Text style={{ textAlign: 'center' }}>status: {this.props.app.stream_view.schedule.status.toString()}</Text>
                            <Text style={{ textAlign: 'center' }}>donate: {this.props.app.stream_view.impression.donate ? 'true' : 'false'}</Text>
                            <Text style={{ textAlign: 'center' }}>loggedIn: {this.props.app.local.isLoggedIn.toString()}</Text> */}
                </View>
                {!this.props.app.stream_view.impression.auth ?
                    <View style={styles.box}>
                        <View style={{ width: '100%' }}>

                            <FloatingLabelInput
                                label={translate("user.name")}
                                value={this.props.app.user.info.name}
                                onChangeText={this.props.setUserName}
                                autoCorrect={false}
                            />
                        </View>
                        <TButton
                            onPress={this.goToStream}
                            text={translate("default.join")}
                            type="primary"
                        />
                    </View>
                    :
                    !this.props.app.local.isLoggedIn ?
                        <View style={styles.box}>
                            <TButton
                                onPress={this.goToAuth}
                                text={translate("default.enter")}
                                type="primary"
                            />
                        </View>
                        :
                        <View style={styles.box}>
                            <ProfileBox
                                padding={true}
                                border={true}
                                onPress={() => { }}
                                name={this.props.app.user.info.name + ' ' + this.props.app.user.info.last_name}
                                image={this.props.app.user.info.photo}
                                // topText={'Шо-то сверху'}
                                // botText={'Шо-то снизу'}
                                rate={{ value: 4.5, count: 2 }}
                                style={{ marginBottom: 16 }}
                            />
                            <TButton
                                onPress={this.goToStream}
                                text={translate("default.join")}
                                type="primary"
                            />
                        </View>
                }
            </ContainerBox>
        );
    }
}
const styles = StyleSheet.create({
    imageLogo: {
        width: 70,
        height: 70,
        borderRadius: 10,
        marginBottom: 20,
    },
    box: {
        paddingVertical: 16,
        width: '100%'
    },
    title: {
        fontSize: 27,
        textAlign: 'center'
    },
    btnList: {
        flexDirection: 'column'
    }
});
