import React, { Component } from "react";
import MaskedView from "@react-native-community/masked-view";
import { createRef } from "react";
import {
    ScrollView,
    Text,
    TextInput,
    View,
    Animated,
    ActivityIndicator,
    Button,
    Image,
    TouchableOpacity,
    StyleSheet, Linking, Alert, Share, Dimensions, PermissionsAndroid, FlatList, PanResponder, StatusBar, StatusBarIOS, Keyboard, useWindowDimensions, SafeAreaView, KeyboardAvoidingView, TouchableWithoutFeedback
} from "react-native";
import RtcEngine, {
    ChannelProfile,
    ClientRole,
    RtcLocalView,
    RtcRemoteView,
    VideoDimensions,
    VideoEncoderConfiguration,
    VideoFrameRate,
    VideoRemoteState,
    VideoRenderMode,
} from 'react-native-agora';
// import { TouchableOpacity } from "react-native-gesture-handler";
import KeepAwake from "react-native-keep-awake";
import { translate } from "../../App";
import TButton from "../../components/tButton";
import Fire from "../../firebase";
import Icon from "react-native-vector-icons/Ionicons";
import LinearGradient from "react-native-linear-gradient";
import { COLORS } from "../../assets/styles/styles";
var dimensions = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
};

import { Platform, NativeModules } from 'react-native';
import Orientation, { OrientationLocker, PORTRAIT, LANDSCAPE } from "react-native-orientation-locker";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import LoaderBox from "../../components/loaderBox";
import ChatBox from "../../components/chatBox";
import { Easing } from "react-native-reanimated";
import StreamBox from "../../components/streamBox";
const { StatusBarManager } = NativeModules;

export default class ViewerStream extends Component {
    constructor(props) {
        super(props);
        // console.log(NativeModules)
        // console.log(this.props.app)
        this.state = {
            user: 1,
            channel: this.props.app.stream_view.channel_id,
            isJoined: false,
            isBroadcaster: false,
            broadcasterVideoState: VideoRemoteState.Decoding,
            isAudioActive: true,
            isChatActive: false,
            isStreamStarted: false,
            isStreamFinished: false,
            isSendButtonActive: false,
            isVideoStretched: true,
            scrollY: 0,
            dimensions: {
                width: Dimensions.get('window').width,
                height: Dimensions.get('window').height,
            },
            // keyboardAvoidingOffset: dimensions.height - StatusBarManager.HEIGHT - 32,
            animateVars: {
                chatVar: new Animated.Value(0),
                chatButtonVar: new Animated.Value(0),
                inputWidth: new Animated.Value(0),
                shadowOpacity: new Animated.Value(0),
                windowOpacity: new Animated.Value(0),
                iwindowOpacity: new Animated.Value(0),
                sendButtonActivity: new Animated.Value(0),
            },
            userCount: 0,
            isPortrait: true,
            messages: [
            ]
        }
        // console.log(Orientation)
        // this.
        this.AgoraEngine = null;
        this._animatedLiveIndicator = new Animated.Value(0);
        this.FlatListRef;
        this.messRef;
    }

    async initStream() {
        console.log(this.props.app.stream_view.channel_id)
        this.AgoraEngine = createRef()
        this.AgoraEngine.current = await RtcEngine.create(
            // '7992c14f45514cd289bf1950758e071e', Основной jp
            // '1e2e1300252c4bb9afd5ef247e30bf00', Запасной мой
            '987fd1c2247346a082c28e84f03d5de6'
            // 'c16a92ce413f41598afd66a0d4555fab',
            // 'c7e742d5df23478285a9dc4f4ff62407',w
        );
        this.AgoraEngine.current.setVideoEncoderConfiguration(new VideoEncoderConfiguration({
            dimensions: new VideoDimensions(1280, 720),
            bitrate: 15420,
            frameRate: VideoFrameRate.Fps60,
            degradationPrefer: 2
        }))
        // this.AgoraEngine.current.setVideoEncoderConfiguration({
        //     width: 1280,
        //     height: 720,
        //     bitrate: 15420,
        //     frameRate: 60,
        //     // orientationMode: Adaptative,
        // })
        console.log(this.AgoraEngine.current)

        this.AgoraEngine.current.enableVideo(true);
        this.AgoraEngine.current.setChannelProfile(ChannelProfile.LiveBroadcasting);

        this.AgoraEngine.current.setClientRole(ClientRole.Audience);

        // this.AgoraEngine.current.addListener('RemoteVideoStateChanged', (uid, state) => {
        //     if (uid === 2) this.setBroadcasterVideoState(state);
        // });

        this.AgoraEngine.current.addListener(
            'JoinChannelSuccess',
            (channel, uid, elapsed) => {
                console.log('JoinChannelSuccess', channel, uid, elapsed);
                this.setJoined(true);
            },
        );
        this.AgoraEngine.current.addListener(
            'UserJoined',
            (uid, elapsed) => {
                console.log('UserJoined', uid, elapsed);
            },
        );
        this.AgoraEngine.current.addListener(
            'VideoSizeChanged',
            (stats, width, height, rot) => {
                // console.log({stats, width, height, rot});
                //this.setUserCount(stats.users - 1);
            },
        );
    }

    async onShare() {
        try {
            const result = await Share.share({ message: this.props.app.stream_view.impression.link });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            console.log(error.message);
        }
    }

    setUserCount(count) {
        if (this.state.userCount !== count) {
            this.setState({
                userCount: count
            })
        }
    }
    _onOrientationDidChange = (orientation) => {
        dimensions = {
            screen: {
                width: Dimensions.get('screen').width,
                height: Dimensions.get('screen').height
            }, window: {
                width: Dimensions.get('window').width,
                height: Dimensions.get('window').height
            }
        }
        // console.log(orientation)
        if (!orientation) {
            if (dimensions.window.width < dimensions.window.height) {
                orientation = 'PORTRAIT'
            }
        }
        // console.log(dimensions)
        // console.log(orientation === 'PORTRAIT' ? dimensions.screen.height:dimensions.screen.height)
        this.setState({
            dimensions: {
                // width: orientation === 'PORTRAIT' ? dimensions.screen.width : dimensions.screen.height,
                // height: orientation === 'PORTRAIT' ? dimensions.screen.height : dimensions.screen.width,
                width: dimensions.screen.width,
                height: dimensions.screen.height,
            }
        }, () => {
            // console.log(this.state.dimensions)
            if (orientation === 'PORTRAIT') {
                if (Platform.OS === 'android') {
                    StatusBar.setHidden(false)
                }
                this.setState({ isPortrait: true })
            } else {
                if (Platform.OS === 'android') {
                    StatusBar.setHidden(true)
                }
                this.setState({ isPortrait: false })
            }
            if (this.state.isChatActive) {
                this.openChat()
            } else {
                this.closeChat()
            }
        })

    }

    setFirebase = (type) => {
        if (type) {

            this.firebase = new Fire(this.state.channel);
            this.firebase.onAdd(snapshotBox => {
                console.log(snapshotBox)
                let snapshot = snapshotBox.val();
                if (snapshotBox.val().count !== undefined) {
                    this.setUserCount(snapshotBox.val().count)
                    return
                }
                if (snapshotBox.val().status !== undefined) {
                    if (snapshotBox.val().status === 'on') {
                        this.startStream()
                    }
                    if (snapshotBox.val().status === 'off') {
                        this.finishStream()
                    }
                    return
                }
                let mess = [...this.state.messages];
                let nextId = this.state.messages.length ? this.state.messages[this.state.messages.length - 1].id + 1 : 0;
                mess.push(
                    {
                        id: nextId,
                        message: snapshot.message,
                        timestamp: snapshot.timestamp ? new Date(snapshot.timestamp) : new Date(),
                        user: {
                            id: snapshot.user.id,
                            type: snapshot.user ? snapshot.user.type : '',
                            avatar: snapshot.user.avatar,
                            username: snapshot.user.username,
                        }
                    }
                )
                this.setState({
                    messages: mess
                }, () => {
                    // console.log(this.state.messages)
                })
                // console.log(message)
                // this.setState(previousState => ({
                //     messages: GiftedChat.append(previousState.messages, message),
                // })
            });
            this.firebase.onChange(snapshotBox => {
                console.log(snapshotBox)
                console.log(snapshotBox.val())
                if (snapshotBox.val().count !== undefined) {
                    this.setUserCount(snapshotBox.val().count)
                }
            })
        } else {
            this.firebase.off()

        }
    }

    componentDidMount() {
        // Orientation.addOrientationListener(this._onOrientationDidChange);
        // Orientation.unlockAllOrientations()
        // this.set();
        // this.props.setAppTabBarVisibility(false)
        // this.setFirebase(true)

        // this.closeChat()
    }

    componentDidUpdate() {
        // this.set()
    }

    componentWillUnmount() {
        // Orientation.removeOrientationListener(this._onOrientationDidChange);
        // Orientation.lockToPortrait()
        // StatusBar.setHidden(false)

        // this.AgoraEngine.current.leaveChannel()
        // this.AgoraEngine.current.destroy();

        // this.setFirebase(false)
    }

    set() {
        // if (Platform.OS === 'android') this.requestCameraAndAudioPermission();
        const uid = 3;
        this.initStream().then(() => {
            this.AgoraEngine.current.joinChannel(
                null,
                this.state.channel,
                null,
                uid,
            );
            // this.setRecording();
            // this.firebase.start()
        }
        );
        // return () => {
        //     this.AgoraEngine.current.destroy();
        // };
    }

    setBroadcasterVideoState(data) {
        this.setState({
            broadcasterVideoState: data
        })
    }

    setJoined(type) {
        console.log('joined: ' + type)
        this.setState({
            isJoined: type
        })
    }
    openChat = () => {
        console.log(-dimensions.width)
        Animated.spring(this.state.animateVars.chatVar, {
            toValue: 0,
            duration: 200,
            useNativeDriver: true
        }).start();
        Animated.timing(this.state.animateVars.shadowOpacity, {
            toValue: 1,
            duration: 400,
            useNativeDriver: true
        }).start();
        Animated.timing(this.state.animateVars.windowOpacity, {
            toValue: 0,
            duration: 100,
            useNativeDriver: true
        }).start();
        Animated.timing(this.state.animateVars.iwindowOpacity, {
            toValue: 1,
            duration: 100,
            useNativeDriver: true
        }).start();
        Animated.spring(this.state.animateVars.chatButtonVar, {
            toValue: -this.state.dimensions.width,
            duration: 200,
            useNativeDriver: true
        }).start();
    }
    closeChat = () => {
        Animated.spring(this.state.animateVars.chatVar, {
            toValue: this.state.dimensions.width,
            duration: 200,
            useNativeDriver: true
        }).start();
        Animated.timing(this.state.animateVars.shadowOpacity, {
            toValue: 0,
            duration: 400,
            useNativeDriver: true
        }).start();
        Animated.timing(this.state.animateVars.windowOpacity, {
            toValue: 1,
            duration: 100,
            useNativeDriver: true
        }).start();
        Animated.timing(this.state.animateVars.iwindowOpacity, {
            toValue: 0,
            duration: 100,
            useNativeDriver: true
        }).start();
        Animated.spring(this.state.animateVars.chatButtonVar, {
            toValue: 0,
            duration: 200,
            useNativeDriver: true
        }).start();

        Keyboard.dismiss()
    }
    toggleChat = (type = true) => {
        if (!this.state.isChatActive) {
            if (type) {
                this.setState({ isChatActive: true })
            }
            this.openChat()
        } else {
            if (type) {
                this.setState({ isChatActive: false })
            }
            this.closeChat()
        }
    }
    onSwitchAudio = () => {
        if (!this.state.isAudioActive) {
            this.setState({
                isAudioActive: true
            })
            this.AgoraEngine.current.muteAllRemoteAudioStreams(false);
        } else {
            this.setState({
                isAudioActive: false
            })
            this.AgoraEngine.current.muteAllRemoteAudioStreams(true);
        }
    }
    onVideoStretch = () => {
        if (!this.state.isVideoStretched) {
            this.setState({
                isVideoStretched: true
            })
        } else {
            this.setState({
                isVideoStretched: false
            })
        }
    }

    onInputFocus = () => {

    }
    onMessageSend = () => {
        if (this.props.app.local.chatInputText.length) {
            this.firebase.append({
                message: this.props.app.local.chatInputText,
                timestamp: this.firebase.timestamp,
                user: {
                    avatar: this.props.app.user.info.photo !== '' ? this.props.app.user.info.photo : 'https://joinpro.ru/upload/uf/eb8/eb8dd1868ddabedb66fe28923e0052f7.png',
                    username: this.props.app.user.info.name
                }
            })
            this.props.setChatInputText('');
        }
    }
    goExit = () => {
        if (this.props.app.stream_view.impression.donate) {
            this.goDonate()
        } else {
            var go = () => {
                this.props.navigation.navigate('impressionsScreen', {
                    screen: 'listImpressionScreen',
                });
                this.props.setAppTabBarVisibility(true)
            }
            Alert.alert(
                translate("screen.viewStream.exit"),
                "",
                [
                    { text: translate('screen.createImpression.confirm.cancel'), style: "cancel" },
                    { text: translate('screen.createImpression.confirm.gotIt'), onPress: () => go() }
                ]
            );
        }
    }

    goDonate = () => {
        this.props.navigation.navigate('donateScreen', {
            impression_id: this.props.app.stream_view.impression.id,
            impression: this.props.app.stream_view.impression,
        })
    }

    startStream = () => {
        this.setState({
            isStreamFinished: true,
        })

    }
    finishStream = () => {

        this.setState({
            isStreamStarted: true,
            isStreamFinished: true,
        })
    }
    setChatInputText = (text) => {
        this.props.setChatInputText(text)
    }
    render() {
        let chat = { transform: [{ translateX: this.state.animateVars.chatVar }] }
        let chatAvoid = { transform: [{ translateX: this.state.animateVars.chatButtonVar }] }

        let shadowOpacity = { opacity: this.state.animateVars.shadowOpacity }
        let windowOpacity = { opacity: this.state.animateVars.windowOpacity }
        let iwindowOpacity = { opacity: this.state.animateVars.iwindowOpacity }

        let fullscreenBoxWide = {
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').width * 16 / 9
        }
        if (!this.state.isPortrait) {
            fullscreenBoxWide = {
                height: Dimensions.get('window').height,
                width: Dimensions.get('window').height * 16 / 9
            }
        }
        if (this.state.isVideoStretched) {
            fullscreenBoxWide = {
                width: '100%',
                height: '100%'
            }
        }
        return (
            <StreamBox
                isHost={false}
                channelId={this.props.app.stream_view.channel_id}
                app={this.props.app}
                navigation={this.props.navigation}
                setAppTabBarVisibility={this.props.setAppTabBarVisibility}
                setChatInputText={this.props.setChatInputText}
            ></StreamBox>
        )
        // return (
        //     <>
        //         {!this.state.isJoined ? (
        //             <View style={styles.container}>
        //                 <LoaderBox text={translate("screen.viewStream.joinWait")}></LoaderBox>
        //             </View>
        //         ) : (
        //             <>
        //                 <KeepAwake />
        //                 <StatusBar barStyle={'light-content'} />
        //                 <View style={styles.streamBox}>
        //                     <View style={styles.fullscreen}>
        //                         {/* <View style={fullscreenBoxWide}></View> */}
        //                         <RtcRemoteView.SurfaceView
        //                             style={fullscreenBoxWide}
        //                             uid={1}
        //                             channelId={this.state.channel} />
        //                     </View>
        //                     <Animated.View style={[{
        //                         position: 'absolute',
        //                         width: '100%',
        //                         height: '100%',
        //                     }, shadowOpacity]}>
        //                         <LinearGradient colors={['#00000000', '#00000044', '#00000066', '#00000088', '#000000aa', '#000000cc', '#000000fa',]} style={[styles.linearGradientBack]}>
        //                         </LinearGradient>
        //                     </Animated.View>
        //                     <LinearGradient colors={['#000000aa', '#00000000',]} style={styles.topGrad}>
        //                     </LinearGradient>
        //                     <LinearGradient colors={['#00000000', '#000000aa']} style={styles.botGrad}>
        //                     </LinearGradient>
        //                 </View>
        //                 <SafeAreaView style={styles.streamBox_safetyView}
        //                 // onLayout={() => { console.log('YAW'); this._onOrientationDidChange() }}
        //                 >
        //                     <KeyboardAvoidingView style={styles.streamBox_keyboardAvoidingView} behavior={"padding"}>
        //                         <Animated.View style={[styles.animatedWrapper, windowOpacity, chatAvoid]}>
        //                             <Animated.View style={styles.topLeft}>
        //                                 <View style={styles.userCountContainer}>
        //                                     <Icon name={'eye'} size={16} color={'white'} />
        //                                     <Text style={styles.userCountText}>{this.state.userCount}</Text>
        //                                 </View>
        //                             </Animated.View>
        //                             <Animated.View style={styles.topRight}>
        //                                 {this.props.app.stream_view.impression.donate ?
        //                                     <TouchableOpacity style={[styles.btn, styles.donate]} onPress={this.goDonate}>
        //                                         <Icon name={'heart'} size={24} color={COLORS.white} />
        //                                     </TouchableOpacity>
        //                                     : null}
        //                                 <TouchableOpacity onPress={this.goExit} style={styles.btn}>
        //                                     <Icon name={'exit-outline'} size={24} color={COLORS.black} />
        //                                 </TouchableOpacity>
        //                             </Animated.View>
        //                             <Animated.View style={styles.bottomLeft}>
        //                                 <TouchableOpacity onPress={this.onVideoStretch} style={[styles.btn, { marginLeft: 0 }]}>
        //                                     <Icon name={this.state.isVideoStretched ? "crop-outline" : "expand-outline"} size={24} color={COLORS.black} />
        //                                 </TouchableOpacity>
        //                                 <TouchableOpacity onPress={this.onSwitchAudio} style={[styles.btn, { marginLeft: 8 }]}>
        //                                     <Icon name={this.state.isAudioActive ? "volume-high-outline" : "volume-mute-outline"} size={24} color={COLORS.black} />
        //                                 </TouchableOpacity>
        //                             </Animated.View>
        //                             <Animated.View style={styles.bottomRight}>
        //                                 <TouchableOpacity onPress={this.toggleChat} style={[styles.btn, {
        //                                     width: 96,
        //                                 }]}>
        //                                     <Text style={{ marginRight: 12, opacity: this.state.isChatActive ? 0 : 1 }}>{translate("screen.viewStream.openChat")}</Text>
        //                                     <View style={{ marginLeft: this.state.isChatActive ? 60 : 0 }}>
        //                                         <Icon name={!this.state.isChatActive ? 'chatbubbles-outline' : 'chevron-back-outline'} size={24} color={COLORS.black} />
        //                                     </View>
        //                                 </TouchableOpacity>
        //                             </Animated.View>
        //                         </Animated.View>

        //                         <Animated.View style={[styles.animatedWrapper, chat, iwindowOpacity]}>
        //                             {/* <TouchableWithoutFeedback  onPress={e => {return}}>
        //                                 <View style={{position: 'absolute', zIndex: 20, width: '100%', height: '100%'}}></View>
        //                                 </TouchableWithoutFeedback> */}

        //                             <ChatBox
        //                                 masked={true}
        //                                 messages={this.state.messages}
        //                                 inputValue={this.props.app.local.chatInputText}
        //                                 onInputFocus={this.onInputFocus}
        //                                 onChangeText={this.setChatInputText}
        //                                 onSend={this.onMessageSend}
        //                                 dimensions={this.state.dimensions}
        //                                 leftButtonAction={this.toggleChat}
        //                             ></ChatBox>
        //                         </Animated.View>
        //                     </KeyboardAvoidingView>
        //                 </SafeAreaView>
        //             </>
        //         )
        //         }
        //     </>
        // );
    }
}
const styles = StyleSheet.create({
    streamBox: {
        height: '100%',
        width: '100%',
        backgroundColor: '#f5f5f5',
        // paddingTop: StatusBarManager.HEIGHT,
        position: 'absolute',
    },
    streamBox_safetyView: {
        flex: 1,
        borderColor: "gray",
        position: 'relative'
    },
    streamBox_keyboardAvoidingView: {
        // overflow: 'hidden',
        flex: 1,
        position: 'relative'
    },
    animatedWrapper: {
        flex: 1,
        height: '100%',
        width: '100%',
        position: 'absolute'
    },
    topGrad: {
        position: 'absolute',
        top: 0,
        height: StatusBarManager.HEIGHT,
        width: '100%'
    },
    botGrad: {
        position: 'absolute',
        bottom: 0,
        height: StatusBarManager.HEIGHT - 10,
        width: '100%'
    },
    btn: {
        width: 48,
        height: 48,
        backgroundColor: 'white',
        borderRadius: 12,
        elevation: 8,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        elevation: 2,
        shadowColor: 'black',
        shadowOffset: {
            width: 0,
            height: 0
        },
        shadowOpacity: 0.1,
        shadowRadius: 4,
    },
    donate: {
        backgroundColor: COLORS.primary,
        marginRight: 16,
    },
    topLeft: {
        position: 'absolute',
        top: 16,
        left: 16,
        flexDirection: 'row',
        // backgroundColor: 'green',
        zIndex: 120
    },
    topRight: {
        position: 'absolute',
        top: 16,
        right: 16,
        flexDirection: 'row',
        // backgroundColor: 'green',
        zIndex: 120
    },
    bottomLeft: {
        position: 'absolute',
        bottom: 16,
        left: 16,
        flexDirection: 'row',
        zIndex: 20
    },
    bottomRight: {
        position: 'absolute',
        bottom: 16,
        right: 16,
        flexDirection: 'row',
        height: 48,
        zIndex: 20
    },
    chatButton: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height: 48,
        backgroundColor: 'white',
        borderRadius: 12,
        elevation: 8,
        paddingHorizontal: 12,
        zIndex: 20,
        elevation: 2,
        shadowColor: 'black',
        shadowOffset: {
            width: 0,
            height: 0
        },
        shadowOpacity: 0.1,
        shadowRadius: 4,
    },
    chatButtonBadge: {
        position: 'absolute',
        right: -5,
        top: -10,
        backgroundColor: COLORS.primary,
        paddingVertical: 2,
        paddingHorizontal: 4,
        borderRadius: 50
    },
    chatBox: {
        position: 'absolute',
        left: 0,
        bottom: 0,
        width: '100%',
        height: '100%',
        // left: dimensions.width,
        paddingTop: 100,
        // zIndex: 190
    },
    chatBox_inner: {
        flexGrow: 1,
        width: '100%',
        position: 'relative',
        backgroundColor: '#004400',
        borderWidth: 4,
        borderColor: 'yellow'
        // backgroundColor: 'gray'
    },
    linearGradientBack: {
        position: 'absolute',
        width: '100%',
        height: '100%',
    },
    messageListBox: {
        position: 'absolute',
        zIndex: 20,
        bottom: 0,
        width: '100%',
        height: '100%',
    },
    container: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
    },
    loadingText: {
        fontSize: 18,
        color: '#222',
    },
    fullscreen: {
        // backgroundColor: 'blue',
        // width: dimensions.width,
        width: '100%',
        // height: dimensions.height,
        height: '100%',
        // position: 'absolute',
        // paddingTop: 0,
        // paddingTop: StatusBarManager.HEIGHT,
        // backgroundColor: 'white',
        // marginTop: 0,

        position: 'relative',
        alignItems: 'center',
        justifyContent: 'center',
    },
    exitButtonContainer: {
        flexDirection: 'row',
        position: 'absolute',
        zIndex: 100,
        top: 40,
        right: 16
    },
    userCountContainer: {
        flexDirection: 'row',
        zIndex: 20,
        backgroundColor: '#00000044',
        paddingVertical: 4,
        paddingHorizontal: 8,
        borderRadius: 8,
        alignItems: 'center',
        // marginHorizontal: 10,
        color: '#fff',
    },
    userCountText: {
        fontSize: 14,
        marginLeft: 6,
        color: '#fff'
    },
    buttonContainer: {
        flexDirection: 'row',
        position: 'absolute',
        width: dimensions.width,
        justifyContent: 'space-between',
        backgroundColor: '#00000055',
        bottom: 0,
        width: '100%',
        padding: 16,
        // marginVertical: 20
    },
    recordingContainer: {
        flexDirection: 'row',
        position: 'absolute',
        top: 90,
        // width: 100,
        right: 16,
        backgroundColor: '#00000044',
        paddingVertical: 4,
        paddingHorizontal: 8,
        borderRadius: 8,
        justifyContent: 'flex-end',
        alignItems: 'center',
        // marginHorizontal: 10,
        color: '#fff',
    },
    recordingContainerIndicator: {
        width: 8,
        height: 8,
        borderRadius: 8,
        backgroundColor: 'red',
        marginRight: 8
    },
    recordingContainerText: {
        color: '#fff',
        fontSize: 12
    },
    button: {
        width: 80,
        backgroundColor: '#fff',
        marginBottom: 10,
        paddingVertical: 13,
        borderRadius: 8,
        alignItems: 'center',
        marginHorizontal: 10,
    },
    buttonText: {
        fontSize: 15,
        textAlign: 'center',
    },
    broadcasterVideoStateMessage: {
        position: 'absolute',
        bottom: 0,
        width: '100%',
        height: '100%',
        backgroundColor: '#222',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    broadcasterVideoStateMessageText: {
        color: '#fff',
        fontSize: 20,
    },
    previewBox: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        backgroundColor: COLORS.white,
        zIndex: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    previewBox__gradient: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
    },
    previewBox__title: {
        fontSize: 32,
        fontWeight: '100'
    },
});