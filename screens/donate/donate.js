import React, { Component } from "react";
import {
    Text,
    View,
    StyleSheet,
    ActivityIndicator,
    ScrollView,
    RefreshControl,
    TouchableOpacity,
    Dimensions,
    StatusBar,
    KeyboardAvoidingView,
    Image
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import Icon from "react-native-vector-icons/Ionicons";
import API, { API_DONATE_LIST, API_PAYMENT_APPLE_PURCHASE } from "../../API";
import { COLORS, STYLES } from "../../assets/styles/styles";
import PaymentStripeBox from "../../components/paymentStripeBox";
import TButton from "../../components/tButton";
import Shared_EmptyList from "../_shared/emptyList";

import SelectPicker from "../../components/selectPicker";

import { Platform, NativeModules } from 'react-native';
import HeaderBox from "../../components/headerBox";
import { translate } from "../../App";
import ContainerBox from "../../components/containerBox";
import Orientation from "react-native-orientation-locker";
import ProfileBox from "../../components/profileBox";
import { API_REQUEST } from "../../API_functions";
import * as RNIap from 'react-native-iap';
import * as rootNavigation from "../../navigators/rootNavigator";
import ModalBox from "../../components/modalBox";
const { StatusBarManager } = NativeModules;

let purchaseUpdateSubscription = RNIap.purchaseUpdatedListener((purchase) => {
    console.log('purchaseUpdatedListener', purchase);
    // this.setState({ receipt: purchase.transactionReceipt }, () => this.goNext());
});
let purchaseErrorSubscription = RNIap.purchaseErrorListener((error) => {
    console.log('purchaseErrorListener', error);
    // Alert.alert('purchase error', JSON.stringify(error));
});

export default class Donate extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isListenerAdded: false,
            isActiveBtn: true,
            selectedItem: null,
            selectedCurrency: null,
            items: [],
            currencies: [],
            products: [],
            guide: undefined,
            isTransactionPending: false,
        }
    }

    getList = () => {
        API.get(API_DONATE_LIST)
            .then(res => {
                console.log(res.data)
                if (typeof res.data.status !== 'undefined') {
                    if (res.data.status === 'success') {
                        console.log('list_loaded')
                        console.log(res.data.items)
                        this.setCurrencies(res.data.currency)
                        this.setList(res.data.items);
                        this.setState({ refreshing: false, isNotEmpty: true });
                        if (this.props.app.impression.current) {
                            this.getGuideInfo(this.props.app.impression.current.user_id);
                        }
                    } else {
                        console.log('list_load_error_1')
                        this.setState({ refreshing: false, isNotEmpty: false });
                    }
                } else {
                    console.log('list_load_error_2')
                    this.setState({ refreshing: false, isNotEmpty: false });
                }
            })
            .catch(err => {
                console.log(err)
                this.setState({ refreshing: false, isNotEmpty: false });
            })
    }
    setCurrencies = list => {
        this.setState({ currencies: list }, () => {
            this.setCurrency(this.state.currencies[0].value)
        });
    }
    setList = list => {
        console.log('list setted')
        this.setState({ items: list });
        this.setState({ selectedItem: list[1] })
        console.log(this.state.items)
    }
    onRefresh = () => {
        this.setState({ refreshing: true })

        console.log('refreshed')
        this.getList();
    }
    componentDidMount() {
        Orientation.lockToPortrait()
        this.onRefresh()
        this.prepareProducts();
    }
    selectDonate = donate => {
        this.setState({ selectedItem: donate })
    }
    setCurrency = (value) => {
        let selectedCurrency = null
        this.state.currencies.map(el => {
            if (el.value === value) {
                selectedCurrency = el;
            }
        })
        if (selectedCurrency === null) {
            return;
        }
        this.setState({ selectedCurrency })
    }
    getCurrencies = () => {
        let arr = [...this.state.currencies];
        console.log(arr)
        return arr;
    }

    getGuideInfo = async (id) => {
        API_REQUEST.getUserInfo(id).then(guide => {
            this.setState({
                guide
            })
        }).catch(err => {
            console.log(err)
        })
    }
    getDonateCards = () => {
        if (this.state.selectedItem === null) {
            return;
        }
        return this.state.items.map(el => {
            let isSelected = false;
            let cost = {
                value: el.costs.filter(el => el.currency === this.state.selectedCurrency.value)[0].cost,
                currency: this.state.selectedCurrency.value,
                symbol: this.state.selectedCurrency.symbol,
            }
            if (el.id === this.state.selectedItem.id) {
                isSelected = true;
            }
            return (
                <TouchableOpacity key={"item__" + el.id} onPress={() => {
                    this.selectDonate(el)
                }} style={[styles.donateItem, { backgroundColor: isSelected ? COLORS.primary : COLORS.white }]}>
                    <View style={styles.donateItem_box}>
                        <Text style={[styles.donateItem_currency, { color: isSelected ? COLORS.white : COLORS.gray, marginRight: 4 }]}>{cost.symbol}</Text>
                        <Text style={[styles.donateItem_cost, { color: isSelected ? COLORS.white : COLORS.black }]}>{cost.value}</Text>
                    </View>
                </TouchableOpacity>
            )
        })
    }
    async prepareProducts() {
        if (Platform.OS === 'android') {
            return
        }

        const products = Platform.select({
            ios: [
                'rn_jp_app_donation_1',
                'rn_jp_app_donation_2',
                'rn_jp_app_donation_3',
                'rn_jp_app_donation_4',
                'rn_jp_app_donation_5',
                'rn_jp_app_donation_6'
            ],
            android: [
                'com.example.coins100'
            ]
        });

        RNIap.initConnection().catch(() => {
            console.log('error connecting to the store')
        }).then(() => {
            console.log('connected to the store');

            RNIap.getProducts(products).catch(() => {
                console.log('error finding products')
            }).then((res) => {
                console.log('get products')
                console.log(res)
                this.setState({ products: res })
                // RNIap.requestPurchaseWithOfferIOS(products[0]['productId']).then((res) => {
                //     console.log(res)
                //     RNIap.clearTransactionIOS()
                //     // RNIap.finishTransaction(res.transactionId).then((res) => {
                //     //     console.log(res)
                //     // }).catch((res) => {
                //     //     console.log(res)
                //     // })
                // }).catch((res) => {
                //     console.log(res)
                // })
            })
        })
    }
    toggleSuccessModal = (type = null) => {
        this.setState({
            isModalSuccessActive: type,
        }, () => {
            if (this.state.isModalSuccessActive) {
                this.modalizeSuccessRef.open();
            } else {
                this.modalizeSuccessRef.close();
            }
        });
    };
    toggleErrorModal = (type = null) => {
        this.setState({
            isModalErrorActive: type,
        }, () => {
            if (this.state.isModalErrorActive) {
                this.modalizeErrorRef.open();
            } else {
                this.modalizeErrorRef.close();
            }
        });
    };
    requestPurchase = (id) => {
        // console.log(this.props.app)
        // this.toggleSuccessModal(true)
        // this.toggleErrorModal(true)
        // return
        if (this.state.isTransactionPending) {
            return
        }
        this.setState({ isTransactionPending: id })
        console.log(RNIap)
        RNIap.clearTransactionIOS()

        RNIap.requestPurchase(id).then((res) => {
            this.serverRequest(id)
            console.log(res)
            // RNIap.clearTransactionIOS()
        }).catch((res) => {
            this.toggleErrorModal(true)
            console.log(res)
            this.setState({ isTransactionPending: false })
        })
    }
    serverRequest = id => {
        const formData = new FormData();

        // console.log(this.props.app)

        formData.append('impression_id', this.props.route.params.impression.id);
        formData.append('user_id', this.props.app.user.info.id);
        formData.append('in_app_id', id);

        console.log(formData)

        API.post(API_PAYMENT_APPLE_PURCHASE, formData)
            .then(res => {
                this.toggleSuccessModal(true)
                console.log(res.data)
                RNIap.clearTransactionIOS()
                this.setState({ isTransactionPending: false })
            })
            .catch(err => {
                console.log(err)
                this.setState({ isTransactionPending: false })
            })
    }
    getDonateProducts = () => {
        if (!this.state.products.length) {
            return <View style={{
                backgroundColor: COLORS.lightgray,
                height: 100,
                padding: 16,
                margin: 8,
                borderWidth: 1,
                borderStyle: 'solid',
                borderColor: COLORS.lightgray,
                borderRadius: 8,
                overflow: 'hidden',
                position: 'relative',
                flexGrow: 1,
                flexBasis: '25%'
            }}></View>
        } else {
            return this.state.products.map(el => {
                console.log(el)
                let isActive = el.productId === this.state.isTransactionPending ? true : false;
                let back = this.state.isTransactionPending ? isActive ? COLORS.primary : '#eeeeee' : COLORS.white

                return (
                    <TouchableOpacity key={"item__" + el.productId} onPress={() => {
                        this.requestPurchase(el.productId)
                    }} style={[styles.donateItem, { backgroundColor: back }]} disabled={this.state.isTransactionPending}>
                        <View style={styles.donateItem_box}>
                            <Text style={[styles.donateItem_cost, { color: isActive ? COLORS.white : COLORS.black }]}>{el.price}</Text>
                            <Text style={[styles.donateItem_currency, { color: isActive ? COLORS.white : COLORS.gray, marginLeft: 4 }]}>{el.currency}</Text>
                        </View>
                        <View style={[styles.donateItem_loader, { display: isActive ? 'flex' : 'none' }]}>
                            <ActivityIndicator
                                size={'small'}
                                color={COLORS.black}
                                style={styles.donateItem_loader__indicator}
                            />
                        </View>
                    </TouchableOpacity>
                )
            })
        }
    }
    render() {
        let cost = {
            amount: 0,
            currency: ''
        }
        if (this.state.selectedItem !== null) {
            cost.amount = this.state.selectedItem.costs.filter(el => el.currency === this.state.selectedCurrency.value)[0].amount;
            cost.currency = this.state.selectedCurrency.value.toLowerCase();
        }
        return (
            <ContainerBox
                navigation={this.props.navigation}
                paddingTop={true}
                header={
                    <HeaderBox
                        navigation={this.props.navigation}
                        back={true}
                        scheme={'dark'}
                        gradient={true}
                    // shadow={true}
                    />
                }
                isContentLoaded={this.state.isContentLoaded}
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
                title={translate("screen.donate.title")}
            >
                <View style={{ marginTop: 20 }}></View>
                {this.props.route.params.impression ?
                    <View style={styles.box}>
                        <View style={styles.impressionPreview}>
                            <View style={styles.impressionPreview__imageBox}>
                                {this.props.app.impression.current.gallery[0] ?
                                    <Image
                                        style={styles.impressionPreview__image}
                                        source={{ uri: this.props.app.impression.current.gallery[0] }}
                                    />
                                    : <Image
                                        style={styles.impressionPreview__image}
                                        source={require('../../assets/images/ref__impression_image_default.jpg')}
                                    />}
                            </View>
                            <View style={styles.impressionPreview__box}>
                                <Text style={styles.impressionPreview__text}>
                                    {translate("screen.donate.subtitle")}
                                </Text>
                                <Text style={styles.impressionPreview__title}>
                                    {this.props.route.params.impression.title}
                                </Text>
                            </View>
                        </View>
                    </View>
                    : null}
                {this.state.guide !== undefined ?
                    <ProfileBox
                        padding={true}
                        border={true}
                        onPress={() => {
                            rootNavigation.push('impressionViewNavigator', {
                                screen: "userScreen",
                                params: {
                                    id: this.state.guide.id
                                }
                            })
                        }}
                        name={this.state.guide.name + ' ' + (this.state.guide.last_name ? this.state.guide.last_name : '')}
                        image={this.state.guide.photo}
                        topText={translate("guide.conducting")}
                        // botText={'Шо-то снизу'}
                        rate={this.state.guide.rate}
                        style={{ marginBottom: 0, width: '100%' }}
                    />
                    :
                    null
                }
                {Platform.OS === 'android' ?
                    <>
                        <View style={styles.box}>
                            <SelectPicker
                                onValueChange={(value) => { this.setCurrency(value) }}
                                // placeholder={'Выберите валюту'}
                                value={this.state.selectedCurrency !== null ? this.state.selectedCurrency.value : ''}
                                items={this.getCurrencies()}
                            />
                        </View>
                        <View style={[styles.box, styles.donateList]}>
                            {this.getDonateCards()}
                        </View>
                        <View style={styles.box}>

                            <PaymentStripeBox
                                style={styles.donate}
                                padding={true}
                                border={true}
                                shadow={true}
                                actionAfterPayment={() => {
                                    this.props.navigation.navigate('mainScreen', {
                                    });
                                    this.props.navigation.popToTop()
                                    // this.props.navigation.navigate('impressionsScreen', {
                                    //     screen: 'listImpressionScreen',
                                    // });
                                }}

                                amount={cost.amount}
                                currency={cost.currency}
                                impression_id={555555}
                                billingDetails={{
                                    email: this.props.app.user.email,
                                    name: this.props.app.user.info.name,
                                }}
                                addFields={[
                                    {
                                        fieldName: translate("user.yourName"),
                                        fieldValue: this.props.app.user.info.name,
                                        fieldAction: this.props.setUserName
                                    },
                                    {
                                        fieldName: translate("user.email"),
                                        fieldValue: this.props.app.user.email,
                                        fieldAction: this.props.setUserEmail
                                    },
                                ]}
                            />

                            <TButton
                                onPress={() => {
                                    this.props.navigation.popToTop()
                                }}
                                text={translate("screen.donate.doNotWant")}
                                type="simple"
                                style={{ marginTop: 32 }}
                            />
                        </View>
                    </>
                    :
                    <>
                        <View style={[styles.box, styles.donateList]}>
                            {this.getDonateProducts()}
                        </View>
                        <View style={styles.box}>
                            <TButton
                                onPress={() => {
                                    this.props.navigation.popToTop()
                                }}
                                text={translate("screen.donate.doNotWant")}
                                type="simple"
                                style={{ marginTop: 32 }}
                            />
                        </View>
                        <ModalBox
                            modalRef={ref => this.modalizeSuccessRef = ref}
                            // headerTitle={translate('default.filterName')}
                            // animated={this.animated}
                            tapGestureEnabled={false}
                            footer={
                                <View style={{ flexDirection: 'row', margin: -8 }}>
                                    <TButton
                                        style={{ flexGrow: 1, margin: 8 }}
                                        notStretch={true}
                                        type={'primary'}
                                        onPress={() => { this.toggleSuccessModal(false) }}
                                        text={'Закрыть'}
                                    />
                                </View>
                            }
                        >
                            <View style={{ alignItems: 'center', textAlign: 'center' }}>
                                <Icon name="checkmark-circle-outline" size={128} color={COLORS.primary}></Icon>
                                <Text style={[STYLES.base.title, { textAlign: 'center' }]}>{translate("screen.donate.successModalTitle")}</Text>
                                <Text style={[STYLES.base.subtitle, { textAlign: 'center' }]}>{translate("screen.donate.successModalSubtitle")}</Text>
                                <Text style={[STYLES.base.text, { textAlign: 'center' }]}>{translate("screen.donate.successModalText")}</Text>
                            </View>
                        </ModalBox>
                        <ModalBox
                            modalRef={ref => this.modalizeErrorRef = ref}
                            tapGestureEnabled={false}
                            footer={
                                <View style={{ flexDirection: 'row', margin: -8 }}>
                                    <TButton
                                        style={{ flexGrow: 1, margin: 8 }}
                                        notStretch={true}
                                        type={'primary'}
                                        onPress={() => { this.toggleErrorModal(false) }}
                                        text={'Закрыть'}
                                    />
                                </View>
                            }
                        >
                            <View style={{ alignItems: 'center', textAlign: 'center' }}>
                                <Icon name="close-circle-outline" size={128} color={COLORS.red}></Icon>
                                <Text style={[STYLES.base.title, { textAlign: 'center' }]}>{translate("screen.donate.errorModalTitle")}</Text>
                                <Text style={[STYLES.base.subtitle, { textAlign: 'center' }]}>{translate("screen.donate.errorModalSubtitle")}</Text>
                                <Text style={[STYLES.base.text, { textAlign: 'center' }]}>{translate("screen.donate.errorModalText")}</Text>
                            </View>
                        </ModalBox>
                    </>
                }
            </ContainerBox>

        );

    }
}
const styles = StyleSheet.create({
    payScreen: {
        alignItems: 'center',
        textAlign: 'center',
        paddingHorizontal: 16,
        paddingVertical: 24
    },
    description: {
        marginTop: 4,
        textAlign: 'center'
    },
    impressionPreview: {
        backgroundColor: COLORS.lightgray,
        padding: 16,
        borderRadius: 12,
        flexDirection: 'row'
    },
    impressionPreview__imageBox: {
        borderRadius: 8,
        overflow: 'hidden',
        width: 56,
        position: 'relative',
        marginRight: 16,
        minHeight: 72,
        backgroundColor: COLORS.gray
    },
    impressionPreview__image: {
        position: 'absolute',
        width: '100%',
        height: '100%'
    },
    impressionPreview__box: {
        overflow: 'hidden',
        flexDirection: 'column',
        flex: 1,
    },
    impressionPreview__text: {
        color: COLORS.black,
        marginBottom: 4
    },
    impressionPreview__title: {
        color: COLORS.black,
        fontSize: 18,
        maxWidth: "100%"

    },
    info: {
        marginTop: 16,
        textAlign: 'center',
        color: COLORS.gray
    },
    box: {
        width: '100%',
        paddingVertical: 16,
    },
    donate: {
        marginVertical: 0,
    },
    donateList: {
        marginVertical: 0,
        marginTop: -8,
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: 'auto',
        // width: Dimensions.get('window').width,
        marginHorizontal: -8,
    },
    donateItem: {
        padding: 16,
        margin: 8,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: COLORS.lightgray,
        borderRadius: 8,
        backgroundColor: COLORS.white,
        overflow: 'hidden',
        position: 'relative',
        flexGrow: 1,
        flexBasis: '25%'
    },
    donateItem_title: {
        color: COLORS.primary,
        position: 'absolute',
        fontSize: 50,
        fontWeight: 'bold',
        opacity: 0.15,
        lineHeight: 50,
        right: '4%',
        top: '20%',
        textAlign: 'right',
        width: '200%'
    },
    donateItem_gradient: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
    },
    donateItem_box: {
        // marginTop: 48,
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'center',
        position: 'relative'
    },
    donateItem_costs: {
        flexDirection: 'row',
        alignItems: 'flex-end',
    },
    donateItem_cost: {
        color: COLORS.black,
        fontSize: 22,
        lineHeight: 24,
        marginBottom: -3
    },
    donateItem_currency: {
        color: COLORS.gray,
        fontSize: 16,
        marginBottom: -3,
    },
    donateItem_action: {
        color: COLORS.lightgray,
        fontSize: 16
    },
    donateItem_loader: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffffffaa'
    },
    donateItem_loader__indicator: {
    },
    titleBox: {
        marginTop: 16,
        alignItems: 'center',
        marginBottom: 8,
    },
    titleBox_title: {
        color: COLORS.black,
        textAlign: 'center',
        fontSize: 28,
    },
    titleBox_description: {
        color: COLORS.black,
        textAlign: 'center',
        marginTop: 8,
        fontSize: 14,
    },
});
