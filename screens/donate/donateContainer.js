import React, { Component } from "react";
// import {} from "../../store/user/actions";
import { connect } from "react-redux";
import { setUserEmail, setUserName } from "../../store/user/actions";
import Donate from "./donate";

class DonateContainer extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Donate
        route={this.props.route}
        navigation={this.props.navigation}
        app={this.props.app}
        setUserEmail={this.props.setUserEmail}
        setUserName={this.props.setUserName}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    app: state.app
  };
};

const mapDispatchToProps = {
    setUserEmail,
    setUserName
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DonateContainer);
