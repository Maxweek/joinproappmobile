import React, { Component } from "react";
import {
  setAppTabBarVisibility, setUserLang
} from "../../store/user/actions";
import { connect } from "react-redux";
import Auth from "./auth";

class AuthContainer extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Auth
        route={this.props.route}
        navigation={this.props.navigation}
        app={this.props.app}
        setAppTabBarVisibility={this.props.setAppTabBarVisibility}
        setUserLang={this.props.setUserLang}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    app: state.app
  };
};

const mapDispatchToProps = {
  setAppTabBarVisibility,
  setUserLang,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthContainer);
