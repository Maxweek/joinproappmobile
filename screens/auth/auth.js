import React, { Component } from "react";
import I18n from "i18n-js";
import {
    ScrollView,
    Text,
    TextInput,
    View,
    Animated,
    Button,
    Image,
    StyleSheet, Linking, Alert,
    TouchableOpacity,
    Dimensions,
    KeyboardAvoidingView,
    SafeAreaView
} from "react-native";
import { name as appName, version as appVersion } from '../../app.json';
import Icon from "react-native-vector-icons/Ionicons";
import TButton from "../../components/tButton";
import API, { API_USER_INFO, API_USER_LOGIN_EMAIL, API_USER_CHECK, API_USER_LOGOUT, API_IMPRESSION_STREAM_CHECK } from "../../API";
import { COLORS } from "../../assets/styles/styles";
import Shared_EmptyList from "../_shared/emptyList";
import FloatingLabelInput from "../../components/floatingLabelInput";
import { setLanguageCode, translate } from "../../App";

import { Platform, NativeModules } from 'react-native';
import HeaderBox from "../../components/headerBox";
import ContainerBox from "../../components/containerBox";
const { StatusBarManager } = NativeModules;

const window = Dimensions.get('window');

export default class Auth extends Component {
    _isMounted = false;
    constructor(props) {
        super(props);
        this.state = {
            notification: {
                isVisible: false,
                color: COLORS.danger,
                text: ''
            }
        }
    }
    componentDidMount() {
        this.props.setAppTabBarVisibility(true)

    }
    handleUserPhoneAuth = () => {
        this.props.navigation.navigate('authPhoneScreen');
    }
    handleUserEmailAuth = () => {
        this.props.navigation.navigate('authEmailScreen');
    }
    handleUserRegistration = () => {
        this.props.navigation.navigate('registrationScreen');
    }
    render() {
        let notif_styles = {
            opacity: this.state.notification.isVisible ? 1 : 0,
            color: this.state.notification.color
        }
        return (
            <ContainerBox
                navigation={this.props.navigation}
                paddingTop={true}
                header={<HeaderBox
                    gradient={true}
                    scheme={'dark'}
                    shadow={false}
                    right={[{
                        icon: 'settings-outline', action: () => { this.props.navigation.navigate('profileSettingsScreen') }
                    }
                    ]}
                />}
                title={translate("screen.auth.title")}
                subtitle={translate("screen.auth.subtitle")}
            >
                <Text style={notif_styles}>{this.state.notification.text}</Text>
                <View style={{ marginTop: 40 }}></View>
                <TButton
                    onPress={this.handleUserEmailAuth}
                    text={translate("screen.auth.authButtonEmail")}
                    type="primary"
                />
                <Text style={styles.text}>{translate("default.or")}</Text>
                <TButton
                    onPress={this.handleUserPhoneAuth}
                    text={translate("screen.auth.authButtonPhone")}
                    type="primary"
                />
                {/* <View style={{ flexDirection: 'row', marginTop: 20, marginHorizontal: -8 }}>
                    <TouchableOpacity style={{ margin: 8, backgroundColor: '#f0f0f0', flexGrow: 1, alignItems: 'center', padding: 8, borderRadius: 12 }}>
                        <Icon name="ios-logo-google" size={30} color="#14ccb4" />
                    </TouchableOpacity>
                    <TouchableOpacity style={{ margin: 8, backgroundColor: '#f0f0f0', flexGrow: 1, alignItems: 'center', padding: 8, borderRadius: 12 }}>
                        <Icon name="ios-logo-vk" size={30} color="#14ccb4" />
                    </TouchableOpacity>
                    <TouchableOpacity style={{ margin: 8, backgroundColor: '#f0f0f0', flexGrow: 1, alignItems: 'center', padding: 8, borderRadius: 12 }}>
                        <Icon name="ios-logo-facebook" size={30} color="#14ccb4" />
                    </TouchableOpacity>
                </View> */}
                <View style={{ marginTop: 16 }}></View>
                <Text style={styles.text}>{translate("screen.auth.noAccount")}</Text>

                <TButton
                    onPress={this.handleUserRegistration}
                    text={translate("default.registration")}
                    type="simple"
                />
                <View style={styles.info}>
                    <Text>v {appVersion}</Text>
                    <Text>{appName}</Text>
                </View>
            </ContainerBox>
        );
    }
}
const styles = StyleSheet.create({
    info: {
        marginTop: 32,
        opacity: 0.2,
        alignItems: 'center'
    },
    text: {
        textAlign: 'center',
        color: COLORS.gray,
        marginVertical: 8,
    }
});
