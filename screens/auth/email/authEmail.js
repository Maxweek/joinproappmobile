import React, { Component } from "react";
import {
    Text,
    View,
    StyleSheet, 
    Dimensions,
} from "react-native";
import FloatingLabelInput from "../../../components/floatingLabelInput";
import { setLanguageCode, translate, _link } from "../../../App";
import TButton from "../../../components/tButton";
import API, {  API_USER_CHECK, API_USER_LOGIN_EMAIL, API_USER_LOGOUT, getApiToken, setApiToken } from "../../../API";
import { COLORS, STYLES } from "../../../assets/styles/styles";

import HeaderBox from "../../../components/headerBox";
import ContainerBox from "../../../components/containerBox";
import axios from "axios";

export default class AuthEmail extends Component {
    _isMounted = false;
    constructor(props) {
        super(props);
        this.state = {
            isButtonLoading: false,
            notification: {
                isVisible: false,
                color: COLORS.danger,
                text: ''
            }
        }
    }
    componentDidMount() {
        this.props.setAppTabBarVisibility(false)
    }
    componentWillUnmount() {
        this.props.setAppTabBarVisibility(true)
    }
    setAppLogged(type) {
        console.log('setAppLogged');
        if (type) {
            this.props.setAppLogged(type);
            this.props.navigation.navigate('profileScreenStack');
        } else {
            this.props.setAppLogged(type);
        }
    }
    handlePress(type = null) {
        this.setAppLogged(true)
    }
    handleCheck = () => {
        API.get(API_USER_CHECK)
            .then(res => {
                console.log(res)
            })
    };
    handleLogout = () => {
        API.get(API_USER_LOGOUT)
            .then(res => {
                console.log(res)
            })
    };
    handleUserLoggedIn = () => {
        const formData = new FormData();

        formData.append('user-email', this.props.app.user.email);
        formData.append('user-password', this.props.app.user.password);
        formData.append('push-token', this.props.app.pushToken);
        formData.append('jwt', true);

        console.log('beforeLogin')

        this.setState({ isButtonLoading: true })

        API.post(API_USER_LOGIN_EMAIL, formData)
            .then(res => {
                if (typeof res.data.status !== "undefined") {
                    if (res.data.status === 'success') {
                        console.log('success');
                        console.log(res);
                        setApiToken(res.data.token)
                        this.setState({ isButtonLoading: false, notification: { ...this.state.notification, text: '' } })
                        this.setAppLogged(true);
                    }
                    if (res.data.status === 'error') {
                        console.log('login error status')
                        console.log(res)
                        this.setState({ isButtonLoading: false, notification: { text: this.getErrorText(res), color: COLORS.danger, isVisible: true } })
                        this.setAppLogged(false)
                    }
                } else {
                    console.log('login error 2')
                    console.log(res)
                    this.setState({ isButtonLoading: false, notification: { text: translate("screen.auth.authError_1"), color: COLORS.danger, isVisible: true } })
                    this.setAppLogged(false)
                }
            }).catch(res => {
                console.log('login catch error')
                console.log(res)
                this.setState({ isButtonLoading: false, notification: { text: translate("screen.auth.authError_2"), color: COLORS.danger, isVisible: true } })
                this.setAppLogged(false)
            })
    };

    getErrorText = (res) => {
        console.log(res)
        let errText = [];
        if (res.data.error) {
            Object.keys(res.data.error).map(el => {
                errText.push(res.data.error[el]);
            })
        }
        console.log(errText.join("\n "))

        return errText.join("\n ");
    }

    render() {
        let notif_styles = {
            textAlign: 'center',
            opacity: this.state.notification.isVisible ? 1 : 0,
            color: this.state.notification.color,
            marginVertical: 8
        }
        return (
            <ContainerBox
                navigation={this.props.navigation}
                paddingTop={true}
                header={<HeaderBox
                    gradient={true}
                    scheme={'dark'}
                    shadow={false}
                    navigation={this.props.navigation}
                    back={true}
                />}
                title={translate("screen.auth.title")}
            >
                <Text style={notif_styles}>{this.state.notification.text}</Text>
                <FloatingLabelInput
                    label={translate("user.email")}
                    value={this.props.app.user.email}
                    onChangeText={this.props.setUserEmail}
                    autoCorrect={false}
                />
                <FloatingLabelInput
                    label={translate("user.password")}
                    value={this.props.app.user.password}
                    onChangeText={this.props.setUserPassword}
                    autoCorrect={false}
                    secureTextEntry={true}
                />
                <View style={{ marginTop: 20 }}></View>
                <TButton
                    onPress={this.handleUserLoggedIn}
                    text={translate("default.authorize")}
                    type="primary"
                    isLoading={this.state.isButtonLoading}
                    isLocked={this.state.isButtonLoading}
                />
                <Text style={STYLES.footText}>
                    {translate("screen.auth.agree")} {' '}
                    <Text onPress={() => { _link(translate("default.userAgreementUrl")) }} style={{ textDecorationLine: "underline" }}>
                        {translate("default.userAgreement")}
                    </Text>
                    {' '}{translate("default.and")}{' '}
                    <Text onPress={() => { _link(translate("default.privacyPolicyUrl")) }} style={{ textDecorationLine: "underline" }}>
                        {translate("default.privacyPolicy")}
                    </Text>
                </Text>
            </ContainerBox>
        );
    }
}
const styles = StyleSheet.create({
});
