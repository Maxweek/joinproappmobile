import React, { Component } from "react";
import {
  setAppLogged,
  setAppLoading,
  setUserEmail,
  setUserPhone,
  setUserPassword,
  setUserInfo,
  setImpressionStream,
  setAppTabBarVisibility
} from "../../../store/user/actions";
import { connect } from "react-redux";
import AuthEmail from "./authEmail";

class AuthEmailContainer extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <AuthEmail
        route={this.props.route}
        navigation={this.props.navigation}
        app={this.props.app}
        setAppLogged={this.props.setAppLogged}
        setAppLoading={this.props.setAppLoading}
        setUserEmail={this.props.setUserEmail}
        setUserPhone={this.props.setUserPhone}
        setUserPassword={this.props.setUserPassword}
        setUserInfo={this.props.setUserInfo}
        setImpressionStream={this.props.setImpressionStream}
        setUserLang={this.props.setUserLang}
        setAppTabBarVisibility={this.props.setAppTabBarVisibility}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    app: state.app
  };
};

const mapDispatchToProps = {
  setAppLogged,
  setAppLoading,
  setUserEmail,
  setUserPhone,
  setUserPassword,
  setUserInfo,
  setImpressionStream,
  setAppTabBarVisibility,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthEmailContainer);
