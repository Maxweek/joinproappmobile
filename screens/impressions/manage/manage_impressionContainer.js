import React, { Component } from "react";
// import {} from "../../store/user/actions";
import { connect } from "react-redux";
import Manage_Impression from "./manage_impression";

class Manage_ImpressionContainer extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Manage_Impression
        route={this.props.route}
        navigation={this.props.navigation}
        user={this.props.state}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    state: state.user
  };
};

const mapDispatchToProps = {
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Manage_ImpressionContainer);
