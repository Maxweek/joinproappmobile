import React, { Component } from "react";
// import {} from "../../store/user/actions";
import { connect } from "react-redux";
import { setImpressionList } from "../../../store/user/actions";
import List_Impressions from "./list_impressions";

class List_ImpressionsContainer extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <List_Impressions
        route={this.props.route}
        navigation={this.props.navigation}
        app={this.props.app}
        setImpressionList={this.props.setImpressionList}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    app: state.app
  };
};

const mapDispatchToProps = {
  setImpressionList
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(List_ImpressionsContainer);
