import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  Image,
  Dimensions,
  FlatList,
  Button,
  TouchableOpacity,
  RefreshControl
} from "react-native";
import Shared_EmptyList from "../../_shared/emptyList";
import { TabView, SceneMap } from 'react-native-tab-view';
import Animated from 'react-native-reanimated';
import { createRef } from "react";
import ScrollableTabBar from "../../_shared/scrollableTabBar";
import Icon from "react-native-vector-icons/Ionicons";

import Modal from 'react-native-modal';
import API, { API_IMPRESSION_LIST, API_IMPRESSION_LIST_OWN } from "../../../API";
import { translate } from "../../../App";
import List_ComponentImpressions from "../components/list";
// import { map } from "core-js/core/array";

export default class List_Impressions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isNotEmpty: false,
      refreshing: false,
      index: 0,
      pos: [],
      colCount: 2,
      modal: {
        isModalActive: false,
        text: '',
        actions: []
      },
      routes: [
        { key: 'all', title: translate("impression.status.all") },
        { key: 'publish', title: translate("impression.status.publish") },
        { key: 'moderation', title: translate("impression.status.moderation") },
        { key: 'draft', title: translate("impression.status.draft") },
        { key: 'ban', title: translate("impression.status.ban") },
        { key: 'archive', title: translate("impression.status.archive") },
      ]
    }
    this._scrollView = createRef()
    this._scrollView_holder = createRef()
    this._animateHolderPosition = new Animated.Value(0);

  }
  componentDidMount() {
    this.onRefresh()
  }
  // componentDidUpdate() {
  //   this.onRefresh();
  // }
  onRefresh = () => {
    this.setState({ refreshing: true })

    console.log('refreshed')
    this.getList();
  }
  setList = list => {
    this.props.setImpressionList(list)
  }
  getList = () => {
    API.get(API_IMPRESSION_LIST_OWN)
      .then(res => {
        console.log(res.data)
        if (typeof res.data.status !== 'undefined') {
          if (res.data.status === 'success') {
            console.log('list_loaded')
            console.log(res.data.impressions)
            this.setList(res.data.impressions);

            let { routes } = this.state;

            routes.map(route => {
              let count = 0;
              let items = [];
              if (route.key === 'all') {
                items = res.data.impressions;
                count = items.length
              } else {
                items = res.data.impressions.filter(el => el.status === route.key);
                count = items.length
              }
              route.items = items;
              route.count = count;
            })
            this.setState({ refreshing: false, routes, isNotEmpty: true });
          } else {
            console.log('list_load_error_1')
            this.setState({ refreshing: false, isNotEmpty: false });
          }
        } else {
          console.log('list_load_error_2')
          this.setState({ refreshing: false, isNotEmpty: false });
        }
      })
      .catch(err => {
        console.log(err)
        this.setState({ refreshing: false, isNotEmpty: false });
      })
  }
  componentDidUpdate() {
  }
  openCardOptions = item => {
    this.setState({
      modal: {
        ...this.state.modal,
        text: item.title + ' #' + item.id,
        actions: [
          {
            name: translate("default.view"),
            icon: 'ios-eye-outline',
            action: () => {
              this.props.navigation.navigate('viewImpressionNavigator', { screen: "viewImpressionScreen", params: { id: item.id, alternativeDesign: true } });
              this.toggleModal(false);
              console.log(translate("default.view"))
            },
          },
          {
            name: translate("default.edit"),
            icon: 'ios-pencil-outline',
            action: () => {
              this.props.navigation.navigate('editImpressionScreen', { id: item.id, title: item.title });
              this.toggleModal(false);
              console.log(translate("default.edit"))
            },
          },
          {
            name: translate("default.manage"),
            icon: 'ios-grid-outline',
            action: () => {
              this.props.navigation.navigate('manageImpressionScreen', { id: item.id, title: item.title });
              this.toggleModal(false);
              console.log(translate("default.manage"))
            },
          },
          {
            name: translate("default.schedule"),
            icon: 'ios-calendar-outline',
            action: () => {
              this.props.navigation.navigate('scheduleImpressionScreen', { id: item.id, title: item.title });
              this.toggleModal(false);
              console.log(translate("default.schedule"))
            },
          },
        ]
      }
    }, () => {
      this.toggleModal(true);
    });
  }

  toggleModal = (type = null) => {
    this.setState({
      modal: {
        ...this.state.modal,
        isModalActive: type !== null ? !this.state.modal.isModalActive : type
      }
    });
  };
  renderScene = ({ route }) => {
    return <ScrollView refreshControl={<RefreshControl
      colors={["#14ccb4", "#f22424"]}
      refreshing={this.state.refreshing}
      onRefresh={this.onRefresh}

    />} contentContainerStyle={{ paddingHorizontal: 16 }}>
      <List_ComponentImpressions onCardPress={this.openCardOptions} sharedActions={this.createImpression} items={route.items}></List_ComponentImpressions>
    </ScrollView>

  }

  _updateTabBar = i => {
    // console.log(i)
    this.setState({ index: i });
    // this.scrollableTabBar.updateTabBar(i)
  }
  createImpression = () => {
    this.props.navigation.navigate('createImpressionScreen')
  }
  render() {
    const {
      index,
      routes
    } = this.state;
    return (
      <View style={{ flex: 1, backgroundColor: 'white', justifyContent: 'center' }}>
        {/* <Button title="Show modal" onPress={this.toggleModal} /> */}
        <Modal
          isVisible={this.state.modal.isModalActive}
          onBackdropPress={() => this.toggleModal(false)}
          onSwipeComplete={() => this.toggleModal(false)}
          useNativeDriverForBackdrop={true}
          swipeDirection="down"
          style={{ justifyContent: 'flex-end', margin: 0 }}
        >
          <View style={styles.modalInner}>
            <View style={styles.modalInner_swiper}></View>
            <View style={styles.modalInner_title}>
              <Text>{this.state.modal.text}</Text>
            </View>
            {this.state.modal.actions.map(el =>
              <TouchableOpacity style={styles.modalInner_action} onPress={el.action}>
                <Icon name={el.icon} size={24} color="#14ccb4" style={{ marginRight: 10 }} />
                <Text>{el.name}</Text>
              </TouchableOpacity>
            )}
            {/* <Button title="Hide modal" onPress={this.toggleModal} /> */}
          </View>
        </Modal>
        {this.state.isNotEmpty ?
          <TabView
            renderTabBar={(props) => <ScrollableTabBar {...props} ref={ref => this.scrollableTabBar = ref} index={index} tabBarUpdate={this._updateTabBar} />}
            navigationState={{ index, routes }}
            renderScene={this.renderScene}
            onIndexChange={index => this._updateTabBar(index)}
            initialLayout={{ width: Dimensions.get('window').width }}
          />
          :
          <ScrollView refreshControl={<RefreshControl
            colors={["#14ccb4", "#f22424"]}
            refreshing={this.state.refreshing}
            onRefresh={this.onRefresh}
            style={{ height: Dimensions.get('window').height }}
          />}>
            <Shared_EmptyList
              title={translate("screen.impression.list.empty.title")}
              subtitle={translate("screen.impression.list.empty.subtitle")}
              actionText={translate("screen.impression.list.empty.actionText")}
              onPress={this.createImpression}
            />
          </ScrollView>
        }
      </View>
    )
  }
}
const styles = StyleSheet.create({
  cardList: {
    backgroundColor: '#fefefe',
    width: Dimensions.get('window').width,
    // flexDirection: 'row',
    // flexWrap: 'wrap'
    padding: 10
  },
  card: {
    width: (Dimensions.get('window').width / 2) - 20,
    // height: (Dimensions.get('window').width / 2),
    margin: 5,
    paddingBottom: 5,
    position: 'relative'
  },
  cardTitle: {
    marginTop: 4,
    fontSize: 16,
    lineHeight: 22,
    color: "#2a2d43",
    fontWeight: 'bold'
  },
  cardPrice: {
    marginTop: 4,
    fontSize: 14,
    lineHeight: 22,
    color: "#2a2d43",
    fontWeight: 'bold'
  },
  cardDate: {
    position: 'absolute',
    bottom: 8,
    left: 8,
    fontSize: 15,
    color: 'white',
    fontWeight: 'bold',
    textShadowRadius: 4,
  },
  cardImageBox: {
    width: '100%',
    backgroundColor: '#dedede',
    height: (Dimensions.get('window').width / 2),
    borderRadius: 10,
    position: 'relative',
    overflow: 'hidden'
  },
  cardImage: {
    width: '100%',
    height: '100%'
  },
  container: {
    top: 0,
    flex: 1,
    backgroundColor: "#fff",
    position: "relative",
  },
  inner: {
    alignItems: "center",
    justifyContent: 'space-around',
    height: "100%",
  },
  main: {
    alignItems: 'center',
    marginTop: 40
  },
  title: {
    fontSize: 27,
  },
  tabBar: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    flex: 1,
    maxHeight: 50,
    backgroundColor: '#dedede',
    position: 'relative'
    // paddingTop: Constants.statusBarHeight,
  },
  tabItem: {
    // flex: 1,
    alignItems: 'center',
    padding: 16,
  },
  tabBar_holder: {
    backgroundColor: 'black',
    width: 20,
    height: 2,
    position: 'absolute',
    bottom: 0
  },
  modalInner: {
    backgroundColor: 'white',
    bottom: 0,
    width: '100%',
    paddingTop: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    position: 'relative',
    paddingBottom: 20
  },
  modalInner_swiper: {
    position: 'absolute',
    width: 30,
    height: 4,
    top: 4,
    left: (Dimensions.get('window').width - 30) / 2,
    borderRadius: 20,
    backgroundColor: '#dedede'
  },
  modalInner_title: {
    padding: 16,
    fontSize: 20
  },
  modalInner_action: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 8,
    paddingHorizontal: 16,
    borderTopColor: '#dedede',
    borderTopWidth: 1,
    borderStyle: 'solid'
  },
  cardLiveLabel: {
    position: 'absolute',
    top: 0,
    zIndex: 20,
    backgroundColor: '#e45050',
    paddingVertical: 4,
    paddingHorizontal: 12,
    borderBottomRightRadius: 10,
    elevation: 10
  },
  cardLiveLabelText: {
    fontSize: 12,
    color: 'white',
    fontWeight: 'bold',
    textTransform: 'uppercase'
  },
});
