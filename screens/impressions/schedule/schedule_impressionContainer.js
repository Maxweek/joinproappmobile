import React, { Component } from "react";
// import {} from "../../store/user/actions";
import { connect } from "react-redux";
import Schedule_Impression from "./schedule_impression";

class Schedule_ImpressionContainer extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Schedule_Impression
        route={this.props.route}
        navigation={this.props.navigation}
        user={this.props.state}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    state: state.user
  };
};

const mapDispatchToProps = {
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Schedule_ImpressionContainer);
