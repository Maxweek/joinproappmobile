import React, { Component } from "react";
import {
    Text,
    View,
    StyleSheet,
    Image,
    Dimensions,
    ScrollView,
    ActivityIndicator,
    RefreshControl,
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import API, { API_IMPRESSION_READ } from "../../../API";
import { API_REQUEST } from "../../../API_functions";
import { translate } from "../../../App";
import { COLORS, STYLES } from "../../../assets/styles/styles";
import ProfileBox from "../../../components/profileBox";
import RatingBox from "../../../components/ratingBox";
import TButton from "../../../components/tButton";
import * as rootNavigation from "../../../navigators/rootNavigator";

import { Platform, NativeModules } from 'react-native';
import { SafeAreaView } from "react-native-safe-area-context";
import HeaderBox from "../../../components/headerBox";
import Orientation, { OrientationLocker, PORTRAIT, LANDSCAPE } from "react-native-orientation-locker";
import VideoPlayerBox from "../../../components/videoBox";
import StoriesBox from "../../../components/storiesBox";
import Impressions from "../impressions";
// import HTML from 'react-native-render-html';
const { StatusBarManager } = NativeModules;

// var _TIMER = 

export default class View_Impression extends Component {
    constructor(props) {
        super(props);
        console.log(this.props)
        this.state = {
            impression: {
                id: this.props.route.params.id,
            },
            guide: undefined,
            loaded: false,
            isListenerAdded: false,
            isEmpty: false,
            stories: [],
            _info: null,
            _info_2: null,
            isVideoLoaded: false,
            isInited: false,
            isVideoFullscreen: false,
            isElementsVisible: true,
            videoBoxPosition: {}
        }
        this.player = {}
        this.storiesBox = undefined
        this.scrollRef = {}
    }
    addListener = () => {
        // this.addListener();
        if (!this.state.isListenerAdded) {
            this.unsubscribe = this.props.navigation.addListener('focus', () => {
                // this.onRefresh()
                if (this.storiesBox) {
                    this.storiesBox.slideResume();
                }
            });
            this.setState({ isListenerAdded: true })
        }
    }
    componentDidMount() {
        console.log('MOUNTED')
        this.addListener();
        this.props.setAppTabBarVisibility(false)
        this.onRefresh();
        Orientation.lockToPortrait();
    }
    componentWillUnmount() {
        // this.props.setAppTabBarVisibility(true)
        // this.player.stop();
    }

    onRefresh = () => {
        console.log('this.storiesBox')
        console.log(this.storiesBox)
        this.setState({ refreshing: true })
        this.getImpression()
        this.props.setAppTabBarVisibility(false)


    }

    getImpression = () => {
        console.log(API_IMPRESSION_READ + this.state.impression.id)
        API.get(API_IMPRESSION_READ + this.state.impression.id)
            .then(res => {
                console.log('IMPRESSION READ')
                console.log(res.data)
                if (typeof res.data.status !== 'undefined') {
                    if (res.data.status === 'success') {
                        this.setImpression(res.data.impression);
                        this.setState({ refreshing: false })
                    } else {
                        console.log('impression_load_error_1')
                        this.setState({ refreshing: false });
                    }
                } else {
                    console.log('impression_load_error_2')
                    this.setState({ refreshing: false });
                }
            })
            .catch(err => {
                console.log(err)
                this.setState({ refreshing: false });
            })
    }

    setImpression = (impression) => {
        if (impression.id !== 'undefined') {
            this.props.setImpressionCurrent(impression)
            this.setState({
                stories: []
            }, function () {
                this.setState({ loaded: true, isEmpty: false })
            })
            this.getGuideInfo(this.props.app.impression.current.user_id);
            if (this.storiesBox) {
                // this.storiesBox.timer.clear()
                this.storiesBox.resetStories()


            }
            // this.setStories();
        } else {
            this.setState({ loaded: true, isEmpty: true })
        }
    }

    getGuideInfo = async (id) => {
        API_REQUEST.getUserInfo(id).then(guide => {
            this.setState({
                guide
            })
        })
    }



    render() {
        console.log(this.state)
        console.log(this.props.app.impression.current)
        // console.log(StatusBarManager)
        let specialVideoStyles = {
            position: this.state.isElementsVisible ? 'relative' : 'absolute',
            top: 0,
            zIndex: 2000
        }
        if (!this.state.loaded) {
            return (
                <View style={styles.container}>
                    <View style={[styles.inner, { justifyContent: 'center', alignItems: 'center' }]}>
                        <ActivityIndicator
                            size={60}
                            color={COLORS.primary}
                        />
                        <Text style={{ marginTop: 8 }}>{translate("impression.loading")}</Text>
                    </View>
                </View>
            );
        }
        if (this.state.isEmpty) {
            return (
                <View style={styles.container}>
                    <View style={[styles.inner, { justifyContent: 'center', alignItems: 'center' }]}>
                        <Text>{translate("impression.undefined")}</Text>
                    </View>
                </View>
            )
        }
        return (
            <>
                <SafeAreaView edges={['left', 'bottom', 'right']} style={styles.container}>

                    <View style={styles.inner}>
                        {this.state.isElementsVisible ?
                            <HeaderBox
                                back={true}
                                gradient={true}
                                scheme={'light'}
                                navigation={this.props.navigation}
                                // title={"yowyowyowyowyow"}
                                right={
                                    [
                                        // { icon: 'ios-heart-outline', action: () => { } },
                                        // { icon: 'ios-share-social-outline', action: () => { } },
                                    ]
                                }
                            />
                            : null}
                        <ScrollView style={styles.main}
                            ref={ref => this.scrollRef = ref}
                            // scrollEventThrottle={2}
                            // onScroll={this.onScroll}
                            scrollEnabled={this.state.isElementsVisible}
                            indicatorStyle={'black'}
                            refreshControl={<RefreshControl
                                colors={["#14ccb4", "#f22424"]}
                                refreshing={this.state.refreshing}
                                onRefresh={this.onRefresh}
                            />}
                        >
                            <StoriesBox
                                ref={ref => this.storiesBox = ref}
                                delay={5000}
                                title={this.props.app.impression.current.title}
                                // description={this.props.app.impression.current.short_description}
                                stories={this.props.app.impression.current.gallery}
                            >
                            </StoriesBox>
                            {this.props.app.impression.current.attributes.length > 0 ?
                                <View style={[styles.box, styles.optionsBox]}>
                                    <View style={styles.options}>
                                        {this.props.app.impression.current.attributes.map(el => {
                                            let color;
                                            switch (el.status) {
                                                case "important":
                                                    color = COLORS.green;
                                                    break;
                                                case "attention":
                                                    color = COLORS.red;
                                                    break;
                                                default:
                                                    color = COLORS.gray;
                                            }
                                            return (
                                                <View key={el.title} style={styles.options__item}>
                                                    <View style={styles.options__item_icon}>
                                                        <Icon name={el.icon} size={24} color={color} />
                                                    </View>
                                                    <Text style={[styles.options__item_title, { color: color }]}>{el.title}</Text>
                                                </View>
                                            )
                                        })}
                                    </View>
                                </View>
                                : null}
                            {this.props.app.impression.current.video.source ?
                                <View onLayout={event => {
                                    const layout = event.nativeEvent.layout;
                                    this.setState({ videoBoxPosition: layout })
                                }} style={[{ marginTop: 16 }, specialVideoStyles]}>
                                    <VideoPlayerBox
                                        video={this.props.app.impression.current.video}
                                        setVisibleElements={type => {
                                            if (type) {
                                                if (this.storiesBox) {
                                                    this.storiesBox.slideResume();
                                                }
                                            } else {
                                                if (this.storiesBox) {
                                                    this.storiesBox.slideStop();
                                                }
                                            }
                                            this.setState({ isElementsVisible: type })
                                        }}
                                        scrollRef={this.scrollRef}
                                        position={this.state.videoBoxPosition}
                                    />
                                    {/* {Platform.OS === 'android' ? */}
                                        <View style={styles.box}>
                                            <TButton
                                                onPress={() => {
                                                    this.props.navigation.navigate('donateScreen', {
                                                        impression_id: this.state.impression.id,
                                                        impression: this.props.app.impression.current
                                                    })
                                                }}
                                                text={translate("pay.completeButtonText")}
                                            ></TButton>
                                        </View>
                                        {/* : null} */}
                                </View>
                                : null}

                            <View style={styles.box}>
                                {this.state.guide !== undefined ?
                                    <ProfileBox
                                        padding={false}
                                        border={false}
                                        onPress={() => {
                                            rootNavigation.push('impressionViewNavigator', {
                                                screen: "userScreen",
                                                params: {
                                                    id: this.state.guide.id
                                                }
                                            })
                                        }}
                                        name={this.state.guide.name + ' ' + (this.state.guide.last_name ? this.state.guide.last_name : '')}
                                        image={this.state.guide.photo}
                                        topText={translate("guide.conducting")}
                                        rate={this.state.guide.rate}
                                        style={{ marginBottom: 0 }}
                                    />
                                    :
                                    null
                                }

                                <Text style={styles.h2}>
                                    {translate("default.description")}
                                </Text>
                                <Text style={styles.p}>
                                    {this.props.app.impression.current.description}
                                </Text>
                                {this.props.app.impression.current.sections.map(el => {
                                    return (
                                        <View key={el.key}>
                                            <Text style={styles.h3}>
                                                {el.title}
                                            </Text>
                                            <Text style={styles.p}>
                                                {el.description}
                                            </Text>
                                            {el.image !== null ?
                                                <Image style={{ width: '100%', height: 200, marginTop: 4, resizeMode: 'center' }} source={{ uri: el.image }} />
                                                : null}
                                        </View>
                                    )
                                })}
                            </View>
                        </ScrollView>
                        {this.state.isElementsVisible ?
                            <View style={styles.actionBox}>
                                <View style={styles.actionBox__box}>
                                    <Text style={styles.actionBox__price}>{this.props.app.impression.current.price !== 0 ? this.props.app.impression.current.price : translate('default.free')}</Text>

                                    <RatingBox rate={this.props.app.impression.current.rate} style={styles.actionBox__stars} />
                                    {/* {this.getRate(this.props.app.impression.current.rate, styles.actionBox__stars)} */}
                                </View>
                                {this.props.app.impression.current.schedule.length && !this.props.route.params.alternativeDesign ?
                                    <View style={styles.actionBox__btn}>
                                        <TButton
                                            text={translate('default.look')}
                                            notStretch={true}
                                            type='primary'
                                            onPress={() => {
                                                console.log(this.props.navigation)
                                                if (this.storiesBox) {
                                                    this.storiesBox.slideStop();
                                                }
                                                
                                                this.props.navigation.navigate('viewImpressionStreamScreen', {

                                                })
                                            }}
                                        />
                                    </View>
                                    : null}
                            </View>
                            : null}
                    </View>
                </SafeAreaView >
            </>
        );
    }
}
const styles = StyleSheet.create({
    card: {
        borderWidth: 1,
        borderColor: '#dedede',
        borderStyle: 'solid',
        padding: 20,
        flexGrow: 1,
        margin: 10,
        borderRadius: 10
    },
    container: {
        // top: 0,
        flex: 1,
        backgroundColor: "#fff",
        position: "relative",
        // paddingTop: StatusBarManager.HEIGHT,
    },
    header: {
        position: 'absolute',
        height: StatusBarManager.HEIGHT + 76,
        zIndex: 20,
        top: 0,
        left: 0,
        width: Dimensions.get('window').width,
    },
    header_shadow: {
        position: 'absolute',
        width: '100%',
        height: '100%',
    },
    headerBox: {
        position: 'relative',
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: StatusBarManager.HEIGHT,
        justifyContent: 'space-between'
    },
    headerBox_text: {
        fontSize: 14,
        color: COLORS.white,
        lineHeight: 20,
    },
    inner: {
        // alignItems: "center",
        // justifyContent: 'space-around',
        height: "100%",
        position: 'relative',
    },
    main: {
        position: 'relative',
        width: '100%'
        // alignItems: 'center'
    },
    title: {
        fontSize: 27,
    },
    stories: {
        marginHorizontal: -4,
        marginVertical: 8,
        flexDirection: 'row'
    },
    stories__item: {
        flexGrow: 1,
        // width: '100%',
        marginHorizontal: 4,
        height: 3,
        borderRadius: 3,
        backgroundColor: "#ffffff88",
        overflow: "hidden",
        position: 'relative',
    },
    stories__item_progress: {
        position: 'absolute',
        right: '100%',
        width: '100%',
        height: '100%',
        backgroundColor: '#ffffff'
    },
    box: {
        padding: 16,
    },
    previewBox: {
        position: 'relative',
        paddingTop: 48
    },
    previewBox_shadow: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0
    },
    optionsBox: {
        borderTopLeftRadius: 16,
        borderTopRightRadius: 16,
        marginTop: -16,
        backgroundColor: '#ffffff',
        borderBottomColor: COLORS.lightgray,
        borderBottomWidth: 1,
        borderStyle: 'solid'
    },
    options: {
        // marginVertical: -8
    },
    options__item: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        marginVertical: 8
    },
    options__item_icon: {
        width: 24,
        height: 24,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 16
    },
    options__item_title: {
        fontSize: 16,
        lineHeight: 24,
        color: COLORS.black
    },
    user: {
        flexDirection: 'row'
    },
    user_image: {
        width: 56,
        height: 56,
        position: 'relative',
        borderRadius: 56,
        overflow: 'hidden',
        marginRight: 16
    },
    user_box: {
        position: 'relative'
    },
    user_action: {
        fontSize: 14,
        color: COLORS.black,
        lineHeight: 20
    },
    user_name: {
        fontSize: 16,
        lineHeight: 24,
        color: COLORS.black,
        fontWeight: 'bold'
    },
    user_rating: {},
    stars: {
        flexDirection: 'row',
        marginHorizontal: -2,
        alignItems: 'center'
    },
    stars__item: {
        width: 16,
        height: 16,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 2
    },
    stars__count: {
        marginLeft: 4,
    },
    stars__count_text: {
        color: COLORS.gray,
        fontSize: 12
    },
    h2: {
        fontSize: 20,
        lineHeight: 28,
        color: COLORS.black,
        fontWeight: 'bold',
        marginTop: 16,
        marginBottom: 4
    },
    h3: {
        fontSize: 16,
        lineHeight: 24,
        color: COLORS.black,
        marginTop: 16,
        fontWeight: 'bold'
    },
    p: {
        color: COLORS.black,
        fontSize: 16,
        lineHeight: 24
    },
    actionBox: {
        // position: 'absolute',
        backgroundColor: 'white',
        padding: 8,
        paddingLeft: 16,
        zIndex: 20,
        elevation: 40,
        shadowColor: 'black',
        shadowOffset: {
            width: 0,
            height: -4
        },
        shadowOpacity: 0.1,
        shadowRadius: 4,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        minHeight: 68
    },
    actionBox__box: {

    },
    actionBox__price: {
        fontSize: 16,
        lineHeight: 24,
        color: COLORS.black,
        fontWeight: 'bold'
    },
    actionBox__stars: {
        marginTop: 4
    },
    actionBox__btn: {
    },
});
