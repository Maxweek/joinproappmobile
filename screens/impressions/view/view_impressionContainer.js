import React, { Component } from "react";
// import {} from "../../store/user/actions";
import { connect } from "react-redux";
import { setAppTabBarVisibility, setImpressionCurrent } from "../../../store/user/actions";
import View_Impression from "./view_impression";

class View_ImpressionContainer extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View_Impression
        route={this.props.route}
        navigation={this.props.navigation}
        user={this.props.state}
        setAppTabBarVisibility={this.props.setAppTabBarVisibility}
        setImpressionCurrent={this.props.setImpressionCurrent}
        app={this.props.app}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    app: state.app
  };
};

const mapDispatchToProps = {
  setAppTabBarVisibility,
  setImpressionCurrent
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(View_ImpressionContainer);
