import React, { Component } from "react";
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import { translate } from "../../../App";
import TButton from "../../../components/tButton";

export default class Edit_Impression extends Component {
    constructor(props) {
        super(props);
    }
    edit() {
        this.props.navigation.navigate('createImpressionScreen', {
            screen: 'setShortDescription_createImpressionScreen',
            params: {
                impression_id: this.props.route.params.id
            }

        });
        // this.props.navigation.navigate('viewImpressionScreen', { id: item.id });
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.inner}>
                    <View style={styles.main}>
                        <Text style={styles.title}>{translate("default.editing")}</Text>
                        <Text>{this.props.route.params.title} #{this.props.route.params.id}</Text>
                        <TButton
                            style={{ margin: 16 }}
                            text={translate("default.edit")}
                            onPress={() => this.edit()}
                        />
                    </View>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    card: {
        borderWidth: 1,
        borderColor: '#dedede',
        borderStyle: 'solid',
        padding: 20,
        flexGrow: 1,
        margin: 10,
        borderRadius: 10
    },
    container: {
        top: 0,
        flex: 1,
        backgroundColor: "#fff",
        position: "relative",
    },
    inner: {
        alignItems: "center",
        justifyContent: 'space-around',
        height: "100%",
    },
    main: {
        alignItems: 'center'
    },
    title: {
        fontSize: 27,
    },
});
