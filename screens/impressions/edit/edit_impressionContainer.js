import React, { Component } from "react";
// import {} from "../../store/user/actions";
import { connect } from "react-redux";
import Edit_Impression from "./edit_impression";

class Edit_ImpressionContainer extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Edit_Impression
        route={this.props.route}
        navigation={this.props.navigation}
        user={this.props.state}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    state: state.user
  };
};

const mapDispatchToProps = {
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Edit_ImpressionContainer);
