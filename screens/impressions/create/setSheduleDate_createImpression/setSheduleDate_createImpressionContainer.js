import React, { Component } from "react";
// import {} from "../../store/user/actions";
import { connect } from "react-redux";
import {
    setNewImpressionIsNew
} from "../../../../store/user/actions";
import SetSheduleDate_createImpression from "./setSheduleDate_createImpression";

class SetSheduleDate_createImpressionContainer extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <SetSheduleDate_createImpression
                route={this.props.route}
                navigation={this.props.navigation}
                app={this.props.app}
                setNewImpressionIsNew={this.props.setNewImpressionIsNew}
            />
        );
    }
}

const mapStateToProps = (state) => {
    return {
        app: state.app
    };
};

const mapDispatchToProps = {
    setNewImpressionIsNew
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SetSheduleDate_createImpressionContainer);
