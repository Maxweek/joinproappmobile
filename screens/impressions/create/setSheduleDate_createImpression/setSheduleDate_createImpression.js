import React, { Component } from "react";
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Button,
    Dimensions,
    Image,
    ScrollView,
    FlatList,
    RefreshControl
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import FloatingLabelInput from "../../../../components/floatingLabelInput";
import TButton from "../../../../components/tButton";
import API, { API_IMPRESSION_SHEDULE_LIST, API_IMPRESSION_SHEDULE_CREATE, API_IMPRESSION_SHEDULE_DELETE } from "../../../../API";
import Modal from 'react-native-modal';
import { Calendar, CalendarList, LocaleConfig } from "react-native-calendars";
import { translate } from "../../../../App";

export default class SetSheduleDate_createImpression extends Component {
    constructor(props) {
        super(props);
        this.curDate = {
            dateObject: new Date()
        };
        this.curDate.formatted = this.curDate.dateObject.getFullYear() + '-' + ((this.curDate.dateObject.getMonth() + 1).toString().padStart(2, '0')) + '-' + ((this.curDate.dateObject.getDate() + 1).toString().padStart(2, '0'))
        this.curDate.minDate = this.curDate.dateObject.getFullYear() + '-' + ((this.curDate.dateObject.getMonth() + 1).toString().padStart(2, '0')) + '-' + ((this.curDate.dateObject.getDate()).toString().padStart(2, '0'))
        this.state = {
            refreshing: false,
            edit_id: null,
            step: 0,
            modal: {
                isModalActive: false,
            },
            calendar: {
                selectDate: [],
            },
            times: [],
            timeGroups: [
                {
                    id: 1,
                    name: translate("datetime.dayTimes.night"),
                    in: 0,
                    out: 3,
                },
                {
                    id: 2,
                    name: translate("datetime.dayTimes.morning"),
                    in: 4,
                    out: 10,
                },
                {
                    id: 3,
                    name: translate("datetime.dayTimes.day"),
                    in: 11,
                    out: 16,
                },
                {
                    id: 4,
                    name: translate("datetime.dayTimes.afternoon"),
                    in: 17,
                    out: 23,
                },
            ],
            dates: []
        }
        LocaleConfig.locales['ru'] = {
            monthNames: translate("datetime.monthNames"),
            monthNamesShort: translate("datetime.monthNamesShort"),
            dayNames: translate("datetime.dayNames"),
            dayNamesShort: translate("datetime.dayNamesShort"),
            today: translate("datetime.today"),
        };
        LocaleConfig.defaultLocale = 'ru';
        console.log(this.props.app.impression.new.shedule.duration)

    }
    componentDidMount() {
        // this.setTimes()
        console.log('NEXT_IS_CHECK_IS_NEW')
        console.log(this.props.app.impression.new.isNew)
        this.onRefresh();
    }
    onRefresh = () => {
        this.setState({ refreshing: true })
        this.getShedule();
    }
    getShedule = () => {
        const formData = new FormData();
        formData.append('impression_id', this.props.app.impression.new.id)
        API.post(API_IMPRESSION_SHEDULE_LIST, formData)
            .then(res => {
                console.log(res.data)
                let acc = 0;
                if (res.data.schedule.length > 0) {
                    res.data.schedule.map(el => {
                        el.id = acc;
                        el.error = '';
                        acc++;
                    })
                }
                this.setState({
                    dates: res.data.schedule, refreshing: false
                })
            })
    }
    setTimes = () => {
        let acc = 0;
        let times = [];

        for (let i = 0; i <= 47; i++) {
            let time = Math.floor(i / 2);
            let type = this.state.timeGroups.filter(group => (group.in <= time && group.out >= time))[0].id;
            let timeObj = {
                id: acc,
                time: time + (i % 2 ? ':30' : ':00'),
                type: type,
                isSelected: false,
                isHolded: false
            }
            acc++;
            times.push(timeObj);
        }
        this.setState({ times })
        // TODO делать запрос на сервак и скрыть текущее расписание
    }
    selectTime = obj => {
        if (obj.isHolded) {
            return
        }
        let times = [...this.state.times];
        // times[obj.id + 1].isHolded = true;
        times.map(el => {
            if (el.id === obj.id) {
                // times[el.id - 1].isHolded = !el.isSelected;
                el.isSelected = !el.isSelected;
            }
        })
        this.setState({ times })
    }
    update = () => {
        // setNewImpressionSheduleDurationSummary
        // this.sendUpdate();
        console.log('nextISNEW')
        console.log(this.props.app.impression.new)
        if(this.props.app.impression.new.isNew){
            this.props.navigation.navigate('confirm_createImpressionScreen')
        } else {
            this.props.setNewImpressionIsNew(true)
            this.props.navigation.navigate('listImpressionsScreen')
        }
    }
    renderDate = item => {
        let isError = item.error !== '' ? true : false;
        let errorStyles = { color: '#999ba5' }
        let addStyles = {}
        if (isError) {
            addStyles = errorStyles
        }
        return (
            <View key={item.id} style={{ flexDirection: 'row', marginBottom: 10 }}>
                <View style={{ justifyContent: 'center', alignItems: 'center', marginRight: 24, width: 32, paddingBottom: 10, }}>
                    <Text style={[{ fontSize: 20, lineHeight: 28 }, addStyles]}>{item.date.day}</Text>
                    <Text style={[{ fontSize: 14, lineHeight: 20 }, addStyles]}>{LocaleConfig.locales['ru'].monthNamesShort[item.date.dayofweek - 1]}</Text>
                </View>
                <View style={{ borderBottomWidth: 1, borderBottomColor: '#dedede', flexGrow: 1, flexDirection: 'row', paddingBottom: 10, }}>
                    <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'flex-start', flexGrow: 1 }}>
                        <Text style={[{ fontSize: 16, lineHeight: 24 }, addStyles]}>{item.time.in} - {item.time.out}</Text>
                        {isError ?
                            <Text style={{ fontSize: 14, lineHeight: 20, color: '#ef476f' }}>{item.error}</Text>
                            : null}
                    </TouchableOpacity>
                    <TouchableOpacity style={{ backgroundColor: '#00000003', borderRadius: 10, width: 48, alignItems: 'center', justifyContent: 'center', marginLeft: 24 }} onPress={() => { this.removeDate(item.date_id) }}>
                        <Icon name={'trash-outline'} size={24} color={'#dedede'} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    removeDate = id => {
        const formData = new FormData();
        formData.append('impression_id', this.props.app.impression.new.id)
        formData.append('impression_date_id', id)
        API.post(API_IMPRESSION_SHEDULE_DELETE, formData)
            .then(res => {
                console.log(res.data)
                if (res.data.status === 'success') {
                    this.onRefresh();
                }
            }).catch(err => {
                console.log(err);
            })
    }
    addDate = () => {
        this.toggleModal(true);
    }
    toggleModal = (type = null) => {
        this.setState({
            modal: {
                ...this.state.modal,
                isModalActive: type !== null ? !this.state.modal.isModalActive : type
            },
            step: 0,
        });
    };
    nextStep = step => {
        this.setTimes()
        this.setState({ step })
    }
    setNewDaySelected = date => {
        const selectDate = {};
        selectDate[date.dateString] = {
            selected: true,
        };
        this.setState({ calendar: { markedDates: selectDate, selectDate: date } });
    };
    createObjects = () => {
        let list = [];
        let times = [...this.state.times];
        times.map(el => {
            if (el.isSelected) {
                list.push({
                    date: this.state.calendar.selectDate.dateString,
                    time: el.time,
                })
            }
        })
        console.log(list);
        this.sendDates(list);
    }
    sendDates = list => {
        const formData = new FormData();
        formData.append('impression_id', this.props.app.impression.new.id)
        formData.append('impression_dates', JSON.stringify(list))
        API.post(API_IMPRESSION_SHEDULE_CREATE, formData)
            .then(res => {
                console.log(res.data)
                if (res.data.status === 'success') {
                    this.onRefresh();
                    this.toggleModal(false);
                }
            }).catch(err => {
                console.log(API_IMPRESSION_SHEDULE_CREATE);
                console.log(err);
            })

    }
    render() {
        let maxDate = new Date();
        maxDate.setDate(maxDate.getDate() + 90);
        maxDate = maxDate.getFullYear() + '-' + ((maxDate.getMonth() + 1).toString().padStart(2, '0')) + '-' + ((maxDate.getDate() + 1).toString().padStart(2, '0'));
        return (
            <View style={styles.container}>
                <Modal
                    isVisible={this.state.modal.isModalActive}
                    onBackdropPress={() => this.toggleModal(false)}
                    onSwipeComplete={() => this.toggleModal(false)}
                    useNativeDriverForBackdrop={true}
                    swipeDirection="down"
                    propagateSwipe
                    style={{ justifyContent: 'flex-end', margin: 0 }}
                >
                    <View style={styles.modalInner}>
                        <View style={styles.modalInner_swiper}></View>
                        {this.state.step === 0 ?
                            <>
                                <CalendarList
                                    horizontal={true}
                                    pagingEnabled={true}
                                    calendarWidth={Dimensions.get('window').width}
                                    current={this.curDate.formatted}
                                    markedDates={this.state.calendar.markedDates}
                                    minDate={this.curDate.minDate}
                                    maxDate={maxDate}
                                    onDayPress={this.setNewDaySelected}
                                    onDayLongPress={(day) => { console.log('selected day', day) }}
                                    onMonthChange={(month) => { console.log('month changed', month) }}
                                    hideArrows={false}
                                    renderArrow={(direction) => (direction === 'left' ? <Icon name={'chevron-back-outline'} size={24} color={'#14ccb4'} /> : <Icon name={'chevron-forward-outline'} size={24} color={'#14ccb4'} />)}
                                    hideExtraDays={false}
                                    hideDayNames={false}
                                    firstDay={1}
                                    onPressArrowLeft={subtractMonth => subtractMonth()}
                                    onPressArrowRight={addMonth => addMonth()}
                                    disableAllTouchEventsForDisabledDays={false}
                                    enableSwipeMonths={true}
                                    style={{ height: 400 }}
                                    pastScrollRange={0}
                                    futureScrollRange={24}
                                    renderHeader={date => {
                                        const header = date.toString('MMMM yyyy');
                                        const [month, year] = header.split(' ');
                                        const textStyle = {
                                            fontSize: 18,
                                            fontWeight: 'bold',
                                            paddingTop: 10,
                                            paddingBottom: 10,
                                            color: '#2a2d43',
                                            paddingRight: 5
                                        };

                                        return (
                                            <View
                                                style={{
                                                    flexDirection: 'row',
                                                    justifyContent: 'space-between',
                                                    marginTop: 10,
                                                    marginBottom: 10
                                                }}
                                            >
                                                <Text style={{ marginLeft: 5, ...textStyle }}>{`${month}`}</Text>
                                                <Text style={{ marginRight: 5, ...textStyle }}>{year}</Text>
                                            </View>
                                        );
                                    }}
                                    theme={{
                                        backgroundColor: '#000000',
                                        calendarBackground: '#ffffff',
                                        textSectionTitleColor: '#b6c1cd',
                                        textSectionTitleDisabledColor: '#d9e1e8',
                                        selectedDayBackgroundColor: '#14ccb4',
                                        selectedDayTextColor: 'white',
                                        todayTextColor: '#14ccb4',
                                        // dayTextColor: 'yellow',
                                        textDisabledColor: '#d9e1e8',
                                        dotColor: '#00adf5',
                                        selectedDotColor: '#ffffff',
                                        arrowColor: 'orange',
                                        disabledArrowColor: '#d9e1e8',
                                        monthTextColor: 'blue',
                                        indicatorColor: 'blue',
                                        textDayFontWeight: '300',
                                        textMonthFontWeight: 'bold',
                                        textDayHeaderFontWeight: '300',
                                        textDayFontSize: 16,
                                        textMonthFontSize: 16,
                                        textDayHeaderFontSize: 16
                                    }}

                                />
                                <TButton
                                    style={{ margin: 16, zIndex: 200 }}
                                    onPress={() => { this.nextStep(1) }}
                                    text={translate("default.confirm")}
                                />
                            </>
                            :
                            <ScrollView style={{ height: 600 }} scrollEventThrottle={16}>
                                <View flex={1} onStartShouldSetResponder={() => true}>
                                    {this.state.timeGroups.map(group => {
                                        return (
                                            <>
                                                <Text style={{ paddingHorizontal: 24 }}>{group.name}</Text>
                                                <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginBottom: 24, marginTop: 8, paddingHorizontal: 24 }}>
                                                    {this.state.times.filter(el => el.type === group.id).map(el => {
                                                        let addStyles = el.isHolded ? { opacity: 0.4 } : {}
                                                        return (
                                                            <TouchableOpacity style={[{ borderColor: el.isSelected ? '#14ccb4' : '#0f2a2d43', borderWidth: 1, borderRadius: 10, paddingVertical: 8, width: 70, alignItems: 'center', margin: 8, }, addStyles]} onPress={() => { this.selectTime(el) }}>
                                                                <Text>{el.time}</Text>
                                                            </TouchableOpacity>
                                                        )
                                                    })}
                                                </View>
                                            </>
                                        )
                                    })}
                                </View>
                                <TButton
                                    style={{ margin: 16, zIndex: 200 }}
                                    onPress={() => { this.createObjects() }}
                                    text={translate("default.confirm")}
                                />
                            </ScrollView>
                        }
                    </View>
                </Modal>
                <View style={styles.inner}>
                    {/* <Text>Даты проведения</Text> */}
                    <FlatList
                        data={this.state.dates}
                        renderItem={({ item }) => this.renderDate(item)}
                        keyExtractor={item => item.id}
                        style={styles.dateList}
                        refreshControl={<RefreshControl
                            colors={["#14ccb4", "#f22424"]}
                            refreshing={this.state.refreshing}
                            onRefresh={this.onRefresh}
                        />}
                    />
                    {/* {this.state.dates.map(el => this.renderDate(el))} */}
                    <View style={{
                        shadowColor: 'black',
                        shadowOpacity: 0.26,
                        shadowOffset: { width: 0, height: 2 },
                        shadowRadius: 10,
                        elevation: 3,
                        backgroundColor: 'white'
                    }}>
                        <View>
                            <TouchableOpacity style={{ flexDirection: 'row', paddingVertical: 12, marginBottom: 8, paddingHorizontal: 12, }} onPress={this.addDate}>
                                <View style={{ justifyContent: 'center', alignItems: 'center', marginRight: 24, width: 32 }}>
                                    <Icon name={'add-outline'} size={24} color={'#000000'} />
                                </View>
                                <View style={{ flexGrow: 1, flexDirection: 'row' }}>
                                    <View style={{ justifyContent: 'center', alignItems: 'flex-start', flexGrow: 1 }}>
                                        <Text style={{ fontSize: 16, lineHeight: 24 }}>{translate("default.add")}</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View>
                            <TButton
                                style={{ margin: 16 }}
                                onPress={this.update}
                                text={translate("default.next")}
                            />
                        </View>
                    </View>
                </View>
            </View >
        );
    }
}
const styles = StyleSheet.create({
    card: {
        borderWidth: 1,
        borderColor: '#dedede',
        borderStyle: 'solid',
        padding: 20,
        flexGrow: 1,
        margin: 10,
        borderRadius: 10
    },
    dateList: {
        // backgroundColor: 'yellow',
        width: Dimensions.get('window').width,
        // flexGrow: 1,
        // height: "auto",
        // justifyContent: 'flex-end',
        // flexDirection: 'row',
        // flexWrap: 'wrap'
        padding: 10
    },
    container: {
        top: 0,
        flexGrow: 1,
        backgroundColor: "#fff",
        position: "relative",
    },
    inner: {
        alignItems: "center",
        // justifyContent: 'space-around',
        // top: 24,
        flexGrow: 1,
        maxHeight: '100%',
        // height: "100%",
    },
    main: {
        width: Dimensions.get('window').width - 40
        // alignItems: 'center'
    },
    title: {
        fontSize: 27,
    },
    modalInner: {
        backgroundColor: 'white',
        bottom: 0,
        width: '100%',
        paddingTop: 10,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        position: 'relative',
        // paddingBottom: 20
    },
    modalInner_swiper: {
        position: 'absolute',
        width: 30,
        height: 4,
        top: 4,
        left: (Dimensions.get('window').width - 30) / 2,
        borderRadius: 20,
        backgroundColor: '#dedede'
    },
    modalInner_title: {
        padding: 16,
        fontSize: 20
    },
    modalInner_action: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 8,
        paddingHorizontal: 16,
        borderTopColor: '#dedede',
        borderTopWidth: 1,
        borderStyle: 'solid'
    },
});
