import React, { Component } from "react";
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Button,
    Dimensions,
    Image
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import FloatingLabelInput from "../../../../components/floatingLabelInput";
import TButton from "../../../../components/tButton";
import API, { API_IMPRESSION_UPDATE, API_IMPRESSION_PHOTO_ADD } from "../../../../API";
import PhotoUpload from "react-native-photo-upload";

import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
// import { transform } from "@babel/core";
import { translate } from "../../../../App";

export default class SetShortDescription_createImpression extends Component {
    constructor(props) {
        super(props);
        this.state = {
            imageUri: null
        }
    }
    sendUpdate = () => {
        const formData = new FormData();

        formData.append('impression_id', this.props.app.impression.new.id)
        formData.append('impression_title', this.props.app.impression.new.name)
        formData.append('impression_description', this.props.app.impression.new.description)

        API.post(API_IMPRESSION_UPDATE, formData)
            .then(res => {
                console.log(res.data)
            })

        let photoFormData = new FormData();

        photoFormData.append('impression_id', this.props.app.impression.new.id)
        // photoFormData.append('file', this.props.app.impression.new.image)
        photoFormData.append('file', {
            uri: this.props.app.impression.new.image.uri,
            name: this.props.app.impression.new.image.fileName,
            type: this.props.app.impression.new.image.type,
            size: this.props.app.impression.new.image.fileSize,
        })

        console.log(photoFormData._parts)

        API.post(API_IMPRESSION_PHOTO_ADD, photoFormData, {
            headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            // onUploadProgress: callback,
        })
            .then(res => {
                console.log(res.data)
            })
    }
    update = () => {
        this.sendUpdate();
        this.props.navigation.navigate('setSheduleMain_createImpressionScreen')
    }
    setImage = () => {
        launchImageLibrary(
            {
                mediaType: 'photo',
                includeBase64: false,
                maxHeight: 720,
                maxWidth: 720,
            },
            (response) => {
                this.setState({
                    imageUri: response.uri
                })
                this.props.setNewImpressionImage(response)
                console.log(response);
            }
        )
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.inner}>
                    <View style={styles.main}>
                        {/* <Text style={styles.title}>Назовите</Text> */}
                        {this.state.imageUri !== null ?
                            <Image
                                style={{
                                    paddingVertical: 30,
                                    width: 150,
                                    height: 150,
                                    borderRadius: 75
                                }}
                                resizeMode='cover'
                                source={{
                                    uri: this.state.imageUri
                                }}
                            />
                            : null}
                        <Text>type_id: {this.props.app.impression.new.type_id}</Text>
                        <View>
                            <FloatingLabelInput
                                label={translate("default.title")}
                                value={this.props.app.impression.new.name}
                                onChangeText={this.props.setNewImpressionName}
                            />
                            <FloatingLabelInput
                                label={translate("default.description")}
                                value={this.props.app.impression.new.description}
                                onChangeText={this.props.setNewImpressionDescription}
                            />
                            <TButton
                                onPress={this.setImage}
                                text={translate("default.addPhoto")}
                            />
                            <TButton
                                style={{ marginTop: 10 }}
                                onPress={this.update}
                                text={translate("default.next")}
                            />
                            {/* <PhotoUpload
                                onPhotoSelect={avatar => {
                                    if (avatar) {
                                        console.log('Image base64 string: ', avatar)
                                    }
                                }}
                            >
                                <Image
                                    style={{
                                        paddingVertical: 30,
                                        width: 150,
                                        height: 150,
                                        borderRadius: 75
                                    }}
                                    resizeMode='cover'
                                    source={{
                                        uri: 'https://www.sparklabs.com/forum/styles/comboot/theme/images/default_avatar.jpg'
                                    }}
                                />
                            </PhotoUpload> */}
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    card: {
        borderWidth: 1,
        borderColor: '#dedede',
        borderStyle: 'solid',
        padding: 20,
        flexGrow: 1,
        margin: 10,
        borderRadius: 10
    },
    container: {
        top: 0,
        flex: 1,
        backgroundColor: "#fff",
        position: "relative",
    },
    inner: {
        alignItems: "center",
        justifyContent: 'space-around',
        height: "100%",
    },
    main: {
        width: Dimensions.get('window').width - 40
        // alignItems: 'center'
    },
    title: {
        fontSize: 27,
    },
});
