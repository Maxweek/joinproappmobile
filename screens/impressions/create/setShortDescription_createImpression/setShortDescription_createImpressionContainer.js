import React, { Component } from "react";
// import {} from "../../store/user/actions";
import { connect } from "react-redux";
import { setNewImpressionName, setNewImpressionDescription, setNewImpressionImage } from "../../../../store/user/actions";
import SetShortDescription_createImpression from "./setShortDescription_createImpression";

class SetShortDescription_createImpressionContainer extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <SetShortDescription_createImpression
                route={this.props.route}
                navigation={this.props.navigation}
                app={this.props.app}
                setNewImpressionName={this.props.setNewImpressionName}
                setNewImpressionDescription={this.props.setNewImpressionDescription}
                setNewImpressionImage={this.props.setNewImpressionImage}
            />
        );
    }
}

const mapStateToProps = (state) => {
    return {
        app: state.app
    };
};

const mapDispatchToProps = {
    setNewImpressionName,
    setNewImpressionDescription,
    setNewImpressionImage
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SetShortDescription_createImpressionContainer);
