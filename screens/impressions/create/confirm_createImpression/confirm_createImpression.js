import React, { Component } from "react";
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
    Image,
    Alert
} from "react-native";
import Modal from 'react-native-modal';
import Icon from "react-native-vector-icons/Ionicons";
import API, { API_IMPRESSION_UPDATE } from "../../../../API";
import { translate } from "../../../../App";
import TButton from "../../../../components/tButton";

export default class Confirm_createImpression extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: {
                isModalActive: false,
                title: '',
                description: '',
            },
        }
    }
    onPublic = (status) => {
        let title;
        let description;
        switch (status) {
            case 'draft':
                title = translate('screen.createImpression.confirm.hasBeenSavedDraft');
                description = translate('screen.createImpression.confirm.hasBeenSavedDraft_desc');
                break;
            case 'moderation':
                title = translate('screen.createImpression.confirm.hasBeenSendedModerate');
                description = translate('screen.createImpression.confirm.hasBeenSendedModerate_desc');
                break;
            case 'delete':
                title = translate('screen.createImpression.confirm.hasBeenDeleted');
                description = translate('screen.createImpression.confirm.hasBeenDeleted_desc');
                break;
        }
        const formData = new FormData();

        formData.append('impression_id', this.props.app.impression.new.id)
        formData.append('impression_status', status)

        API.post(API_IMPRESSION_UPDATE, formData)
            .then(res => {
                console.log(res.data)
                this.setState({
                    modal: {
                        ...this.state.modal,
                        title: title,
                        description: description,
                    }
                }, () => {
                    this.toggleModal(true)
                })
            })
    }
    deleteAlert = () => {
        Alert.alert(
            translate('screen.createImpression.confirm.wannaDelete'),
            translate('screen.createImpression.confirm.dataWillBeDeleted'),
            [
                { text: translate('screen.createImpression.confirm.cancel'), style: "cancel" },
                { text: translate('screen.createImpression.confirm.gotIt'), onPress: () => this.onPublic('delete') }
            ]
        );
    }
    toggleModal = (type = null) => {
        this.setState({
            modal: {
                ...this.state.modal,
                isModalActive: type !== null ? !this.state.modal.isModalActive : type
            },
        });
    };
    onConfirm = () => {
        this.toggleModal(false)
        this.props.clearNewImpression()
        this.props.navigation.navigate('listImpressionsScreen');
    }
    render() {
        return (
            <View style={styles.container}>
                <Modal
                    isVisible={this.state.modal.isModalActive}
                    onBackdropPress={() => this.onConfirm(false)}
                    onSwipeComplete={() => this.onConfirm(false)}
                    useNativeDriverForBackdrop={true}
                    swipeDirection="down"
                    style={{ justifyContent: 'flex-end', margin: 0 }}
                >
                    <View style={styles.modalInner}>
                        <View style={styles.modalInner_swiper}></View>
                        <View style={{justifyContent: 'space-between', minHeight: 300}}>
                            <View>
                                <Text style={styles.modalInner_title}>{this.state.modal.title}</Text>
                                <Text style={styles.modalInner_description}>{this.state.modal.description}</Text>
                            </View>
                            <TButton
                                style={{ margin: 16 }}
                                text={translate('screen.createImpression.confirm.gotIt')}
                                onPress={() => { this.onConfirm() }}
                            />
                        </View>
                    </View>
                </Modal>
                <View style={styles.inner}>
                    <View style={styles.main}>
                        <Text style={styles.title}>{translate('screen.createImpression.confirm.title')}</Text>
                        <View>
                            {this.props.app.impression.new.image !== null ?
                                <Image
                                    style={{
                                        paddingVertical: 30,
                                        width: 150,
                                        height: 150,
                                        borderRadius: 75
                                    }}
                                    resizeMode='cover'
                                    source={{
                                        uri: this.props.app.impression.new.image.uri
                                    }}
                                />
                                : null}
                            <Text>id: {this.props.app.impression.new.id}</Text>
                            <Text>type_id: {this.props.app.impression.new.type_id}</Text>
                            <Text>name: {this.props.app.impression.new.name}</Text>
                            <Text>description: {this.props.app.impression.new.description}</Text>
                            <Text>duration: {this.props.app.impression.new.shedule.duration.summary}</Text>
                        </View>
                        <TButton
                            style={{ marginTop: 10, marginHorizontal: 16 }}
                            text={translate('screen.createImpression.confirm.saveDraft')}
                            onPress={() => { this.onPublic('draft') }}
                        />
                        <TButton
                            style={{ marginTop: 10, marginHorizontal: 16 }}
                            text={translate('screen.createImpression.confirm.sendModerate')}
                            onPress={() => { this.onPublic('moderation') }}
                        />
                        <TButton
                            style={{ marginTop: 10, marginHorizontal: 16 }}
                            text={translate('screen.createImpression.confirm.deleteImpression')}
                            type="danger"
                            onPress={() => { this.deleteAlert() }}
                        />
                    </View>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    card: {
        borderWidth: 1,
        borderColor: '#dedede',
        borderStyle: 'solid',
        flexDirection: 'row',
        alignItems: 'center',
        padding: 20,
        flexGrow: 1,
        margin: 10,
        borderRadius: 10
    },
    container: {
        top: 0,
        flex: 1,
        backgroundColor: "#fff",
        position: "relative",
    },
    inner: {
        alignItems: "center",
        justifyContent: 'space-around',
        height: "100%",
    },
    main: {
        alignItems: 'center',
        // width: Dimensions.get('window') - 40
    },
    title: {
        fontSize: 27,
    },
    modalInner: {
        backgroundColor: 'white',
        bottom: 0,
        width: '100%',
        paddingTop: 10,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        position: 'relative',
        // paddingBottom: 20
    },
    modalInner_swiper: {
        position: 'absolute',
        width: 30,
        height: 4,
        top: 4,
        left: (Dimensions.get('window').width - 30) / 2,
        borderRadius: 20,
        backgroundColor: '#dedede'
    },
    modalInner_title: {
        padding: 16,
        fontWeight: 'bold',
        fontSize: 24
    },
    modalInner_description: {
        padding: 16,
        fontSize: 14,
    },
});
