import React, { Component } from "react";
import { connect } from "react-redux";
import { clearNewImpression } from "../../../../store/user/actions";
import Confirm_createImpression from "./confirm_createImpression";

class Confirm_createImpressionContainer extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Confirm_createImpression
        route={this.props.route}
        navigation={this.props.navigation}
        app={this.props.app}
        clearNewImpression={this.props.clearNewImpression}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    app: state.app
  };
};

const mapDispatchToProps = {
  clearNewImpression
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Confirm_createImpressionContainer);
