import React, { Component } from "react";
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Button,
    Dimensions,
    Image
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import FloatingLabelInput from "../../../../components/floatingLabelInput";
import TButton from "../../../../components/tButton";
import API, { API_IMPRESSION_UPDATE } from "../../../../API";
import { translate } from "../../../../App";

export default class SetSheduleMain_createImpression extends Component {
    constructor(props) {
        super(props);
        console.log(this.props.route)
        this.state = {
            imageUri: null,
            isListenerAdded: false,
        }
        console.log(this.props.app.impression.new.shedule.duration)
    }
    componentDidMount() {
        this.checkRoute();
        this.addListener();
    }
    checkRoute = () => {
        console.log('NEXT_IS_ROUTE_PARAMS')
        console.log(this.props.route.params)
        if (this.props.route.params !== undefined) {
            console.log('NEXT_IS_SHOULD_SET_NEW_FALSE')
            this.props.setNewImpressionId(this.props.route.params.impression_id);
            this.props.setNewImpressionIsNew(false)
        }
    }
    addListener = () => {
        if(!this.state.isListenerAdded){
            this.unsubscribe = this.props.navigation.addListener('focus', () => {
                this.checkRoute()
                console.log('focus')
            });
            this.setState({isListenerAdded: true})
        }
    }
    sendUpdate = () => {

        let summary = parseInt(this.props.app.impression.new.shedule.duration.minutes);
        summary += parseInt(this.props.app.impression.new.shedule.duration.days) * 24 * 60;
        summary += parseInt(this.props.app.impression.new.shedule.duration.hours) * 60;
        console.log(this.props.app.impression.new.shedule.duration)
        if (parseInt(summary) > 0) {
            const formData = new FormData();

            formData.append('impression_id', this.props.app.impression.new.id)
            formData.append('impression_duration', summary)

            API.post(API_IMPRESSION_UPDATE, formData)
                .then(res => {
                    this.props.setNewImpressionSheduleDurationSummary(summary)
                    console.log(res.data)
                })
        }
    }
    update = () => {
        // setNewImpressionSheduleDurationSummary
        this.sendUpdate();
        this.props.navigation.navigate('setSheduleDate_createImpressionScreen')
    }
    onChangeNewImpressionSheduleDurationDays = e => {
        let val = e;
        if (this.check(val)) {
            this.props.setNewImpressionSheduleDurationDays(val)
        }
    }
    onChangeNewImpressionSheduleDurationHours = e => {
        let val = e;
        if (this.check(val)) {
            this.props.setNewImpressionSheduleDurationHours(val)
        }
    }
    onChangeNewImpressionSheduleDurationMinutes = e => {
        let val = e;
        if (this.check(val)) {
            this.props.setNewImpressionSheduleDurationMinutes(val)
        }
    }
    check = value => {
        console.log(isNumber(value))
        if (isNumber(value)) {
            return true;
        } else {
            return false;
        }

        function isNumber(val) {
            return !isNaN(val);
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.inner}>
                    <View style={styles.main}>
                        <Text style={styles.title}>{translate("screen.createImpression.setSheduleMain.title")}</Text>

                        <Text>id: {this.props.app.impression.new.id}</Text>
                        <Text>type_id: {this.props.app.impression.new.type_id}</Text>
                        <Text>{translate("screen.createImpression.setSheduleMain.duration")}</Text>
                        <View>
                            <View style={{ flexDirection: 'row' }}>
                                <FloatingLabelInput
                                    label={translate("datetime.times.days")}
                                    value={this.props.app.impression.new.shedule.duration.days}
                                    onChangeText={this.onChangeNewImpressionSheduleDurationDays}
                                    style={{ flex: 1, margin: 10 }}
                                    keyboardType="number-pad"
                                />
                                <FloatingLabelInput
                                    label={translate("datetime.times.hours")}
                                    value={this.props.app.impression.new.shedule.duration.hours}
                                    onChangeText={this.onChangeNewImpressionSheduleDurationHours}
                                    style={{ flex: 1, margin: 10 }}
                                    keyboardType="number-pad"
                                />
                                <FloatingLabelInput
                                    label={translate("datetime.times.minutes")}
                                    value={this.props.app.impression.new.shedule.duration.minutes}
                                    onChangeText={this.onChangeNewImpressionSheduleDurationMinutes}
                                    style={{ flex: 1, margin: 10 }}
                                    keyboardType="number-pad"
                                />
                            </View>
                            <TButton
                                style={{ marginTop: 10 }}
                                onPress={this.update}
                                text={translate("default.next")}
                            />
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    card: {
        borderWidth: 1,
        borderColor: '#dedede',
        borderStyle: 'solid',
        padding: 20,
        flexGrow: 1,
        margin: 10,
        borderRadius: 10
    },
    container: {
        top: 0,
        flex: 1,
        backgroundColor: "#fff",
        position: "relative",
    },
    inner: {
        alignItems: "center",
        justifyContent: 'space-around',
        height: "100%",
    },
    main: {
        width: Dimensions.get('window').width - 40
        // alignItems: 'center'
    },
    title: {
        fontSize: 27,
    },
});
