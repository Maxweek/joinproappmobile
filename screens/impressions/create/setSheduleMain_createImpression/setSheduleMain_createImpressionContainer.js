import React, { Component } from "react";
// import {} from "../../store/user/actions";
import { connect } from "react-redux";
import {
    setNewImpressionSheduleDurationDays,
    setNewImpressionSheduleDurationHours,
    setNewImpressionSheduleDurationMinutes,
    setNewImpressionSheduleDurationSummary,
    setNewImpressionId,
    setNewImpressionIsNew
} from "../../../../store/user/actions";
import SetSheduleMain_createImpression from "./setSheduleMain_createImpression";

class SetSheduleMain_createImpressionContainer extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <SetSheduleMain_createImpression
                route={this.props.route}
                navigation={this.props.navigation}
                app={this.props.app}
                setNewImpressionSheduleDurationDays={this.props.setNewImpressionSheduleDurationDays}
                setNewImpressionSheduleDurationHours={this.props.setNewImpressionSheduleDurationHours}
                setNewImpressionSheduleDurationMinutes={this.props.setNewImpressionSheduleDurationMinutes}
                setNewImpressionSheduleDurationSummary={this.props.setNewImpressionSheduleDurationSummary}
                setNewImpressionId={this.props.setNewImpressionId}
                setNewImpressionIsNew={this.props.setNewImpressionIsNew}
            />
        );
    }
}

const mapStateToProps = (state) => {
    return {
        app: state.app
    };
};

const mapDispatchToProps = {
    setNewImpressionSheduleDurationDays,
    setNewImpressionSheduleDurationHours,
    setNewImpressionSheduleDurationMinutes,
    setNewImpressionSheduleDurationSummary,
    setNewImpressionId,
    setNewImpressionIsNew
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SetSheduleMain_createImpressionContainer);
