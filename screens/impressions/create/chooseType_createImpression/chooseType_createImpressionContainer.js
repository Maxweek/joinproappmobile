import React, { Component } from "react";
import { connect } from "react-redux";
import { setNewImpressionTypeId, setNewImpressionId, setNewImpressionIsNew } from "../../../../store/user/actions";
import ChooseType_createImpression from "./chooseType_createImpression";

class ChooseType_createImpressionContainer extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <ChooseType_createImpression
        route={this.props.route}
        navigation={this.props.navigation}
        app={this.props.app}
        setNewImpressionTypeId={this.props.setNewImpressionTypeId}
        setNewImpressionId={this.props.setNewImpressionId}
        setNewImpressionIsNew={this.props.setNewImpressionIsNew}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    app: state.app
  };
};

const mapDispatchToProps = {
  setNewImpressionTypeId,
  setNewImpressionId,
  setNewImpressionIsNew
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChooseType_createImpressionContainer);
