import React, { Component } from "react";
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import API, { API_IMPRESSION_CREATE } from "../../../../API";
import { translate } from "../../../../App";
import { COLORS } from "../../../../assets/styles/styles";
import ContainerBox from "../../../../components/containerBox";
import HeaderBox from "../../../../components/headerBox";

export default class ChooseType_createImpression extends Component {
    constructor(props) {
        super(props);
    }
    sendImpressionTemp = type_id => {
        const formData = new FormData();

        formData.append('impression_type_id', type_id)

        API.post(API_IMPRESSION_CREATE, formData)
            .then(res => {
                console.log(res.data)
                console.log(res.data.impression_id)
                this.props.setNewImpressionId(res.data.impression_id)
                this.props.setNewImpressionIsNew(true)
            })
    }
    selectTypeId = id => {
        this.props.setNewImpressionTypeId(id)
        this.sendImpressionTemp(id);
        this.props.navigation.navigate('setShortDescription_createImpressionScreen')
    }
    render() {
        return (
            <ContainerBox
                navigation={this.props.navigation}
                paddingTop={true}
                header={<HeaderBox
                    gradient={true}
                    scheme={'dark'}
                    shadow={false}
                    navigation={this.props.navigation}
                    back={true}
                    title={translate("navigator.createImpression.chooseType")}
                />}
                title={translate("screen.createImpression.chooseType.title")}
            >
                <View style={{ flexDirection: 'row', marginVertical: 16, marginHorizontal: -16 }}>
                    <TouchableOpacity style={styles.card} onPress={() => this.selectTypeId(0)}>
                        <Icon name="ios-pricetag" size={24} color={COLORS.primary} style={{ marginRight: 10 }} />
                        <Text>{translate("screen.createImpression.chooseType.online")}</Text>
                    </TouchableOpacity>
                    {/* <TouchableOpacity style={styles.card} onPress={() => this.selectTypeId(1)}>
                                <Icon name="ios-pricetag" size={24} color="#14ccb4" style={{ marginRight: 10 }} />
                                <Text>{translate("screen.createImpression.chooseType.offline")}</Text>
                            </TouchableOpacity> */}
                </View>
            </ContainerBox>
        );
    }
}
const styles = StyleSheet.create({
    card: {
        borderWidth: 1,
        borderColor: '#dedede',
        borderStyle: 'solid',
        flexDirection: 'row',
        alignItems: 'center',
        padding: 16,
        flexGrow: 1,
        margin: 16,
        borderRadius: 8
    },
});
