import React, { Component } from "react";
import {
    Text,
    View,
    StyleSheet,
    Button,
    ScrollView,
    StatusBar,
    RefreshControl,
    Dimensions,
    ActivityIndicator,
    SafeAreaView,
    Animated
} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import API, { API_FILTER, API_IMPRESSION_LIST } from "../../API";
import { translate } from "../../App";
import { COLORS } from "../../assets/styles/styles";
import list_impressionsContainer from "../impressions/list/list_impressionsContainer";
import Shared_EmptyList from "../_shared/emptyList";
import List_ComponentImpressions from "./components/list";
import List_Impressions from "./list/list_impressions";
import List_ImpressionsContainer from "./list/list_impressionsContainer";
// import Shared_EmptyList from "../_shared/emptyList";
import Icon from "react-native-vector-icons/Ionicons";
import Modal from 'react-native-modal';
import SelectPicker from "../../components/selectPicker";
import TButton from "../../components/tButton";

import { Platform, NativeModules } from 'react-native';
import HeaderBox from "../../components/headerBox";
import ContainerBox from "../../components/containerBox";
import { Modalize } from "react-native-modalize";
import ProfileSettings from "../profile/settings/settings_profile";
import { Portal } from "react-native-portalize";
import FilterBox from "../../components/filterBox";
import ModalBox from "../../components/modalBox";
import I18n from "i18n-js";
import PushNotification from "react-native-push-notification";

const { StatusBarManager } = NativeModules;

export default class Impressions extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isListenerAdded: false,
            isActiveBtn: true,
            refreshing: false,
            items: [],
            filters: [],
            modal: {
                isModalActive_sort: false,
                isModalActive_filter: false,
            },
            isContentLoaded: false
        }
        this.filters = [];
        // this.animated = new Animated.Value(0)
        this.modalizeRef
    }

    createChannel = () => {
        PushNotification.createChannel(
            {
                channelId: 'test-channel',
                channelName: 'test Channel'
            }
        )
    }

    handleNotification = () => {
        PushNotification.localNotification({
            channelId: 'test-channel',
            title: 'Clicked!',
            message: 'kek'
        })
    }

    getList = () => {
        const formData = this.getFilterData()

        // console.log(formData)
        
        API.post(API_IMPRESSION_LIST, formData)
            .then(res => {
                // console.log(res)
                if (typeof res.data.status !== 'undefined') {
                    if (res.data.status === 'success') {
                        // console.log(res.data.impressions)
                        this.setList(res.data.impressions);
                        this.setState({ refreshing: false, items: res.data.impressions, isNotEmpty: true, isContentLoaded: true });
                    } else {
                        console.log('list_load_error_1')
                        this.setState({ refreshing: false, isNotEmpty: false });
                    }
                } else {
                    console.log('list_load_error_2')
                    this.setState({ refreshing: false, isNotEmpty: false });
                }
            })
            .catch(err => {
                console.log(err)
                this.setState({ refreshing: false, isNotEmpty: false });
            })
    }
    getFilterData = () => {
        let filters = new FormData();

        let stateFilters = []


        this.state.filters.map(el => {
            stateFilters.push({
                name: el.name,
                data: el.data.map(e => {
                    return {
                        type: e.type,
                        key: e.key,
                        value: e.value,
                        isActive: e.isActive
                    }
                })
            })
        })
        if(this.props.route.params?.filters){
            stateFilters = this.props.route.params.filters
        } else {
            stateFilters = this.state.filters
        }
        // stateFilters.map(el => {
        // })
        
        // console.log('FILTERS')
        // console.log(this.props.route.params?.filters)
        // console.log(stateFilters)
        filters.append('filters', JSON.stringify(stateFilters));
        
        if(stateFilters[0]){
            if(stateFilters[0].options?.length){
                stateFilters[0].options?.map(el => {
                    let key = Object.keys(el)[0]
                    filters.append(key, el[key]);
                })
            }
        }
            
        return filters
    }
    getFilter = () => {
        const formData = new FormData()
        console.log('FILTER CODE ' + I18n.locale)
        formData.append('language_code', I18n.locale)

        API.post(API_FILTER, formData)
            .then(res => {
                console.log(res.data)
                this.setState({ filters: res.data.filters });
                this.getList();
                // if (typeof res.data.status !== 'undefined') {
                //     if (res.data.status === 'success') {
                //         console.log('list_loaded')
                //         console.log(res.data.impressions)
                //     } else {
                //         console.log('list_load_error_1')
                //         this.setState({ refreshing: false, isNotEmpty: false });
                //     }
                // } else {
                //     console.log('list_load_error_2')
                //     this.setState({ refreshing: false, isNotEmpty: false });
                // }
            })
            .catch(err => {
                console.log(err)
                this.setState({ refreshing: false, isNotEmpty: false });
            })

    }
    setList = list => {
        // console.log('list setted')
        this.props.setImpressionList(list)
        // console.log(this.state.items)
    }
    onRefresh = () => {
        this.setState({ refreshing: true })
        this.getList();
        // this.getFilter()
    }
    componentDidMount() {
        // console.log('NAVIGATION')
        // console.log(this.props)
        this.addListener();
        // this.getList();
        this.getFilter()
        this.createChannel();
    }
    toggleFilterModal = (type = null) => {
        this.setState({
            modal: {
                ...this.state.modal,
                isModalActive_filter: type
            },
        }, () => {
            if (this.state.modal.isModalActive_filter) {
                this.modalizeFilterRef.open();
            } else {
                this.modalizeFilterRef.close();
            }
        });
    };
    addListener = () => {
        if (!this.state.isListenerAdded) {
            this.unsubscribe = this.props.navigation.addListener('focus', () => {
                this.props.setAppTabBarVisibility(true)
                this.getList();
                this.getFilter();
            });
            this.setState({ isListenerAdded: true })
        }
    }
    resetFilters = () => {
        this.modalizeFilterRef.close()
        this.getFilter();
        this.onRefresh()
        this.filters = [];
    }
    applyFilters = () => {
        this.setState({
            ...this.state.filters, filters: this.filters
        }, () => {
            this.onRefresh()
            // this.getList();
            this.modalizeFilterRef.close()
        })
    }
    prepareFilters = data => {
        this.filters = data;
        // console.log(this.filters)
    }
    renderFilters = () => {

        return <FilterBox
            filters={this.state.filters}
            prepare={this.prepareFilters}
        />
    }
    render() {
        console.log(this.props.route.params)
        return (
            <>
                <ContainerBox
                    navigation={this.props.navigation}
                    header={
                        <HeaderBox
                            scheme={'dark'}
                            title={this.props.route.params?.title ? this.props.route.params?.title : translate("screen.impression.title")}
                            shadow={true}
                            navigation={this.props.navigation}
                            back={this.props.route.params?.title ? true : false}
                            // left={[{ icon: 'filter-outline', action: () => { this.handleNotification() } }]}
                            right={!this.props.route.params?.title ? [{ icon: 'funnel-outline', action: () => { this.toggleFilterModal(true) } }] : false}
                        />
                    }
                    isContentLoaded={this.state.isContentLoaded}
                    refreshing={this.state.refreshing}
                    onRefresh={this.onRefresh}>
                    <List_ComponentImpressions
                        navigation={this.props.navigation}
                        items={this.state.items}
                        noAction={true}>
                    </List_ComponentImpressions>
                </ContainerBox>
                <ModalBox
                    modalRef={ref => this.modalizeFilterRef = ref}
                    headerTitle={translate('default.filterName')}
                    animated={this.animated}
                    tapGestureEnabled={false}
                    footer={
                        <View style={{ flexDirection: 'row', margin: -8 }}>

                            <TButton
                                style={{ flexGrow: 1, margin: 8 }}
                                notStretch={true}
                                type={'secondary'}
                                onPress={() => { this.resetFilters() }}
                                text={translate('default.filterReset')}
                            />
                            <TButton
                                style={{ flexGrow: 2, margin: 8 }}
                                notStretch={true}
                                onPress={() => { this.applyFilters() }}
                                text={translate('default.filterApply')}
                            />
                        </View>
                    }
                >
                    {this.renderFilters()}
                    {/* {this.renderFilters()} */}
                </ModalBox>
            </>
        );

    }
}
const styles = StyleSheet.create({
    container: {
        top: 0,
        flex: 1,
        backgroundColor: "#fff",
        position: "relative",
    },
    inner: {
        alignItems: "center",
        justifyContent: 'space-around',
        height: "100%",

        paddingTop: StatusBarManager.HEIGHT + 60
    },
    main: {
        alignItems: 'center',
        // paddingTop: 60
    },
    title: {
        fontSize: 27,
    },
    box: {
        padding: 16
    },

});
