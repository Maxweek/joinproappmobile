import React, { Component } from "react";
import {
    Text,
    View,
    StyleSheet,
    ScrollView,
    Image,
    Dimensions,
    FlatList,
    Button,
    TouchableOpacity,
    RefreshControl
} from "react-native";
import Shared_EmptyList from "../../_shared/emptyList";
import { TabView, SceneMap } from 'react-native-tab-view';
import Animated from 'react-native-reanimated';
import { createRef } from "react";
import ScrollableTabBar from "../../_shared/scrollableTabBar";
import Icon from "react-native-vector-icons/Ionicons";

import Modal from 'react-native-modal';
import API, { API_IMPRESSION_LIST } from "../../../API";
import { translate } from "../../../App";
import { COLORS } from "../../../assets/styles/styles";
import Card from "../../../components/card";
// import { map } from "core-js/core/array";

export default class List_ComponentImpressions extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isNotEmpty: false,
            refreshing: false,
            isListenerAdded: false,
            index: 0,
            pos: [],
            colCount: 2,
            items: this.props.items,
            modal: {
                isModalActive: false,
                text: '',
                actions: []
            },
        }
        this._scrollView = createRef()
        this._scrollView_holder = createRef()
        this._animateHolderPosition = new Animated.Value(0);

    }

    renderCard = (item) => (
        <Card
            item={item}
            onPress={e => { this.viewImpression(item) }}
            style={{ margin: 4, width: (Dimensions.get('screen').width / 2) - 20 }}
        />
        // <TouchableOpacity style={styles.card} key={item.id} onPress={() => { this.viewImpression(item) }}>
        //     <View style={styles.cardImageBox}>
        //         {item.streaming === true ? <View style={styles.cardLiveLabel}>
        //             <Text style={styles.cardLiveLabelText}>online</Text>
        //         </View> : null}
        //         {item.streaming === false ? null : null}
        //         {item.streaming === 'recorded' ? <View style={[styles.cardLiveLabel, {backgroundColor: COLORS.gray} ]}>
        //             <Text style={styles.cardLiveLabelText}>в записи</Text>
        //         </View> : null}
        //         <Image
        //             style={{ width: '100%', height: '100%' }}
        //             source={{ uri: item.preview }}
        //         />
        //         <View style={styles.cardDateBox}>
        //             <Text style={styles.cardDate}>{item.date_local}</Text>
        //         </View>
        //     </View>
        //     <Text style={styles.cardTitle}>{item.title}</Text>
        //     <Text style={styles.cardPrice}>{item.price === 0 ? translate("default.free") : item.price + ' ₽'}</Text>
        // </TouchableOpacity>
    )

    _updateTabBar = i => {
        this.setState({ index: i });
    }
    viewImpression = (item) => {
        if (this.props.navigation) {
            this.props.navigation.push('impressionViewNavigator', {
                screen: "viewImpressionScreen",
                params: {
                    id: item.id
                }
            })
        } else {
            this.props.onCardPress(item)
        }
    }
    renderList() {
        return this.props.items.map(item =>
            this.renderCard(item)
        )
    }
    render() {
        return <View style={[styles.cardList, { justifyContent: this.props.items.length ? 'flex-start' : 'center' }]}>
            {this.props.items.length ?
                <View style={styles.cardList__inner}>
                    {this.renderList()}
                </View>
                :
                <Shared_EmptyList
                    title={translate("screen.impression.list.empty.title")}
                    subtitle={translate("screen.impression.list.empty.subtitle")}
                    actionText={translate("screen.impression.list.empty.actionText")}
                    onPress={this.props.sharedActions}
                    noAction={this.props.noAction}
                />}

        </View>

    }
}
const styles = StyleSheet.create({
    cardList: {
        // backgroundColor: '#fefefe',
        // width: Dimensions.get('window').width,
        // width: '100%',
        paddingVertical: 16,
        // marginHorizontal: -16,
        width: '100%',
        height: '100%'
    },
    cardList__inner: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        // width: Dimensions.get('window').width - 8,
        flexWrap: 'wrap',
        margin: -4,
    },
    card: {
        width: (Dimensions.get('window').width / 2) - 20,
        // height: (Dimensions.get('window').width / 2),
        // margin: 5,
        paddingBottom: 5,
        position: 'relative'
    },
    cardTitle: {
        marginTop: 4,
        fontSize: 16,
        lineHeight: 22,
        color: "#2a2d43",
        fontWeight: 'bold'
    },
    cardPrice: {
        marginTop: 4,
        fontSize: 14,
        lineHeight: 22,
        color: "#2a2d43",
        fontWeight: 'bold'
    },
    cardDateBox: {
        position: 'absolute',
        bottom: 8,
        left: 8,
        right: 8
    },
    cardDate: {
        fontSize: 14,
        color: 'white',
        fontWeight: 'bold',
        textShadowRadius: 4,
        textShadowColor: 'black'
    },
    cardImageBox: {
        width: '100%',
        backgroundColor: '#dedede',
        height: (Dimensions.get('window').width / 2),
        borderRadius: 10,
        position: 'relative',
        overflow: 'hidden'
    },
    cardImage: {
        width: '100%',
        height: '100%'
    },
    container: {
        top: 0,
        flex: 1,
        backgroundColor: "#fff",
        position: "relative",
    },
    inner: {
        alignItems: "center",
        justifyContent: 'space-around',
        height: "100%",
    },
    main: {
        alignItems: 'center',
        marginTop: 40
    },
    title: {
        fontSize: 27,
    },
    tabBar: {
        flexDirection: 'row',
        paddingHorizontal: 20,
        flex: 1,
        maxHeight: 50,
        backgroundColor: '#dedede',
        position: 'relative'
        // paddingTop: Constants.statusBarHeight,
    },
    tabItem: {
        // flex: 1,
        alignItems: 'center',
        padding: 16,
    },
    tabBar_holder: {
        backgroundColor: 'black',
        width: 20,
        height: 2,
        position: 'absolute',
        bottom: 0
    },
    modalInner: {
        backgroundColor: 'white',
        bottom: 0,
        width: '100%',
        paddingTop: 10,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        position: 'relative',
        paddingBottom: 20
    },
    modalInner_swiper: {
        position: 'absolute',
        width: 30,
        height: 4,
        top: 4,
        left: (Dimensions.get('window').width - 30) / 2,
        borderRadius: 20,
        backgroundColor: '#dedede'
    },
    modalInner_title: {
        padding: 16,
        fontSize: 20
    },
    modalInner_action: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 8,
        paddingHorizontal: 16,
        borderTopColor: '#dedede',
        borderTopWidth: 1,
        borderStyle: 'solid'
    },
    cardLiveLabel: {
        position: 'absolute',
        top: 0,
        zIndex: 20,
        backgroundColor: '#e45050',
        paddingVertical: 4,
        paddingHorizontal: 12,
        borderBottomRightRadius: 10,
        elevation: 10
    },
    cardLiveLabel__recorded: {
        backgroundColor: '#a0a0a0',
    },
    cardLiveLabelText: {
        fontSize: 12,
        color: 'white',
        fontWeight: 'bold',
        textTransform: 'uppercase'
    },
});
