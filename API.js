import * as axios from 'axios';
import RNSecureStorage, { ACCESSIBLE, ACCESS_CONTROL, AUTHENTICATION_TYPE } from 'react-native-secure-storage';

export const API_USER_LOGIN_EMAIL = '/user/auth/email/'; // POST
export const API_USER_LOGIN_PHONE = '/user/auth/phone/auth/'; // POST
export const API_USER_LOGIN_PHONE_GETCODE = '/user/auth/phone/getcode/'; // POST
export const API_USER_REGISTER_GETCODE = '/user/register/phone/getcode/'; // POST
export const API_USER_REGISTER = '/user/register/phone/'; // POST
export const API_USER_LOGOUT = '/profile/logout/'; // POST
export const API_USER_CHECK = '/profile/status/'; // GET
export const API_USER_INFO = '/profile/info/'; // GET
export const API_USER_READ = '/user/read/'; // GET + id
export const API_IMPRESSION_LIST = '/impression/list/'; // GET
export const API_IMPRESSION_LIST_OWN = '/impression/list/own/'; // GET
export const API_IMPRESSION_READ = '/impression/read/'; // GET + id
export const API_IMPRESSION_CREATE = '/impression/create/'; // POST
export const API_IMPRESSION_UPDATE = '/impression/update/'; // POST
export const API_IMPRESSION_SHEDULE_LIST = '/impression/schedule/list/'; // POST
export const API_IMPRESSION_SHEDULE_CREATE = '/impression/schedule/create/'; // POST
export const API_IMPRESSION_SHEDULE_DELETE = '/impression/schedule/delete/'; // POST
export const API_IMPRESSION_PHOTO_ADD = '/impression/photo/add/'; // POST
export const API_IMPRESSION_STREAM_CHECK = '/impression/stream/check/'; // GET
export const API_IMPRESSION_STREAM_RECORDING_START = '/impression/stream/recording/start/'; // POST
export const API_IMPRESSION_STREAM_RECORDING_CHECK = '/impression/stream/recording/query/'; // POST
export const API_IMPRESSION_STREAM_RECORDING_STOP = '/impression/stream/recording/stop/'; // POST
export const API_DONATE_LIST = '/donate/list/'; // POST
export const API_FILTER = '/filter/'; // GET
export const API_PAYMENT_APPLE_PURCHASE = '/payment/apple/purchase/'; // GET

const config = {
    // accessControl: ACCESS_CONTROL.BIOMETRY_ANY_OR_DEVICE_PASSCODE,
    accessible: ACCESSIBLE.WHEN_UNLOCKED,
    authenticationPrompt: 'auth with yourself',
    service: 'example',
    authenticateType: AUTHENTICATION_TYPE.BIOMETRICS,
}

let _axios = axios.create({
    headers: {
        "Access-Control-Allow-Origin": '*',
        'Content-Type': 'multipart/form-data; charset=utf-8; boundary="another cool boundary";',
    },
    baseURL: `https://joinpro.ru/api`,
})

async function setToken() {
    const token = await RNSecureStorage.getItem('token', config);
    setApiToken(token)
}
export async function isTokenExist() {
    const token = await RNSecureStorage.getItem('token', config);
    if(token){
        return true
    } else {
        return false;
    }
}

setToken();


export const setApiToken = async (token) => {
    console.log('token: ' + token)
    if (token) {
        _axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
        await RNSecureStorage.setItem('token', token, config);
    } else {
        _axios.defaults.headers.common['Authorization'] = 'null';
        await RNSecureStorage.removeItem('token', config);
    }
    console.log('header: ' + _axios.defaults.headers.common['Authorization'])
}

export const getApiToken = () => {
    setTimeout(() => { _axios.defaults.headers.common['Authorization'] }, 5000)
    return _axios.defaults.headers.common['Authorization']
}


export const setAxios = () => {
    // _axios = 
}

export default _axios;

