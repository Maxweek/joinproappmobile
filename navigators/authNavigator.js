import React, { Component } from "react";

import { createStackNavigator } from "@react-navigation/stack";

import { connect } from "react-redux";
import AuthContainer from "../screens/auth/authContainer";
import registrationContainer from "../screens/registration/registrationContainer";
import { STYLES } from "../assets/styles/styles";
import authPhoneContainer from "../screens/auth/phone/authPhoneContainer";
import authEmailContainer from "../screens/auth/email/authEmailContainer";

const Stack = createStackNavigator();

class AuthNavigator extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidUpdate() {
    }

    render() {

        return (
            <Stack.Navigator style={STYLES.container}>
                {/* <Stack.Screen
                    name="profileScreenStack"
                    component={profileContainer}
                    options={{
                        headerShown: false,
                    }}
                /> */}
                <Stack.Screen
                    name="authScreen"
                    component={AuthContainer}
                    // component={Home}
                    options={{
                        headerShown: false,
                    }}
                />
                <Stack.Screen
                    name="authPhoneScreen"
                    component={authPhoneContainer}
                    // component={Home}
                    options={{
                        headerShown: false,
                    }}
                />
                <Stack.Screen
                    name="authEmailScreen"
                    component={authEmailContainer}
                    // component={Home}
                    options={{
                        headerShown: false,
                    }}
                />
                <Stack.Screen
                    name="registrationScreen"
                    component={registrationContainer}
                    options={{
                        headerShown: false,
                    }}
                />
                {/* <Stack.Screen
                    name="Live"
                    component={Live}
                    options={{
                        headerShown: false,
                    }}
                /> */}
            </Stack.Navigator>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        state: state,
    };
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(AuthNavigator);

