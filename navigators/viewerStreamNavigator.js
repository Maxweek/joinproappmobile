import React, { Component } from "react";
import { StyleSheet, Text, View, AppRegistry, Dimensions } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { connect } from "react-redux";
import { STYLES } from "../assets/styles/styles";
import { translate } from "../App";
import viewerContainer from "../screens/viewer/viewerContainer";
import viewerStreamContainer from "../screens/viewer/viewerStreamContainer";
// import Home from "../screens/Home";
import Live from "../screens/Live";

const Stack = createStackNavigator();

class viewerStreamNavigator extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidUpdate() {
    }

    render() {
        return (
            <Stack.Navigator style={STYLES.container}>
                <Stack.Screen
                    name="streamSettingsScreen"
                    component={viewerContainer}
                    options={{
                        title: 'Присоединение к стриму',
                        headerShown: false,
                    }}
                />
                <Stack.Screen
                    name="streamScreen"
                    component={viewerStreamContainer}
                    options={{
                        headerShown: false,
                    }}
                />
                {/* <Stack.Screen
                    name="streamScreenq"
                    component={Home}
                    options={{
                        headerShown: false,
                    }}
                /> */}
                {/* <Stack.Screen
                    name="streamScreenqs"
                    component={Live}
                    options={{
                        headerShown: false,
                    }}
                /> */}
            </Stack.Navigator>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        state: state,
    };
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(viewerStreamNavigator);

const styles = StyleSheet.create({
});
