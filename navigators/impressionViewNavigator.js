import React, { Component } from "react";
import { StyleSheet, Text, View, AppRegistry, Dimensions } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { connect } from "react-redux";
import { STYLES } from "../assets/styles/styles";
import { translate } from "../App";
import impressionsContainer from "../screens/impressions/impressionsContainer";
import view_impressionContainer from "../screens/impressions/view/view_impressionContainer";
import viewerStreamNavigator from "./viewerStreamNavigator";
import donateContainer from "../screens/donate/donateContainer";
import userContainer from "../screens/user/userContainer";

const Stack = createStackNavigator();

class ImpressionViewNavigator extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidUpdate() {
    }

    render() {
        return (
            <Stack.Navigator style={STYLES.container}>
                <Stack.Screen
                    name="viewImpressionScreen"
                    component={view_impressionContainer}
                    options={{
                        headerShown: false,
                    }}
                />
                <Stack.Screen
                    name="viewImpressionStreamScreen"
                    component={viewerStreamNavigator}
                    options={{
                        headerShown: false,
                    }}
                />
                <Stack.Screen
                    name="donateScreen"
                    component={donateContainer}
                    options={{
                        // presentation: "transparentModal",
                        headerShown: false,
                    }}
                />
                <Stack.Screen
                    name="userScreen"
                    component={userContainer}
                    options={{
                        // presentation: "transparentModal",
                        headerShown: false,
                    }}
                />
                {/* <Stack.Screen
                    name="videoScreen"
                    component={videoBox}
                    options={{
                        // presentation: "transparentModal",
                        headerShown: false,
                    }}
                /> */}
            </Stack.Navigator>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        state: state,
    };
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(ImpressionViewNavigator);

const styles = StyleSheet.create({
});
