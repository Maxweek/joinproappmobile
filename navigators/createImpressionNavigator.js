import React, { Component } from "react";
import { StyleSheet, Text, View, AppRegistry, Dimensions } from "react-native";

import { createStackNavigator } from "@react-navigation/stack";

import { connect } from "react-redux";
import chooseType_createImpressionContainer from "../screens/impressions/create/chooseType_createImpression/chooseType_createImpressionContainer";
import setShortDescription_createImpressionContainer from "../screens/impressions/create/setShortDescription_createImpression/setShortDescription_createImpressionContainer";
import confirm_createImpressionContainer from "../screens/impressions/create/confirm_createImpression/confirm_createImpressionContainer";
import setSheduleMain_createImpressionContainer from "../screens/impressions/create/setSheduleMain_createImpression/setSheduleMain_createImpressionContainer";
import setSheduleDate_createImpressionContainer from "../screens/impressions/create/setSheduleDate_createImpression/setSheduleDate_createImpressionContainer";
import { STYLES } from "../assets/styles/styles";
import { translate } from "../App";

const Stack = createStackNavigator();

class CreateImpressionNavigator extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidUpdate() {
    }

    render() {

        return (
            <Stack.Navigator style={STYLES.container}>
                <Stack.Screen
                    name="chooseType_createImpressionScreen"
                    component={chooseType_createImpressionContainer}
                    options={{
                        // headerTitle: 
                        headerShown: false,
                    }}
                />
                <Stack.Screen
                    name="setShortDescription_createImpressionScreen"
                    component={setShortDescription_createImpressionContainer}
                    options={{
                        headerTitle: translate("navigator.createImpression.setShortDescription")
                        // headerShown: false,
                    }}
                />
                <Stack.Screen
                    name="confirm_createImpressionScreen"
                    component={confirm_createImpressionContainer}
                    options={{
                        headerTitle: translate("navigator.createImpression.confirm")
                        // headerShown: false,
                    }}
                />
                <Stack.Screen
                    name="setSheduleMain_createImpressionScreen"
                    component={setSheduleMain_createImpressionContainer}
                    options={{
                        headerTitle: translate("navigator.createImpression.setSheduleMain")
                        // headerShown: false,
                    }}
                />
                <Stack.Screen
                    name="setSheduleDate_createImpressionScreen"
                    component={setSheduleDate_createImpressionContainer}
                    options={{
                        headerTitle: translate("navigator.createImpression.setSheduleDate")
                        // headerShown: false,
                    }}
                />
                {/* <Stack.Screen
                    name="registrationScreen"
                    component={registrationContainer}
                    options={{
                        headerShown: false,
                    }}
                /> */}
            </Stack.Navigator>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        state: state,
    };
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(CreateImpressionNavigator);

const styles = StyleSheet.create({
});
