import React, { Component } from "react";
import { StyleSheet, Text, View, AppRegistry, Dimensions } from "react-native";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { connect } from "react-redux";
import homeContainer from "../screens/home/homeContainer";
import likesContainer from "../screens/likes/likesContainer";
import Icon from "react-native-vector-icons/Ionicons";
import profileNavigator from "./profileNavigator";
import { COLORS, STYLES } from "../assets/styles/styles";
import { translate } from "../App";
import devContainer from "../screens/dev/devContainer";
import viewerStreamNavigator from "./viewerStreamNavigator";
// import Home from "../screens/home/home";

import impressionsContainer from "../screens/impressions/impressionsContainer";
import impressionNavigator from "./impressionNavigator";
import donateContainer from "../screens/donate/donateContainer";
import homeNavigator from "./homeNavigator";
// import Icon from "react-native-ionicons";

const Tab = createBottomTabNavigator();

class AppNavigator extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    checkPolling = () => {
        console.log('check: check')
        // console.log(this.state.app.local.isLoggedIn)
        if (this.props.app.local.isLogged) {

        }
        setTimeout(this.checkPolling, 5000)
    }
    // componentDidUpdate() {
    //     this.checkPolling()
    // }
    componentDidMount() {
        // this.checkPolling()
    }

    render() {
        return (
            <Tab.Navigator
                initialRouteName="Feed"
                tabBarOptions={{
                    // tabBarVisible: false,
                    style: STYLES.TAB_BAR,
                    labelStyle: STYLES.TAB_BAR.label,
                    activeTintColor: COLORS.primary,
                    // activeBackgroundColor: this.props.app.local.isLoggedIn ? 'white' : '#dbfffa',
                    // inactiveBackgroundColor: this.props.app.local.isLoggedIn ? 'white' : '#dbfffa'
                }}
            >
                {/* <Tab.Screen name="homeScreen" component={homeContainer}
                    options={{
                        tabBarVisible: this.props.app.local.isTabBarVisible,
                        tabBarLabel: translate("navigator.app.homeContainer"),
                        tabBarIcon: ({ focused, color, size }) => (
                            focused ? <Icon name="ios-search" size={size} color={color} /> : <Icon name="ios-search-outline" size={size} color={color} />
                        ),
                    }}
                />
                <Tab.Screen name="likesScreen" component={likesContainer}
                    options={{
                        tabBarVisible: this.props.app.local.isTabBarVisible,
                        tabBarLabel: translate("navigator.app.likesContainer"),
                        tabBarIcon: ({ focused, color, size }) => (
                            focused ? <Icon name="ios-heart" size={size} color={color} /> : <Icon name="ios-heart-outline" size={size} color={color} />
                        ),
                        tabBarColor: '#dedede',
                    }} /> */}
                <Tab.Screen name="homeScreen" component={homeNavigator}
                    options={{
                        tabBarVisible: this.props.app.local.isTabBarVisible,
                        tabBarLabel: translate("navigator.app.homeContainer"),
                        tabBarIcon: ({ focused, color, size }) => {
                            let name = focused ? 'ios-rocket' : "ios-rocket-outline";
                            return <Icon name={name} size={24} color={color} />
                        },
                        tabBarColor: '#dedede',
                    }} />
                <Tab.Screen name="impressionsScreen" component={impressionNavigator}
                    options={{
                        tabBarVisible: this.props.app.local.isTabBarVisible,
                        tabBarLabel: translate("navigator.app.impressionsContainer"),
                        tabBarIcon: ({ focused, color, size }) => {
                            let name = focused ? 'ios-planet' : "ios-planet-outline";
                            return <Icon name={name} size={24} color={color} />
                        },
                        tabBarColor: '#dedede',
                    }} />
                {/* <Tab.Screen name="donateScreen" component={donateContainer}
                    options={{
                        tabBarVisible: this.props.app.local.isTabBarVisible,
                        tabBarLabel: 'donatello',
                        tabBarIcon: ({ focused, color, size }) => (
                            focused ? <Icon name="ios-heart" size={size} color={color} /> : <Icon name="ios-heart-outline" size={size} color={color} />
                        ),
                        tabBarColor: '#dedede',
                    }} /> */}
                {/*
                
                <Tab.Screen name="devViewStreamScreen" component={viewerStreamNavigator}
                    options={{
                        tabBarVisible: this.props.app.local.isTabBarVisible,
                        tabBarLabel: 'Viewer',
                        tabBarIcon: ({ focused, color, size }) => (
                            focused ? <Icon name="ios-heart" size={size} color={color} /> : <Icon name="ios-heart-outline" size={size} color={color} />
                        ),
                        tabBarColor: '#dedede',
                    }} /> */}
                {/* 
                
                <Tab.Screen name="chatScreen" component={chatContainer}
                    options={{
                        tabBarVisible: this.props.app.local.isTabBarVisible,
                        tabBarLabel: translate("navigator.app.chatContainer"),
                        tabBarIcon: ({ focused, color, size }) => (
                            focused ? <Icon name="ios-chatbubbles" size={size} color={color} /> : <Icon name="ios-chatbubbles-outline" size={size} color={color} />
                        ),
                        tabBarBadge: 3,
                    }}
                /> */}
                {/* <Tab.Screen name="devScreen" component={devContainer}
                    options={{
                        tabBarVisible: this.props.app.local.isTabBarVisible,
                        tabBarLabel: 'develop',
                        tabBarIcon: ({ focused, color, size }) => (
                            focused ? <Icon name="ios-heart" size={size} color={color} /> : <Icon name="ios-heart-outline" size={size} color={color} />
                        ),
                        tabBarColor: '#dedede',
                    }} /> */}
                <Tab.Screen name="profileScreen" component={profileNavigator}
                    options={{
                        tabBarVisible: this.props.app.local.isTabBarVisible,
                        tabBarLabel: translate("navigator.app.profileNavigator"),
                        tabBarIcon: ({ focused, color, size }) => {
                            let name = focused ? 'ios-person' : "ios-person-outline";
                            return <Icon name={name} size={24} color={color} />
                        },
                    }}
                />
            </Tab.Navigator>
        );
        // } else {
        //     if (this.props.state.user.info.type === 0) {
        //         return (
        //             <Stack.Navigator style={styles.container}>
        //                 <Stack.Screen
        //                     name="Домашняя хоста"
        //                     component={hostContainer}
        //                     options={{
        //                         headerShown: false,
        //                     }}
        //                 />
        //                 <Stack.Screen
        //                     name="Стрим"
        //                     component={hostStreamContainer}
        //                     options={{
        //                         headerShown: false,
        //                     }}
        //                 />
        //             </Stack.Navigator>
        //         )
        //     } else if (this.props.state.user.info.type === 1) {
        //         return (
        //             <Stack.Navigator style={styles.container}>
        //                 <Stack.Screen
        //                     name="Домашняя юзера"
        //                     component={viewerContainer}
        //                     options={{
        //                         headerShown: false,
        //                     }}
        //                 />
        //                 <Stack.Screen
        //                     name="Просмотр"
        //                     component={viewerStreamContainer}
        //                     options={{
        //                         headerShown: false,
        //                     }}
        //                 />
        //             </Stack.Navigator>
        //         )
        //     } else if (this.props.state.user.info.type === 2) {
        //         return (
        //             <Tab.Navigator
        //                 initialRouteName="Feed"
        //                 tabBarOptions={{
        //                     activeTintColor: '#14ccb4',
        //                 }}
        //             >
        //                 <Tab.Screen name="Лупа" component={homeContainer}
        //                     options={{
        //                         tabBarLabel: "sss",
        //                         tabBarIcon: ({ focused, color, size }) => (
        //                             focused ? <Icon name="ios-search" size={size} color={color} /> : <Icon name="ios-search-outline" size={size} color={color} />
        //                         ),
        //                     }}
        //                 />
        //                 <Tab.Screen name="Нраица" component={likesContainer}
        //                     options={{
        //                         // tabBarLabel: "sss",
        //                         tabBarIcon: ({ focused, color, size }) => (
        //                             focused ? <Icon name="ios-heart" size={size} color={color} /> : <Icon name="ios-heart-outline" size={size} color={color} />
        //                         ),
        //                         tabBarColor: '#dedede',
        //                     }} />
        //                 <Tab.Screen name="Чатец" component={chatContainer}
        //                     options={{
        //                         tabBarLabel: "Чатец",
        //                         tabBarIcon: ({ focused, color, size }) => (
        //                             focused ? <Icon name="ios-chatbubbles" size={size} color={color} /> : <Icon name="ios-chatbubbles-outline" size={size} color={color} />
        //                         ),
        //                         tabBarBadge: 3,
        //                     }}
        //                 />
        //                 <Tab.Screen name="Простофиль" component={profileContainer}
        //                     options={{
        //                         tabBarLabel: "Простофиль",
        //                         tabBarIcon: ({ focused, color, size }) => (
        //                             focused ? <Icon name="ios-person" size={size} color={color} /> : <Icon name="ios-person-outline" size={size} color={color} />
        //                         ),
        //                     }}
        //                 />
        //             </Tab.Navigator>
        //         )
        //     }
        // }
    }
}

const mapStateToProps = (state) => {
    return {
        app: state.app,
    };
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(AppNavigator);

const styles = StyleSheet.create({
});
