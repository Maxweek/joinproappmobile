import React, { Component } from "react";
import { StyleSheet, Text, View, AppRegistry, Dimensions } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { connect } from "react-redux";
import hostContainer from "../screens/host/hostContainer";
import hostStreamContainer from "../screens/host/hostStreamContainer";
import { STYLES } from "../assets/styles/styles";
import { translate } from "../App";

const Stack = createStackNavigator();

class hostStreamNavigator extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidUpdate() {
    }

    render() {
        return (
            <Stack.Navigator style={STYLES.container}>
                <Stack.Screen
                    name="streamSettingsScreen"
                    component={hostContainer}
                    options={{
                        // title: translate('navigator.hostStream.streamSettingsScreen'),
                        headerShown: false,
                    }}
                />
                <Stack.Screen
                    name="streamScreen"
                    component={hostStreamContainer}
                    options={{
                        headerShown: false,
                    }}
                />
            </Stack.Navigator>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        state: state,
    };
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(hostStreamNavigator);

const styles = StyleSheet.create({
});
