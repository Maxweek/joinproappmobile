import React, { Component } from "react";
import { StyleSheet, Text, View, AppRegistry, Dimensions } from "react-native";

import { createStackNavigator, HeaderBackButton } from "@react-navigation/stack";

import { connect } from "react-redux";
import profileContainer from "../screens/profile/profileContainer";
import authNavigator from "./authNavigator";
import createImpressionNavigator from "./createImpressionNavigator";
import list_impressionsContainer from "../screens/impressions/list/list_impressionsContainer";
import hostStreamNavigator from "./hostStreamNavigator";
import view_impressionContainer from "../screens/impressions/view/view_impressionContainer";
import edit_impressionContainer from "../screens/impressions/edit/edit_impressionContainer";
import manage_impressionContainer from "../screens/impressions/manage/manage_impressionContainer";
import schedule_impressionContainer from "../screens/impressions/schedule/schedule_impressionContainer";
import { STYLES } from "../assets/styles/styles";
import { translate } from "../App";
import profile_innerContainer from "../screens/profile/inner/inner_profileContainer";
import settings_innerContainer from "../screens/profile/settings/settings_profileContainer";
import impressionViewNavigator from "./impressionViewNavigator";

const Stack = createStackNavigator();

class ProfileNavigator extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidUpdate() {
    }

    render() {
        return (
            <Stack.Navigator style={STYLES.container} initialRouteName="profileRoot">
                {this.props.app.local.isLoggedIn ?
                    <Stack.Screen
                        name="profileScreenStack"
                        component={profileContainer}
                        options={{
                            title: translate("navigator.profile.stackNavigator"),
                            headerShown: false,
                        }}
                    />
                    :
                    <Stack.Screen
                        name="authScreen"
                        component={authNavigator}
                        options={{
                            title: translate("navigator.profile.authScreen"),
                            headerShown: false,
                        }}
                    />
                }
                <Stack.Screen
                    name="profileInnerScreen"
                    component={profile_innerContainer}
                    options={{
                        title: translate("navigator.profile.inner"),
                        headerShown: false,
                    }}
                />
                <Stack.Screen
                    name="profileSettingsScreen"
                    component={settings_innerContainer}
                    options={{
                        title: translate("navigator.profile.settings"),
                        headerShown: false,
                    }}
                />

                <Stack.Screen
                    name="createImpressionScreen"
                    component={createImpressionNavigator}
                    options={{
                        title: translate("navigator.profile.createImpression"),
                        headerShown: false,
                    }}
                />
                <Stack.Screen
                    name="listImpressionsScreen"
                    component={list_impressionsContainer}
                    options={{
                        title: translate("navigator.profile.listImpressions"),
                        // headerShown: false,
                    }}
                />
                <Stack.Screen
                    name="viewImpressionNavigator"
                    component={impressionViewNavigator}
                    options={{
                        title: translate("navigator.profile.viewImpression"),
                        headerShown: false,
                    }}
                />
                <Stack.Screen
                    name="editImpressionScreen"
                    component={edit_impressionContainer}
                    options={{
                        title: translate("navigator.profile.editImpression"),
                        // headerShown: false,
                    }}
                />
                <Stack.Screen
                    name="manageImpressionScreen"
                    component={manage_impressionContainer}
                    options={{
                        title: translate("navigator.profile.manageImpression"),
                        // headerShown: false,
                    }}
                />
                <Stack.Screen
                    name="scheduleImpressionScreen"
                    component={schedule_impressionContainer}
                    options={{
                        title: translate("navigator.profile.cheduleImpression"),
                        // headerShown: false,
                    }}
                />
                <Stack.Screen
                    name="hostStreamScreen"
                    component={hostStreamNavigator}
                    options={{
                        title: translate("navigator.profile.hostStream"),
                        headerShown: false,
                    }}
                />
            </Stack.Navigator>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        app: state.app,
    };
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileNavigator);

const styles = StyleSheet.create({
});
