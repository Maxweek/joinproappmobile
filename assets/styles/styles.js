import { StyleSheet } from "react-native";

export const COLORS = {
    primary: '#14ccb4',
    secondary: '#14ccb4',
    transparent: 'transparent',
    white: 'white',
    black: '#2a2d43',
    gray: '#999ba5',
    yellow: '#ffbc6d',
    lightgray: '#eeeff0',
    middlegray: '#dedede',
    alert: '#FFBC6D',
    danger: '#ef476f',
    green: '#14ccb4',
    lightgreen: '#d6fffa',
    red: '#ef476f',
}

export const BUTTON_STYLES = {
}

export const STYLES = {
    base: {
        maintitle: {},
        title: {
            fontSize: 24,
            fontWeight: 'bold',
            marginVertical: 8,
        },
        subtitle: {
            fontSize: 20,
            marginVertical: 4
        },
        middle: {},
        text: {
            fontSize: 16,
            marginVertical: 4
        },
        elevation: {
            default: 6,
            big: 12
        }
    },
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center",
    },
    footText: {
        textAlign: 'center',
        color: COLORS.gray,
        marginTop: 16
    },
    BUTTON: {
        textIcon: {
            position: 'absolute'
        },
        text: {
            textAlign: 'center',
            color: 'white',
            fontWeight: 'bold',
            fontSize: 16,
            lineHeight: 24,
        }
    },
    TAB_BAR: {
        // height: 54,
        // paddingBottom: 20,
        label: {
            marginTop: -4,
            paddingBottom: 6
        }
    }

};