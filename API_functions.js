import API, { API_USER_READ } from "./API"

const setGetRequest = async ({ type, url, callback = null }) => {
    return await API.get(url)
        .then(function (res) {
            console.log(res.data)
            if (typeof res.data.status !== 'undefined') {
                if (res.data.status === 'success') {
                    callback()
                    return res.data;
                } else {
                    console.log(type + ' status NOT SUCCESS')
                    return false;
                }
            } else {
                console.log(type + ' status UNDEFINED')
                return false;
            }
        })
        .catch(function (err) {
            console.log(err)
            console.log(type + ' request FAILED')
            return false;
        })
}

export const API_REQUEST = {
    getUserInfo: async (id, callback = () => { }) => {
        let data = await setGetRequest({
            type: 'getUserInfo',
            url: API_USER_READ + id,
            returnableObj: 'user',
            callback
        })
        console.log(data)
        return data.user;
    }
}
