import React, { Component } from 'react';
import {
  StatusBar,
  I18nManager,
  StyleSheet,
  Platform,
  Linking,
} from "react-native";

import { NavigationContainer } from '@react-navigation/native';
import { navigationRef } from './navigators/rootNavigator';
import * as rootNavigation from './navigators/rootNavigator'
import { createStore } from "redux";
import { Provider } from "react-redux";

import AppNavigator from './navigators/appNavigator';

import rootReducer from './store/reducers'
import DeviceInfo from 'react-native-device-info';
import { setAppDeviceId, setAppLogged, setPushToken, SET_APP_LOGGED } from './store/user/actions';
import { COLORS } from './assets/styles/styles';

import * as RNLocalize from "react-native-localize";
import i18n from "i18n-js";
import memoize from "lodash.memoize"
import { SafeAreaProvider } from 'react-native-safe-area-context';
import Orientation from 'react-native-orientation-locker';
import { Host } from 'react-native-portalize';
import NotifService from './notifService';
import { isTokenExist, setAxios } from './API';
import messaging from '@react-native-firebase/messaging';
// import reactNativeSecureStorage from 'react-native-secure-storage';
// import Home from './screens/home/home';
// import AuthNavigator from './navigators/profileNavigator';

const store = createStore(rootReducer);
const translationGetters = {
  // lazy requires (metro bundler does not support symlinks)
  ru: () => require("./assets/lang/ru.json"),
  en: () => require("./assets/lang/en.json"),
};


export const translate = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key)
);

export const _link = url => {
  Linking.openURL(url)
}

export const getDateFormat = datetime => {
  let date = new Date(datetime * 1000);

  let dateObj = {
    date: date,
    month: date.getMonth() + 1,
    day: date.getDate(),
    weekDay: date.getDay(),
    hours: date.getHours(),
    minutes: date.getMinutes(),
    monthNameShort: translate('datetime.monthNamesShort')[date.getMonth()],
    weekDayShort: translate('datetime.monthNamesShort')[date.getDay()],
  }

  dateObj.formatted = {
    month: dateObj.month.toString().padStart(2, '0'),
    day: dateObj.day.toString().padStart(2, '0'),
    hours: dateObj.hours.toString().padStart(2, '0'),
    minutes: dateObj.minutes.toString().padStart(2, '0'),
  }

  // console.log(datetime)
  // console.log(dateObj)

  return dateObj
}

export const timestampToDate = timestamp => {
  let datetime = new Date(timestamp);
  let date = [datetime.getFullYear(), datetime.getMonth(), datetime.getDay()].join('.')
  let time = [datetime.getHours(), datetime.getMinutes()].join(':')

  return date + ' ' + time;
}

const setI18nConfig = () => {
  // console.log(store.getState().app.local.lang)
  // fallback if no available language fits
  const fallback = { languageTag: "en", isRTL: false };
  const currentLang = store.getState().app.user.info.lang
  let localize;
  if (currentLang !== null) {
    localize = { languageTag: currentLang, isRTL: false };
  } else {
    localize = RNLocalize.findBestAvailableLanguage(Object.keys(translationGetters)) || fallback;
  }
  // console.log(store.getState().app.user.info.lang)
  const { languageTag, isRTL } = localize;

  // clear translation cache
  translate.cache.clear();
  // update layout direction
  I18nManager.forceRTL(isRTL);
  // set i18n-js config
  i18n.translations = { [languageTag]: translationGetters[languageTag]() };
  i18n.locale = languageTag;
  // console.log(i18n.locale)
};

export const setLanguageCode = () => {
  setI18nConfig()
}

async function checkAuth() {
  let pushToken = await messaging().getToken();
  store.dispatch(setPushToken(pushToken));
  isTokenExist().then(res => {
    console.log('ssssdsdadsadsadsadsad2')
    store.dispatch(setAppLogged(res))
  }).catch(err => {
    console.log('ssssdsdadsadsadsadsad3')
    console.log(err)
  })
  // console.log('isTokenExist_')
  // console.log(isExist)
  // setAppLogged(isExist)
}

export default class App extends Component {
  constructor(props) {
    super(props);
    setI18nConfig(); // set initial config

    this.notif = new NotifService(
      this.onRegister.bind(this),
      this.onNotif.bind(this),
    );
    // setAxios();
    checkAuth();
  }

  onRegister(token) {
    console.log('NAVIGATE')
    console.log(this.props)
    // this.setState({registerToken: token.token, fcmRegistered: true});
  }

  onNotif(notif) {
    console.log(notif)
    console.log(notif.data.TYPE)
    if (notif.data.TYPE === 'impressionView') {
      rootNavigation.navigate('impressionViewNavigator', {
        screen: "viewImpressionScreen",
        params: {
          id: notif.data.id
        }
      })
    }
    // Alert.alert(notif.title, notif.message);
  }

  handleLocalizationChange = () => {
    setI18nConfig();
    this.forceUpdate();
  }

  componentDidMount() {

    Orientation.lockToPortrait()
    let device = DeviceInfo.getUniqueId()
    setAppDeviceId(device)
    RNLocalize.addEventListener("change", this.handleLocalizationChange);
  }

  componentWillUnmount() {
    RNLocalize.removeEventListener("change", this.handleLocalizationChange);
  }

  render() {
    return (
      <Provider store={store}>
        <SafeAreaProvider>
          <NavigationContainer ref={navigationRef}>
            <Host>

              <StatusBar
                barStyle={Platform.OS === 'android' ? "light-content" : "dark-content"}
                hidden={false}
                backgroundColor={COLORS.primary}
                translucent={true}
              />
              <AppNavigator />
            </Host>
          </NavigationContainer>
        </SafeAreaProvider>
      </Provider>
    );
  }
}
const styles = StyleSheet.create({});